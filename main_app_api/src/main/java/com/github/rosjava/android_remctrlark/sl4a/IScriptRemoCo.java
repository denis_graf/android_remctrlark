/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.binders.IRcaScriptBinder;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;

import org.jetbrains.annotations.NotNull;

public interface IScriptRemoCo {

	IRemoCo getRemoCo();

	IRcaScriptBinder getScriptBinder();

	void onScriptBound();

	void onScriptShutdown();

	void onScriptShutdownComplete();

	void onScriptExitReached();

	void handleScriptError(Throwable e);

	void onUnprocessedScriptProcessMessage(@NotNull String message);

}
