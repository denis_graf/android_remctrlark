/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config;

import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

public interface IRemoCoConfigRetriever {

	@NotNull
	IRcaId getId();

	@NotNull
	URI retrieveMasterUri();

	@NotNull
	RemoConConfigNewMaster retrieveNewMaster();

	boolean isMasterMutable();

	boolean containsTypeDefinition();

	@NotNull
	IRcaId retrieveTypeId();

	@NotNull
	IRcaId retrieveType();

	@NotNull
	String retrieveApk();

	@NotNull
	Map<GraphName, Object> retrieveParams();

	@NotNull
	Collection<IRemoCoParam> retrieveResolvedConfigParams();

	@NotNull
	Map<GraphName, GraphName> retrieveRemappings();

	@NotNull
	Collection<IRemoCoRemapping> retrieveResolvedConfigRemappings();

}
