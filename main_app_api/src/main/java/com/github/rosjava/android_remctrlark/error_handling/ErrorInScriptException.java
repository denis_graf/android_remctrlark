/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.error_handling;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class ErrorInScriptException extends RcaException {
	public ErrorInScriptException(@NotNull String scriptId, @NotNull JSONObject excInfo) {
		//noinspection ConstantConditions
		super("Error in script '" + scriptId + "'.", excInfo != null ? new RcaPyException(excInfo) : null);
	}

	public RcaPyException getAsPyException() {
		return (RcaPyException) getCause();
	}
}
