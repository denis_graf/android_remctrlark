/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import org.jetbrains.annotations.NotNull;

public interface IMainFileNamesManager {

	@NotNull
	String getExternalStorageFullDir();

	@NotNull
	String getInternalStorageFullDir();

	@NotNull
	String getMainAppApk();

	@NotNull
	String getMainAppApkFullDir();

	@NotNull
	String getPluginApkFullDir(@NotNull String apk);

	@NotNull
	String getMainScriptFullDir();

	@NotNull
	String getRemoConConfigSuffix();

	@NotNull
	String getRemoConConfigsFullDir(@NotNull String apk);

	@NotNull
	String getJavaMsgsFullDir(String apk);

	@NotNull
	String getPythonFullDir();

	@NotNull
	String getPythonLibFullDir();

	@NotNull
	String getPythonLibDynLoadFullDir();

	@NotNull
	String getPythonLibPythonDir();

	@NotNull
	String getPythonExtrasFullDir();

	@NotNull
	String getPythonExtrasPythonFullDir();

	@NotNull
	String getPythonExtrasTempFullDir();

	@NotNull
	String getRcaPythonCoreFullDir();

	@NotNull
	String getPythonBinRelFileName();

	@NotNull
	String getPythonBinFullFileName();

	@NotNull
	String getPyRemoConsRelDir();

	@NotNull
	String getPyRemoConsFullDir(@NotNull String apk);

	@NotNull
	String getPyRemoCompsRelDir();

	@NotNull
	String getPyRemoCompsFullDir(@NotNull String apk);

	@NotNull
	String getRemoConViewerFullFileName();

	@NotNull
	String getRemoCompViewerFullFileName();

	IPyRemoCoFileNamesManager getPyRemoCoFileNamesManager(@NotNull String type, @NotNull String apk);

	IPyRemoConFileNamesManager getPyRemoConFileNamesManager(@NotNull String type, @NotNull String apk);

	IPyRemoCompFileNamesManager getPyRemoCompFileNamesManager(@NotNull String type, @NotNull String apk);
}
