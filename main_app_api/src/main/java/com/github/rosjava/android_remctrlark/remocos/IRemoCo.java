/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos;

import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.application.IRcaLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventListener;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLog;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLogEntry;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;

import java.util.List;
import java.util.Map;

public interface IRemoCo {

	String getIdName();

	@NotNull
	NameResolver getNameResolver();

	String getType();

	String getApk();

	Map<GraphName, Object> getParams();

	Map<GraphName, Object> getParams(String namespace);

	Map<GraphName, Object> getParams(GraphName namespace);

	Object getParam(GraphName name);

	@NotNull
	String getImplDoc();

	@NotNull
	IRcaLog getLog();

	void addLogEntry(IRcaLogEntry entry);

	void clearLog();

	ViewGroup getImplUiLayer();

	IRcaBinder getBinder();

	void addListener(IRcaEventListener listener);

	boolean removeListener(IRcaEventListener listener);

	void dispatchEvent(String name, Object data);

	void handleErrorException(Throwable e);

	void handleWarningException(Throwable e);

	boolean cancelException(Throwable e);

	void cancelAllExceptions();

	boolean containsException(Throwable exception);

	boolean isInErrorState();

	boolean isInWarningState();

	boolean isCreated();

	boolean isStarted();

	boolean isShutdown();

	boolean isRelaunching();

	boolean isRebootRemoConOnRelaunchEnabled();

	boolean isOnImplThread();

	// TODO: remove unnecessary calls to updateMetaUiLayer()
	void updateMetaUiLayer();

	void startInspectMode();

	boolean isSelectedInInspectMode();

	List<IRemoCoImplLogEntry> retrieveImplLogEntries();

	List<IRemoCoLogEntry> retrieveInternalLogEntries();

	void setOnNewLogEntryListener(IOnNewRemoCoLogEntryListener listener);

	void setOnStateChangedListener(IOnRemoCoStateChangedListener listener);

	Object getSessionState();

	void setSessionState(Object state);

	IRcaLayoutInflaterFactory getApkViewFactory();
}
