/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config;

import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;

import java.util.Collection;
import java.util.List;

public interface IRemoConConfigRetriever extends IRemoCoConfigRetriever {
	@NotNull
	IRemoConConfig getConfig();

	@NotNull
	String getConfigFileFullDir();

	@NotNull
	String getBaseConfApk();

	@NotNull
	Collection<IRemoCoNamespace> retrieveResolvedConfigNamespaces();

	@NotNull
	Collection<IRemoCoType> retrieveResolvedConfigTypes();

	@NotNull
	GraphName retrieveNamespace(IRcaId nsId);

	@NotNull
	IRcaId retrieveType(IRcaId tId);

	boolean retrieveRemoConRebootOnRelaunchEnabled(@NotNull IRemoCo remoCo);

	@NotNull
	String retrieveApk(IRcaId tId);

	IRemoCompConfigRetriever retrieveRemoCompConfig(@NotNull IRcaId id);

	boolean retrieveRemoCosToRelaunch(@NotNull List<IRcaId> allRemoCos, @NotNull IRemoConMutablesConfigCommit commit, List<IRcaId> remoCosToRelaunch);

	void applyMutablesConfigCommit(@NotNull IRemoConMutablesConfigCommit commit);

	@NotNull
	IRemoCoConfigRetriever retrieveRemoCoConfig(@NotNull IRcaId rcaId);

	void setCurrentStateAsInitial();
}
