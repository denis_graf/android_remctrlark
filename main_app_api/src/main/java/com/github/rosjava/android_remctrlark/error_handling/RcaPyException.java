/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.error_handling;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class RcaPyException extends Exception {
	private String mClassName = "<unknown>";
	private String mStr = "";
	private String mMessage = "";
	private RcaPyException mCause = null;
	private String mTraceback = "";

	RcaPyException(@NotNull JSONObject excInfo) {
		try {
			mMessage = excInfo.getString("message");
		} catch (JSONException ignored) {
		}
		try {
			mCause = new RcaPyException(excInfo.getJSONObject("cause"));
		} catch (JSONException ignored) {
		}
		try {
			mClassName = excInfo.getString("class_name");
		} catch (JSONException ignored) {
		}
		try {
			mStr = excInfo.getString("str");
		} catch (JSONException ignored) {
		}
		try {
			mTraceback = excInfo.getString("traceback");
		} catch (JSONException ignored) {
		}
	}

	@Override
	final public String getMessage() {
		return getPyTraceback();
	}

	final public String getSimpleMessage() {
		return !isEmpty(mMessage) ? mMessage : !isEmpty(mStr) ? mStr : mClassName;
	}

	final public String getDetailedMessage() {
		return getMessage();
	}

	@Override
	public Throwable getCause() {
		return mCause != null ? mCause : super.getCause();
	}

	public String toPyString() {
		return mStr;
	}

	public String getPyClassName() {
		return mClassName;
	}

	public String getPyTraceback() {
		return mTraceback;
	}

	private boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}
}
