/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.Context;
import android.view.ActionMode;

import com.github.rosjava.android_remctrlark.event_handling.IRcaOnKeyListener;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.sl4a.IMainScript;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.NameResolver;
import org.ros.node.NodeMainExecutor;

import java.net.URI;
import java.util.TimerTask;

public interface IRcaContext {

	@NotNull
	IRcaApplication getApp();

	Context getActivityContext();

	@NotNull
	NameResolver getRootNameResolver();

	void scheduleTask(TimerTask task, long delay);

	@NotNull
	IRcaSettings getSettings();

	@NotNull
	IRemoCon getRemoCon();

	boolean isRemoConCreated();

	boolean isRemoConStarted();

	boolean isRemoConActive();

	boolean isRemoConShutdown();

	boolean isRemoConRelaunching();

	@NotNull
	String getPluginApk();

	IRemoCo findRemoCo(String id);

	IRemoCo findRemoCo(String id, boolean includeBackup);

	@NotNull
	NodeMainExecutor getNodeExecutor();

	void setPyMainScript(IMainScript mainScript);

	IMainScript getPyMainScript();

	void alertDialog(@NotNull String msg, @NotNull String title);

	void setKeyListener(IRcaOnKeyListener listener);

	ActionMode startInspectMode(@NotNull ActionMode.Callback callback);

	void startRemoConInspectMode();

	boolean isInspectMode();

	@NotNull
	IRcaLoadingTask startLoadingTask(@NotNull String msg);

	boolean isLoadingMode();

	void handleError(Throwable e);

	void onHandleRemoCoErrorException(IRemoCo remoCo, Throwable e);

	boolean postToUiThread(ICheckedRunnable runnable);

	boolean syncRunOnUiThread(@NotNull ICheckedRunnable runnable);

	boolean runOnUiThread(@NotNull ICheckedRunnable runnable);

	boolean isOnUiThread();

	void postRelaunchRemoCo(@NotNull String idName);

	void postRelaunchRemoCo(@NotNull String idName, final Runnable onFinishedHook);

	void postRelaunchRemoCos(@NotNull IRemoConMutablesConfigCommit config);

	void openFileInExplorer(@NotNull String fileName);

	void openFileInTextEditor(@NotNull String fileName);

	@NotNull
	IRemoConConfigRetriever getRemoConConfig();

	URI getCurrentMasterUri();

	void setOnMutablesConfigChangedListener(IOnMutablesConfigChangedListener listener);
}
