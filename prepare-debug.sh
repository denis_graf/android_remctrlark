#!/bin/bash

SCRIPT_DIR=$(dirname $0)
. ${SCRIPT_DIR}/scripts-config.sh

echo "adb forward tcp:${PYTHON_AP_PORT_LOCAL} tcp:${PYTHON_AP_PORT_REMOTE}"
if ! $RUN_ADB forward tcp:${PYTHON_AP_PORT_LOCAL} tcp:${PYTHON_AP_PORT_REMOTE} ; then
  exit 1
fi

echo "adb shell setprop debug.assert 1"
if ! $RUN_ADB shell setprop debug.assert 1 ; then
  exit 1
fi
