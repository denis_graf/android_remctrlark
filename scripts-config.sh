#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

# Project
PROJECT_PACKAGE=com.github.rosjava.android_remctrlark

# Debugging
PYTHON_AP_PORT_REMOTE=45007
PYTHON_AP_PORT_LOCAL=9999

# External
RUN_ADB=adb

# Internal
EXTERNAL_STORAGE_ON_DEVICE_DIR=$($RUN_ADB shell echo -n '$EXTERNAL_STORAGE')
PACKAGE_ON_DEVICE_PATH=$EXTERNAL_STORAGE_ON_DEVICE_DIR/$PROJECT_PACKAGE/
MAIN_PROJECT_DIR=$(dirname $BASH_SOURCE)
REMOCON_CONFIGS_DIR=$MAIN_PROJECT_DIR/main_app/src/main/remocon_configs
PYTHON_CORE_DIR=$MAIN_PROJECT_DIR/rca_plugin_python/com.github.rosjava.android_remctrlark/core
PUSH_REMOCON_CONFIGS=$MAIN_PROJECT_DIR/push-remocon-conigs.sh
PUSH_PYTHON_PROJECT=$MAIN_PROJECT_DIR/push-python-project.sh

PUSH_PYTHON_CORE_PROJECT=$MAIN_PROJECT_DIR/python_core/src/core/push-project.sh
RUN_GRADLE=$MAIN_PROJECT_DIR/gradlew
