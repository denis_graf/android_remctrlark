/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2012, Anthony Prieur & Daniel Oppenheim. All rights reserved.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original from SL4A modified to allow to embed Interpreter and scripts into an APK
 */

package com.android.python27.support;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;

import java.io.InputStream;

public class InstallActivity extends Activity {
	protected abstract class AInstallAsyncTask extends AsyncTask<Void, Integer, Boolean> {

		protected abstract boolean install();

		protected abstract void onFinished(boolean installStatus);

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: *DEG* catch errors...

			Log.i(RcaGlobalConstants.LOG_TAG, "Installing...");

			// show progress dialog
			sendmsg("showProgressDialog", "");

			// Copy all resources
			return install();
		}

		protected final void setMessageProgressDialog(String message) {
			sendmsg("setMessageProgressDialog", message);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
		}

		@Override
		protected void onPostExecute(Boolean installStatus) {
			sendmsg("dismissProgressDialog", "");
			if (installStatus) {
				sendmsg("installSucceed", "");
			} else {
				sendmsg("installFailed", "");
			}
			onFinished(installStatus);
		}

		protected void sendmsg(String key, String value) {
			Message message = installerHandler.obtainMessage();
			Bundle bundle = new Bundle();
			bundle.putString(key, value);
			message.setData(bundle);
			installerHandler.sendMessage(message);
		}
	}

	private ProgressDialog myProgressDialog;

	final Handler installerHandler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			Bundle bundle = message.getData();

			assert bundle != null;
			assert getApplicationContext() != null;

			if (bundle.containsKey("showProgressDialog")) {
				if (myProgressDialog == null || !myProgressDialog.isShowing()) {
					myProgressDialog = ProgressDialog.show(InstallActivity.this, "Please wait...", "Installing...", true);
				}
			} else if (bundle.containsKey("setMessageProgressDialog")) {
				if (myProgressDialog.isShowing()) {
					myProgressDialog.setMessage(bundle.getString("setMessageProgressDialog"));
				}
			} else if (bundle.containsKey("dismissProgressDialog")) {
				if (myProgressDialog.isShowing()) {
					myProgressDialog.dismiss();
				}
			}/* else if (bundle.containsKey("installSucceed")) {
				Toast toast = Toast.makeText(getApplicationContext(), "Install Succeed.", Toast.LENGTH_LONG);
				toast.show();
			}*/ else if (bundle.containsKey("installFailed")) {
				Toast toast = Toast.makeText(getApplicationContext(), "Install Failed. Please check logs.", Toast.LENGTH_LONG);
				toast.show();
			}
		}
	};

	protected final InputStream getZipResourceContent(String rcaApk, String name) {
		InputStream content = null;
		final Context apkContext;
		try {
			apkContext = createPackageContext(rcaApk, CONTEXT_INCLUDE_CODE);
			int id = apkContext.getResources().getIdentifier(name, "raw", rcaApk);
			if (id != 0) {
				content = apkContext.getResources().openRawResource(id);
				content.reset();
			} else {
				content = null;
			}
		} catch (Throwable e) {
			Log.e(RcaGlobalConstants.LOG_TAG, "Failed to install ZIP resource '" + name + "' from APK '" + rcaApk + "'.", e);
		}
		return content;
	}
}
