/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.android.python27.support;

import android.os.Bundle;
import android.util.Log;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.IMainFileNamesManager;
import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.googlecode.android_scripting.FileUtils;

import java.io.File;
import java.io.InputStream;

public class InstallPythonServiceActivity extends InstallActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.install);

		installPythonService();
	}

	protected void onInstallPythonServiceFinished(boolean installStatus) {
		if (installStatus) {
			setResult(RESULT_OK);
		} else {
			setResult(RESULT_CANCELED);
		}
		finish();
	}

	private void installPythonService() {
		assert RcaService.getApp() != null;

		final IMainFileNamesManager fileNamesManager = RcaService.getApp().getFileNamesManager();
		final String installPath = fileNamesManager.getInternalStorageFullDir() + "/";
		final String mainAppDir = fileNamesManager.getMainAppApk();
		final String mainAppFullPath = fileNamesManager.getMainAppApkFullDir() + "/";
		final String pythonTempPath = fileNamesManager.getPythonExtrasTempFullDir() + "/";
		final String pythonExtrasPath = fileNamesManager.getPythonExtrasFullDir() + "/";
		final String pythonBinaryRelFile = fileNamesManager.getPythonBinRelFileName();
		final String pythonBinaryFullFile = fileNamesManager.getPythonBinFullFileName();

		File testedFile = new File(pythonBinaryFullFile);
		boolean installNeeded = !testedFile.exists();

		if (!installNeeded) {
			testedFile = new File(fileNamesManager.getMainScriptFullDir());
			installNeeded = !testedFile.exists();
		}

		if (!installNeeded) {
			testedFile = new File(fileNamesManager.getPythonExtrasFullDir());
			installNeeded = !testedFile.exists();
		}

		if (installNeeded) {
			new AInstallAsyncTask() {
				@Override
				protected boolean install() {
					Utils.createDirectoryOnExternalStorage(mainAppDir);

					// python core project
					InputStream content = getZipResourceContent(mainAppDir, RcaGlobalConstants.PYTHON_CORE_PROJECT_ZIP_NAME);
					boolean succeed = Utils.unzip(content, mainAppFullPath, false);

					// python -> /data/data/com.android.python27/files/python
					content = getZipResourceContent(mainAppDir, RcaGlobalConstants.PYTHON_ZIP_NAME);
					succeed &= Utils.unzip(content, installPath, true);
					try {
						//noinspection OctalInteger
						FileUtils.chmod(new File(pythonBinaryFullFile), 0755);
					} catch (Exception e) {
						Log.e(RcaGlobalConstants.LOG_TAG, "Failed to set permissions for '" + pythonBinaryRelFile + "'.", e);
						succeed = false;
					}

					// python extras -> /sdcard/com.android.python27/extras/python
					content = getZipResourceContent(mainAppDir, RcaGlobalConstants.PYTHON_EXTRAS_ZIP_NAME);
					Utils.createDirectoryOnExternalStorage(pythonExtrasPath);
					Utils.createDirectoryOnExternalStorage(pythonTempPath);
					succeed &= Utils.unzip(content, pythonExtrasPath, false);

					// python ROS extras -> /sdcard/com.android.python27/extras/python
					content = getZipResourceContent(mainAppDir, RcaGlobalConstants.PYTHON_EXTRAS_ROS_ZIP_NAME);
					Utils.createDirectoryOnExternalStorage(pythonExtrasPath);
					Utils.createDirectoryOnExternalStorage(pythonTempPath);
					succeed &= Utils.unzip(content, pythonExtrasPath, false);

					return succeed;
				}

				@Override
				protected void onFinished(boolean installStatus) {
					onInstallPythonServiceFinished(installStatus);
				}
			}.execute();
		} else {
			onInstallPythonServiceFinished(true);
		}
	}
}
