/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.IRcaBinderMethod;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.error_handling.MethodNotFoundException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.RemoCoInfoRetriever;
import com.github.rosjava.android_remctrlark.remocos.impls.python.RemoConPyRelauncher;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFieldsToJsonMappable;
import com.github.rosjava.android_remctrlark.sl4a.RemoCoInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class IpyQueryProxy extends APyProxy implements IRcaPyProxy {

	static final List<String> mHosts = new ArrayList<String>();

	@SuppressWarnings("UnusedDeclaration")
	private IRcaBinder mBinder = new ReflectingBinder(new IReflectingBinderImpl() {
		public List<String> getScripts() {
			IRemoCon remoCon = getRcaContext().getRemoCon();
			List<IRemoComp> remoComps = remoCon.findAllRemoComps();
			List<String> scriptsIds = new ArrayList<String>(remoComps.size() + 1);
			if (hasPythonScript(remoCon)) {
				scriptsIds.add(remoCon.getIdName());
			}
			for (IRemoComp remoComp : remoComps) {
				if (hasPythonScript(remoComp)) {
					scriptsIds.add(remoComp.getIdName());
				}
			}
			return scriptsIds;
		}

		public void scriptRelaunch(String scriptId) {
			getRcaContext().postRelaunchRemoCo(scriptId);
		}

		public IRcaFieldsToJsonMappable getScriptInfo(String scriptId) {
			return new RemoCoInfo(getMainFileNamesManager(), getRemoCo(scriptId));
		}

		public List<String> getProxies() {
			List<IRcaPyProxy> proxies = mRcaFacade.getAllProxies();
			List<String> proxyIds = new ArrayList<String>(proxies.size());
			int index = 1;
			for (IRcaPyProxy proxy : proxies) {
				proxyIds.add(getHostAbbreviation(proxy.getHostname()) + "_" + proxy.getPid() + "_" +
						proxy.getType() + "_" + index++);
			}
			return proxyIds;
		}

		public String getRemoCon() {
			IRemoCon remoCon = getRcaContext().getRemoCon();
			return remoCon.getIdName();
		}

		public List<String> getRemoComps() {
			IRemoCon remoCon = getRcaContext().getRemoCon();
			List<IRemoComp> remoComps = remoCon.findAllRemoComps();
			List<String> remoCompIds = new ArrayList<String>(remoComps.size());
			for (IRemoComp remoComp : remoComps) {
				remoCompIds.add(remoComp.getIdName());
			}
			return remoCompIds;
		}

		public IRcaFieldsToJsonMappable getRemoCoInfo(String remoCoId) {
			return new RemoCoInfo(getMainFileNamesManager(), getRemoCo(remoCoId));
		}

		public String getRemoCoDetailedInfo(String remoCoId) {
			return RemoCoInfoRetriever.retrieveFormattedInfo(remoCoId);
		}

		public List<String> getRemoCoBinderMethods(String remoCoId) {
			IRcaBinder binder = getRemoCo(remoCoId).getBinder();
			List<String> methodNames;
			if (binder == null) {
				methodNames = new ArrayList<String>();
			} else {
				List<IRcaBinderMethod> methods = binder.getMethods();
				methodNames = new ArrayList<String>(methods.size());
				for (IRcaBinderMethod method : methods) {
					methodNames.add(method.getName());
				}
			}
			return methodNames;
		}

		public String getRemoCoBinderMethod(String remoCoId, String methodName) throws MethodNotFoundException {
			IRcaBinder binder = getRemoCo(remoCoId).getBinder();
			List<IRcaBinderMethod> methods = binder.getMethods();
			for (IRcaBinderMethod method : methods) {
				if (method.getName().equals(methodName)) {
					return method.getDoc();
				}
			}
			throw new MethodNotFoundException(methodName);
		}

		public void dispatchEvent(String dispatcherId, String eventName, Object data) {
			getRemoCo(dispatcherId).dispatchEvent(eventName, data);
		}

		public Object callRemoCoBinderMethod(String remoCoId, String methodName, JSONArray args) throws RcaBinderInvocationException {
			return getRemoCo(remoCoId).getBinder().invokeMethod(methodName, args);
		}

		public void relaunchRemoCo(String remoCoId) throws RcaBinderInvocationException {
			getRcaContext().postRelaunchRemoCo(remoCoId);
		}

		public void RemoConRelauncher_commit(JSONObject commit) throws JSONException, RcaException, URISyntaxException {
			RemoConPyRelauncher.commit(commit);
		}

		private IRemoCo getRemoCo(String remoCoId) {
			IRemoCon remoCon = getRcaContext().getRemoCon();
			if (remoCon.getIdName().equals(remoCoId)) {
				return remoCon;
			}
			return remoCon.findRemoCompByIdName(remoCoId);
		}

		private boolean hasPythonScript(IRemoCo remoCo) {
			return getMainFileNamesManager().getPyRemoCoFileNamesManager(remoCo.getType(), remoCo.getApk()) != null;
		}
	});

	public IpyQueryProxy(IRcaFacade rcaFacade) {
		super(rcaFacade);
	}

	@Override
	public IRcaBinder getBinder() {
		return mBinder;
	}

	@Override
	public void shutdown() {
	}

	static String getHostAbbreviation(String hostname) {
		int index;
		synchronized (mHosts) {
			index = mHosts.indexOf(hostname);
			if (index == -1) {
				index = mHosts.size();
				mHosts.add(hostname);
			}
		}
		return "host" + (index + 1);
	}
}
