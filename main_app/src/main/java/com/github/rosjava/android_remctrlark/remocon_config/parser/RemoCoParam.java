/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoParam;

import org.ros.namespace.GraphName;

import java.io.Serializable;

class RemoCoParam extends ARemoCoMutable implements IRemoCoParam, Serializable {

	static final String XML_TAG = "param";
	static final String XML_ATTR_NAME = "name";
	static final String XML_ATTR_VALUE = "value";
	static final String XML_ATTR_TYPE = "type";

	GraphName mName = null;
	Object mValue = null;
	String mType = null;

	@Override
	public GraphName getName() {
		return mName;
	}

	@Override
	public Object getValue() {
		return mValue;
	}

	@Override
	public String getType() {
		return mType;
	}

}
