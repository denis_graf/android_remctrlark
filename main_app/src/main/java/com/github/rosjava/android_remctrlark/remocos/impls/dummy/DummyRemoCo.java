/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.dummy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.rca_plugin_java.R;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImpl;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCoConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext;

public class DummyRemoCo implements IJavaRemoCoImpl {

	protected IJavaRemoCoContext mRcaContext;
	protected Context mApkContext;
	protected ViewGroup mParentView;

	public void init(IJavaRemoCoContext context) {
		mRcaContext = context;
		mApkContext = mRcaContext.getApkContext();
		mParentView = mRcaContext.getLayout().getParentView();
	}

	public void start(@SuppressWarnings("UnusedParameters") IJavaRemoCoConfiguration configuration) {
	}

	@Override
	public void createUI() {
		// Note: Here mParentView.getContext() must be used for loading correctly the resources.
		assert mParentView.getContext() != null;
		LayoutInflater.from(mParentView.getContext()).inflate(R.layout.dummy_remoco, mParentView);
	}

	@Override
	public void shutdown() {
	}
}
