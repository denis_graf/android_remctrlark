/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.error_handling;

public class ErrorInRemoCoImplCodeException extends RcaException {

	// TODO: implement handling by the component providing the custom code...

	public ErrorInRemoCoImplCodeException(/*ICustomCodeComponent component, */ Throwable cause) {
		super("Uncaught error in custom code.", cause);
	}

	public ErrorInRemoCoImplCodeException(/*ICustomCodeComponent component, */) {
		this(null);
	}

}
