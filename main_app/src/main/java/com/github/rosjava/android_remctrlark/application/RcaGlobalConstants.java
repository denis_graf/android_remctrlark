/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import com.android.python27.config.GlobalConstants;

import java.net.URI;

public class RcaGlobalConstants {

	public static final String LOG_TAG = GlobalConstants.LOG_TAG;

	public static final URI NULL_URI = URI.create("rca:null").normalize();

	public static final String MAIN_APK = "com.github.rosjava.android_remctrlark";

	public static final String PYTHON_ZIP_NAME = "python_27";
	public static final String PYTHON_EXTRAS_ZIP_NAME = "python_extras_27";
	public static final String PYTHON_EXTRAS_ROS_ZIP_NAME = "ros_python_extras_27";
	public static final String PYTHON_CORE_PROJECT_ZIP_NAME = "android_remctrlark_python_core";
	public static final String PYTHON_PLUGIN_PROJECT_ZIP_NAME = "rca_plugin_python";
	public static final String PYTHON_REMOCONS_DIR_NAME = "remocons";
	public static final String PYTHON_REMOCOMPS_DIR_NAME = "remocomps";
	public static final String PYTHON_SHARED_DIR_NAME = "shared";
	public static final String REMOCON_CONFIGS_DIR_NAME = "remocon_configs";
	public static final String REMOCON_CONFIGS_ZIP_NAME = REMOCON_CONFIGS_DIR_NAME;
	public static final String JAVA_MSGS_DIR_NAME = "msgs";
	public static final String JAVA_MSGS_ZIP_NAME = "java_ros_msgs";
	public static final String JAVA_PLUGIN_FACTORY_CLASS_NAME = ".rca_plugin_java.factories.JavaRemoCoImplsFactory";
	public static final int CHECK_ROS_MASTER_CONNECTION_TIMEOUT = 5000;
}
