/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rosjava.android_remctrlark.R;

import java.util.List;

public class InitialChooserListItemAdapter extends ArrayAdapter<IInitialChooserListItem> {

	private final Context mContext;
	private final List<IInitialChooserListItem> mRemoConConfigsListItems;

	public InitialChooserListItemAdapter(Context context, List<IInitialChooserListItem> values) {
		super(context, R.layout.remocon_configs_list_item, values);

		mContext = context;
		mRemoConConfigsListItems = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.remocon_configs_list_item, parent, false);
		}
		assert convertView != null;

		final IInitialChooserListItem item = mRemoConConfigsListItems.get(position);
		((TextView) convertView.findViewById(R.id.title)).setText(item.getTitle());
		((ImageView) convertView.findViewById(R.id.logo)).setImageDrawable(item.getLogo());

		final TextView subtitleView = (TextView) convertView.findViewById(R.id.subtitle);
		if (item.getSubtitle() != null) {
			subtitleView.setText(item.getSubtitle());
			subtitleView.setTypeface(null, Typeface.ITALIC);
			subtitleView.setVisibility(View.VISIBLE);
		} else {
			subtitleView.setVisibility(View.GONE);
		}

		return convertView;
	}
}
