/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rca_logging;

import org.jetbrains.annotations.NotNull;

public class RcaJavaLog extends ARcaLog {

	public static final String TAG_JAVA = "RCA_JAVA";
	public static final RcaJavaLog LOG = new RcaJavaLog();
	private final String mSubSubTag;

	public RcaJavaLog() {
		this(null);
	}

	public RcaJavaLog(String subSubTag) {
		mSubSubTag = subSubTag;
	}

	@NotNull
	@Override
	protected String getTag() {
		return TAG_JAVA;
	}

	@NotNull
	@Override
	protected String getSubTag() {
		return mSubSubTag != null ? getClassName() + ":" + mSubSubTag : getClassName();
	}
}
