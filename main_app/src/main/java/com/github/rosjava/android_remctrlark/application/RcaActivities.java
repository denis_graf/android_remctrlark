/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfig;
import com.google.common.io.Files;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;

class RcaActivities {
	private static final String DM_TITLE_PREFIX = "<DEV> ";
	// TODO: move file names into the file manager
	private static final String LOGOS_PATH = ".logos/";

	private RcaActivities() {
	}

	static String updateTitle(IRcaSettings settings, String title) {
		final boolean developerModeEnabled = settings.getDeveloperModeEnabled();
		if (developerModeEnabled != title.startsWith(DM_TITLE_PREFIX)) {
			return developerModeEnabled ? DM_TITLE_PREFIX + title : title.substring(DM_TITLE_PREFIX.length());
		}
		return null;
	}

	@NotNull
	static Drawable retrieveRemoCoViewerLogo(@NotNull Resources resources) {
		return resources.getDrawable(R.drawable.remoco_viewer_logo);
	}

	@NotNull
	static Drawable retrievePluginLogo(@NotNull Resources resources, @NotNull IMainFileNamesManager fileNamesManager, String apk) {
		return retrieveRemoConLogo(resources, fileNamesManager, null, apk, null);
	}

	public static Drawable retrieveRemoConsCategoryLogo(Resources resources, IMainFileNamesManager fileNamesManager, String fullFileName, String apk) {
		return retrieveRemoConLogo(resources, fileNamesManager, fullFileName, apk, null);
	}

	@NotNull
	static Drawable retrieveRemoConLogo(@NotNull Resources resources, IMainFileNamesManager fileNamesManager,
	                                    String fullFileName, String apk, IRemoConConfig remoConConfigPreview) {
		File file = null;
		String configDir = null;
		String logoName = null;
		String baseConfApk = remoConConfigPreview != null ? remoConConfigPreview.getBaseConfApk() : null;

		if (fullFileName != null) {
			final File configFile = new File(fullFileName);
			configDir = configFile.getParent();
			logoName = remoConConfigPreview != null && remoConConfigPreview.getLogo() != null ?
					remoConConfigPreview.getLogo() :
					(configFile.isDirectory() ? configFile.getName() : Files.getNameWithoutExtension(configFile.getName())) + ".png";
			file = new File(configDir, LOGOS_PATH + logoName);
		}

		if ((file == null || !file.exists()) && fileNamesManager != null) {
			if (configDir != null) {
				file = retrieveFallbackLogoFile(logoName, configDir, false);
			}

			if ((file == null || !file.exists()) && remoConConfigPreview != null) {
				file = retrieveFallbackLogoFile(logoName, fileNamesManager.getRemoConConfigsFullDir(baseConfApk), !baseConfApk.equals(apk));
			}

			if ((file == null || !file.exists()) && configDir != null) {
				file = retrieveFallbackLogoFile(logoName, configDir);
			}

			if ((file == null || !file.exists()) && remoConConfigPreview != null) {
				file = retrieveFallbackLogoFile(logoName, fileNamesManager.getRemoConConfigsFullDir(baseConfApk));
			}

			if ((file == null || !file.exists()) && apk != null) {
				file = retrieveFallbackLogoFile(logoName, fileNamesManager.getRemoConConfigsFullDir(apk));
			}
		}

		Drawable logo = null;
		if (file != null && file.exists()) {
			logo = Drawable.createFromPath(file.getAbsolutePath());
		}
		if (logo == null) {
			logo = resources.getDrawable(R.drawable.default_plugin_logo);
			assert logo != null;
		}

		return logo;
	}

	private static File retrieveFallbackLogoFile(String logoName, String configFullDir) {
		return retrieveFallbackLogoFile(logoName, configFullDir, true);
	}

	private static File retrieveFallbackLogoFile(String logoName, String configFullDir, boolean fallbackToDefault) {
		File file = null;
		final String dir = configFullDir + "/" + LOGOS_PATH;

		if (logoName != null && !logoName.isEmpty()) {
			file = new File(dir + logoName);
		}

		if (fallbackToDefault && (file == null || !file.exists())) {
			file = new File(dir + "default.png");
		}

		return file;
	}

	public static String retrieveRemoConTitle(@NotNull IRemoConConfig config) {
		return FilenameUtils.getBaseName(config.getConfigFileFullDir());
	}

	public static void openFileInTextEditor(Activity activity, String fileName) {
		Intent intent = new Intent(Intent.ACTION_DEFAULT);
		intent.setDataAndType(Uri.parse("file://" + fileName), "text/plain");
		activity.startActivity(intent);
	}

	public static void openFileInExplorer(Activity activity, String fileName, int requestCode) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setDataAndType(Uri.parse("file://" + fileName), "file/*");
		activity.startActivityForResult(Intent.createChooser(intent, "Open Folder"), requestCode);
	}
}
