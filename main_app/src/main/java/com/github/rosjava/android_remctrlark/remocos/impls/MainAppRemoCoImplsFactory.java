/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.github.rosjava.android_remctrlark.application.IMainAppRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.IRcaPlugin;
import com.github.rosjava.android_remctrlark.application.IRcaSettings;
import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.remocos.IMainRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.java.JavaMainRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.remocos.impls.python.PythonMainRemoCoImplsFactory;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import dalvik.system.DexClassLoader;

public class MainAppRemoCoImplsFactory extends AMainRemoCoImplsFactory implements IMainAppRemoCoImplsFactory {

	private final Map<String, IRcaPlugin> mPlugins = new HashMap<String, IRcaPlugin>();
	private final List<IMainRemoCoImplsFactory> mRemoCoImplsFactories = new CopyOnWriteArrayList<IMainRemoCoImplsFactory>();

	public MainAppRemoCoImplsFactory(IRcaApplication app) {
		super(app);
	}

	@Override
	public void reload() {
		synchronized (mPlugins) {
			mPlugins.clear();
		}
//	    loadPlugins();

		synchronized (mRemoCoImplsFactories) {
			for (IMainRemoCoImplsFactory factory : mRemoCoImplsFactories) {
				factory.reload();
			}
		}
	}

	public void applySettings(@NotNull IRcaSettings settings) {
		synchronized (mRemoCoImplsFactories) {
			final IRcaApplication app = getApp();
			mRemoCoImplsFactories.clear();

			// Note: The order is important!
			if (settings.getPythonReplaceJavaRemoCos()) {
				mRemoCoImplsFactories.add(new PythonMainRemoCoImplsFactory(app));
				mRemoCoImplsFactories.add(new JavaMainRemoCoImplsFactory(app));
			} else {
				mRemoCoImplsFactories.add(new JavaMainRemoCoImplsFactory(app));
				mRemoCoImplsFactories.add(new PythonMainRemoCoImplsFactory(app));
			}
		}
	}

	@Override
	public Set<String> getAllRemoConTypes(String apk) {
		synchronized (mRemoCoImplsFactories) {
			final Set<String> result = new TreeSet<String>();
			for (IMainRemoCoImplsFactory factory : mRemoCoImplsFactories) {
				result.addAll(factory.getAllRemoConTypes(apk));
			}
			return result;
		}
	}

	@Override
	public Set<String> getAllRemoCompTypes(String apk) {
		synchronized (mRemoCoImplsFactories) {
			final Set<String> result = new TreeSet<String>();
			for (IMainRemoCoImplsFactory factory : mRemoCoImplsFactories) {
				result.addAll(factory.getAllRemoCompTypes(apk));
			}
			return result;
		}
	}

	@Override
	public IRemoConImpl createRemoConImpl(String idName, String type, String apk) {
		synchronized (mRemoCoImplsFactories) {
			IRemoConImpl impl = null;

			for (IMainRemoCoImplsFactory factory : mRemoCoImplsFactories) {
				impl = factory.createRemoConImpl(idName, type, apk);
				if (impl != null) {
					break;
				}
			}

			return impl;
		}
	}

	@Override
	public IRemoCompImpl createRemoCompImpl(String idName, String type, String apk) {
		synchronized (mRemoCoImplsFactories) {
			IRemoCompImpl impl = null;

			for (IMainRemoCoImplsFactory factory : mRemoCoImplsFactories) {
				impl = factory.createRemoCompImpl(idName, type, apk);
				if (impl != null) {
					break;
				}
			}

			return impl;
		}
	}

	@NotNull
	public String[] retrievePluginApks() {
		final PackageManager packageManager = getApp().getApplicationContext().getPackageManager();
		assert packageManager != null;
		return packageManager.getPackagesForUid(getApp().getApplicationInfo().uid);
	}

	@NotNull
	@Override
	public IRcaPlugin getPlugin(@NotNull String pluginApk) throws RcaPluginCouldNotBeLoadedException {
		synchronized (mPlugins) {
			IRcaPlugin plugin = mPlugins.get(pluginApk);
			if (plugin == null) {
				plugin = loadPlugin(pluginApk);
				mPlugins.put(pluginApk, plugin);
			}
			return plugin;
		}
	}

	@NotNull
	@Override
	public IRcaPlugin createFallbackPlugin(@NotNull final String pluginApk) {
		return new IRcaPlugin() {
			@NotNull
			@Override
			public String getApk() {
				return pluginApk;
			}

			@NotNull
			@Override
			public ApplicationInfo getApplicationInfo() {
				return getApp().getApplicationInfo();
			}

			@NotNull
			@Override
			public Context createContext() {
				return createFallbackContext();
			}

			@NotNull
			@Override
			public ClassLoader getClassLoader() {
				return getClass().getClassLoader();
			}
		};
	}

/*
	private void loadPlugins() {
		synchronized (mPlugins) {
			final String[] pluginApks = retrievePluginApks();
			for (String apk : pluginApks) {
				try {
					mPlugins.put(apk, loadPlugin(apk));
				} catch (RcaPluginCouldNotBeLoadedException e) {
					RcaService.getLog().e("invalid plugin", e);
				}
			}
		}
	}
*/

	@NotNull
	private IRcaPlugin loadPlugin(final String pluginApk) throws RcaPluginCouldNotBeLoadedException {
/*		Some notes on realizing a Plug-In-Architecture:
		- Manifest's 'sharedUserId': same by plug-ing-APK
		- plug-ing-APK has to be signed with same key!
		- Manifest's 'process': same by plug-ing-APK, if additional components are needed -> optimal solution :-), an guarantee for extending rights of the APK!
		- a factory for RemoComps is loaded dynamically from the plug-in-APK, by creating a new DexClassLoader from plug-ing-APK-file
		- overcome the cast problem: http://stackoverflow.com/questions/10254170/how-do-i-cast-a-dynamically-loaded-class-to-its-interface
			- in gradle the plug-in-APK has to set the dependency of the RemCtrlArk-plug-in-interfaces-lib as 'provided' (supported in gradle since version 8.0)
			- current approach: extract classes.jar from rcajava/build/libs/rcajava-[xyz].aar and use this library (without modifying anything)
			- old quick approach: pack the plug-in-interfaces into a .jar (get classes.dex of the APK -> dex2jar -> delete all other classes -> put in the jar a META-INF file with file MANIFEST-IMF with text: Manifest-Version: 1.0 in it)
		- caution: all used classes have to be in the APK! use a library, or don't forget to import the classes, even if they are not used!!! else they will not be found, because they are not in the APK!
*/
		boolean found = false;
		String[] plugins = retrievePluginApks();
		for (String plugin : plugins) {
			if (plugin.equals(pluginApk)) {
				found = true;
				break;
			}
		}
		if (!found) {
			throw new RcaPluginCouldNotBeLoadedException(pluginApk, new RcaException("Plugin not found."));
		}

		final ApplicationInfo pluginAppInfo;
		try {
			final PackageManager packageManager = getApp().getApplicationContext().getPackageManager();
			assert packageManager != null;
			pluginAppInfo = packageManager.getApplicationInfo(pluginApk, 0);
		} catch (Throwable e) {
			throw new RcaPluginCouldNotBeLoadedException(pluginApk, e);
		}

		return new IRcaPlugin() {
			private final Object mPluginClassLoaderLock = new Object();
			private ClassLoader mPluginClassLoader = null;

			@NotNull
			@Override
			public String getApk() {
				return pluginApk;
			}

			@NotNull
			@Override
			public ApplicationInfo getApplicationInfo() {
				return pluginAppInfo;
			}

			@NotNull
			@Override
			public Context createContext() {
				try {
					return getApp().getApplicationContext().createPackageContext(getApk(), Context.CONTEXT_INCLUDE_CODE);
				} catch (PackageManager.NameNotFoundException e) {
					RcaService.getLog().e("new plugin package context could not be created, fallback to new main application's context", e);
				}
				return createFallbackContext();
			}

			@NotNull
			@Override
			public ClassLoader getClassLoader() throws RcaPluginCouldNotBeLoadedException {
				synchronized (mPluginClassLoaderLock) {
					if (mPluginClassLoader == null) {
						try {
							final String tmpDirName = "dex_" + pluginApk;
							File tmpDir = getApp().getApplicationContext().getDir(tmpDirName, Context.MODE_PRIVATE);
							if (tmpDir.list().length > 0) {
								// For dynamically reloading plugins, we have to remove all outdated files.
								try {
									FileUtils.deleteDirectory(tmpDir);
									tmpDir = getApp().getApplicationContext().getDir(tmpDirName, Context.MODE_PRIVATE);
								} catch (IOException e) {
									RcaService.getLog().w("ignoring: outdated temporary dex files of a plugin could not be removed", e);
								}
							}
							mPluginClassLoader = new DexClassLoader(pluginAppInfo.sourceDir, tmpDir.getAbsolutePath(), null, getClass().getClassLoader());
						} catch (Throwable e) {
							throw new RcaPluginCouldNotBeLoadedException(pluginApk, e);
						}
					}
					return mPluginClassLoader;
				}
			}
		};
	}

	private Context createFallbackContext() {
		try {
			return getApp().getApplicationContext().createPackageContext(RcaGlobalConstants.MAIN_APK, Context.CONTEXT_INCLUDE_CODE);
		} catch (PackageManager.NameNotFoundException e) {
			RcaService.getLog().e("new main application package context could not be created, fallback to main application's context", e);
		}
		return getApp().getApplicationContext();
	}
}
