/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;

class RcaSettings implements IRcaSettings {

	static final String PREF_KEY_REMOCON_BLOCK_NOT_RESPONDING_REMOCOS = "pref_key_remocon_block_not_responding_remocos";
	static final String PREF_KEY_PYTHON = "pref_key_python";
	static final String PREF_KEY_PYTHON_PORT = "pref_key_python_port";
	static final String PREF_KEY_REMOCOS_SEARCHING_ORDER = "pref_key_remocos_searching_order";
	static final String PREF_KEY_DEVELOPER_MODE = "pref_key_developer_mode";
	static final String PREF_KEY_LOGGING_REMOCO_LOG_SIZE = "pref_key_logging_remoco_log_size";
	static final String PREF_KEY_PYTHON_NO_HANDSHAKE = "pref_key_python_no_handshake";
	static final String PREF_KEY_PYTHON_EXTERNAL_RCA_PYCORE = "pref_key_python_external_rca_pycore";
	static final String PREF_KEY_PYTHON_LOGCAT_LOGGING = "pref_key_python_logcat_logging";
	static final String PREF_KEY_PYTHON_OPTIMIZATION = "pref_key_python_optimization";
	static final String PREF_KEY_ROS_NOT_ANONYMOUS = "pref_key_ros_not_anonymous";
	static final String PREF_KEY_ROS_NAMESPACE = "pref_key_ros_namespace";
	static final String PREF_KEY_REMOCON_INSPECT_AFTER_LOADED = "pref_key_remocon_inspect_after_loaded";
	static final String PREF_KEY_REMOCON_INSPECT_SHUTDOWN_ERRORS = "pref_key_remocon_inspect_shutdown_errors";
	static final String PREF_KEY_REMOCON_INSPECT_ON_CLICK_ERROR = "pref_key_remocon_inspect_on_click_error";
	static final String PREF_KEY_REMOCON_INSPECT_ON_CLICK_HANGING = "pref_key_remocon_inspect_on_click_hanging";

	private final int mRemoconBlockNotRespondingRemocosTimeout;
	private final boolean mPythonEnabled;
	private final int mPythonPort;
	private final boolean mPythonReplaceJavaRemoCos;
	private final int mDmLoggingRemoCoLogSize;
	private final boolean mDeveloperModeEnabled;
	private final boolean mDmPythonNoHandshake;
	private final boolean mDmPythonLogcatLogging;
	private final int mDmPythonOptimization;
	private final boolean mDmRosNotAnonymousEnabled;
	private final String mDmRosNamespace;
	private final boolean mDmRemoConInspectAfterLoaded;
	private final boolean mDmRemoConInspectShutdownErrors;
	private final boolean mDmRemoConInspectOnClickError;
	private final boolean mDmPythonExternalRcaPyCore;
	private final boolean mDmRemoConInspectOnClickHanging;

	RcaSettings(@NotNull SharedPreferences sharedPref) {
		mRemoconBlockNotRespondingRemocosTimeout = Integer.parseInt(sharedPref.getString(PREF_KEY_REMOCON_BLOCK_NOT_RESPONDING_REMOCOS, "5")) * 1000;

		mPythonEnabled = sharedPref.getBoolean(PREF_KEY_PYTHON, true);
		mPythonPort = mPythonEnabled ? Integer.parseInt(sharedPref.getString(PREF_KEY_PYTHON_PORT, "45007")) : -1;
		mPythonReplaceJavaRemoCos = mPythonEnabled && Integer.parseInt(sharedPref.getString(PREF_KEY_REMOCOS_SEARCHING_ORDER, "1")) == 2;

		mDeveloperModeEnabled = sharedPref.getBoolean(PREF_KEY_DEVELOPER_MODE, false);
		mDmLoggingRemoCoLogSize = mDeveloperModeEnabled ? Integer.parseInt(sharedPref.getString(PREF_KEY_LOGGING_REMOCO_LOG_SIZE, "1000")) : 0;
		mDmPythonNoHandshake = (mDeveloperModeEnabled && mPythonEnabled) && sharedPref.getBoolean(PREF_KEY_PYTHON_NO_HANDSHAKE, false);
		mDmPythonExternalRcaPyCore = (mDeveloperModeEnabled && mPythonEnabled && mDmPythonNoHandshake) && sharedPref.getBoolean(PREF_KEY_PYTHON_EXTERNAL_RCA_PYCORE, false);
		mDmPythonLogcatLogging = (mDeveloperModeEnabled && mPythonEnabled) && sharedPref.getBoolean(PREF_KEY_PYTHON_LOGCAT_LOGGING, true);
		// Note: Don't use optimizations in not developer mode, because they seem not to work properly. If activated,
		// python needs to much time for loading scripts...
		mDmPythonOptimization = (mDeveloperModeEnabled && mPythonEnabled) ? Integer.parseInt(sharedPref.getString(PREF_KEY_PYTHON_OPTIMIZATION, "0")) : 0;
		mDmRosNotAnonymousEnabled = sharedPref.getBoolean(PREF_KEY_ROS_NOT_ANONYMOUS, false);
		mDmRosNamespace = (mDeveloperModeEnabled && mDmRosNotAnonymousEnabled) ?
				sharedPref.getString(PREF_KEY_ROS_NAMESPACE, "/android_remctrlark") :
				"/android_remctrlark_" + android.os.Process.myPid() + "_" + System.currentTimeMillis();
		mDmRemoConInspectAfterLoaded = mDeveloperModeEnabled && sharedPref.getBoolean(PREF_KEY_REMOCON_INSPECT_AFTER_LOADED, true);
		mDmRemoConInspectShutdownErrors = mDeveloperModeEnabled && sharedPref.getBoolean(PREF_KEY_REMOCON_INSPECT_SHUTDOWN_ERRORS, true);
		mDmRemoConInspectOnClickError = mDeveloperModeEnabled && sharedPref.getBoolean(PREF_KEY_REMOCON_INSPECT_ON_CLICK_ERROR, true);
		mDmRemoConInspectOnClickHanging = mDeveloperModeEnabled && sharedPref.getBoolean(PREF_KEY_REMOCON_INSPECT_ON_CLICK_HANGING, false);
	}

	@Override
	public int getRemoConBlockNotRespondingRemoCosTimeout() {
		return mRemoconBlockNotRespondingRemocosTimeout;
	}

	@Override
	public boolean getPythonEnabled() {
		return mPythonEnabled;
	}

	@Override
	public int getPythonPort() {
		return mPythonPort;
	}

	@Override
	public boolean getPythonReplaceJavaRemoCos() {
		return mPythonReplaceJavaRemoCos;
	}

	@Override
	public int getDmLoggingRemoCoLogSize() {
		return mDmLoggingRemoCoLogSize;
	}

	@Override
	public boolean getDeveloperModeEnabled() {
		return mDeveloperModeEnabled;
	}

	@Override
	public boolean getDmPythonNoHandshake() {
		// Note: If handshake is activated, and an unauthenticated caller tries to access the proxy, then the sl4a rpc
		// server sends a security exception to the caller and shuts down.
		return mDmPythonNoHandshake;
	}

	@Override
	public boolean getDmPythonExternalRcaPyCore() {
		return mDmPythonExternalRcaPyCore;
	}

	@Override
	public boolean getDmPythonLogcatLogging() {
		return mDmPythonLogcatLogging;
	}

	@Override
	public int getDmPythonOptimization() {
		return mDmPythonOptimization;
	}

	@Override
	public boolean getDmRosNotAnonymousEnabled() {
		return mDmRosNotAnonymousEnabled;
	}

	@Override
	public String getDmRosNamespace() {
		return mDmRosNamespace;
	}

	@Override
	public boolean getDmRemoConInspectAfterLoaded() {
		return mDmRemoConInspectAfterLoaded;
	}

	@Override
	public boolean getDmRemoConInspectShutdownErrors() {
		return mDmRemoConInspectShutdownErrors;
	}

	@Override
	public boolean getDmRemoConInspectOnClickError() {
		return mDmRemoConInspectOnClickError;
	}

	@Override
	public boolean getDmRemoConInspectOnClickHanging() {
		return mDmRemoConInspectOnClickHanging;
	}
}
