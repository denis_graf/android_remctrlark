/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.content.Intent;
import android.os.Bundle;

import com.github.rosjava.android_remctrlark.R;

public class RemoCosInspectorActivity extends ARemoCoInspectorActivity implements RemoCosInspectorFragment.Callbacks {

	public static final String TAB_INFO = RemoCoInspectorInfoFragment.TAB;
	public static final String TAB_LOG = RemoCoInspectorLogFragment.TAB;

	private boolean mTwoPane = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remocos_inspector_activity);

		if (findViewById(R.id.remocos_inspector_details_container) != null) {
			mTwoPane = true;
			getRemoCosInspectorFragment().setActivateOnItemClick(true);
		}

		selectRemoCoListItem(mRemoCoId, false);
	}

	@Override
	public void selectRemoCo(final String remoCoId) {
		selectRemoCoListItem(remoCoId, true);
	}

	protected void selectRemoCoListItem(final String remoCoId, final boolean updateRemoCoList) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final RemoCosInspectorFragment fragment = getRemoCosInspectorFragment();
				if (updateRemoCoList) {
					fragment.updateRemoCoList();
				}
				fragment.selectRemoCoListItem(remoCoId);
			}
		});
	}

	@Override
	public void onRemoCoSelected(String remoCoId) {
		if (mTwoPane) {
			mRemoCoId = remoCoId;
			final Bundle args = new Bundle();
			args.putString(RemoCoInspectorFragment.ARG_REMOCO_ID, remoCoId);
			args.putString(RemoCoInspectorFragment.ARG_SHOW_TAB, mShowTab);
			final RemoCoInspectorFragment fragment = new RemoCoInspectorFragment();
			fragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.replace(R.id.remocos_inspector_details_container, fragment)
					.commit();
			updateSubtitle();
		} else {
			final Intent detailIntent = new Intent(this, RemoCoInspectorActivity.class);
			startDetailsActivity(remoCoId, detailIntent);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_DETAIL_ACTIVITY) {
			if (resultCode == RESULT_OK) {
				if (!isFinishing() && mTwoPane) {
					getRemoCosInspectorFragment().updateRemoCoList();
				}
			}
		}
	}
}
