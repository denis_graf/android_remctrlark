/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoTypeNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.RemoCoCustomEvent;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;

public final class PythonRemoCompImpl extends APythonRemoCoImpl implements IRemoCompImpl {

	public PythonRemoCompImpl(IRcaApplication app, String idName, String type, String apk) throws RcaPluginCouldNotBeLoadedException, RemoCoTypeNotFoundException {
		super(app, idName, type, apk);
	}

	@Override
	protected IReflectingBinderImpl createInternalScriptBinderImpl() {
		return new InternalScriptBinder() {
			@SuppressWarnings("UnusedDeclaration")
			public void RemoCompEventDispatcher_dispatch(String name, Object data) {
				getRemoCo().dispatchEvent(RemoCoCustomEvent.EVENT_NAME, new RemoCoCustomEvent(name, data));
			}
		};
	}

	@Override
	public IRcaBinder create(IRemoComp remoComp) {
		super.create(remoComp);
		return getBinder();
	}

}
