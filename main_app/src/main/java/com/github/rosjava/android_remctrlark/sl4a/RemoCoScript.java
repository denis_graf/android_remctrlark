/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.error_handling.ErrorInScriptException;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;

import org.jetbrains.annotations.NotNull;

public class RemoCoScript extends ARcaScript implements IRcaScript {

	private final IScriptRemoCo mScriptRemoCo;

	public RemoCoScript(IRcaFacade rcaFacade, IScriptRemoCo scriptRemoCo) {
		super(rcaFacade, scriptRemoCo.getRemoCo().getIdName(), scriptRemoCo.getScriptBinder());
		mScriptRemoCo = scriptRemoCo;
	}

	public IRemoCo getRemoCo() {
		return mScriptRemoCo.getRemoCo();
	}

	@Override
	protected void onBound() {
		mScriptRemoCo.onScriptBound();
	}

	@Override
	protected void onShutdown() {
		mScriptRemoCo.onScriptShutdown();
	}

	@Override
	protected void onShutdownComplete() {
		mScriptRemoCo.onScriptShutdownComplete();
	}

	@Override
	public void handleError(ErrorInScriptException e) {
		mScriptRemoCo.handleScriptError(e);
	}

	@Override
	protected void onScriptExitReached() {
		mScriptRemoCo.onScriptExitReached();
	}

	@Override
	protected void onUnprocessedScriptProcessMessage(@NotNull String message) {
		mScriptRemoCo.onUnprocessedScriptProcessMessage(message);
	}
}
