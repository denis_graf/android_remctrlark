/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config;

import android.util.Pair;

import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class RemoConMutablesConfig implements ISerializableRemoConMutablesConfig {
	protected URI mMasterUri = null;
	protected RemoConConfigNewMaster mNewMaster = null;
	protected Map<IRcaId, Pair<IRcaId, String>> mTypes = new HashMap<IRcaId, Pair<IRcaId, String>>();
	protected Map<IRcaId, GraphName> mNamespaces = new HashMap<IRcaId, GraphName>();
	protected Map<IRcaId, Map<GraphName, Object>> mParams = new HashMap<IRcaId, Map<GraphName, Object>>();
	protected Map<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>> mRemappings = new HashMap<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>>();

	public RemoConMutablesConfig() {
		onConstruct();
	}

	protected void onConstruct() {
	}

	@Override
	public URI getMasterUri() {
		return mMasterUri;
	}

	@Override
	public RemoConConfigNewMaster getNewMaster() {
		return mNewMaster;
	}

	@NotNull
	@Override
	public Map<IRcaId, Pair<IRcaId, String>> getTypes() {
		return mTypes;
	}

	@NotNull
	@Override
	public Map<IRcaId, GraphName> getNamespaces() {
		return mNamespaces;
	}

	@NotNull
	@Override
	public Map<IRcaId, Map<GraphName, Object>> getParams() {
		return mParams;
	}

	@NotNull
	@Override
	public Map<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>> getRemappings() {
		return mRemappings;
	}

	public void setAllToInitialState() {
		mMasterUri = null;
		mNewMaster = null;
		mTypes.clear();
		mNamespaces.clear();
		mParams.clear();
		mRemappings.clear();
	}

	public void applyCommit(@NotNull IRemoConMutablesConfigCommit commit) {
		if (commit.isSetAllToInitialState()) {
			setAllToInitialState();
		}

		final URI masterUri = commit.getMasterUri();
		if (masterUri != null) {
			mMasterUri = masterUri;
		}

		final RemoConConfigNewMaster newMaster = commit.getNewMaster();
		if (newMaster != null) {
			mNewMaster = newMaster;
		}

		mTypes.putAll(commit.getTypes());
		mNamespaces.putAll(commit.getNamespaces());

		for (Map.Entry<IRcaId, Map<GraphName, Object>> entry : commit.getParams().entrySet()) {
			final IRcaId id = entry.getKey();
			Map<GraphName, Object> params = mParams.get(id);
			if (params == null) {
				params = new HashMap<GraphName, Object>();
				mParams.put(id, params);
			}
			params.putAll(entry.getValue());
		}

		for (Map.Entry<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>> entry : commit.getRemappings().entrySet()) {
			final IRcaId id = entry.getKey();
			Map<GraphName, Pair<GraphName, IRcaId>> remappings = mRemappings.get(id);
			if (remappings == null) {
				remappings = new HashMap<GraphName, Pair<GraphName, IRcaId>>();
				mRemappings.put(id, remappings);
			}
			remappings.putAll(entry.getValue());
		}
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(mMasterUri);
		out.writeObject(mNewMaster);

		out.writeObject(mTypes.size());
		for (Map.Entry<IRcaId, Pair<IRcaId, String>> entry : mTypes.entrySet()) {
			out.writeObject(entry.getKey());
			final Pair<IRcaId, String> value = entry.getValue();
			out.writeObject(value.first);
			out.writeObject(value.second);
		}

		out.writeObject(mNamespaces.size());
		for (Map.Entry<IRcaId, GraphName> entry : mNamespaces.entrySet()) {
			out.writeObject(entry.getKey());
			out.writeObject(entry.getValue().toString());
		}

		out.writeObject(mParams.size());
		for (Map.Entry<IRcaId, Map<GraphName, Object>> entry : mParams.entrySet()) {
			out.writeObject(entry.getKey());
			final Map<GraphName, Object> value = entry.getValue();
			out.writeObject(value.size());
			for (Map.Entry<GraphName, Object> valueEntry : value.entrySet()) {
				out.writeObject(valueEntry.getKey().toString());
				out.writeObject(valueEntry.getValue());
			}
		}

		out.writeObject(mRemappings.size());
		for (Map.Entry<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>> entry : mRemappings.entrySet()) {
			out.writeObject(entry.getKey());
			final Map<GraphName, Pair<GraphName, IRcaId>> value = entry.getValue();
			out.writeObject(value.size());
			for (Map.Entry<GraphName, Pair<GraphName, IRcaId>> valueEntry : value.entrySet()) {
				out.writeObject(valueEntry.getKey().toString());
				final Pair<GraphName, IRcaId> valueValue = valueEntry.getValue();
				out.writeObject(valueValue.first.toString());
				out.writeObject(valueValue.second);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		mMasterUri = (URI) in.readObject();
		mNewMaster = (RemoConConfigNewMaster) in.readObject();
		mTypes = new HashMap<IRcaId, Pair<IRcaId, String>>();
		mNamespaces = new HashMap<IRcaId, GraphName>();
		mParams = new HashMap<IRcaId, Map<GraphName, Object>>();
		mRemappings = new HashMap<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>>();

		int size = (Integer) in.readObject();
		for (int i = 0; i < size; ++i) {
			final IRcaId key = (IRcaId) in.readObject();
			final IRcaId valueFirst = (IRcaId) in.readObject();
			final String valueSecond = (String) in.readObject();
			mTypes.put(key, new Pair<IRcaId, String>(valueFirst, valueSecond));
		}

		size = (Integer) in.readObject();
		for (int i = 0; i < size; ++i) {
			final IRcaId key = (IRcaId) in.readObject();
			final GraphName value = GraphName.of((String) in.readObject());
			mNamespaces.put(key, value);
		}

		size = (Integer) in.readObject();
		for (int i = 0; i < size; ++i) {
			final IRcaId key = (IRcaId) in.readObject();
			final Integer valueSize = (Integer) in.readObject();
			final Map<GraphName, Object> value = new HashMap<GraphName, Object>(valueSize);
			mParams.put(key, value);
			for (int j = 0; j < size; ++j) {
				final GraphName valueKey = GraphName.of((String) in.readObject());
				final Object valueValue = in.readObject();
				value.put(valueKey, valueValue);
			}
		}

		size = (Integer) in.readObject();
		for (int i = 0; i < size; ++i) {
			final IRcaId key = (IRcaId) in.readObject();
			final Integer valueSize = (Integer) in.readObject();
			final Map<GraphName, Pair<GraphName, IRcaId>> value = new HashMap<GraphName, Pair<GraphName, IRcaId>>(valueSize);
			mRemappings.put(key, value);
			for (int j = 0; j < size; ++j) {
				final GraphName valueKey = GraphName.of((String) in.readObject());
				final GraphName valueFirst = GraphName.of((String) in.readObject());
				final IRcaId valueSecond = (IRcaId) in.readObject();
				value.put(valueKey, new Pair<GraphName, IRcaId>(valueFirst, valueSecond));
			}
		}
	}
}
