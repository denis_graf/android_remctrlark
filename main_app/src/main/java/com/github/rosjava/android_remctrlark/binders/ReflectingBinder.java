/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.binders;

import com.github.rosjava.android_remctrlark.error_handling.MethodNotFoundException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaMethod;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaParam;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaReturn;
import com.googlecode.android_scripting.rpc.JsonArgsConverter;

import org.json.JSONArray;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Note: Overloaded methods are not supported!
 */
public class ReflectingBinder implements IRcaBinder {

	private final IReflectingBinderImpl mImpl;

	public ReflectingBinder(IReflectingBinderImpl impl) {
		mImpl = impl;
	}

	@Override
	public Object invokeMethod(String name, Object[] args) throws RcaBinderInvocationException {
		Object result;
		try {
			Method foundMethod = getMethod(name);
			result = foundMethod.invoke(mImpl, args);
		} catch (Exception e) {
			throw new RcaBinderInvocationException(name, e);
		}
		return result;
	}

	@Override
	public Object invokeMethod(String name, JSONArray args) throws RcaBinderInvocationException {
		Object[] castedArgs;

		try {
			if (mImpl == null) {
				throw new RcaException("No binder exists.");
			}

			Method foundMethod = getMethod(name);
			Class<?>[] parameterTypes = foundMethod.getParameterTypes();
			castedArgs = new Object[parameterTypes.length];

			if (parameterTypes.length != args.length()) {
				throw new RcaException("Expected " + parameterTypes.length + " argument(s), but found " + args.length() + ".");
			}

			for (int i = 0; i < parameterTypes.length; ++i) {
				castedArgs[i] = JsonArgsConverter.convertParameter(args, i, parameterTypes[i]);
			}

		} catch (Exception e) {
			throw new RcaBinderInvocationException(name, e);
		}

		return invokeMethod(name, castedArgs);
	}

	@Override
	public List<IRcaBinderMethod> getMethods() {
		Method[] foundMethods = findAllMethods();
		List<IRcaBinderMethod> result = new ArrayList<IRcaBinderMethod>(foundMethods.length);
		for (Method method : foundMethods) {
			final String name = method.getName();
			final String doc = createMethodDoc(method);

			result.add(new IRcaBinderMethod() {
				@Override
				public String getName() {
					return name;
				}

				@Override
				public String getDoc() {
					return doc;
				}
			});
		}
		return result;
	}

	private String createMethodDoc(Method method) {
		final String name = method.getName();
		final String docIndent = "    ";
		String docHead = name + "(";

		RcaMethod methodDoc = method.getAnnotation(RcaMethod.class);
		String docBody = methodDoc != null ? docIndent + methodDoc.value() + "\n" : "";

		String docTail = "";
		Annotation[][] paramsAnnotations = method.getParameterAnnotations();
		Class[] paramTypes = method.getParameterTypes();
		int i = 0;
		for (Annotation[] paramAnnotations : paramsAnnotations) {
			RcaParam paramDoc = null;
			for (Annotation paramAnnotation : paramAnnotations) {
				if (paramAnnotation instanceof RcaParam) {
					paramDoc = (RcaParam) paramAnnotation;
					break;
				}
			}
			String paramName = paramDoc != null ? paramDoc.name() : "param" + (i + 1);
			docHead += (i == 0 ? "" : ", ") + paramName;
			docBody += paramDoc != null ? docIndent + "@param " + paramName + ": " + paramDoc.value() + "\n" : "";

			Class paramType = paramTypes[i++];
			docTail += docIndent + "@type " + paramName + ": " + paramType.getName() + "\n";
		}

		docHead += ")\n";

		RcaReturn returnDoc = method.getAnnotation(RcaReturn.class);
		docBody += returnDoc != null ? docIndent + "@return: " + returnDoc.value() + "\n" : "";

		String returnTypeName = method.getReturnType().getName();
		docTail += !returnTypeName.isEmpty() && !returnTypeName.equals("void") ? docIndent + "@rtype: " + returnTypeName + "\n" : "";

		return docHead + docBody + docTail;
	}

	private Method getMethod(String name) throws RcaException {
		Method foundMethod = null;
		Method[] methods = findAllMethods();
		for (Method method : methods) {
			if (method.getName().equals(name)) {
				foundMethod = method;
				break;
			}
		}

		if (foundMethod == null) {
			throw new MethodNotFoundException(name);
		}

		return foundMethod;
	}

	private Method[] findAllMethods() {
		List<Method[]> methodsArrays = new ArrayList<Method[]>();
		int methodsCount = 0;

		if (mImpl != null) {
			Class<?> currentClass = mImpl.getClass();
			for (Class<?> superClass = currentClass.getSuperclass();
			     superClass != null;  // Note: If currentClass is Object, then superClass is null, and we have to stop.
			     currentClass = superClass, superClass = currentClass.getSuperclass()) {
				Method[] declaredMethods = currentClass.getDeclaredMethods();
				methodsCount += declaredMethods.length;
				methodsArrays.add(declaredMethods);
			}
		}

		Method[] result = new Method[methodsCount];
		int position = 0;
		for (Method[] methodsArray : methodsArrays) {
			for (Method method : methodsArray) {
				result[position++] = method;
			}
		}
		return result;
	}

}
