/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.google.common.base.Preconditions;

class PyRemoConFileNamesManager implements IPyRemoConFileNamesManager {

	private static final String REMOCON_MAIN_SCRIPT_REL_PATH = "python/main.py";
	private static final String REMOCON_MAIN_LAYOUT_REL_PATH = "res/layout/main.xml";


	// TODO: adapt all hardcoded references

	private final String mRelDir;
	private final String mFullDir;
	private final String mType;


	public PyRemoConFileNamesManager(IMainFileNamesManager mainFileNamesManager, String type, String apk) {
		assert apk != null;
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(!apk.isEmpty());
		}
		mType = type;
		mRelDir = mainFileNamesManager.getPyRemoCompsRelDir();
		mFullDir = mainFileNamesManager.getPyRemoCompsFullDir(apk);
	}

	@Override
	public String getFullDir() {
		return mFullDir;
	}

	@Override
	public String getMainFullDir() {
		return mFullDir + "/" + mType;
	}

	@Override
	public String getMainRelDir() {
		return mRelDir + "/" + mType;
	}

	@Override
	public String getMainScriptRelName() {
		return getMainRelDir() + "/" + REMOCON_MAIN_SCRIPT_REL_PATH;
	}

	@Override
	public String getMainScriptFullName() {
		return getMainFullDir() + "/" + REMOCON_MAIN_SCRIPT_REL_PATH;
	}

	@Override
	public String getMainLayoutFullName() {
		return getMainFullDir() + "/" + REMOCON_MAIN_LAYOUT_REL_PATH;
	}

}
