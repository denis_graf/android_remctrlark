/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;

import java.util.LinkedList;
import java.util.List;

public class RemoCosInspectorFragment extends ListFragment {

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	public interface Callbacks {
		void onRemoCoSelected(String remoCoId);
	}

	private static Callbacks mDummyCallbacks = new Callbacks() {
		@Override
		public void onRemoCoSelected(String remoCoId) {
		}
	};

	private static class RemoCoDetailsListItem {
		public final String mRemoCoId;

		private RemoCoDetailsListItem(String remoCoId) {
			mRemoCoId = remoCoId;
		}

		@Override
		public String toString() {
			return mRemoCoId;
		}
	}

	private Callbacks mCallbacks = mDummyCallbacks;
	private List<RemoCoDetailsListItem> mRemoCoListItems = new LinkedList<RemoCoDetailsListItem>();

	public RemoCosInspectorFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		updateRemoCoList();
	}

	public boolean selectRemoCoListItem(String remoCoId) {
		assert getListView() != null;
		int position = 0;
		for (RemoCoDetailsListItem item : mRemoCoListItems) {
			if (item.mRemoCoId.equals(remoCoId)) {
				break;
			}
			++position;
		}
		if (position != mRemoCoListItems.size()) {
			getListView().setItemChecked(position, true);
			callCallbacksOnItemSelected(position);
			return true;
		}

		final String remoConId = RcaService.getContext().getRemoCon().getIdName();
		return !remoCoId.equals(remoConId) && selectRemoCoListItem(remoConId);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = mDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		super.onListItemClick(listView, view, position, id);
		callCallbacksOnItemSelected(position);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		assert getListView() != null;
		getListView().setChoiceMode(activateOnItemClick
				? ListView.CHOICE_MODE_SINGLE
				: ListView.CHOICE_MODE_NONE);
	}

	/**
	 * Notify the active callbacks interface (the activity, if the fragment is attached to one) that an item has been
	 * selected.
	 *
	 * @param position Position of the item to be selected.
	 */
	private void callCallbacksOnItemSelected(int position) {
		mCallbacks.onRemoCoSelected(mRemoCoListItems.get(position).mRemoCoId);
	}

	private void setActivatedPosition(int position) {
		assert getListView() != null;
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	public void updateRemoCoList() {
		mRemoCoListItems.clear();
		final IRemoCon remoCon = RcaService.getContext().getRemoCon();
		mRemoCoListItems.add(new RemoCoDetailsListItem(remoCon.getIdName()));
		for (IRemoComp remoComp : remoCon.findAllRemoComps(true)) {
			mRemoCoListItems.add(new RemoCoDetailsListItem(remoComp.getIdName()));
		}

		assert getActivity() != null;
		setListAdapter(new ArrayAdapter<RemoCoDetailsListItem>(
				getActivity(),
				android.R.layout.simple_list_item_activated_1,
				android.R.id.text1,
				mRemoCoListItems));
	}
}
