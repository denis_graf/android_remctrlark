/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config;

import android.util.Pair;

import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;

import java.net.URI;
import java.util.Map;

public class RemoConMutablesConfigCommit implements IRemoConMutablesConfigCommit {
	private final IRemoConMutablesConfig mConfig;
	private final boolean mSetAllToInitialState;

	public RemoConMutablesConfigCommit(IRemoConMutablesConfig config, boolean setAllToInitialState) {
		mConfig = config;
		mSetAllToInitialState = setAllToInitialState;
	}

	@Override
	public boolean isSetAllToInitialState() {
		return mSetAllToInitialState;
	}

	@Override
	public URI getMasterUri() {
		return mConfig.getMasterUri();
	}

	@Override
	public RemoConConfigNewMaster getNewMaster() {
		return mConfig.getNewMaster();
	}

	@NotNull
	@Override
	public Map<IRcaId, Pair<IRcaId, String>> getTypes() {
		return mConfig.getTypes();
	}

	@NotNull
	@Override
	public Map<IRcaId, GraphName> getNamespaces() {
		return mConfig.getNamespaces();
	}

	@NotNull
	@Override
	public Map<IRcaId, Map<GraphName, Object>> getParams() {
		return mConfig.getParams();
	}

	@NotNull
	@Override
	public Map<IRcaId, Map<GraphName, Pair<GraphName, IRcaId>>> getRemappings() {
		return mConfig.getRemappings();
	}
}
