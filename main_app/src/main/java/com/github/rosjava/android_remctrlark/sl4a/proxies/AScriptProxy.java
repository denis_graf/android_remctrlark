/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventDispatcher;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.github.rosjava.android_remctrlark.sl4a.IRcaScript;

public abstract class AScriptProxy extends APyProxy implements IScriptProxy, IRcaPyProxy {
	private IRcaScript mScript;

	public AScriptProxy(IRcaFacade rcaFacade, IRcaScript script) {
		super(rcaFacade);
		mScript = script;
		mScript.bindScriptProxy(this);
		mRcaFacade.putPyScript(mScript);
	}

	@Override
	public final IRcaBinder getBinder() {
		return mScript.getBinder();
	}

	@Override
	public final void shutdown() {
		if (mScript != null) {
			mScript.onShutdownScriptProxy();
			mRcaFacade.removePyScript(mScript);
			mScript = null;
		}
	}

	@Override
	public IRcaEventDispatcher getEventDispatcher() {
		return mRcaFacade.getEventDispatcher();
	}
}
