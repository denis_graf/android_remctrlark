/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rca_logging;

import org.jetbrains.annotations.NotNull;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public abstract class ARcaLog implements IRcaLog {
	public static final String LEVEL_DEBUG = "DEBUG";
	public static final String LEVEL_INFO = "INFO";
	public static final String LEVEL_ERROR = "ERROR";
	public static final String LEVEL_WARN = "WARN";

	@NotNull
	protected abstract String getTag();

	/**
	 * This method can be overridden, but should not be called in the inherited class, otherwise the result can be
	 * invalid when {@link #getClassName()} is called used.
	 *
	 * @return the subtag for logging
	 */
	@NotNull
	protected String getSubTag() {
		return getClassName();
	}

	@Override
	public final void d(@NotNull String msg) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.d(tag, composeMessage(LEVEL_DEBUG, subTag, msg));
		onLog(RcaLogLevel.DEBUG, tag, subTag, msg, null);
	}

	@Override
	public final void i(@NotNull String msg) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.i(tag, composeMessage(LEVEL_INFO, subTag, msg));
		onLog(RcaLogLevel.INFO, tag, subTag, msg, null);
	}

	@Override
	public final void e(Throwable e) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.e(tag, composeMessage(LEVEL_ERROR, subTag, createExceptionMessage(null, e)));
		onLog(RcaLogLevel.ERROR, tag, subTag, null, e);
	}

	@Override
	public final void e(String msg, Throwable e) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.e(tag, composeMessage(LEVEL_ERROR, subTag, createExceptionMessage(msg, e)));
		onLog(RcaLogLevel.ERROR, tag, subTag, msg, e);
	}

	@Override
	public final void e(@NotNull String msg) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.e(tag, composeMessage(LEVEL_ERROR, subTag, msg));
		onLog(RcaLogLevel.ERROR, tag, subTag, msg, null);
	}

	@Override
	public final void w(Throwable e) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.w(tag, composeMessage(LEVEL_WARN, subTag, createExceptionMessage(null, e)));
		onLog(RcaLogLevel.WARN, tag, subTag, null, e);
	}

	@Override
	public final void w(String msg, Throwable e) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.w(tag, composeMessage(LEVEL_WARN, subTag, createExceptionMessage(msg, e)));
		onLog(RcaLogLevel.WARN, tag, subTag, msg, e);
	}

	@Override
	public final void w(@NotNull String msg) {
		final String tag = getTag();
		final String subTag = getSubTag();
		android.util.Log.w(tag, composeMessage(LEVEL_WARN, subTag, msg));
		onLog(RcaLogLevel.WARN, tag, subTag, msg, null);
	}

	protected void onLog(RcaLogLevel logLevel, String tag, String subTag, String msg, Throwable exception) {
	}

	/**
	 * Only allowed to be called in {@link #getSubTag()}, otherwise the result can be invalid.
	 *
	 * @return the class name of the caller object of the logging method
	 */
	@NotNull
	protected final String getClassName() {
		try {
			StackTraceElement stackTraceElement = new Exception().getStackTrace()[3];
			String className = stackTraceElement.getClassName();
			return className.substring(className.lastIndexOf(".") + 1);
		} catch (Throwable ignored) {
			android.util.Log.w(RcaJavaLog.TAG_JAVA, "Class name could not be determined for logging!");
			return "";
		}
	}

	@NotNull
	protected String composeMessage(@NotNull String level, @NotNull String subTag, @NotNull String msg) {
		return level + (subTag.isEmpty() ? " " : " [" + subTag + "] ") + msg;
	}

	@NotNull
	private static String createExceptionMessage(String msg, Throwable e) {
		if (e == null) {
			return msg;
		}
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		e.printStackTrace(printWriter);
		return msg != null ? msg + "\n" + writer.toString() : writer.toString();
	}
}
