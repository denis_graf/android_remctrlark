/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.R;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RemoCoInspectorFragment extends ARemoCoInspectorDetailFragment {

	public static final String ARG_SHOW_TAB = "show_tab";

	private static final Map<ActionBar.Tab, TabsAdapter.TabInfo> mTabsMap = new HashMap<ActionBar.Tab, TabsAdapter.TabInfo>();

	private String mShowTab = null;

	public RemoCoInspectorFragment() {
	}

	public interface Callbacks {
		void onDetailTabSelected(String tab);
	}

	private static Callbacks mDummyCallbacks = new Callbacks() {
		@Override
		public void onDetailTabSelected(String tab) {
		}
	};

	private Callbacks mCallbacks = mDummyCallbacks;

	@Override
	public View onRemoInspectorCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.remoco_inspector_fragment, container, false);
		assert rootView != null;

		assert getArguments() != null;
		mShowTab = getArguments().getString(ARG_SHOW_TAB);
		if (savedInstanceState != null) {
			mShowTab = savedInstanceState.getString(ARG_SHOW_TAB, mShowTab);
		}

		final Activity activity = getActivity();
		assert activity != null;
		final ActionBar actionBar = activity.getActionBar();
		assert actionBar != null;
		final ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.remoco_inspector_details_pager);
		assert viewPager != null;

		final Bundle args = new Bundle();
		args.putString(ARemoCoInspectorDetailFragment.ARG_REMOCO_ID, getRemoCo().getIdName());

		final TabsAdapter tabsAdapter = new TabsAdapter(activity, actionBar, viewPager);
		tabsAdapter.addTab(actionBar.newTab().setText(RemoCoInspectorInfoFragment.TAB),
				RemoCoInspectorInfoFragment.class, args,
				mShowTab == null || mShowTab.equals(RemoCoInspectorInfoFragment.TAB));
		tabsAdapter.addTab(actionBar.newTab().setText(RemoCoInspectorLogFragment.TAB),
				RemoCoInspectorLogFragment.class, args,
				mShowTab != null && mShowTab.equals(RemoCoInspectorLogFragment.TAB));

		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(ARG_SHOW_TAB, mShowTab);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = mDummyCallbacks;
	}

	/**
	 * Copyright (C) 2011 The Android Open Source Project
	 * <p/>
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 * <p/>
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * <p/>
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 * <p/>
	 * <p/>
	 * This is a helper class that implements the management of tabs and all
	 * details of connecting a ViewPager with associated TabHost.  It relies on a
	 * trick.  Normally a tab host has a simple API for supplying a View or
	 * Intent that each tab will show.  This is not sufficient for switching
	 * between pages.  So instead we make the content part of the tab host
	 * 0dp high (it is not shown) and the TabsAdapter supplies its own dummy
	 * view to show as the tab content.  It listens to changes in tabs, and takes
	 * care of switch to the correct paged in the ViewPager whenever the selected
	 * tab changes.
	 * <p/>
	 * Modified by Denis Graf.
	 */
	public class TabsAdapter extends FragmentPagerAdapter implements ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final ArrayList<TabsAdapter.TabInfo> mTabs = new ArrayList<TabsAdapter.TabInfo>();
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;

		final class TabInfo {
			private ARemoCoInspectorDetailFragment mFragment = null;
			private final Class<? extends ARemoCoInspectorDetailFragment> mClass;
			private final Bundle mArgs;
			public final boolean mSelectedOnStartup;

			TabInfo(Class<? extends ARemoCoInspectorDetailFragment> fragmentClass, Bundle args, final boolean fragmentTabSelectedOnStartup) {
				mClass = fragmentClass;
				mArgs = args;
				mSelectedOnStartup = fragmentTabSelectedOnStartup;
			}
		}

		public TabsAdapter(Activity activity, ActionBar actionBar, ViewPager pager) {
			super(activity.getFragmentManager());
			mContext = activity;
			mActionBar = actionBar;
			mViewPager = pager;

			if (mActionBar.getTabCount() > 0) {
				// Note: If there are already tabs from a detached instance of this fragment, then we have to remove the
				// fragments attached to the tabs. Otherwise the new fragments won't be created.
				final FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
				for (int i = 0; i < mActionBar.getTabCount(); ++i) {
					final TabsAdapter.TabInfo tabInfo = mTabsMap.remove(mActionBar.getTabAt(i));
					if (tabInfo != null) {
						transaction.remove(tabInfo.mFragment);
					}
				}
				mActionBar.removeAllTabs();
				transaction.commit();
			} else {
				mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			}

			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<? extends ARemoCoInspectorDetailFragment> fragmentClass, Bundle args, boolean setSelected) {
			final TabInfo info = new TabInfo(fragmentClass, args, setSelected);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab, setSelected);

			if (BuildConfig.DEBUG) {
				Preconditions.checkState(!mTabsMap.containsKey(tab));
			}
			mTabsMap.put(tab, info);

			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			final TabInfo info = mTabs.get(position);
			info.mFragment = (ARemoCoInspectorDetailFragment) Fragment.instantiate(mContext, info.mClass.getName(), info.mArgs);
			if (info.mSelectedOnStartup) {
				info.mFragment.setDetailFragmentStateOnStartup(true);
			}
			return info.mFragment;
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		@Override
		public void onPageScrollStateChanged(int state) {
		}

		@Override
		public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
			final Object tag = tab.getTag();
			for (int i = 0; i < mTabs.size(); ++i) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
					break;
				}
			}
			mShowTab = tab.getText().toString();
			callOnFragmentTabSelected((TabInfo) tab.getTag(), true);
			mCallbacks.onDetailTabSelected(mShowTab);
		}

		@Override
		public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
			callOnFragmentTabSelected((TabInfo) tab.getTag(), false);
		}

		@Override
		public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
		}

		private void callOnFragmentTabSelected(@NotNull TabInfo info, boolean selected) {
			if (info.mFragment != null) {
				info.mFragment.changeDetailFragmentState(selected);
			}
		}
	}
}
