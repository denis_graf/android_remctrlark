/*
 * Copyright (C) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original DefaultMessageFactory.java written by Damon Kohler (damonkohler@google.com) modified by Denis Graf.
 */
package com.github.rosjava.android_remctrlark.ros_messages;

import org.ros.internal.message.MessageInterfaceClassProvider;
import org.ros.internal.message.MessageProxyFactory;
import org.ros.message.MessageDeclaration;
import org.ros.message.MessageDefinitionProvider;
import org.ros.message.MessageFactory;

public class RcaMessageFactory implements MessageFactory {

	private final MessageDefinitionProvider mMessageDefinitionProvider;
	private final MessageProxyFactory mMessageProxyFactory;

	public RcaMessageFactory(MessageDefinitionProvider messageDefinitionProvider, MessageInterfaceClassProvider messageInterfaceClassProvider) {
		mMessageDefinitionProvider = messageDefinitionProvider;
		mMessageProxyFactory = new MessageProxyFactory(messageInterfaceClassProvider, this);
	}

	@Override
	public <T> T newFromType(String messageType) {
		final String messageDefinition = mMessageDefinitionProvider.get(messageType);
		final MessageDeclaration messageDeclaration = MessageDeclaration.of(messageType, messageDefinition);
		return mMessageProxyFactory.newMessageProxy(messageDeclaration);
	}
}
