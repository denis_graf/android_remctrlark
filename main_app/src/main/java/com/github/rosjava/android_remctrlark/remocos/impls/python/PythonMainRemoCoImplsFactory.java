/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import com.github.rosjava.android_remctrlark.application.IPyRemoCoFileNamesManager;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.AMainRemoCoImplsFactory;

import org.apache.commons.io.filefilter.DirectoryFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Set;
import java.util.TreeSet;

public class PythonMainRemoCoImplsFactory extends AMainRemoCoImplsFactory {

	public PythonMainRemoCoImplsFactory(IRcaApplication app) {
		super(app);
	}

	@Override
	public void reload() {
	}

	@Override
	public Set<String> getAllRemoConTypes(String apk) {
		return getAllRemoCosTypes(getApp().getFileNamesManager().getPyRemoConsFullDir(apk), apk);
	}

	@Override
	public Set<String> getAllRemoCompTypes(String apk) {
		return getAllRemoCosTypes(getApp().getFileNamesManager().getPyRemoCompsFullDir(apk), apk);
	}

	@Override
	public IRemoConImpl createRemoConImpl(String idName, String type, String apk) {
		if (!isPythonEnabled()) {
			return null;
		}

		PythonRemoConImpl impl = null;
		IPyRemoCoFileNamesManager fileNamesManager = getFileNamesManager(type, apk);
		if (fileNamesManager != null) {
			try {
				impl = new PythonRemoConImpl(getApp(), idName, type, apk);
			} catch (RcaException ignored) {
			}
		}
		return impl;
	}

	@Override
	public IRemoCompImpl createRemoCompImpl(String idName, String type, String apk) {
		if (!isPythonEnabled()) {
			return null;
		}

		PythonRemoCompImpl impl = null;
		IPyRemoCoFileNamesManager fileNamesManager = getFileNamesManager(type, apk);
		if (fileNamesManager != null) {
			try {
				impl = new PythonRemoCompImpl(getApp(), idName, type, apk);
			} catch (RcaException ignored) {
			}
		}
		return impl;
	}

	private Set<String> getAllRemoCosTypes(String remoCosFullDir, String apk) {
		final Set<String> result = new TreeSet<String>();
		final File dir = new File(remoCosFullDir);
		final File[] files = dir.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY);
		if (files != null) {
			for (File file : files) {
				final String type = file.getName();
				final File mainScriptFile = new File(getFileNamesManager(type, apk).getMainScriptFullName());
				if (mainScriptFile.exists()) {
					result.add(type);
				}
			}
		}
		return result;
	}

	private boolean isPythonEnabled() {
		final IRcaContext context = RcaService.getContext();
		assert context != null;
		return context.getSettings().getPythonEnabled();
	}

	private IPyRemoCoFileNamesManager getFileNamesManager(String type, String apk) {
		return getApp().getFileNamesManager().getPyRemoCoFileNamesManager(type, apk);
	}

}
