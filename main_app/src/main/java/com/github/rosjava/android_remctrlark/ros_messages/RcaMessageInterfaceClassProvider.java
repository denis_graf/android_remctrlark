/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original DefaultMessageInterfaceClassProvider.java written by Damon Kohler (damonkohler@google.com) modified by Denis Graf.
 */
package com.github.rosjava.android_remctrlark.ros_messages;


import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.google.common.collect.Maps;

import org.jetbrains.annotations.NotNull;
import org.ros.internal.message.MessageInterfaceClassProvider;
import org.ros.internal.message.RawMessage;

import java.util.Map;

public class RcaMessageInterfaceClassProvider implements MessageInterfaceClassProvider {
	private final Map<String, Class<?>> cache = Maps.newConcurrentMap();
	private final Class<?> mPluginRawMessage;

	public RcaMessageInterfaceClassProvider(@NotNull ClassLoader classLoader) {
		try {
			mPluginRawMessage = classLoader.loadClass(RawMessage.class.getName());
		} catch (ClassNotFoundException e) {
			throw new RcaRuntimeError(RawMessage.class.getName() + " could not be loaded from class loader.");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Class<T> get(String messageType) {
		if (cache.containsKey(messageType)) {
			return (Class<T>) cache.get(messageType);
		}
		try {
			final String className = messageType.replace("/", ".");
			final Class<T> messageInterfaceClass = (Class<T>) getClass().getClassLoader().loadClass(className);
			cache.put(messageType, messageInterfaceClass);
			return messageInterfaceClass;
		} catch (Throwable ignored) {
		}
		return (Class<T>) mPluginRawMessage;
	}
}
