/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.IRcaLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.IRcaBinderMethod;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RcaUnexpectedError;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoImplNotCreatedError;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoTypeNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventListener;
import com.github.rosjava.android_remctrlark.event_handling.SimpleEventDispatcher;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLog;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLogEntry;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogEntriesComparator;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogEntry;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoConfigRetriever;
import com.github.rosjava.android_remctrlark.remocos.IOnNewRemoCoLogEntryListener;
import com.github.rosjava.android_remctrlark.remocos.IOnRemoCoStateChangedListener;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImplLogEntry;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoLogEntry;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.github.rosjava.android_remctrlark.remocos.RemoCoStateChange;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;
import org.ros.node.NodeConfiguration;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class ARemoCo extends FrameLayout implements IRemoCo {

	@Override
	protected void finalize() throws Throwable {
		if (BuildConfig.DEBUG) {
			final String msg = "remoco main instance finalized";
			boolean logged = false;
			try {
				getLog().d(msg);
				logged = true;
			} catch (Throwable ignored) {
			}
			if (!logged) {
				try {
					RcaService.getLog().d(msg + ", id='" + getIdName() + "'");
				} catch (Throwable ignored) {
				}
			}
		}
		super.finalize();
	}

	public static class LogEntry extends RcaLogEntry implements IRemoCoLogEntry {
		private String mRemoCoId;

		public LogEntry(String remoCoId, @NotNull IRcaLogEntry entry) {
			this(remoCoId, entry.getLogLevel(), entry.getTime(), entry.getTag(), entry.getSubTag(), entry.getMessage(), entry.getException());
		}

		public LogEntry(String remoCoId, RcaLogLevel logLevel, long time, String tag, String subTag, String message, Throwable exception) {
			super(logLevel, time, tag, subTag, message, exception);
			mRemoCoId = remoCoId;
		}

		public void setRemoCoId(String id) {
			mRemoCoId = id;
		}

		@Override
		public String getRemoCoId() {
			return mRemoCoId;
		}
	}

	private class BinderWrapper implements IRcaBinder {

		private IRcaBinder mDelegate;

		public void setDelegate(IRcaBinder delegate) {
			synchronized (mRelaunchLock) {
				mDelegate = delegate;
			}
		}

		@Override
		public Object invokeMethod(String name, Object[] args) throws RcaBinderInvocationException {
			synchronized (mRelaunchLock) {
				return mDelegate.invokeMethod(name, args);
			}
		}

		@Override
		public Object invokeMethod(String name, JSONArray args) throws RcaBinderInvocationException {
			synchronized (mRelaunchLock) {
				return mDelegate.invokeMethod(name, args);
			}
		}

		@Override
		public List<IRcaBinderMethod> getMethods() {
			synchronized (mRelaunchLock) {
				return mDelegate.getMethods();
			}
		}
	}

	private boolean mBeforeFirstCreateStateInitialized = false;
	private String mBeforeFirstCreateType = null;
	private String mBeforeFirstCreateApk = null;

	private boolean mImplCreated = false;
	private boolean mCreated = false;
	private boolean mStarted = false;
	private boolean mShutdown = false;
	private boolean mRelaunching = false;
	private final Object mRelaunchLock = new Object();

	private final RemoCoMetaUiLayer mMetaUiLayer;
	private ViewGroup mImplUiLayer;
	private WeakReference<IRcaLayoutInflaterFactory> mViewFactoryRef = null;

	private final BinderWrapper mBinder = new BinderWrapper();
	private final SimpleEventDispatcher mEventDispatcher = new SimpleEventDispatcher();
	private final Map<GraphName, Object> mParams = new HashMap<GraphName, Object>();

	private String mType = null;
	private String mApk = null;
	private IRemoCoImpl mImpl = null;
	private NameResolver mNameResolver = null;

	private final int mMaxLogEntriesSize = getRcaContext().getSettings().getDmLoggingRemoCoLogSize();
	private final SortedSet<IRemoCoLogEntry> mInternalLogEntries =
			getRcaContext().getSettings().getDeveloperModeEnabled() ? new TreeSet<IRemoCoLogEntry>(new RcaLogEntriesComparator()) : null;
	private final SortedSet<IRemoCoImplLogEntry> mImplLogEntries =
			getRcaContext().getSettings().getDeveloperModeEnabled() ? new TreeSet<IRemoCoImplLogEntry>(new RcaLogEntriesComparator()) : null;
	private IOnNewRemoCoLogEntryListener mOnNewLogEntryListener = null;
	private IOnRemoCoStateChangedListener mOnStateChangedListener = null;

	private final IRcaLog mLog = new RcaJavaLog() {
		@Override
		protected void onLog(RcaLogLevel logLevel, String tag, String subTag, String msg, Throwable exception) {
			addLogEntry(new LogEntry(getIdName(), logLevel, System.currentTimeMillis(), tag, subTag, msg, exception));
		}

		@NotNull
		@Override
		protected final String getSubTag() {
			final String className = getClassName();
			return className.isEmpty() ? getIdName() : className + ":" + getIdName();
		}
	};


	public ARemoCo(Context context) {
		super(context);
		mMetaUiLayer = new RemoCoMetaUiLayer(context, this);
		addView(mMetaUiLayer);
	}

	public ARemoCo(Context context, String type, String apk) {
		this(context);
		setType(type);
		setApk(apk);
	}

	public ARemoCo(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mMetaUiLayer = new RemoCoMetaUiLayer(context, this);
		addView(mMetaUiLayer);
	}

	@Override
	public boolean isCreated() {
		if (BuildConfig.DEBUG && mCreated) {
			Preconditions.checkState(mImplCreated);
		}
		return mCreated;
	}

	@Override
	public boolean isStarted() {
		if (BuildConfig.DEBUG && mStarted) {
			Preconditions.checkState(mImplCreated);
			Preconditions.checkState(mCreated);
		}
		return mStarted;
	}

	@Override
	public boolean isShutdown() {
		return mShutdown;
	}

	@Override
	public void startInspectMode() {
		mMetaUiLayer.startInspectMode();
	}

	@Override
	public boolean isSelectedInInspectMode() {
		return mMetaUiLayer.isSelectedInInspectMode();
	}


	@Override
	public List<IRemoCoImplLogEntry> retrieveImplLogEntries() {
		if (!getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return new ArrayList<IRemoCoImplLogEntry>();
		}
		assert mImplLogEntries != null;
		synchronized (mImplLogEntries) {
			return new ArrayList<IRemoCoImplLogEntry>(mImplLogEntries);
		}
	}

	@Override
	public List<IRemoCoLogEntry> retrieveInternalLogEntries() {
		if (!getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return new ArrayList<IRemoCoLogEntry>();
		}
		assert mInternalLogEntries != null;
		synchronized (mInternalLogEntries) {
			return new ArrayList<IRemoCoLogEntry>(mInternalLogEntries);
		}
	}

	@Override
	public void setOnNewLogEntryListener(final IOnNewRemoCoLogEntryListener listener) {
		mOnNewLogEntryListener = listener == null ? null : new IOnNewRemoCoLogEntryListener() {
			@Override
			public void onNewLogEntry(IRemoCo remoCo, IRemoCoLogEntry entry) {
				if (RcaService.getContext() == null) {
					remoCo.setOnNewLogEntryListener(null);
				} else {
					listener.onNewLogEntry(remoCo, entry);
				}
			}
		};
	}

	@Override
	public void setOnStateChangedListener(final IOnRemoCoStateChangedListener listener) {
		mOnStateChangedListener = listener == null ? null : new IOnRemoCoStateChangedListener() {
			@Override
			public void onStateChanged(IRemoCo remoCo, RemoCoStateChange stateChange) {
				if (RcaService.getContext() == null) {
					remoCo.setOnStateChangedListener(null);
				} else {
					listener.onStateChanged(remoCo, stateChange);
				}
			}
		};
	}

	@Override
	public final void addLogEntry(IRcaLogEntry entry) {
		if (!getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return;
		}
		if (entry instanceof ARemoCo.LogEntry) {
			final LogEntry remoCoEntry = (LogEntry) entry;
			if (!getIdName().equals(remoCoEntry.getRemoCoId())) {
				remoCoEntry.setRemoCoId(getIdName());
			}
			if (entry instanceof IRemoCoImplLogEntry) {
				addImplLogEntry((IRemoCoImplLogEntry) remoCoEntry);
			} else {
				addInternalLogEntry(remoCoEntry);
			}
		} else if (entry instanceof IRemoCoLogEntry && getIdName().equals(((IRemoCoLogEntry) entry).getRemoCoId())) {
			addInternalLogEntry((IRemoCoLogEntry) entry);
		} else {
			addInternalLogEntry(new LogEntry(getIdName(), entry));
		}
	}

	@Override
	public final Object getSessionState() {
		return getRemoCon().getSessionState(this);
	}

	@Override
	public final void setSessionState(Object state) {
		getRemoCon().setSessionState(this, state);
	}

	@Override
	public IRcaLayoutInflaterFactory getApkViewFactory() {
		return mViewFactoryRef != null ? mViewFactoryRef.get() : null;
	}

	protected final RemoCon getRemoCon() {
		return (RemoCon) getRcaContext().getRemoCon();
	}

	protected final void setViewFactory(IRcaLayoutInflaterFactory factory) {
		mViewFactoryRef = new WeakReference<IRcaLayoutInflaterFactory>(factory);
	}

	private void addImplLogEntry(IRemoCoImplLogEntry entry) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getRcaContext().getSettings().getDeveloperModeEnabled());
		}
		assert mImplLogEntries != null;
		addLogEntryInternal(mImplLogEntries, entry);
	}

	private void addInternalLogEntry(IRemoCoLogEntry entry) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getRcaContext().getSettings().getDeveloperModeEnabled());
		}
		assert mInternalLogEntries != null;
		addLogEntryInternal(mInternalLogEntries, entry);
	}

	private <T extends IRemoCoLogEntry> void addLogEntryInternal(final SortedSet<T> logEntries, final T entry) {
		if (!getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return;
		}
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(entry.getRemoCoId().equals(getIdName()));
		}
		//noinspection SynchronizationOnLocalVariableOrMethodParameter
		synchronized (logEntries) {
			logEntries.add(entry);
			if (logEntries.size() > mMaxLogEntriesSize) {
				logEntries.remove(logEntries.first());
			}
		}
		callOnNewLogEntryListener(entry);
	}

	@Override
	public final void clearLog() {
		if (!getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return;
		}
		assert mImplLogEntries != null;
		assert mInternalLogEntries != null;
		synchronized (mImplLogEntries) {
			mImplLogEntries.clear();
		}
		synchronized (mInternalLogEntries) {
			mInternalLogEntries.clear();
		}
	}

	public void create() {
		create(false);
	}

	private void create(boolean forceDummyCreation) {
		if (forceDummyCreation && mImplUiLayer != null) {
			getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
				@Override
				public void run() {
					if (mImplUiLayer != null) {
						removeView(mImplUiLayer);
						mImplUiLayer.removeAllViews();
						mImplUiLayer = null;
					}
				}
			});
		}
		if (BuildConfig.DEBUG) {
			try {
				checkNotOnUiThread();
				Preconditions.checkState(!isCreated());
				Preconditions.checkState(!isStarted());
				Preconditions.checkState(!isShutdown());
				Preconditions.checkState(mImplUiLayer == null);
			} catch (Throwable e) {
				handleErrorException(e);
			}
		}
		try {
			initializeBeforeCreateState();
		} catch (Throwable e) {
			handleErrorException(e);
		}
		try {
			initializeType();
		} catch (Throwable e) {
			handleErrorException(e);
			if (mType == null) {
				mType = "no_type_defined";
			}
		}
		try {
			initializeParams();
		} catch (Throwable e) {
			handleErrorException(e);
		}
		try {
			mImplUiLayer = new FrameLayout(getContext());
			createImpl(forceDummyCreation);
			getImpl().createUI();
		} catch (Throwable e) {
			handleErrorException(e);
			if (!forceDummyCreation) {
				create(true);
			} else {
				getLog().w("dummy could not be created");
			}
		}
	}

	protected final void attachImplUiLayer() {
		getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				if (BuildConfig.DEBUG) {
					// When first created, there is only {@code mMetaUiLayer}. When relaunching there is also
					// {@code mImplUiLayer} at index 0 (for drawing before {@code mMetaUiLayer}), which is not
					// removed yet to avoid flickering.
					Preconditions.checkState(isRelaunching() || getChildCount() == 1);
					Preconditions.checkState(!isRelaunching() || getChildCount() == 2);
				}
				final View implUiLayer = getChildAt(0);
				if (implUiLayer != mMetaUiLayer) {
					removeViewAt(0);
					if (implUiLayer instanceof ViewGroup) {
						((ViewGroup) implUiLayer).removeAllViews();
					}
				}
				addView(mImplUiLayer, 0);
			}
		});
	}

	protected abstract void start(@NotNull NodeConfiguration defaultNodeConfiguration);

	protected void shutdown() {
		mImpl = null;
		mBinder.setDelegate(null);
	}

	protected final void relaunch(NodeConfiguration defaultNodeConfiguration) {
		ViewGroup oldImplUiLayer = null;
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			mRelaunching = true;
			updateMetaUiLayer();

			final boolean wasCreated = mCreated;
			final boolean wasStarted = mStarted;

			synchronized (mRelaunchLock) {
				shutdown();

				oldImplUiLayer = mImplUiLayer;
				restoreBeforeCreateState();

				if (wasCreated) {
					create();
				}

				if (wasStarted && defaultNodeConfiguration != null) {
					start(defaultNodeConfiguration);
				}
			}
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			final ViewGroup fOldImplUiLayer = oldImplUiLayer;
			getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
				@Override
				public void run() {
					// Note: If errors occurred during creation, it's not guaranteed that the old ui layer has been
					// removed. To be sure, we do it here (once again).
					if (fOldImplUiLayer != null) {
						removeView(fOldImplUiLayer);
						fOldImplUiLayer.removeAllViews();
					}
				}
			});
			mRelaunching = false;
			callOnStateChangedListener(RemoCoStateChange.RELAUNCHED);
			updateMetaUiLayer();
		}
	}

	@Override
	public void updateMetaUiLayer() {
		mMetaUiLayer.updateState();
	}

	protected void setCreated() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mImplCreated);
		}
		mCreated = true;
		callOnStateChangedListener(RemoCoStateChange.CREATED);
	}

	protected void setStarted() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mImplCreated);
			Preconditions.checkState(mCreated);
		}
		mStarted = true;
		callOnStateChangedListener(RemoCoStateChange.STARTED);
	}

	protected void setShutdown() {
		mShutdown = true;
		callOnStateChangedListener(RemoCoStateChange.SHUTDOWN);
	}

	protected IRemoCoImpl getImpl() {
		if (!mImplCreated) {
			throw new RemoCoImplNotCreatedError();
		}
		return mImpl;
	}

	@NotNull
	protected abstract Pair<IRemoCoImpl, IRcaBinder> createNewImpl() throws RemoCoTypeNotFoundException, RcaPluginCouldNotBeLoadedException;

	@NotNull
	protected abstract Pair<IRemoCoImpl, IRcaBinder> createNewDummyImpl() throws RcaPluginCouldNotBeLoadedException;

	@Override
	public abstract String getIdName();

	@NotNull
	@Override
	public NameResolver getNameResolver() {
		if (mNameResolver == null) {
			mNameResolver = getRcaContext().getRootNameResolver().newChild(getIdName());
		}
		return mNameResolver;
	}

	public final void setType(String type) {
		if (BuildConfig.DEBUG) {
			checkImplNotCreated();
		}
		mType = type;
	}

	@Override
	public final String getType() {
		return mType;
	}

	public final void setApk(String apk) {
		if (BuildConfig.DEBUG) {
			checkImplNotCreated();
		}
		mApk = apk;
	}

	@Override
	public final String getApk() {
		return mApk;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<GraphName, Object> getParams() {
		return (Map<GraphName, Object>) new HashMap<GraphName, Object>(mParams).clone();
	}

	@Override
	public Map<GraphName, Object> getParams(String namespace) {
		return namespace != null ? getParams(GraphName.of(namespace)) : getParams();
	}

	@Override
	public Map<GraphName, Object> getParams(GraphName namespace) {
		if (namespace == null) {
			return getParams();
		}
		String resolvedNamespace = getNameResolver().resolve(namespace).toString();
		if (!resolvedNamespace.endsWith("/")) {
			resolvedNamespace += "/";
		}
		final Map<GraphName, Object> result = new HashMap<GraphName, Object>();
		for (Map.Entry<GraphName, Object> entry : mParams.entrySet()) {
			if (entry.getKey().toString().startsWith(resolvedNamespace)) {
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}

	@Override
	public Object getParam(GraphName name) {
		return mParams.get(getNameResolver().resolve(name));
	}

	@NotNull
	@Override
	public final String getImplDoc() {
		return mImpl != null ? mImpl.getDoc() : "";
	}

	@NotNull
	@Override
	public IRcaLog getLog() {
		return mLog;
	}

	@Override
	public final ViewGroup getImplUiLayer() {
		return mImplUiLayer;
	}

	protected final RemoCoMetaUiLayer getMetaUiLayer() {
		return mMetaUiLayer;
	}

	@Override
	public final IRcaBinder getBinder() {
		return mBinder;
	}

	@Override
	public final void addListener(IRcaEventListener listener) {
		mEventDispatcher.addListener(listener);
	}

	@Override
	public final boolean removeListener(IRcaEventListener listener) {
		return mEventDispatcher.removeListener(listener);
	}

	@Override
	public final void dispatchEvent(String name, Object data) {
		mEventDispatcher.dispatch(name, data);
	}

	@Override
	public final void handleErrorException(Throwable e) {
		mMetaUiLayer.handleErrorException(e);
		getRcaContext().onHandleRemoCoErrorException(this, e);
		callOnStateChangedListener(RemoCoStateChange.EXCEPTIONS_STATE_CHANGED);
	}

	@Override
	public final void handleWarningException(Throwable e) {
		mMetaUiLayer.handleWarningException(e);
		callOnStateChangedListener(RemoCoStateChange.EXCEPTIONS_STATE_CHANGED);
	}

	@Override
	public final boolean cancelException(Throwable e) {
		boolean result = mMetaUiLayer.cancelException(e);
		callOnStateChangedListener(RemoCoStateChange.EXCEPTIONS_STATE_CHANGED);
		return result;
	}

	@Override
	public final void cancelAllExceptions() {
		mMetaUiLayer.cancelAllExceptions();
		callOnStateChangedListener(RemoCoStateChange.EXCEPTIONS_STATE_CHANGED);
	}

	@Override
	public final boolean containsException(Throwable exception) {
		return mMetaUiLayer.containsException(exception);
	}

	@Override
	public final boolean isInErrorState() {
		return mMetaUiLayer.isInErrorState();
	}

	@Override
	public final boolean isInWarningState() {
		return mMetaUiLayer.isInWarningState();
	}

	@Override
	public boolean isRelaunching() {
		return mRelaunching;
	}

	@Override
	public boolean isRebootRemoConOnRelaunchEnabled() {
		return mImpl != null && mImpl.isRebootRemoConOnRelaunchEnabled();
	}

	@Override
	public boolean isOnImplThread() {
		return mImpl != null && mImpl.isOnImplThread();
	}

	public IRemoCoConfigRetriever retrieveRemoCoConfig() {
		return getRcaContext().getRemoConConfig().retrieveRemoCoConfig(new RcaId(getIdName()));
	}

	private void createImpl(boolean forceDummyCreation) {
		if (forceDummyCreation) {
			mImplCreated = false;
			mImpl = null;
		}
		Pair<IRemoCoImpl, IRcaBinder> result = null;
		try {
			if (getType() == null) {
				// No type is specified, neither in the layout nor in the config file. Fallback to the own id.
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(getIdName().equals(retrieveRemoCoConfig().retrieveType().toString()));
				}
				setType(getIdName());
			}

			if (BuildConfig.DEBUG) {
				checkImplNotCreated();
				String type = getType();
				String apk = getApk();

				assert type != null;
				assert apk != null;
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(!type.isEmpty());
					Preconditions.checkState(!apk.isEmpty());
					Preconditions.checkState(mImpl == null);
				}

			}

			if (!forceDummyCreation) {
				result = createNewImpl();
			}
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			if (result == null) {
				// This can be only the case when an exception was thrown. Create a dummy implementation.
				getLog().w("creating the remo-co implementation failed, creating a dummy implementation...");
				try {
					result = createNewDummyImpl();
				} catch (RcaPluginCouldNotBeLoadedException e) {
					handleErrorException(new RcaUnexpectedError("Dummy RemoCo could not be created.", e));
				}
			}
			if (result != null) {
				assert result.first != null;
				mImpl = result.first;
				mBinder.setDelegate(result.second);
				mImplCreated = true;
			} else {
				// Handling unexpected state...
				mImpl = null;
				mBinder.setDelegate(null);
				mImplCreated = true;
			}
		}
	}

	private void initializeType() {
		final IRemoCoConfigRetriever config = retrieveRemoCoConfig();
		setType(config.retrieveType().toString());
		setApk(config.retrieveApk());
	}

	private void initializeParams() {
		mParams.putAll(retrieveRemoCoConfig().retrieveParams());
	}

	@NotNull
	protected final NodeConfiguration setupRemoCoNodeConfiguration(@NotNull NodeConfiguration nodeConfiguration) {
		final NameResolver parentResolver = NameResolver.newFromNamespaceAndRemappings(
				getRcaContext().getSettings().getDmRosNamespace(), retrieveRemoCoConfig().retrieveRemappings());

		nodeConfiguration.setParentResolver(parentResolver.newChild(getIdName()));
		nodeConfiguration.setNodeName("node");

		return nodeConfiguration;
	}

	protected final IRcaContext getRcaContext() {
		IRcaContext rcaContext = (IRcaContext) getContext();
		assert rcaContext != null;
		return rcaContext;
	}

	protected final void checkNotOnUiThread() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(!getRcaContext().isOnUiThread(), "Not allowed to run on UI thread!");
		}
	}

	private void checkImplNotCreated() {
		if (BuildConfig.DEBUG) {
			String idName = "<unknown>";
			try {
				idName = getIdName();
			} catch (Throwable ignored) {
			}
			Preconditions.checkState(!mImplCreated,
					"Attempt to recreate an already created underlying implementation of RemoCo '" + idName + "'.");
		}
	}

	private void initializeBeforeCreateState() {
		if (!mBeforeFirstCreateStateInitialized) {
			mBeforeFirstCreateType = mType;
			mBeforeFirstCreateApk = mApk;
			mBeforeFirstCreateStateInitialized = true;
		}
	}

	private void restoreBeforeCreateState() {
		// Note: Don't remove {@code mImplUiLayer} here to avoid flickering. It will be done later (if at all), when a
		// new instance is created during relaunching.
		mType = mBeforeFirstCreateType;
		mApk = mBeforeFirstCreateApk;

		mImplUiLayer = null;
		mImpl = null;
		mBinder.setDelegate(null);

		mImplCreated = false;
		mCreated = false;
		mStarted = false;
		mShutdown = false;
		mParams.clear();

		cancelAllExceptions();
	}

	private void callOnStateChangedListener(final RemoCoStateChange stateChange) {
		// Note: To avoid deadlocks, we call the listener (if defined) on a new thread.
		if (mOnStateChangedListener != null) {
			new Thread() {
				@Override
				public void run() {
					try {
						// Note: {@code mOnStateChangedListener} may change from other threads.
						final IOnRemoCoStateChangedListener listener = mOnStateChangedListener;
						if (listener != null) {
							listener.onStateChanged(ARemoCo.this, stateChange);
						}
					} catch (Throwable e) {
						handleErrorException(e);
					}
				}
			}.start();
		}
	}

	private <T extends IRemoCoLogEntry> void callOnNewLogEntryListener(final T entry) {
		// Note: To avoid deadlocks, we call the listener (if defined) on a new thread.
		if (mOnNewLogEntryListener != null) {
			new Thread() {
				@Override
				public void run() {
					try {
						// Note: {@code mOnNewLogEntryListener} may change from other threads.
						final IOnNewRemoCoLogEntryListener listener = mOnNewLogEntryListener;
						if (listener != null) {
							listener.onNewLogEntry(ARemoCo.this, entry);
						}
					} catch (Throwable e) {
						handleErrorException(e);
					}
				}
			}.start();
		}
	}
}
