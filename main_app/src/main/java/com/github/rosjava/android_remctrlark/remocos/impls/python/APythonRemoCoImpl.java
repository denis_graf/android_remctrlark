/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IPyRemoCoFileNamesManager;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.binders.IRcaScriptBinder;
import com.github.rosjava.android_remctrlark.binders.RcaScriptTwoWaysBinder;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.error_handling.LayoutException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoTypeNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaViewEventListener;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.ARemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.Layout;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFieldsToJsonMappable;
import com.github.rosjava.android_remctrlark.sl4a.IScriptRemoCo;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.ros.namespace.GraphName;
import org.ros.node.NodeConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

abstract class APythonRemoCoImpl extends ARemoCoImpl implements IRemoCoImpl {

	@SuppressWarnings("UnusedDeclaration")
	class InternalScriptBinder implements IReflectingBinderImpl {

		public String getNamespace() {
			return getRemoCo().getNameResolver().getNamespace().toString();
		}

		public Map<String, Map<String, String>> layoutQuery() {
			return mLayout.getViewAsMap();
		}

		public Map<String, String> layoutQueryDetail(String viewId) throws LayoutException {
			return mLayout.getViewDetail(viewId);
		}

		public void onScriptInitialized() {
			getRemoCo().getLog().i("script initialized");
			mScriptInitializedLatch.countDown();
		}

		public PyNodeConfiguration createNodeConfiguration(String nodeName) {
			return new PyNodeConfiguration(createDefaultNodeConfiguration(nodeName));
		}

		public void layoutSetProperty(String viewId, String property, Object value) throws LayoutException {
			mLayout.handlerSetViewProperty(viewId, property, value);
		}

		public Object layoutGetProperty(String viewId, String property) throws LayoutException {
			return mLayout.getViewProperty(viewId, property);
		}

		public void layoutSetList(String viewId, JSONArray items) throws LayoutException {
			mLayout.handlerSetViewList(viewId, items);
		}

		public void layoutSetOnCustomEventListener(String viewId, Boolean enable) throws LayoutException {
			mLayout.setOnCustomEventListener(viewId, enable ? mCustomEventListener : null);
		}

		public void layoutSetOnClickListener(String viewId, Boolean enable) throws LayoutException {
			mLayout.setOnClickListener(viewId, enable ? mClickListener : null);
		}

		public void layoutSetOnItemClickListener(String viewId, Boolean enable) throws LayoutException {
			mLayout.setOnItemClickListener(viewId, enable ? mItemClickListener : null);
		}

		public void layoutSetOnTouchListener(JSONArray actions, String viewId, Boolean enable) throws LayoutException, JSONException {
			List<Integer> castedActions = new ArrayList<Integer>(actions.length());
			for (int i = 0; i < actions.length(); i++) {
				int action = (int) actions.getLong(i);
				if (!mTouchListener.isActionSupported(action)) {
					throw new LayoutException(new RcaException("Unsupported touch action: " + Integer.toString(action) + "."));
				}
				castedActions.add(action);
			}
			mTouchListener.setActions(castedActions);
			mLayout.setOnTouchListener(viewId, enable ? mTouchListener : null);
		}

		public Object layoutCallMethod(final String viewId, final String methodName, final JSONArray args) throws LayoutException {
			// Note: We can't call the method on the impl thread here, since we would end up in a deadlock.
			return mLayout.callMethod(viewId, methodName, args);
		}

		public Object layoutCallMethodOnUiThread(String viewId, String methodName, JSONArray args) throws LayoutException {
			return mLayout.handlerCallMethod(viewId, methodName, args);
		}

		public void layoutCreateMain() throws LayoutException {
			ViewGroup remoCoRootView = getRemoCo().getImplUiLayer();
			mLayout.handlerLoadLayoutFromFile(getApkContext(), remoCoRootView, getFileNamesManager().getMainLayoutFullName());
		}

		public void layoutCreateFromXmlString(String xml) throws LayoutException {
			ViewGroup remoCoRootView = getRemoCo().getImplUiLayer();
			mLayout.handlerLoadLayoutFromXmlString(getApkContext(), remoCoRootView, xml);
		}

		public IRcaFieldsToJsonMappable startLoadingTask(String msg) {
			final RemoCoLoadingTask task = APythonRemoCoImpl.this.startRemoCoLoadingTask(msg);
			final int taskId = task != null ? task.getId() : 0;
			return new IRcaFieldsToJsonMappable() {
				public final int task_id = taskId;
			};
		}

		public void RemoCoLoadingTask_done(Integer taskId) {
			final RemoCoLoadingTask task = getRemoCoLoadingTaskById(taskId);
			if (task != null) {
				task.done();
			}
		}

		public Map<String, Object> getParams(String namespace) {
			final Map<String, Object> result = new HashMap<String, Object>();
			for (Map.Entry<GraphName, Object> entry : getRemoCo().getParams(namespace).entrySet()) {
				result.put(entry.getKey().toString(), entry.getValue());
			}
			return result;
		}

		public Object getParam(String name) {
			return getRemoCo().getParam(GraphName.of(name));
		}

		public void requestRemoConRebootOnRelaunch(Boolean enable) {
			mRebootRemoConOnRelaunch = enable;
		}

		public boolean isOnShutdown() {
			return getRemoCo().isShutdown();
		}

		public boolean isOnRelaunch() {
			return getRemoCo().isRelaunching();
		}

		public Object getRemoConSessionState() {
			return getRemoCo().getSessionState();
		}

		public void setRemoConSessionState(Object state) {
			getRemoCo().setSessionState(state);
		}
	}

	private class ScriptRemoCo implements IScriptRemoCo {
		private final CountDownLatch mScriptShutdownCompleteLatch = new CountDownLatch(1);

		public CountDownLatch getBoundLatch() {
			return mScriptInitializedLatch;
		}

		public CountDownLatch getShutdownCompleteLatch() {
			return mScriptShutdownCompleteLatch;
		}

		@Override
		public IRemoCo getRemoCo() {
			return APythonRemoCoImpl.this.getRemoCo();
		}

		@Override
		public IRcaScriptBinder getScriptBinder() {
			return mBinder;
		}

		@Override
		public synchronized void onScriptBound() {
			getRemoCo().getLog().i("script bound");
		}

		@Override
		public synchronized void onScriptShutdown() {
			if (!getRemoCo().isShutdown() && !getRemoCo().isInErrorState() && !getRemoCo().isRelaunching()) {
				getRemoCo().getLog().w("script is shutting down unexpectedly...");
				handleScriptError(new RcaException("unexpected script shutdown"));
			} else {
				getRemoCo().getLog().i("script is shutting down...");
			}
			mScriptInitializedLatch.countDown();
		}

		@Override
		public synchronized void onScriptShutdownComplete() {
			getRemoCo().getLog().i("script shutdown complete");
			releaseAllLatches();
		}

		@Override
		public void onScriptExitReached() {
			getRemoCo().getLog().i("script exit reached");
			releaseAllLatches();
		}

		@Override
		public synchronized void handleScriptError(Throwable e) {
			getRemoCo().getLog().i("handling script error...");
			getRemoCo().handleErrorException(e);
			releaseAllLatches();
		}

		@Override
		public void onUnprocessedScriptProcessMessage(@NotNull String message) {
			getRemoCo().getLog().w("ignoring unknown script process message: '" + message + "'");
		}

		private void releaseAllLatches() {
			mScriptInitializedLatch.countDown();
			mScriptShutdownCompleteLatch.countDown();
		}
	}

	private String mDoc = "";
	private final CountDownLatch mScriptInitializedLatch = new CountDownLatch(1);
	private final ScriptRemoCo mScriptRemoCo = new ScriptRemoCo();

	private class TouchListener implements View.OnTouchListener {
		private List<Integer> mActions = null;

		public void setActions(List<Integer> actions) {
			mActions = actions;
		}

		public boolean isActionSupported(int action) {
			return action == MotionEvent.ACTION_DOWN ||
					action == MotionEvent.ACTION_UP ||
					action == MotionEvent.ACTION_CANCEL;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			assert mActions != null;
			final int action = event.getActionMasked();

			if (mActions.contains(action)) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(isActionSupported(action));
				}
				Set<Map.Entry<String, String>> entries = getViewInfoForPython(v).entrySet();
				final Map<String, Object> data = new HashMap<String, Object>(entries.size() + 1);
				for (Map.Entry<String, String> entry : entries) {
					data.put(entry.getKey(), entry.getValue());
				}
				data.put("action", action);

				// Note: The main thread must always be free when calling Python methods, to avoid deadlocks caused by
				// some from Python called Java routines, which require being executed on the main thread.
				postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() throws RcaBinderInvocationException {
						rpcRemoCoHandleLayoutEvent("touch", data);
					}
				});
			}

			return false;
		}
	}

	private final TouchListener mTouchListener = new TouchListener();

	private final IRcaViewEventListener mCustomEventListener = new IRcaViewEventListener() {
		@Override
		public void onCustomEvent(View view, String name, Object data) {
			final Map<String, Object> custom = new HashMap<String, Object>();
			custom.put("name", name);
			custom.put("data", data);

			final Map<String, Object> eventData = new HashMap<String, Object>();
			eventData.putAll(getViewInfoForPython(view));
			eventData.put("custom", custom);

			// Note: The main thread must always be free when calling Python methods, to avoid deadlocks caused by
			// some from Python called Java routines, which require being executed on the main thread.
			postToImplThread(new ICheckedRunnable() {
				@Override
				public void run() throws RcaBinderInvocationException {
					rpcRemoCoHandleLayoutEvent("custom", eventData);
				}
			});
		}
	};

	private final View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			final Map<String, String> data = getViewInfoForPython(view);

			// Note: The main thread must always be free when calling Python methods, to avoid deadlocks caused by
			// some from Python called Java routines, which require being executed on the main thread.
			postToImplThread(new ICheckedRunnable() {
				@Override
				public void run() throws RcaBinderInvocationException {
					rpcRemoCoHandleLayoutEvent("click", data);
				}
			});
		}
	};

	private final AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> aview, View aitem, int position, long id) {
			final Map<String, String> data = getViewInfoForPython(aview);
			data.put("position", String.valueOf(position));

			// Note: The main thread must always be free when calling Python methods, to avoid deadlocks caused by
			// some from Python called Java routines, which require being executed on the main thread.
			postToImplThread(new ICheckedRunnable() {
				@Override
				public void run() throws RcaBinderInvocationException {
					rpcRemoCoHandleLayoutEvent("itemclick", data);
				}
			});
		}
	};

	private final RcaScriptTwoWaysBinder mBinder = new RcaScriptTwoWaysBinder(getIdName(), new ReflectingBinder(createInternalScriptBinderImpl())) {
		@Override
		protected Object invokeRpcHandlingMethod(String name, Object[] argsObj, JSONArray argsJson) throws RcaBinderInvocationException {
			return super.invokeRpcHandlingMethod(name, argsObj, argsJson);
		}

		@Override
		protected Object invokeScriptMethod(final String name, final Object[] argsObj, final JSONArray argsJson) throws RcaBinderInvocationException {
			final Object vResult[] = new Object[1];
			final RcaBinderInvocationException vException[] = new RcaBinderInvocationException[1];
			syncRunOnImplThread(new ICheckedRunnable() {
				@Override
				public void run() {
					try {
						vResult[0] = superInvokeMethod(name, argsObj, argsJson);
					} catch (RcaBinderInvocationException e) {
						vException[0] = e;
					}
				}
			});
			if (vException[0] != null) {
				throw new RcaBinderInvocationException(name, vException[0]);
			}
			return vResult[0];
		}
	};

	private final Layout mLayout = new Layout(getApkViewFactory());
	private final IPyRemoCoFileNamesManager mFileNamesManager;

	public APythonRemoCoImpl(IRcaApplication app, String idName, String type, String apk) throws RcaPluginCouldNotBeLoadedException, RemoCoTypeNotFoundException {
		super(app, idName, type, apk);
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument(!type.isEmpty());
		}
		mFileNamesManager = app.getFileNamesManager().getPyRemoCoFileNamesManager(type, apk);
		if (mFileNamesManager == null) {
			throw new RemoCoTypeNotFoundException(type, apk);
		}
	}

	protected IScriptRemoCo getAsScriptRemoCo() {
		return mScriptRemoCo;
	}

	protected Layout getLayout() {
		return mLayout;
	}

	protected abstract IReflectingBinderImpl createInternalScriptBinderImpl();

	protected final IPyRemoCoFileNamesManager getFileNamesManager() {
		return mFileNamesManager;
	}

	protected final IRcaScriptBinder getBinder() {
		return mBinder;
	}

	protected Object rpcInvokeInternalMethod(String name, Object... args) throws RcaBinderInvocationException {
		return mBinder.invokeInternalMethod(name, args);
	}

	private String rpcGetRemoCoDoc() throws RcaBinderInvocationException {
		return (String) rpcInvokeInternalMethod("get_remoco_doc");
	}

	private void rpcRemoCoStart() throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remoco_start");
	}

	private void rpcRemoCoShutdown() throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remoco_shutdown");
	}

	private void rpcRemoCoCreateUI() throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remoco_create_ui");
	}

	private void rpcRemoCoHandleLayoutEvent(String name, Object data) throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remoco_handle_layout_event", name, data);
	}

	@Override
	protected void create(@NotNull IRemoCo remoCo) {
		super.create(remoCo);
		getRemoCo().getLog().i("sending request for starting script...");
		getRcaContext().getPyMainScript().startRemoCoScript(getAsScriptRemoCo());
		try {
			mScriptRemoCo.getBoundLatch().await();
		} catch (InterruptedException e) {
			getRemoCo().getLog().w("interrupted on starting script", e);
			Thread.currentThread().interrupt();
		}
		try {
			mDoc = rpcGetRemoCoDoc();
		} catch (Throwable e) {
			remoCo.handleErrorException(new RcaException("Could not retrieve RemoCo documentation.", e));
		}
	}

	@NotNull
	@Override
	public String getDoc() {
		return mDoc != null ? mDoc : "";
	}

	@Override
	public void start(final NodeConfiguration nodeConfiguration) {
		super.start(nodeConfiguration);
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws RcaBinderInvocationException {
				rpcRemoCoStart();
			}
		});
	}

	@Override
	public void shutdown() {
		mLayout.setListeners(null, null, null, null);
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws RcaBinderInvocationException {
				rpcRemoCoShutdown();
				try {
					mScriptRemoCo.getShutdownCompleteLatch().await();
				} catch (InterruptedException e) {
					getRemoCo().getLog().w(e);
					Thread.currentThread().interrupt();
				}
			}
		});

		shutdownImplThread();
	}

	@Override
	public void createUI() {
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws RcaBinderInvocationException {
				rpcRemoCoCreateUI();
			}
		});
	}

	private Map<String, String> getViewInfoForPython(View view) {
		return mLayout.getInflater().getViewInfo(view);
	}
}
