/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;

import org.jetbrains.annotations.NotNull;

abstract class ARemoCoInspectorActivity extends Activity implements RemoCoInspectorFragment.Callbacks {

	public static final String EXTRA_REMOCO_ID = RemoCoInspectorFragment.ARG_REMOCO_ID;
	public static final String EXTRA_SHOW_TAB = RemoCoInspectorFragment.ARG_SHOW_TAB;

	protected static final int REQUEST_CODE_DETAIL_ACTIVITY = 2;

	protected String mRemoCoId = null;
	protected String mShowTab = null;
	private boolean mFinishInNotTwoPane = false;

	public abstract void selectRemoCo(String remoCoId);

	@NotNull
	protected RemoCosInspectorFragment getRemoCosInspectorFragment() {
		final RemoCosInspectorFragment listFragment =
				(RemoCosInspectorFragment) getFragmentManager().findFragmentById(R.id.remocos_inspector_list);
		assert listFragment != null;
		return listFragment;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mRemoCoId = getIntent().getStringExtra(EXTRA_REMOCO_ID);
		if (mRemoCoId == null) {
			mRemoCoId = RcaService.getContext().getRemoCon().getIdName();
		}
		assert mRemoCoId != null;

		mShowTab = getIntent().getStringExtra(EXTRA_SHOW_TAB);

		mFinishInNotTwoPane = mShowTab != null;

		updateSubtitle();
		updateResult();
	}

	@Override
	public void onDetailTabSelected(String tab) {
		mShowTab = tab;
		updateResult();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_DETAIL_ACTIVITY) {
			if (resultCode == RESULT_OK) {
				if (mFinishInNotTwoPane) {
					finish();
				} else {
					mShowTab = data.getStringExtra(RemoCoInspectorActivity.EXTRA_SHOW_TAB);
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.remoco_inspector_actionbar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.close:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected final void startDetailsActivity(String remoCoId, Intent detailIntent) {
		detailIntent.putExtra(RemoCoInspectorActivity.EXTRA_REMOCO_ID, remoCoId);
		detailIntent.putExtra(RemoCoInspectorActivity.EXTRA_SHOW_TAB, mShowTab);
		startActivityForResult(detailIntent, REQUEST_CODE_DETAIL_ACTIVITY);
	}

	protected final void updateSubtitle() {
		final String subtitle;
		if (mRemoCoId != null) {
			final IRemoCo remoCo = RcaService.getContext().findRemoCo(mRemoCoId, true);
			subtitle = (remoCo instanceof IRemoCon ? "RemoCon: " : "RemoComp: ") + mRemoCoId;
		} else {
			subtitle = null;
		}
		assert getActionBar() != null;
		getActionBar().setSubtitle(subtitle);
	}

	private void updateResult() {
		final Intent intent = new Intent();
		intent.putExtra(EXTRA_SHOW_TAB, mShowTab);
		setResult(RESULT_OK, intent);
	}
}
