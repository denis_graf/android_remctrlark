/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.retriever;

import android.util.Pair;

import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoNamespace;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConSpecialization;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.RcaId;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RemoConConfigRetriever extends ARemoCoConfigRetriever implements ISerializeableRemoConConfigRetriever {

	private class MutablesConfig extends RemoConMutablesConfig {
		@Override
		public void setAllToInitialState() {
			super.setAllToInitialState();
			applyCommit(new RemoConMutablesConfigCommit(mInitialMutablesConfig, false));
		}
	}

	private static class ResolvedRemoCoType implements IRemoCoType {
		private final IRcaId mTId;
		private final IRcaId mType;
		private final String mApk;
		private final boolean mMutable;

		public ResolvedRemoCoType(ARemoCoConfigRetriever retriever, IRcaId tId, boolean mutable) {
			mTId = tId;
			mType = retriever.retrieveType(tId);
			mApk = retriever.retrieveApk(tId);
			mMutable = mutable;
		}

		@Override
		public IRcaId getId() {
			return mTId;
		}

		@Override
		public IRcaId getType() {
			return mType;
		}

		@Override
		public String getApk() {
			return mApk;
		}

		@Override
		public boolean isMutable() {
			return mMutable;
		}
	}

	private static class ResolvedRemoCoNamespace implements IRemoCoNamespace {
		private final IRcaId mNsId;
		private final GraphName mNs;
		private final boolean mMutable;

		public ResolvedRemoCoNamespace(ARemoCoConfigRetriever retriever, IRcaId nsId, boolean mutable) {
			mNsId = nsId;
			mNs = retriever.retrieveResolvedNamespace(mNsId);
			mMutable = mutable;
		}

		@Override
		public IRcaId getId() {
			return mNsId;
		}

		@Override
		public GraphName getNamespace() {
			return mNs;
		}

		@Override
		public boolean isMutable() {
			return mMutable;
		}
	}

	private RemoConMutablesConfig mInitialMutablesConfig = new RemoConMutablesConfig();
	private MutablesConfig mMutablesConfig = new MutablesConfig();

	/**
	 * Needed for serialization/deserialization.
	 */
	protected RemoConConfigRetriever() {
	}

	public RemoConConfigRetriever(@NotNull IRemoConConfig config, @NotNull IRcaId id, NameResolver rootNameResolver) {
		super(config, id, rootNameResolver);
	}

	public RemoConConfigRetriever(@NotNull IRemoConConfig config, @NotNull NameResolver rootNameResolver) {
		super(config, config.getId(), rootNameResolver);
	}

	@NotNull
	@Override
	public IRemoConConfig getConfig() {
		return super.getRemoConConfig();
	}

	@NotNull
	@Override
	public String getConfigFileFullDir() {
		return getRemoConConfig().getConfigFileFullDir();
	}

	@NotNull
	@Override
	public String getBaseConfApk() {
		return getRemoConConfig().getBaseConfApk();
	}

	@NotNull
	@Override
	public Collection<IRemoCoNamespace> retrieveResolvedConfigNamespaces() {
		final Map<IRcaId, IRemoCoNamespace> map = new HashMap<IRcaId, IRemoCoNamespace>();
		for (IRemoCoNamespace namespace : getRemoConConfig().getGlobals().getNamespaces().values()) {
			map.put(namespace.getId(), new ResolvedRemoCoNamespace(this, namespace.getId(), namespace.isMutable()));
		}
		for (Map.Entry<IRcaId, GraphName> entry : getMutablesConfig().getNamespaces().entrySet()) {
			final IRcaId nsId = entry.getKey();
			map.put(nsId, new ResolvedRemoCoNamespace(this, nsId, true));
		}
		return map.values();
	}

	@NotNull
	@Override
	public Collection<IRemoCoType> retrieveResolvedConfigTypes() {
		final Map<IRcaId, IRemoCoType> map = new HashMap<IRcaId, IRemoCoType>();
		for (IRemoCoType type : getRemoConConfig().getGlobals().getTypes().values()) {
			map.put(type.getId(), new ResolvedRemoCoType(this, type.getId(), type.isMutable()));
		}
		for (Map.Entry<IRcaId, Pair<IRcaId, String>> entry : getMutablesConfig().getTypes().entrySet()) {
			final IRcaId tId = entry.getKey();
			map.put(tId, new ResolvedRemoCoType(this, tId, true));
		}
		return map.values();
	}

	@Override
	public boolean retrieveRemoConRebootOnRelaunchEnabled(@NotNull IRemoCo remoCo) {
		final Boolean enabled = getConfig().getSettings().getRebootOnTypeMap().get(
				new Pair<IRcaId, String>(new RcaId(remoCo.getType()), remoCo.getApk()));
		return enabled != null ? enabled : remoCo.isRebootRemoConOnRelaunchEnabled();
	}

	@NotNull
	@Override
	public final IRcaId getId() {
		return getRemoConConfig().getId();
	}

	@NotNull
	@Override
	public IRemoCompConfigRetriever retrieveRemoCompConfig(@NotNull IRcaId id) {
		return new RemoCompConfigRetriever(this, id);
	}

	@Override
	public boolean retrieveRemoCosToRelaunch(@NotNull List<IRcaId> allRemoCos, @NotNull IRemoConMutablesConfigCommit commit, @NotNull List<IRcaId> remoCosToRelaunch) {
		final RemoConConfigRetriever newRemoConConfig = copy();
		newRemoConConfig.applyMutablesConfigCommit(commit);

		for (IRcaId remoCo : allRemoCos) {
			if (needsToRelaunch(remoCo, newRemoConConfig)) {
				remoCosToRelaunch.add(remoCo);
			}
		}

		return isMasterChanged(this, newRemoConConfig);
	}

	@Override
	public synchronized void applyMutablesConfigCommit(@NotNull IRemoConMutablesConfigCommit commit) {
		mMutablesConfig.applyCommit(commit);
	}

	@NotNull
	@Override
	public final IRcaId retrieveTypeId() {
		final IRcaId typeId = getRemoConConfig().getTypeId();
		return typeId != null ? typeId : getId();
	}

	@NotNull
	public final IRemoCoConfigRetriever retrieveRemoCoConfig(@NotNull IRcaId remoCoId) {
		return getId().equals(remoCoId) ? this : retrieveRemoCompConfig(remoCoId);
	}

	@Override
	public void setCurrentStateAsInitial() {
		mInitialMutablesConfig.applyCommit(new RemoConMutablesConfigCommit(mMutablesConfig, true));
	}

	@Override
	protected final List<IRemoConSpecialization> retrieveSpecializations() {
		final List<IRemoConSpecialization> specs = new LinkedList<IRemoConSpecialization>();
		for (IRemoConSpecialization spec : getRemoConConfig().getSpecializations()) {
			if (conditionMatch(spec.getCondition())) {
				specs.add(spec);
			}
		}
		return specs;
	}

	@NotNull
	@Override
	protected IRemoConMutablesConfig getMutablesConfig() {
		return mMutablesConfig;
	}

	private boolean needsToRelaunch(@NotNull IRcaId remoCo, @NotNull RemoConConfigRetriever newRemoConConfig) {
		final IRemoCoConfigRetriever remoCoConfig = retrieveRemoCoConfig(remoCo);
		final IRemoCoConfigRetriever newRemoCoConfig = newRemoConConfig.retrieveRemoCoConfig(remoCo);

		return isMasterChanged(remoCoConfig, newRemoCoConfig) ||
				!remoCoConfig.retrieveType().equals(newRemoCoConfig.retrieveType()) ||
				!remoCoConfig.retrieveApk().equals(newRemoCoConfig.retrieveApk()) ||
				!remoCoConfig.retrieveParams().equals(newRemoCoConfig.retrieveParams()) ||
				!remoCoConfig.retrieveRemappings().equals(newRemoCoConfig.retrieveRemappings());
	}

	private boolean isMasterChanged(@NotNull IRemoCoConfigRetriever remoCoConfig, @NotNull IRemoCoConfigRetriever newRemoCoConfig) {
		final boolean remoConStarted = RcaService.getContext().isRemoConStarted();
		return (remoConStarted && !remoCoConfig.retrieveNewMaster().equals(newRemoCoConfig.retrieveNewMaster())) ||
				(remoConStarted && !remoCoConfig.retrieveMasterUri().equals(newRemoCoConfig.retrieveMasterUri()));
	}

	private RemoConConfigRetriever copy() {
		final RemoConConfigRetriever copy = new RemoConConfigRetriever(getRemoConConfig(), getRootNameResolver());
		copy.applyMutablesConfigCommit(new RemoConMutablesConfigCommit(mInitialMutablesConfig, true));
		copy.setCurrentStateAsInitial();
		copy.applyMutablesConfigCommit(new RemoConMutablesConfigCommit(mMutablesConfig, false));
		return copy;
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(mInitialMutablesConfig);
		out.writeObject(mMutablesConfig);
	}

	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		mInitialMutablesConfig = (RemoConMutablesConfig) in.readObject();
		mMutablesConfig = (MutablesConfig) in.readObject();
	}
}
