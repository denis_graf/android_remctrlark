/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.RemoCompAnchor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Motivation and original idea from:
 * http://justanapplication.wordpress.com/2011/12/18/two-android-applications-a-shared-ui-element-and-a-shared-process-a-tale-of-classloaders-and-confusion/
 */
public abstract class ARcaPluginsLayoutInflaterFactory implements IRcaLayoutInflaterFactory {

	// TODO: #61: optimize loading classes -> implement some caching

	private static final String[] mCustomViewClassesPrefixes = new String[]{
			"org.ros.",
	};

	private final Context mMainAppContext;
	private final Context mPluginApkContext;
	private final ClassLoader mClassLoader;

	public ARcaPluginsLayoutInflaterFactory(Context pluginApkContext, ClassLoader classLoader) {
		mMainAppContext = RcaService.getContext().getActivityContext();
		mPluginApkContext = pluginApkContext;
		mClassLoader = classLoader;
	}

	@Override
	public ClassLoader getRcaPluginClassLoader() {
		return mClassLoader;
	}

	@Override
	public View onCreateView(String name, Context context, int defStyle) {
		return onCreateView(name, context, null, defStyle);
	}

	@Override
	public View onCreateView(String name, Context context) {
		return onCreateView(name, context, null, 0);
	}

	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		return onCreateView(name, context, attrs, 0);
	}

	protected View onCreateView(String name, Context context, AttributeSet attrs, int defStyle) {
		View result;
		try {
			result = createRcaViewFromCurrentPlugin(name, attrs, defStyle);
			if (result == null) {
				result = createCustomViewFromCurrentPluginOrMainApp(name, context, attrs, defStyle);
			}
		} catch (Throwable e) {
			result = onErrorCreatingCustomViewClass(name, e);
		}
		if (BuildConfig.DEBUG && result != null) {
			RcaService.getLog().d("custom view class '" + name + "' inflated from package '" +
					mPluginApkContext.getPackageName() + "' with context '" + context.getPackageName() + "'");
		}
		return result;
	}

	private View createRcaViewFromCurrentPlugin(String name, AttributeSet attrs, int defStyle) {
		if (name.equals(RemoCompAnchor.TAG)) {
			final RemoCompAnchor remoCompAnchor;
			if (defStyle != 0) {
				remoCompAnchor = attrs != null ? new RemoCompAnchor(mPluginApkContext, attrs, defStyle) : new RemoCompAnchor(mPluginApkContext, null, defStyle);
			} else {
				remoCompAnchor = attrs != null ? new RemoCompAnchor(mPluginApkContext, attrs) : new RemoCompAnchor(mPluginApkContext);
			}
			if (remoCompAnchor.getApk() == null) {
				remoCompAnchor.setApk(mPluginApkContext.getPackageName());
			}
			return remoCompAnchor;
		}
		return null;
	}

	private View createCustomViewFromCurrentPluginOrMainApp(String name, Context context, AttributeSet attrs, int defStyle) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		for (String prefix : mCustomViewClassesPrefixes) {
			if (name.startsWith(prefix)) {
				// Note: If the view is not in the package of the main app, then we use the context of the plugin.
				// Otherwise we use the context of the main app, so that view find the needed resources.
				// This should work in the most cases. A problem could arrive, when the plugin overwrites a class with
				// an equal name.
				return createViewInstanceTry(name, attrs, defStyle, context, mMainAppContext);
			}
		}

		if (name.startsWith(mPluginApkContext.getPackageName() + ".")) {
			return createViewInstanceTry(name, attrs, defStyle, context, mPluginApkContext);
		}

		return null;
	}

	private View createViewInstanceTry(String name, AttributeSet attrs, int defStyle, Context context, Context fallbackContext) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		final Class<? extends View> viewSubClass;
		try {
			viewSubClass = mClassLoader.loadClass(name).asSubclass(View.class);
		} catch (ClassNotFoundException ignored) {
			return null;
		}

		Context usedContext;
		try {
			assert context.getClassLoader() != null;
			context.getClassLoader().loadClass(name);
			usedContext = context;
		} catch (ClassNotFoundException e) {
			usedContext = fallbackContext;
		}

		View result = null;
		try {
			// Look for a custom static constructor for loading resources.
			/**
			 * @param actualContext The layout inflater's context.
			 * @param rcaPluginContext This view containing RCA plugin context for loading resources.
			 * @param rcaMainAppContext RCA main app's context for loading resources.
			 * @param attrs XML attributes, or {@code null} if inflating programmatically.
			 * @param defStyle Style resource id, or 0 if not set.
			 */
			final Method method = viewSubClass.getMethod("rcaInflate", Context.class, Context.class, Context.class, AttributeSet.class, int.class);
			result = (View) method.invoke(null, context, mPluginApkContext, mMainAppContext, attrs, defStyle);
			if (result == null) {
				throw new RcaRuntimeError("Static constructor " + method.toGenericString() + " returned null.");
			}
		} catch (NoSuchMethodException ignored) {
		}
		if (result == null) {
			if (defStyle != 0) {
				try {
					Constructor<? extends View> constructor = viewSubClass.getConstructor(Context.class, AttributeSet.class, int.class);
					result = constructor.newInstance(usedContext, attrs, defStyle);
				} catch (NoSuchMethodException ignored) {
				}
			} else if (attrs != null) {
				try {
					Constructor<? extends View> constructor = viewSubClass.getConstructor(Context.class, AttributeSet.class);
					result = constructor.newInstance(usedContext, attrs);
				} catch (NoSuchMethodException ignored) {
				}
			} else {
				Constructor<? extends View> constructor = viewSubClass.getConstructor(Context.class);
				result = constructor.newInstance(usedContext);
			}
		}
		return result;
	}
}
