/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.java;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.event_handling.RemoCoCustomEvent;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCompImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.IJavaRemoCompBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCompContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCompConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCompLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IJavaRemoCompEventDispatcher;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCoNotStartedException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.googlecode.android_scripting.jsonrpc.JsonBuilder;

import org.json.JSONException;
import org.ros.node.NodeConfiguration;

public final class JavaRemoCompImpl extends AJavaRemoCoImpl implements IRemoCompImpl {

	private class JavaRemoCompConfiguration extends AJavaRemoCoConfiguration implements IJavaRemoCompConfiguration {
	}

	private class JavaRemoCompContext extends AJavaRemoCoContext implements IJavaRemoCompContext {
		@Override
		public IJavaRemoCompConfig getConfig() {
			return mConfig;
		}

		@Override
		public IJavaRemoCompLayout getLayout() {
			return mLayout;
		}

		@Override
		public IJavaRemoCompEventDispatcher getEventDispatcher() {
			return mEventDispatcher;
		}
	}

	private JavaRemoCompContext mImplContext = new JavaRemoCompContext();

	private class JavaRemoCompConfig extends AJavaRemoCoConfig implements IJavaRemoCompConfig {
	}

	private final JavaRemoCompConfig mConfig = new JavaRemoCompConfig();

	private class JavaRemoCompLayout extends AJavaRemoCoLayout implements IJavaRemoCompLayout {
	}

	private final JavaRemoCompLayout mLayout = new JavaRemoCompLayout();

	private final IJavaRemoCompEventDispatcher mEventDispatcher = new IJavaRemoCompEventDispatcher() {
		@Override
		public void dispatch(String name, Object data) throws JSONException {
			if (!getRemoCo().isCreated()) {
				throw new RemoCoNotStartedException(getIdName());
			}

			// Note: What should we do about this converting? Without it Java-RemoComps are able to exchange data directly...
			// This converting to JSON object is only there for supporting exchanging this Java-RemoComp with a
			// Python-RemoComp which sent data is coming in as a JSON object...
			getRemoCo().dispatchEvent(RemoCoCustomEvent.EVENT_NAME, new RemoCoCustomEvent(name, JsonBuilder.build(data)));
		}
	};

	public JavaRemoCompImpl(IRcaApplication app, String idName, String type, String apk, IJavaRemoCompImpl impl) {
		super(app, idName, type, apk, impl);
	}

	@Override
	protected IJavaRemoCompImpl getImpl() {
		return (IJavaRemoCompImpl) super.getImpl();
	}

	@Override
	public IRcaBinder create(IRemoComp remoComp) {
		super.create(remoComp);

		final IJavaRemoCompBinderImpl vBinder[] = new IJavaRemoCompBinderImpl[1];
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().init(mImplContext);
				vBinder[0] = getImpl().createBinder();
			}
		});

		return new JavaRemoCoImplBinder(vBinder[0]);
	}

	@Override
	public void start(final NodeConfiguration nodeConfiguration) {
		super.start(nodeConfiguration);
		final JavaRemoCompConfiguration configuration = new JavaRemoCompConfiguration();
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().start(configuration);
			}
		});
	}
}
