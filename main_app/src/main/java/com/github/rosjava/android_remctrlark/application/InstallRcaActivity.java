/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import com.android.python27.support.InstallPythonServiceActivity;
import com.android.python27.support.Utils;

import java.io.File;
import java.io.InputStream;

public class InstallRcaActivity extends InstallPythonServiceActivity {

	protected interface IOnInstallPluginFinished {
		void onInstallPluginFinished(boolean installStatus);
	}

	@Override
	protected void onInstallPythonServiceFinished(boolean installStatus) {
		final boolean pythonServiceInstallStatus = installStatus;
		installPlugin(getPackageName(), new IOnInstallPluginFinished() {
			@Override
			public void onInstallPluginFinished(boolean installStatus) {
				onInstallRcaFinished(pythonServiceInstallStatus && installStatus);
			}
		});
	}

	protected void onInstallRcaFinished(boolean installStatus) {
		super.onInstallPythonServiceFinished(installStatus);
	}

	protected final void installPlugin(final String rcaApk, final IOnInstallPluginFinished onFinished) {
		assert RcaService.getApp() != null;

		final IMainFileNamesManager fileNamesManager = RcaService.getApp().getFileNamesManager();
		final String installPath = fileNamesManager.getPluginApkFullDir(rcaApk) + "/";

		final File testedFile = new File(installPath + RcaGlobalConstants.REMOCON_CONFIGS_DIR_NAME);
		boolean installNeeded = !testedFile.exists();

		if (installNeeded) {
			new AInstallAsyncTask() {
				@Override
				protected boolean install() {
					Utils.createDirectoryOnExternalStorage(rcaApk);

					boolean succeed = true;
					InputStream content = getZipResourceContent(rcaApk, RcaGlobalConstants.PYTHON_PLUGIN_PROJECT_ZIP_NAME);
					if (content != null) {
						succeed = Utils.unzip(content, installPath, false);
					}
					succeed &= createDirectoryIfNotExists(installPath + RcaGlobalConstants.PYTHON_REMOCONS_DIR_NAME);
					succeed &= createDirectoryIfNotExists(installPath + RcaGlobalConstants.PYTHON_REMOCOMPS_DIR_NAME);
					succeed &= createDirectoryIfNotExists(installPath + RcaGlobalConstants.PYTHON_SHARED_DIR_NAME);

					content = getZipResourceContent(rcaApk, RcaGlobalConstants.REMOCON_CONFIGS_ZIP_NAME);
					if (content != null) {
						succeed &= Utils.unzip(content, installPath, false);
					}
					succeed &= createDirectoryIfNotExists(installPath + RcaGlobalConstants.REMOCON_CONFIGS_DIR_NAME);

					content = getZipResourceContent(rcaApk, RcaGlobalConstants.JAVA_MSGS_ZIP_NAME);
					if (content != null) {
						succeed &= Utils.unzip(content, installPath, false);
					}
					succeed &= createDirectoryIfNotExists(installPath + RcaGlobalConstants.JAVA_MSGS_DIR_NAME);

					return succeed;
				}

				@Override
				protected void onFinished(boolean installStatus) {
					if (onFinished != null) {
						onFinished.onInstallPluginFinished(installStatus);
					}
				}
			}.execute();
		} else {
			if (onFinished != null) {
				onFinished.onInstallPluginFinished(true);
			}
		}
	}

	private boolean createDirectoryIfNotExists(String path) {
		final File dir = new File(path);
		return dir.exists() || dir.mkdirs();
	}
}
