/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original from SL4A's FullScreenTask.java modified by Denis Graf.
 */

package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.rosjava.android_remctrlark.application.IRcaLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.ForbiddenOperationInCurrentThreadError;
import com.github.rosjava.android_remctrlark.error_handling.LayoutException;
import com.github.rosjava.android_remctrlark.error_handling.RcaError;
import com.github.rosjava.android_remctrlark.event_handling.IObservableRcaView;
import com.github.rosjava.android_remctrlark.event_handling.IRcaViewEventListener;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaXmlStringLayoutInflater;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.googlecode.android_scripting.facade.ui.ViewInflater;
import com.googlecode.android_scripting.rpc.JsonArgsConverter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Layout {

	private final Handler mHandler;
	private final long mHandlerThreadId;
	private ViewInflater mInflater = new ViewInflater();
	private View mInflatedView = null;
	private IRcaViewEventListener mCustomEventListener = null;
	private View.OnClickListener mClickListener = null;
	private AdapterView.OnItemClickListener mItemClickListener = null;
	private View.OnTouchListener mTouchListener = null;

	public Layout(@NotNull IRcaLayoutInflaterFactory factory) {
		mInflater.setViewFactory(factory);
		// Note: The layout is always handled by the main thread.
		mHandler = new Handler(Looper.getMainLooper());
		mHandlerThreadId = Looper.getMainLooper().getThread().getId();
	}

	public ViewInflater getInflater() {
		return mInflater;
	}

	public void setOnCustomEventListener(String viewId, IRcaViewEventListener onCustomEventListener) throws LayoutException {
		View view = getViewByName(viewId);
		if (view != null) {
			if (view instanceof IObservableRcaView) {
				((IObservableRcaView) view).setCustomEventListener(onCustomEventListener);
			} else {
				throw new LayoutException("View '" + viewId + "' does not implement the interface " + IObservableRcaView.class.getSimpleName() + " for dispatching custom events.");
			}
		} else {
			throw createViewNotFoundException(viewId);
		}
	}

	public void setOnClickListener(String viewId, View.OnClickListener onClickListener) throws LayoutException {
		View view = getViewByName(viewId);
		if (view != null) {
			view.setOnClickListener(onClickListener);
		} else {
			throw createViewNotFoundException(viewId);
		}
	}

	public void setOnItemClickListener(String viewId, AdapterView.OnItemClickListener itemClickListener) throws LayoutException {
		View view = getViewByName(viewId);
		if (view != null) {
			try {
				((AdapterView) view).setOnItemClickListener(itemClickListener);
			} catch (RuntimeException e) {
				throw new LayoutException(e);
			}
		} else {
			throw createViewNotFoundException(viewId);
		}
	}

	public void setOnTouchListener(String viewId, View.OnTouchListener touchListener) throws LayoutException {
		View view = getViewByName(viewId);
		if (view != null) {
			view.setOnTouchListener(touchListener);
		} else {
			throw createViewNotFoundException(viewId);
		}
	}

	public void setListeners(IRcaViewEventListener customEventListener,
	                         View.OnClickListener clickListener,
	                         AdapterView.OnItemClickListener itemClickListener,
	                         View.OnTouchListener touchListener) {
		mCustomEventListener = customEventListener;
		mClickListener = clickListener;
		mItemClickListener = itemClickListener;
		mTouchListener = touchListener;
		setViewListeners(mInflatedView, mCustomEventListener, mClickListener, mItemClickListener, mTouchListener);
	}

	static private void setViewListeners(View view,
	                                     IRcaViewEventListener customEventListener,
	                                     View.OnClickListener clickListener,
	                                     AdapterView.OnItemClickListener itemClickListener,
	                                     View.OnTouchListener touchListener) {
		if (view == null && clickListener == null && itemClickListener == null && touchListener == null) {
			return;
		}
		assert view != null;
		if (view instanceof IRemoCo) {
			// Don't touch any RemoCo. They have their own handling of listeners!
			return;
		}
		if (view instanceof IObservableRcaView) {
			try {
				((IObservableRcaView) view).setCustomEventListener(customEventListener);
			} catch (RuntimeException e) {
				// And not all controls support OnClickListener.
			}
		}
		if (view.isClickable()) {
			if (view instanceof AdapterView) {
				try {
					((AdapterView) view).setOnItemClickListener(itemClickListener);
				} catch (RuntimeException e) {
					// Ignore this, not all controls support OnItemClickListener.
				}
			}
			try {
				view.setOnClickListener(clickListener);
				view.setOnTouchListener(touchListener);
			} catch (RuntimeException e) {
				// And not all controls support OnClickListener.
			}
		}
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				setViewListeners(viewGroup.getChildAt(i), customEventListener, clickListener, itemClickListener, touchListener);
			}
		}
	}

	public View findViewByName(String idName) {
		return getViewByName(idName);
	}

	private View getViewByName(String idName) {
		View result = null;
		int id = mInflater.getId(idName);
		if (id != 0) {
			result = mInflatedView.findViewById(id);
		}
		return result;
	}

	public void loadLayoutFromFile(Context context, ViewGroup rootView, String fileName) throws LayoutException {
		File layoutFile = new File(fileName);
		try {
			try {
				InputStreamReader fis = new InputStreamReader(new FileInputStream(layoutFile));
				loadLayout(context, rootView, fis);
			} catch (Exception e) {
				mInflater.getErrors().add(e.toString());
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	public void handlerLoadLayoutFromFile(Context context, final ViewGroup rootView, final String fileName) throws LayoutException {
		checkCurrentThread();
		try {
			final Context fContext = context;
			final CountDownLatch latch = new CountDownLatch(1);
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						loadLayoutFromFile(fContext, rootView, fileName);
					} catch (LayoutException ignored) {
					}
					latch.countDown();
				}
			});
			try {
				latch.await();
			} catch (InterruptedException e) {
				mInflater.getErrors().add(e.toString());
				Thread.currentThread().interrupt();
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	private void loadLayoutFromXmlString(Context context, ViewGroup rootView, String xml) throws LayoutException {
		try {
			try {
				StringReader sr = new StringReader(xml);
				loadLayout(context, rootView, sr);
			} catch (Exception e) {
				mInflater.getErrors().add(e.toString());
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	public void handlerLoadLayoutFromXmlString(Context context, ViewGroup rootView, String xml) throws LayoutException {
		checkCurrentThread();
		try {
			final Context fContext = context;
			final ViewGroup fRootView = rootView;
			final String fXml = xml;
			final CountDownLatch latch = new CountDownLatch(1);
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						loadLayoutFromXmlString(fContext, fRootView, fXml);
					} catch (LayoutException ignored) {
					}
					latch.countDown();
				}
			});
			try {
				latch.await();
			} catch (InterruptedException e) {
				mInflater.getErrors().add(e.toString());
				Thread.currentThread().interrupt();
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	public Map<String, Map<String, String>> getViewAsMap() {
		return mInflater.getViewAsMap(mInflatedView);
	}

	public Map<String, String> getViewDetail(String idName) throws LayoutException {
		Map<String, String> result = new HashMap<String, String>();
		result.put("error", "id not found (" + idName + ")");
		View view = getViewByName(idName);
		if (view != null) {
			result = mInflater.getViewInfo(view);
		} else {
			throw new LayoutException(createViewNotFoundException(idName));
		}
		return result;
	}

	public Object getViewProperty(String idName, String property) throws LayoutException {
		View view = getViewByName(idName);
		if (view != null) {
			return mInflater.getViewProperty(view, property);
		} else {
			throw new LayoutException(createViewNotFoundException(idName));
		}
	}

	public void handlerSetViewProperty(String idName, String property, Object value) throws LayoutException {
		checkCurrentThread();

		try {
			final View view = getViewByName(idName);
			mInflater.getErrors().clear();
			if (view != null) {
				final String fProperty = property;
				final Object fValue = value;
				final CountDownLatch latch = new CountDownLatch(1);
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						mInflater.setProperty(view, fProperty, fValue);
						view.invalidate();
						latch.countDown();
					}
				});
				try {
					latch.await();
				} catch (InterruptedException e) {
					mInflater.getErrors().add(e.toString());
					Thread.currentThread().interrupt();
				}
			} else {
				throw createViewNotFoundException(idName);
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	public void handlerSetViewList(String idName, JSONArray items) throws LayoutException {
		checkCurrentThread();

		try {
			final View view = getViewByName(idName);
			mInflater.getErrors().clear();
			if (view != null) {
				final JSONArray fItems = items;
				final CountDownLatch latch = new CountDownLatch(1);
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						mInflater.setListAdapter(view, fItems);
						view.invalidate();
					}
				});
				try {
					latch.await();
				} catch (InterruptedException e) {
					mInflater.getErrors().add(e.toString());
					Thread.currentThread().interrupt();
				}
			} else {
				throw createViewNotFoundException(idName);
			}
			if (mInflater.getErrors().size() != 0) {
				throw new LayoutException(mInflater.getErrors().get(0));
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
	}

	public Object handlerCallMethod(final String idName, final String methodName, final JSONArray args) throws LayoutException {
		checkCurrentThread();

		final Object[] vResult = new Object[1];
		try {
			final View view = getViewByName(idName);
			mInflater.getErrors().clear();
			if (view != null) {
				final CountDownLatch latch = new CountDownLatch(1);
				final LayoutException[] vException = new LayoutException[1];
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						try {
							vResult[0] = callMethod(idName, methodName, args);
						} catch (LayoutException e) {
							vException[0] = e;
						}
						latch.countDown();
					}
				});
				try {
					latch.await();
				} catch (InterruptedException e) {
					mInflater.getErrors().add(e.toString());
					Thread.currentThread().interrupt();
				}
				if (vException[0] != null) {
					throw vException[0];
				}
			} else {
				throw createViewNotFoundException(idName);
			}
		} catch (Exception e) {
			throw new LayoutException(e);
		}
		return vResult[0];
	}

	public Object callMethod(String idName, String methodName, JSONArray args) throws LayoutException {
		final View view = findViewByName(idName);
		Exception lastError = null;
		try {
			for (Method method : view.getClass().getMethods()) {
				if (method.getName().equals(methodName)) {
					final Class<?>[] parameterTypes = method.getParameterTypes();
					if (args == null) {
						return method.invoke(view);
					} else if (parameterTypes.length == args.length()) {
						boolean castSuccess = false;
						Object[] castedArgs = null;
						try {
							castedArgs = new Object[parameterTypes.length];
							for (int i = 0; i < parameterTypes.length; ++i) {
								castedArgs[i] = JsonArgsConverter.convertParameter(args, i, parameterTypes[i]);
							}
							castSuccess = true;
						} catch (Exception e) {
							lastError = e;
						}
						if (castSuccess) {
							try {
								return method.invoke(view, castedArgs);
							} catch (IllegalArgumentException e) {
								lastError = e;
							}
						}
					}
				}
			}
			if (lastError != null) {
				throw lastError;
			} else {
				throw new LayoutException("Method '" + methodName + "' of view '" + idName + "' not found for given argument types.");
			}
		} catch (Exception e) {
			throw new LayoutException("Errors on calling method '" + methodName + "' of view '" + idName + "'.", e);
		}
	}

	private void loadLayout(@NotNull Context context, @NotNull ViewGroup rootView, @NotNull Reader reader) throws Exception {
		ViewInflater inflater = new ViewInflater();
		inflater.setViewFactory(mInflater.getViewFactory());
		XmlPullParser xml = ViewInflater.getXml(reader);

		// Note: When being in developer mode, we would like to see the view while it is created.
		final boolean attachUiLayerImmediately = RcaService.getContext().getSettings().getDeveloperModeEnabled();
		if (attachUiLayerImmediately) {
			rootView.removeAllViews();
			mInflatedView = inflater.inflate(context, xml, rootView);
		} else {
			mInflatedView = inflater.inflate(context, xml);
		}

		mInflater = inflater;

		if (!attachUiLayerImmediately) {
			rootView.removeAllViews();
			rootView.addView(mInflatedView);
		}

		setListeners(mCustomEventListener, mClickListener, mItemClickListener, mTouchListener);
		mInflatedView.invalidate();
	}

	private void checkCurrentThread() throws RcaError {
		if (mHandlerThreadId == Thread.currentThread().getId()) {
			throw new ForbiddenOperationInCurrentThreadError();
		}
	}

	public static IJavaXmlStringLayoutInflater createLayoutFromXmlString(@NotNull IRcaLayoutInflaterFactory factory,
	                                                                     @NotNull Context context, @NotNull ViewGroup rootView, @NotNull String layout) throws LayoutException {
		final Layout layoutInstance = new Layout(factory);
		layoutInstance.loadLayoutFromXmlString(context, rootView, layout);
		return new IJavaXmlStringLayoutInflater() {
			@Override
			public View findViewByIdName(String idName) {
				return layoutInstance.findViewByName(idName);
			}
		};
	}

	private static LayoutException createViewNotFoundException(String viewId) {
		return new LayoutException("View '" + viewId + "' not found.");
	}
}
