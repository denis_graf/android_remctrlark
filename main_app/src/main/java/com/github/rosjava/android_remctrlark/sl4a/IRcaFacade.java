/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.error_handling.ProxyNotFoundException;
import com.github.rosjava.android_remctrlark.error_handling.ScriptNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventDispatcher;
import com.github.rosjava.android_remctrlark.sl4a.proxies.IRcaPyProxy;
import com.googlecode.android_scripting.facade.EventFacade;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IRcaFacade {

	IRcaContext getRcaContext();

	String getPythonTypeName();

	String getPythonHostname();

	int getPythonPid();

	int getPythonTid();

	String getPyScriptHostPid(String hostname, int pid);

	String getPyScriptHostPid(IRcaScript script);

	IMainScript getNewestMainScript(String ignoredId);

	@NotNull
	IRcaPyProxy getProxyByRid(int rid) throws ProxyNotFoundException;

	List<IRcaPyProxy> getAllProxies();

	void putPyScript(IRcaScript script);

	void removePyScript(IRcaScript script);

	@NotNull
	IRcaScript getScriptById(String id) throws ScriptNotFoundException;

	IRcaScript getScriptByHostPid(String hostname, int pid);

	IRcaEventDispatcher getEventDispatcher();

	void addEventObserver(EventFacade.EventObserver observer);

	void removeEventObserver(EventFacade.EventObserver observer);

	void sendInterruptEvent();
}
