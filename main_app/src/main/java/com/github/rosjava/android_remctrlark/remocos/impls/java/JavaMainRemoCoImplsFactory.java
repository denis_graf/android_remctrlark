/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.java;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCompImpl;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.AMainRemoCoImplsFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class JavaMainRemoCoImplsFactory extends AMainRemoCoImplsFactory {

	private final Map<String, IJavaRemoCoImplsFactory> mFactories = new HashMap<String, IJavaRemoCoImplsFactory>();

	public JavaMainRemoCoImplsFactory(IRcaApplication app) {
		super(app);
	}

	@Override
	public void reload() {
		mFactories.clear();
//		loadFactories();
	}

	@Override
	public Set<String> getAllRemoConTypes(String apk) {
		final Set<String> result = new TreeSet<String>();
		final IJavaRemoCoImplsFactory factory = getFactory(apk);
		if (factory != null) {
			try {
				result.addAll(factory.getAllRemoConTypes());
			} catch (Throwable e) {
				RcaService.getLog().e("error on retrieving all remocon types from apk '" + apk + "'", e);
			}
		}
		return result;
	}

	@Override
	public Set<String> getAllRemoCompTypes(String apk) {
		final Set<String> result = new TreeSet<String>();
		final IJavaRemoCoImplsFactory factory = getFactory(apk);
		if (factory != null) {
			try {
				result.addAll(factory.getAllRemoCompTypes());
			} catch (Throwable e) {
				RcaService.getLog().e("error on retrieving all remocomp types from apk '" + apk + "'", e);
			}
		}
		return result;
	}

	@Override
	public IRemoConImpl createRemoConImpl(String idName, String type, String apk) {
		final IJavaRemoCoImplsFactory factory = getFactory(apk);
		IRemoConImpl impl = null;
		if (factory != null) {
			IJavaRemoConImpl javaImpl = null;
			try {
				javaImpl = factory.createRemoConImpl(type);
			} catch (Throwable e) {
				RcaService.getLog().e("error on retrieving remocon type '" + type + "' from apk '" + apk + "'", e);
			}
			if (javaImpl != null) {
				impl = new JavaRemoConImpl(getApp(), idName, type, apk, javaImpl);
			}
		}
		return impl;
	}

	@Override
	public IRemoCompImpl createRemoCompImpl(String idName, String type, String apk) {
		final IJavaRemoCoImplsFactory factory = getFactory(apk);
		IRemoCompImpl impl = null;
		if (factory != null) {
			IJavaRemoCompImpl javaImpl = null;
			try {
				javaImpl = factory.createRemoCompImpl(type);
			} catch (Throwable e) {
				RcaService.getLog().e("error on retrieving remocomp type '" + type + "' from apk '" + apk + "'", e);
			}
			if (javaImpl != null) {
				impl = new JavaRemoCompImpl(getApp(), idName, type, apk, javaImpl);
			}
		}
		return impl;
	}

	private IJavaRemoCoImplsFactory getFactory(String apk) {
		IJavaRemoCoImplsFactory factory = mFactories.get(apk);
		if (factory == null) {
			factory = loadFactory(apk);
			if (factory != null) {
				mFactories.put(apk, factory);
			}
		}
		return factory;
	}

/*
	private void loadFactories() {
		final String[] pluginApks = getApp().getMainRemoCoImplsFactory().retrievePluginApks();
		for (String apk : pluginApks) {
			final IJavaRemoCoImplsFactory factory = loadFactory(apk);
			if (factory != null) {
				mFactories.put(apk, factory);
			}
		}
	}
*/

	private IJavaRemoCoImplsFactory loadFactory(String apk) {
		// TODO: #61: optimize loading classes -> implement some caching

		final IJavaRemoCoImplsFactory factory;

		try {
			final ClassLoader classLoader = getApp().getMainRemoCoImplsFactory().getPlugin(apk).getClassLoader();
			final Class<? extends IJavaRemoCoImplsFactory> factoryClass =
					classLoader.loadClass(apk + RcaGlobalConstants.JAVA_PLUGIN_FACTORY_CLASS_NAME).asSubclass(IJavaRemoCoImplsFactory.class);
			factory = factoryClass.getDeclaredConstructor().newInstance();
		} catch (Throwable e) {
			RcaService.getLog().w("java-remocos-factory for plugin '" + apk + "' could not be loaded: ", e);
			return null;
		}

		assert factory != null;
		return factory;
	}
}
