/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import com.github.rosjava.android_remctrlark.sl4a.IRcaFieldsToJsonMappable;

import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;
import org.ros.node.NodeConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PyNodeConfiguration implements IRcaFieldsToJsonMappable {

	public final String master_uri;
	public final String master_host;
	public final int master_port;
	public final String ip;
	public final String node_name;
	public final String namespace;
	public final Map<String, String> remappings;

	public PyNodeConfiguration(NodeConfiguration nodeConfiguration) {
		master_uri = nodeConfiguration.getMasterUri().toString();
		master_host = nodeConfiguration.getMasterUri().getHost();
		master_port = nodeConfiguration.getMasterUri().getPort();

		ip = nodeConfiguration.getTcpRosAdvertiseAddress().getHost();
		node_name = nodeConfiguration.getNodeName().toString();

		NameResolver parentResolver = nodeConfiguration.getParentResolver();
		namespace = parentResolver.getNamespace().toString();

		Set<Map.Entry<GraphName, GraphName>> remappingsSet = parentResolver.getRemappings().entrySet();
		remappings = new HashMap<String, String>(remappingsSet.size());
		for (Map.Entry<GraphName, GraphName> remapping : remappingsSet) {
			remappings.put(remapping.getKey().toString(), remapping.getValue().toString());
		}
	}
}
