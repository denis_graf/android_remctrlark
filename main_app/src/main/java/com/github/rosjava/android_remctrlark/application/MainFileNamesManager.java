/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.Context;
import android.os.Environment;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

class MainFileNamesManager implements IMainFileNamesManager {

	private static final String REMOCONS_DIR_NAME = RcaGlobalConstants.PYTHON_REMOCONS_DIR_NAME;
	private static final String REMOCOMPS_DIR_NAME = RcaGlobalConstants.PYTHON_REMOCOMPS_DIR_NAME;
	private static final String REMOCON_CONFIGS_DIR_NAME = RcaGlobalConstants.REMOCON_CONFIGS_DIR_NAME;
	private static final String JAVA_MSGS_DIR_NAME = RcaGlobalConstants.JAVA_MSGS_DIR_NAME;
	private static final String REMOCON_CONFIG_SUFFIX = "reco";
	private static final String REMOCON_VIEWER = ".viewers/remocon." + REMOCON_CONFIG_SUFFIX;
	private static final String REMOCOMP_VIEWER = ".viewers/remocomp." + REMOCON_CONFIG_SUFFIX;

	private final String mExternalStorageFullDir;
	private final String mInternalStorageFullDir;
	private final String mMainAppApk;
	private final Map<String, IPyRemoCoFileNamesManager> mPythonFileNamesManager = new HashMap<String, IPyRemoCoFileNamesManager>();


	public MainFileNamesManager(IRcaApplication app) {
		assert app != null;
		final Context context = app.getApplicationContext();
		mExternalStorageFullDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		assert context.getFilesDir() != null;
		mInternalStorageFullDir = context.getFilesDir().getAbsolutePath();
		mMainAppApk = context.getPackageName();
	}

	@NotNull
	@Override
	public String getExternalStorageFullDir() {
		return mExternalStorageFullDir;
	}

	@NotNull
	@Override
	public String getInternalStorageFullDir() {
		return mInternalStorageFullDir;
	}

	@NotNull
	@Override
	public String getMainAppApk() {
		return mMainAppApk;
	}

	@NotNull
	@Override
	public String getMainAppApkFullDir() {
		return mExternalStorageFullDir + "/" + mMainAppApk;
	}

	@NotNull
	@Override
	public String getPluginApkFullDir(@NotNull String apk) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument(!apk.isEmpty());
		}
		return mExternalStorageFullDir + "/" + apk;
	}

	@NotNull
	public String getRcaPythonCoreFullDir() {
		return getMainAppApkFullDir() + "/core";
	}

	@NotNull
	@Override
	public String getMainScriptFullDir() {
		return getRcaPythonCoreFullDir() + "/remctrlark/main.py";
	}

	@NotNull
	@Override
	public String getRemoConConfigsFullDir(@NotNull String apk) {
		return getPluginApkFullDir(apk) + "/" + REMOCON_CONFIGS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getJavaMsgsFullDir(@NotNull String apk) {
		return getPluginApkFullDir(apk) + "/" + JAVA_MSGS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getRemoConConfigSuffix() {
		return REMOCON_CONFIG_SUFFIX;
	}

	@NotNull
	public String getPythonFullDir() {
		return getInternalStorageFullDir() + "/python";
	}

	@NotNull
	public String getPythonLibFullDir() {
		return getPythonFullDir() + "/lib";
	}

	@NotNull
	public String getPythonLibPythonDir() {
		return getPythonLibFullDir() + "/python2.7";
	}

	@NotNull
	public String getPythonLibDynLoadFullDir() {
		return getPythonLibPythonDir() + "/lib-dynload";
	}

	@NotNull
	public String getPythonExtrasFullDir() {
		return getMainAppApkFullDir() + "/extras";
	}

	@NotNull
	public String getPythonExtrasPythonFullDir() {
		return getPythonExtrasFullDir() + "/python";
	}

	@NotNull
	public String getPythonExtrasTempFullDir() {
		return getPythonExtrasFullDir() + "/tmp";
	}

	@NotNull
	public String getPythonBinRelFileName() {
		return "python/bin/python";
	}

	@NotNull
	public String getPythonBinFullFileName() {
		return getInternalStorageFullDir() + "/" + getPythonBinRelFileName();
	}

	@NotNull
	@Override
	public String getPyRemoConsRelDir() {
		return REMOCONS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getPyRemoConsFullDir(@NotNull String apk) {
		return getPluginApkFullDir(apk) + "/" + REMOCONS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getPyRemoCompsRelDir() {
		return REMOCOMPS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getPyRemoCompsFullDir(@NotNull String apk) {
		return getPluginApkFullDir(apk) + "/" + REMOCOMPS_DIR_NAME;
	}

	@NotNull
	@Override
	public String getRemoConViewerFullFileName() {
		return getRemoConConfigsFullDir(getMainAppApk()) + "/" + REMOCON_VIEWER;
	}

	@NotNull
	@Override
	public String getRemoCompViewerFullFileName() {
		return getRemoConConfigsFullDir(getMainAppApk()) + "/" + REMOCOMP_VIEWER;
	}

	@Override
	public IPyRemoCoFileNamesManager getPyRemoCoFileNamesManager(@NotNull String type, @NotNull String apk) {
		// Note: Type ids have to be unique among RemoCons and RemmoComps.
		synchronized (mPythonFileNamesManager) {
			final String key = createPyRemoCoFileNamesManagerKey(type, apk);
			IPyRemoCoFileNamesManager pyRemoCoFileNamesManager = mPythonFileNamesManager.get(key);

			if (pyRemoCoFileNamesManager == null) {
				pyRemoCoFileNamesManager = new PyRemoCompFileNamesManager(this, type, apk);
			}
			if (!fileExists(pyRemoCoFileNamesManager.getMainFullDir())) {
				pyRemoCoFileNamesManager = new PyRemoConFileNamesManager(this, type, apk);
				if (!fileExists(pyRemoCoFileNamesManager.getMainFullDir())) {
					mPythonFileNamesManager.remove(key);
					return null;
				}
			}

			mPythonFileNamesManager.put(key, pyRemoCoFileNamesManager);
			return pyRemoCoFileNamesManager;
		}
	}

	@Override
	public IPyRemoConFileNamesManager getPyRemoConFileNamesManager(@NotNull String type, @NotNull String apk) {
		IPyRemoConFileNamesManager pyRemoConFileNamesManager;
		try {
			pyRemoConFileNamesManager = (IPyRemoConFileNamesManager) getPyRemoCoFileNamesManager(type, apk);
		} catch (ClassCastException e) {
			return null;
		}
		return pyRemoConFileNamesManager;
	}

	@Override
	public IPyRemoCompFileNamesManager getPyRemoCompFileNamesManager(@NotNull String type, @NotNull String apk) {
		IPyRemoCompFileNamesManager pyRemoCompFileNamesManager;
		try {
			pyRemoCompFileNamesManager = (IPyRemoCompFileNamesManager) getPyRemoCoFileNamesManager(type, apk);
		} catch (ClassCastException e) {
			return null;
		}
		return pyRemoCompFileNamesManager;
	}

	private String createPyRemoCoFileNamesManagerKey(String type, String apk) {
		return apk + "/" + type;
	}

	private boolean fileExists(String fileName) {
		File dir = new File(fileName);
		return dir.exists();
	}

}
