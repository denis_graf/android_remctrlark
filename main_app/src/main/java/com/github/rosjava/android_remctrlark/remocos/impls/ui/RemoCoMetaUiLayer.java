/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ToggleButton;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.IPyRemoCoFileNamesManager;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.IRcaSettings;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos_inspector.RemoCosInspectorActivity;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class RemoCoMetaUiLayer extends FrameLayout {

	private class Overlay extends ToggleButton {

		private MetaLayerState mStyle = MetaLayerState.DEFAULT;
		private ObjectAnimator mAnimator;
		private boolean mAnimatedState = false;

		public Overlay(Context context) {
			super(context);
			setTextOff(null);
			setTextOn(null);
			setText(null);
			setBackgroundResource(R.drawable.btn_inspect_overlay);
		}

		public boolean isDefaultStyle() {
			return mStyle == MetaLayerState.DEFAULT;
		}

		public boolean isWarningStyle() {
			return mStyle == MetaLayerState.WARNING;
		}

		public boolean isErrorStyle() {
			return mStyle == MetaLayerState.ERROR;
		}

		public void updateState() {
			final boolean isRemoConShutdown = getRcaContext().isRemoConShutdown();
			final boolean inErrorState = isInErrorState() ||
					(!mRemoCo.isRelaunching() && !getRcaContext().isRemoConRelaunching() && mRemoCo.isShutdown() && !isRemoConShutdown);
			final boolean inRelaunchState = getRcaSettings().getDeveloperModeEnabled() && mRemoCo.isRelaunching();
			final boolean inWarningState = isInWarningState() &&
					(getRcaSettings().getDeveloperModeEnabled() || !getRcaContext().isLoadingMode());

			if (inErrorState) {
				applyErrorStyle();
			} else if (inRelaunchState) {
				applyRelaunchStyle();
			} else if (inWarningState) {
				applyWarningStyle();
			} else {
				applyDefaultStyle();
			}

			if (inErrorState || inWarningState || (mRemoCo.isCreated() && !mRemoCo.isStarted()) ||
					getRcaContext().isInspectMode() || getRcaContext().isLoadingMode() || isRemoConShutdown) {
				if (RemoCoMetaUiLayer.this.findViewById(getId()) == null) {
					RemoCoMetaUiLayer.this.addView(Overlay.this);
				}
			} else {
				if (RemoCoMetaUiLayer.this.findViewById(getId()) != null) {
					RemoCoMetaUiLayer.this.removeView(Overlay.this);
				}
			}

			setChecked(isSelectedInInspectMode());
		}

		void setMeasuredDimensionInternal(int measuredWidth, int measuredHeight) {
			setMeasuredDimension(measuredWidth, measuredHeight);
		}

		@Override
		protected void drawableStateChanged() {
			if (mAnimatedState) {
				triggerAnimation(!isPressed());
			} else {
				assert getBackground() != null;
				if (getRcaSettings().getDeveloperModeEnabled()) {
					if (isPressed() || isChecked()) {
						getBackground().setAlpha(0xFF);
					} else {
						getBackground().setAlpha(0x88);
					}
				} else {
					getBackground().setAlpha(0x00);
				}
			}
			super.drawableStateChanged();
		}

		private void applyDefaultStyle() {
			if (isDefaultStyle()) {
				return;
			}
			mStyle = MetaLayerState.DEFAULT;
			assert getBackground() != null;
			getBackground().clearColorFilter();
			setAnimation(false);
		}

		private void applyWarningStyle() {
			if (isWarningStyle()) {
				return;
			}
			mStyle = MetaLayerState.WARNING;
			assert getBackground() != null;
			getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xFFFF00));
			setAnimation(true);
		}

		private void applyRelaunchStyle() {
			if (isWarningStyle()) {
				return;
			}
			mStyle = MetaLayerState.RELAUNCH;
			assert getBackground() != null;
			getBackground().setColorFilter(new LightingColorFilter(0x000000, 0x00FF00));
			setAnimation(true);
		}

		private void applyErrorStyle() {
			if (isErrorStyle()) {
				return;
			}
			mStyle = MetaLayerState.ERROR;
			assert getBackground() != null;
			if (getRcaSettings().getDeveloperModeEnabled()) {
				getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xFF0000));
				setAnimation(true);
			} else {
				getBackground().clearColorFilter();
				setAnimation(false);
			}
		}

		private void setAnimation(boolean enable) {
			if (mAnimatedState == enable) {
				return;
			}
			mAnimatedState = enable;
			triggerAnimation(enable);
		}

		private void triggerAnimation(boolean running) {
			if (mAnimator == null) {
				mAnimator = ObjectAnimator.ofInt(getBackground(), "alpha", 0x88, 0xFF);
				mAnimator.setDuration(500);
				mAnimator.setRepeatMode(ValueAnimator.REVERSE);
				mAnimator.setRepeatCount(ValueAnimator.INFINITE);
			}

			if (mAnimator.isRunning() == running) {
				return;
			}

			if (running) {
				mAnimator.start();
			} else {
				mAnimator.end();
				assert getBackground() != null;
				getBackground().setAlpha(0xFF);
			}
		}
	}

	private final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			assert inflater != null;
			inflater.inflate(R.menu.cab_remoco_inspect, menu);
			mActionMode = mode;
			updateActionModeMenu();
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			updateState();
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.cab_remoco_action_info:
					startRemoCosInspectorActivity(RemoCosInspectorActivity.TAB_INFO);
					return true;
				case R.id.cab_remoco_action_log:
					startRemoCosInspectorActivity(RemoCosInspectorActivity.TAB_LOG);
					return true;
				case R.id.cab_remoco_action_resume:
					mode.finish();
					cancelAllExceptions();
					updateState();
					return true;
				case R.id.cab_remoco_action_relaunch:
					mode.finish();
					updateState();
					getRcaContext().postRelaunchRemoCo(mRemoCo.getIdName(), new Runnable() {
						@Override
						public void run() {
							updateState();
						}
					});
					return true;
				case R.id.cab_remoco_action_main_dir: {
					final IPyRemoCoFileNamesManager pyRemoCoFileNamesManager = getPyFileNamesManager();
					if (pyRemoCoFileNamesManager != null) {
						final String fileName = pyRemoCoFileNamesManager.getMainFullDir();
						getRcaContext().openFileInExplorer(fileName);
					}
					return true;
				}
				case R.id.cab_remoco_action_main_layout: {
					final IPyRemoCoFileNamesManager pyRemoCoFileNamesManager = getPyFileNamesManager();
					if (pyRemoCoFileNamesManager != null) {
						final String fileName = pyRemoCoFileNamesManager.getMainLayoutFullName();
						getRcaContext().openFileInTextEditor(fileName);
					}
					return true;
				}
				case R.id.cab_remoco_action_main_script: {
					final IPyRemoCoFileNamesManager pyRemoCoFileNamesManager = getPyFileNamesManager();
					if (pyRemoCoFileNamesManager != null) {
						final String fileName = pyRemoCoFileNamesManager.getMainScriptFullName();
						getRcaContext().openFileInTextEditor(fileName);
					}
					return true;
				}
				default:
					return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mOverlay.setChecked(false);
			mActionMode = null;
		}

		private void startRemoCosInspectorActivity(String tab) {
			assert getContext() != null;
			Intent intent = new Intent(getContext(), RemoCosInspectorActivity.class);
			intent.putExtra(RemoCosInspectorActivity.EXTRA_REMOCO_ID, mRemoCo.getIdName());
			intent.putExtra(RemoCosInspectorActivity.EXTRA_SHOW_TAB, tab);
			getContext().startActivity(intent);
		}
	};

	private final Overlay mOverlay;
	private final List<Throwable> mErrors = new CopyOnWriteArrayList<Throwable>();
	private final List<Throwable> mWarnings = new CopyOnWriteArrayList<Throwable>();
	private final IRemoCo mRemoCo;
	private final boolean mIsRemoCon;

	private ActionMode mActionMode = null;

	public RemoCoMetaUiLayer(Context context, IRemoCo remoCo) {
		super(context);
		mRemoCo = remoCo;
		mIsRemoCon = mRemoCo instanceof IRemoCon;
		mOverlay = new Overlay(context);
		mOverlay.setId(1);

		mOverlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (shouldInspectModeGetEnabledCurrentlyOnClick()) {
					startInspectMode();
				}
				mOverlay.setChecked(isSelectedInInspectMode());
			}
		});

		mOverlay.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (getRcaContext().getRemoCon().isSelectedInInspectMode() || !shouldInspectModeGetEnabledCurrentlyOnClick()) {
					return false;
				}
				getRcaContext().startRemoConInspectMode();
				mOverlay.setPressed(false);
				return true;
			}
		});

		updateState();
	}

	public final boolean isInErrorState() {
		return !mErrors.isEmpty();
	}

	public final boolean isInWarningState() {
		return !mWarnings.isEmpty();
	}

	public void handleErrorException(Throwable error) {
		final String excType = error instanceof Error || error instanceof RuntimeException ? "unchecked" : "checked";
		mRemoCo.getLog().e("Handled " + excType + " exception:", error);
		mErrors.add(error);
		updateState();
	}

	public void handleWarningException(Throwable warning) {
		final String excType = warning instanceof Error || warning instanceof RuntimeException ? "unchecked" : "checked";
		mRemoCo.getLog().w("Handled " + excType + " exception warning:", warning);
		mWarnings.add(warning);
		updateState();
	}

	public boolean cancelException(Throwable exception) {
		// Note: Actually an exception can be either in the error list or in the warnings list, but we want to be sure.
		boolean removed = mErrors.remove(exception);
		removed |= mWarnings.remove(exception);
		if (removed) {
			updateState();
			return true;
		}
		return false;
	}

	public void cancelAllExceptions() {
		if (!(mErrors.isEmpty() && mWarnings.isEmpty())) {
			mErrors.clear();
			mWarnings.clear();
			updateState();
		}
	}

	public boolean containsException(Throwable exception) {
		return mErrors.contains(exception) || mWarnings.contains(exception);
	}

	public void startInspectMode() {
		mOverlay.setChecked(true);

		if (mActionMode != null) {
			return;
		}

		getRcaContext().startInspectMode(mActionModeCallback);
		assert mActionMode != null;
		mActionMode.setTitle("Inspect Mode");
		mActionMode.setSubtitle((mIsRemoCon ? "RemoCon: " : "RemoComp: ") + mRemoCo.getIdName());
	}

	public boolean isSelectedInInspectMode() {
		return mActionMode != null;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (mIsRemoCon) {
			return getRcaContext().getRemoCon().findRemoCompAtPosition((int) ev.getRawX(), (int) ev.getRawY()) != null;
		}
		return super.onInterceptTouchEvent(ev);
	}

	public void updateState() {
		getRcaContext().runOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				mOverlay.updateState();
				updateActionModeMenu();
			}
		});
	}

	void setMeasuredDimensionInternal(int measuredWidth, int measuredHeight) {
		setMeasuredDimension(measuredWidth, measuredHeight);
		mOverlay.setMeasuredDimensionInternal(measuredWidth, measuredHeight);
	}

	private void updateActionModeMenu() {
		if (mActionMode != null) {
			Menu menu = mActionMode.getMenu();
			assert menu != null;

			MenuItem itemRelaunch = menu.findItem(R.id.cab_remoco_action_relaunch);
			MenuItem itemResume = menu.findItem(R.id.cab_remoco_action_resume);
			MenuItem itemMainDir = menu.findItem(R.id.cab_remoco_action_main_dir);
			MenuItem itemMainLayout = menu.findItem(R.id.cab_remoco_action_main_layout);
			MenuItem itemMainScript = menu.findItem(R.id.cab_remoco_action_main_script);

			assert itemRelaunch != null;
			assert itemResume != null;
			assert itemMainDir != null;
			assert itemMainLayout != null;
			assert itemMainScript != null;

			// Note: If {@code pythonRemoCo} is not {@code null}, then it does not necessarily mean that this is python
			// remoco. It can be still a java remoco, if e.g. the order of loading remoco types is set to
			// 'java -> python'. But for now, we don't care...
			final boolean pythonRemoCo = getPyFileNamesManager() != null;

			itemRelaunch.setVisible(!getRcaContext().isLoadingMode() && !isInWarningState() && !mRemoCo.isRelaunching() && !mRemoCo.isShutdown() && mRemoCo.isCreated());
			itemResume.setVisible(isInErrorState() || isInWarningState());
			itemMainDir.setVisible(pythonRemoCo);
			itemMainLayout.setVisible(pythonRemoCo);
			itemMainScript.setVisible(pythonRemoCo);
		}
	}

	private boolean shouldInspectModeGetEnabledCurrentlyOnClick() {
		// Note: We have to check here the real current style of the {@code mOverlay}, not the current inner state,
		// because the UI update is always delayed and we need here the state of the UI.
		return getRcaContext().isInspectMode() ||
				(getRcaSettings().getDeveloperModeEnabled() &&
						(getRcaContext().isLoadingMode() ||
								((!mOverlay.isErrorStyle() || getRcaSettings().getDmRemoConInspectOnClickError()) &&
										(!mOverlay.isWarningStyle() || getRcaSettings().getDmRemoConInspectOnClickHanging()))));
	}

	private IRcaContext getRcaContext() {
		IRcaContext rcaContext = (IRcaContext) getContext();
		assert rcaContext != null;
		return rcaContext;
	}

	private IPyRemoCoFileNamesManager getPyFileNamesManager() {
		return getRcaContext().getApp().getFileNamesManager().
				getPyRemoCoFileNamesManager(mRemoCo.getType(), mRemoCo.getApk());
	}

	private IRcaSettings getRcaSettings() {
		return getRcaContext().getSettings();
	}
}


enum MetaLayerState {
	DEFAULT,
	WARNING,
	RELAUNCH,
	ERROR,
}
