/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.github.rosjava.android_remctrlark.remocos.impls.MutablesConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ros.namespace.GraphName;

import java.net.URI;
import java.net.URISyntaxException;

public class RemoConPyRelauncher {
	public static void commit(JSONObject commit) throws JSONException, RcaException, URISyntaxException {
		final MutablesConfig config = new MutablesConfig();
		if (commit.getBoolean("set_all_to_initial_state")) {
			config.setAllToInitialState();
		}
		{
			final String key = "connect_to_master";
			if (commit.has(key)) {
				final String uri = commit.getString(key);
				config.setMasterUri(new URI(uri));
				config.setNewMaster(RemoConConfigNewMaster.None);
			}
		}
		{
			final String key = "connect_to_new_private_master";
			if (commit.has(key) && commit.getBoolean(key)) {
				config.setNewMaster(RemoConConfigNewMaster.Private);
			}
		}
		{
			final String key = "connect_to_new_public_master";
			if (commit.has(key) && commit.getBoolean(key)) {
				config.setNewMaster(RemoConConfigNewMaster.Public);
			}
		}
		{
			final JSONArray list = commit.getJSONArray("set_namespace");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setNamespace = list.getJSONObject(i);
				config.setNamespace(new RcaId(setNamespace.getString("nsid")), GraphName.of(setNamespace.getString("namespace")));
			}
		}
		{
			final JSONArray list = commit.getJSONArray("set_type");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setType = list.getJSONObject(i);
				final String apk;
				if (setType.has("apk")) {
					apk = setType.getString("apk");
				} else {
					apk = getRcaContext().getRemoConConfig().getBaseConfApk();
				}
				config.setType(new RcaId(setType.getString("tid")), new RcaId(setType.getString("type")), apk);
			}
		}
		{
			final RcaId remoConId = new RcaId(getRcaContext().getRemoCon().getIdName());
			final JSONArray list = commit.getJSONArray("set_param");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setParam = list.getJSONObject(i);
				setParam(config, remoConId, setParam.getString("name"), setParam.get("value"));
			}
		}
		{
			final JSONArray list = commit.getJSONArray("set_remocomp_param");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setRemoCompParam = list.getJSONObject(i);
				setParam(config, new RcaId(setRemoCompParam.getString("id")),
						setRemoCompParam.getString("name"), setRemoCompParam.get("value"));
			}
		}
		{
			final RcaId remoConId = new RcaId(getRcaContext().getRemoCon().getIdName());
			final JSONArray list = commit.getJSONArray("set_remap");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setRemap = list.getJSONObject(i);
				config.setRemapping(remoConId,
						GraphName.of(setRemap.getString("from")),
						GraphName.of(setRemap.getString("to")),
						new RcaId(setRemap.getString("nsid")));
			}
		}
		{
			final JSONArray list = commit.getJSONArray("set_remocomp_remap");
			for (int i = 0; i < list.length(); ++i) {
				final JSONObject setRemoCompRemap = list.getJSONObject(i);
				final IRcaId nsId;
				if (setRemoCompRemap.has("nsid")) {
					nsId = new RcaId(setRemoCompRemap.getString("nsid"));
				} else {
					nsId = null;
				}
				config.setRemapping(new RcaId(setRemoCompRemap.getString("id")),
						GraphName.of(setRemoCompRemap.getString("from")),
						GraphName.of(setRemoCompRemap.getString("to")),
						nsId);
			}
		}
		getRcaContext().postRelaunchRemoCos(config);
	}

	private static void setParam(MutablesConfig config, IRcaId id, String name, Object value) throws RcaException {
		if (!(value instanceof Double || value instanceof Integer || value instanceof Boolean || value instanceof String)) {
			throw new RcaException("Parameter '" + name + "': unsupported type: " + value.getClass().getName() + ".");
		}
		config.setParam(id, getRcaContext().getRootNameResolver().newChild(id.toString()).resolve(name), value);
	}

	private static IRcaContext getRcaContext() {
		return RcaService.getContext();
	}
}
