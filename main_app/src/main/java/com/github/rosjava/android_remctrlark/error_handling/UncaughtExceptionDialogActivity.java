/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.error_handling;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

public class UncaughtExceptionDialogActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO: implement...

		final String title = "TODO";
		final String message = "TODO";

		System.err.append("TODO");
		System.err.flush();

		final Context context = this;
		runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
				alertDialog.setTitle(title);
				alertDialog.setMessage(message);
				alertDialog.setCancelable(false);
				alertDialog.setNeutralButton("OK", null);
				alertDialog.show();
			}
		});
	}
}
