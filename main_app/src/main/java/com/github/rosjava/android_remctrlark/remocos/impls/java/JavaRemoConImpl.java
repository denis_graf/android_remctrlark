/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.java;

import android.view.KeyEvent;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventListener;
import com.github.rosjava.android_remctrlark.event_handling.IRcaOnKeyListener;
import com.github.rosjava.android_remctrlark.event_handling.RemoCoCustomEvent;
import com.github.rosjava.android_remctrlark.event_handling.RemoCompLaunchedEvent;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoConImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoConConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoConContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoConConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoConLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.events.IOnKeyListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IOnRemoCompLaunchedListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompBinder;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompEventsListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompProxy;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoConRelauncher;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.NotOnImplThreadException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCoNotCreatedException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCompBinderInvocationException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCompNotFoundException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.github.rosjava.android_remctrlark.remocos.impls.MutablesConfig;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;
import org.ros.node.NodeConfiguration;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public final class JavaRemoConImpl extends AJavaRemoCoImpl implements IRemoConImpl {

	private class JavaRemoConConfiguration extends AJavaRemoCoConfiguration implements IJavaRemoConConfiguration {
	}

	private class JavaRemoConContext extends AJavaRemoCoContext implements IJavaRemoConContext {
		@Override
		public IJavaRemoConConfig getConfig() {
			return mConfig;
		}

		@Override
		public IJavaRemoConLayout getLayout() {
			return mLayout;
		}

		@Override
		public IRemoCompProxy getRemoCompProxy(String id) {
			if (!getRemoCo().isCreated()) {
				throw new RemoCoNotCreatedException(getIdName());
			}

			RemoCompProxy proxy = mRemoCompProxies.get(id);
			if (proxy == null) {
				final IRemoComp remoComp = getRcaContext().getRemoCon().findRemoCompByIdName(id);

				if (remoComp == null) {
					throw new RemoCompNotFoundException(getIdName(), id);
				}

				proxy = new RemoCompProxy(remoComp);
				mRemoCompProxies.put(id, proxy);
			}
			return proxy;
		}

		@Override
		public IRemoConRelauncher getRelauncher() {
			return mRelauncher;
		}
	}

	private class RemoConRelauncher implements IRemoConRelauncher {
		private MutablesConfig mConfig = new MutablesConfig();

		@Override
		public IRemoConRelauncher connectToMaster(String uri) {
			checkState();
			mConfig.setMasterUri(URI.create(uri));
			mConfig.setNewMaster(RemoConConfigNewMaster.None);
			return this;
		}

		@Override
		public IRemoConRelauncher connectToMaster(URI uri) {
			checkState();
			mConfig.setMasterUri(uri);
			mConfig.setNewMaster(RemoConConfigNewMaster.None);
			return this;
		}

		@Override
		public IRemoConRelauncher connectToNewPublicMaster() {
			checkState();
			mConfig.setNewMaster(RemoConConfigNewMaster.Public);
			return this;
		}

		@Override
		public IRemoConRelauncher connectToNewPrivateMaster() {
			checkState();
			mConfig.setNewMaster(RemoConConfigNewMaster.Private);
			return this;
		}

		@Override
		public IRemoConRelauncher setNamespace(String nsId, String namespace) {
			checkState();
			mConfig.setNamespace(new RcaId(nsId), GraphName.of(namespace));
			return this;
		}

		@Override
		public IRemoConRelauncher setNamespace(String nsId, GraphName namespace) {
			checkState();
			mConfig.setNamespace(new RcaId(nsId), namespace);
			return this;
		}

		@Override
		public IRemoConRelauncher setType(String tId, String type) {
			checkState();
			return setType(tId, type, getRcaContext().getRemoConConfig().getBaseConfApk());
		}

		@Override
		public IRemoConRelauncher setType(String tId, String type, String apk) {
			checkState();
			mConfig.setType(new RcaId(tId), new RcaId(type), apk);
			return this;
		}

		@Override
		public IRemoConRelauncher setParam(String name, double value) {
			checkState();
			return setParam(GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setParam(GraphName name, double value) {
			checkState();
			setParam(new RcaId(getIdName()), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setParam(String name, int value) {
			checkState();
			return setParam(GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setParam(GraphName name, int value) {
			checkState();
			setParam(new RcaId(getIdName()), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setParam(String name, boolean value) {
			checkState();
			return setParam(GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setParam(GraphName name, boolean value) {
			checkState();
			setParam(new RcaId(getIdName()), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setParam(String name, String value) {
			checkState();
			return setParam(GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setParam(GraphName name, String value) {
			checkState();
			setParam(new RcaId(getIdName()), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, String name, double value) {
			checkState();
			return setRemoCompParam(id, GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, GraphName name, double value) {
			checkState();
			setParam(new RcaId(id), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, String name, int value) {
			checkState();
			return setRemoCompParam(id, GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, GraphName name, int value) {
			checkState();
			setParam(new RcaId(id), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, String name, boolean value) {
			checkState();
			return setRemoCompParam(id, GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, GraphName name, boolean value) {
			checkState();
			setParam(new RcaId(id), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, String name, String value) {
			checkState();
			return setRemoCompParam(id, GraphName.of(name), value);
		}

		@Override
		public IRemoConRelauncher setRemoCompParam(String id, GraphName name, String value) {
			checkState();
			setParam(new RcaId(id), name, value);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemap(String from, String to) {
			checkState();
			setRemap(from, to, null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemap(GraphName from, GraphName to) {
			checkState();
			setRemap(from, to, null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemap(String from, String to, String nsId) {
			checkState();
			setRemap(new RcaId(getIdName()), GraphName.of(from), GraphName.of(to), nsId != null ? new RcaId(nsId) : null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemap(GraphName from, GraphName to, String nsId) {
			checkState();
			setRemap(new RcaId(getIdName()), from, to, nsId != null ? new RcaId(nsId) : null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompRemap(String id, String from, String to) {
			checkState();
			setRemoCompRemap(id, from, to, null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompRemap(String id, GraphName from, GraphName to) {
			checkState();
			setRemoCompRemap(id, from, to, null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompRemap(String id, String from, String to, String nsId) {
			checkState();
			setRemap(new RcaId(id), GraphName.of(from), GraphName.of(to), nsId != null ? new RcaId(nsId) : null);
			return this;
		}

		@Override
		public IRemoConRelauncher setRemoCompRemap(String id, GraphName from, GraphName to, String nsId) {
			checkState();
			setRemap(new RcaId(id), from, to, nsId != null ? new RcaId(nsId) : null);
			return this;
		}

		@Override
		public IRemoConRelauncher setAllToInitialState() {
			checkState();
			mConfig.setAllToInitialState();
			return this;
		}

		@Override
		public void commit() {
			checkState();
			try {
				getRcaContext().postRelaunchRemoCos(mConfig);
			} finally {
				mConfig = new MutablesConfig();
			}
		}

		private void setParam(IRcaId id, GraphName name, Object value) {
			if (!(value instanceof Double || value instanceof Integer || value instanceof Boolean || value instanceof String)) {
				throw new RcaRuntimeError("Parameter '" + name + "': unsupported type: " + value.getClass().getName() + ".");
			}
			mConfig.setParam(id, getRcaContext().getRootNameResolver().newChild(id.toString()).resolve(name), value);
		}

		private void setRemap(IRcaId id, GraphName from, GraphName to, IRcaId nsId) {
			mConfig.setRemapping(id, from, to, nsId);
		}

		private void checkState() {
			if (!getRemoCo().isOnImplThread()) {
				throw new NotOnImplThreadException(getIdName());
			}
			if (!getRemoCo().isCreated()) {
				throw new RemoCoNotCreatedException(getIdName());
			}
		}
	}

	private final RemoConRelauncher mRelauncher = new RemoConRelauncher();

	private JavaRemoConContext mImplContext = new JavaRemoConContext();
	private Map<String, RemoCompProxy> mRemoCompProxies = new HashMap<String, RemoCompProxy>();

	private class JavaRemoConConfig extends AJavaRemoCoConfig implements IJavaRemoConConfig {
	}

	private final JavaRemoConConfig mConfig = new JavaRemoConConfig();

	private class JavaRemoConLayout extends AJavaRemoCoLayout implements IJavaRemoConLayout {
		@Override
		public void setOnKeyListener(IOnKeyListener listener) {
			mKeyListener = listener;
		}
	}

	private final JavaRemoConLayout mLayout = new JavaRemoConLayout();
	private IOnKeyListener mKeyListener = null;

	private class RemoCompProxy implements IRemoCompProxy {
		private final RemoCompListener mRemoCompListener = new RemoCompListener();
		private final IRemoComp mRemoComp;

		private class RemoCompListener implements IRcaEventListener {
			private IRemoCompEventsListener mEventsListener;
			private IOnRemoCompLaunchedListener mOnLaunchedListener;

			public synchronized void setEventsListener(IRemoCompEventsListener listener) {
				mEventsListener = listener;
				update();
			}

			public synchronized void setOnLaunchedListener(IOnRemoCompLaunchedListener listener) {
				mOnLaunchedListener = listener;
				update();
			}

			@Override
			public void onRcaEvent(String name, final Object data) {
				if (name.equals(RemoCompLaunchedEvent.EVENT_NAME)) {
					if (mOnLaunchedListener != null) {
						runOnImplThread(new ICheckedRunnable() {
							@Override
							public void run() throws Throwable {
								final IOnRemoCompLaunchedListener listener = mOnLaunchedListener;
								if (listener != null) {
									listener.onRemoCompLaunched(RemoCompProxy.this);
								}
							}
						});
					}
				} else if (name.equals(RemoCoCustomEvent.EVENT_NAME)) {
					final RemoCoCustomEvent castedData = (RemoCoCustomEvent) data;
					if (mEventsListener != null) {
						runOnImplThread(new ICheckedRunnable() {
							@Override
							public void run() throws Throwable {
								final IRemoCompEventsListener listener = mEventsListener;
								if (listener != null) {
									listener.onRemoCompEvent(RemoCompProxy.this, castedData.name, castedData.data);
								}
							}
						});
					}
				}
			}

			private void update() {
				if (mEventsListener == null && mOnLaunchedListener == null) {
					mRemoComp.removeListener(mRemoCompListener);
				} else {
					mRemoComp.addListener(mRemoCompListener);
				}
			}
		}

		private RemoCompProxy(IRemoComp remoComp) {
			mRemoComp = remoComp;
		}

		@Override
		public String getType() {
			return mRemoComp.getType();
		}

		@Override
		public String getApk() {
			return mRemoComp.getApk();
		}

		@Override
		public String getId() {
			return mRemoComp.getIdName();
		}

		@Override
		public IRemoCompBinder getBinder() {
			return new IRemoCompBinder() {
				@Override
				public Object invokeMethod(String name, Object... args) throws RemoCompBinderInvocationException {
					try {
						return mRemoComp.getBinder().invokeMethod(name, args);
					} catch (RcaBinderInvocationException e) {
						throw new RemoCompBinderInvocationException(getIdName(), mRemoComp.getIdName(), name, e);
					}
				}
			};
		}

		@Override
		public void setOnLaunchedListener(IOnRemoCompLaunchedListener listener) {
			mRemoCompListener.setOnLaunchedListener(listener);
		}

		@Override
		public void setEventsListener(IRemoCompEventsListener listener) {
			mRemoCompListener.setEventsListener(listener);
		}

		public void removeAllListeners() {
			setEventsListener(null);
			setOnLaunchedListener(null);
		}
	}

	public JavaRemoConImpl(IRcaApplication app, String idName, String type, String apk, IJavaRemoConImpl javaImpl) {
		super(app, idName, type, apk, javaImpl);
	}

	@Override
	protected IJavaRemoConImpl getImpl() {
		return (IJavaRemoConImpl) super.getImpl();
	}

	@Override
	public IRcaBinder create(IRemoCon remoCon) {
		super.create(remoCon);
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().init(mImplContext);
			}
		});
		return new ReflectingBinder(new BinderImpl());
	}

	private static class BinderImpl implements IReflectingBinderImpl {
	}

	@Override
	public void start(final NodeConfiguration nodeConfiguration) {
		super.start(nodeConfiguration);
		final JavaRemoConConfiguration configuration = new JavaRemoConConfiguration();
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().start(configuration);
			}
		});

		getRcaContext().setKeyListener(new IRcaOnKeyListener() {
			@Override
			public boolean onKey(int keyCode, @NotNull KeyEvent event) {
				try {
					return getRcaContext().isRemoConActive() && mKeyListener != null && mKeyListener.onKey(keyCode, event);
				} catch (Throwable e) {
					getRemoCo().handleErrorException(e);
					return true;
				}
			}
		});
	}

	@Override
	public void shutdown() {
		getRcaContext().setKeyListener(null);
		super.shutdown();
		for (RemoCompProxy proxy : mRemoCompProxies.values()) {
			proxy.removeAllListeners();
		}
		mRemoCompProxies.clear();
	}

	@Override
	public String getIdName(int id) {
		return getApkContext().getResources().getResourceEntryName(id);
	}

}
