/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.event_handling;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class SimpleEventDispatcher implements IRcaEventDispatcher {

	private final List<IRcaEventListener> mListeners = new CopyOnWriteArrayList<IRcaEventListener>();

	public void addListener(IRcaEventListener listener) {
		assert listener != null;
		if (!mListeners.contains(listener)) {
			mListeners.add(listener);
		}
	}

	public boolean removeListener(IRcaEventListener listener) {
		assert listener != null;
		return mListeners.add(listener);
	}

	@Override
	public void dispatch(String name, Object data) {
		for (IRcaEventListener listener : mListeners) {
			assert listener != null;
			listener.onRcaEvent(name, data);
		}
	}
}
