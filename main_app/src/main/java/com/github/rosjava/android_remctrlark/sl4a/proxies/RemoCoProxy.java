/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventListener;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.github.rosjava.android_remctrlark.sl4a.RemoCoInfo;

public class RemoCoProxy extends APyProxy implements IRcaPyProxy {

	private final IRemoCo mRemoCo;
	private final RemoCoInfo mInfo;
	private final IRcaEventListener mEventListener = new IRcaEventListener() {
		@Override
		public void onRcaEvent(String name, Object data) {
			mRcaFacade.getEventDispatcher().dispatch(name, data);
		}
	};

	public RemoCoProxy(IRcaFacade rcaFacade, String id) throws RcaException {
		super(rcaFacade);
		if (!getRcaContext().isRemoConCreated()) {
			throw new RcaException("RemoCon not created yet!");
		}

		mRemoCo = getRcaContext().findRemoCo(id);
		if (mRemoCo == null) {
			throw new RcaException("Requested RemoCo with id '" + id + "' not found!");
		}

		mInfo = new RemoCoInfo(getMainFileNamesManager(), mRemoCo);
		mRemoCo.addListener(mEventListener);
	}

	public RemoCoInfo getInfo() {
		return mInfo;
	}

	@Override
	public IRcaBinder getBinder() {
		return mRemoCo.getBinder();
	}

	@Override
	public void shutdown() {
		mRemoCo.removeListener(mEventListener);
	}

}
