/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.RcaId;

import java.io.Serializable;

class RemoCoType extends ARemoCoMutable implements IRemoCoType, Serializable {

	static final String XML_TAG = "type";
	static final String XML_ATTR_ID = "id";
	static final String XML_ATTR_TYPE = "type";
	static final String XML_ATTR_APK = "apk";

	RcaId mId = null;
	RcaId mType = null;
	String mApk = null;

	@Override
	public IRcaId getId() {
		return mId;
	}

	@Override
	public IRcaId getType() {
		return mType;
	}

	@Override
	public String getApk() {
		return mApk;
	}

}
