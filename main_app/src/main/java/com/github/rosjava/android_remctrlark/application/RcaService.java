/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.github.rosjava.android_remctrlark.rca_logging.IRcaLog;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLogEntry;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogEntry;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.RemoCon;

import java.util.AbstractCollection;
import java.util.LinkedList;

public class RcaService extends Service {

/*
	// The following example code can be used for starting this service from a RCA plugin RemoCo for having the full
	// access to the main application's RCA-context. One shouldn't (wanna) do it, but if you are a hacker...
	// For getting it to work, you have have to include the RCA's main_app_api library, e.g. put the following line
	// into your dependencies in gradle:
	//   provided 'com.github.rosjava.android_remctrlark:main_app_api:0.1.+@jar'

	Intent intent = new Intent();
	intent.setClassName("com.github.rosjava.android_remctrlark", "com.github.rosjava.android_remctrlark.application.RcaService");
	context.startService(intent);
	Preconditions.checkState(context.bindService(intent, new ServiceConnection() {
		@Override public void onServiceConnected(ComponentName name, IBinder service) {
			IRcaService service1 = (IRcaService) service;
			IRcaContext context = service1.getContext();
			context.getSettings();
		}

		@Override public void onServiceDisconnected(ComponentName name) {
		}
	}, Context.BIND_AUTO_CREATE));
*/

	private static class FallbackLog extends RcaJavaLog {
		private final AbstractCollection<IRcaLogEntry> mPendingLogEntries = new LinkedList<IRcaLogEntry>();

		@Override
		protected void onLog(RcaLogLevel logLevel, String tag, String subTag, String msg, Throwable exception) {
			synchronized (mPendingLogEntries) {
				mPendingLogEntries.add(new RcaLogEntry(logLevel, System.currentTimeMillis(), tag, subTag, msg, exception));
			}
			handlePendingLogEntries();
		}

		public void handlePendingLogEntries() {
			final RemoCon remoCon = getRemoConInstance();
			if (remoCon == null) {
				return;
			}
			synchronized (mPendingLogEntries) {
				if (!mPendingLogEntries.isEmpty()) {
					for (IRcaLogEntry entry : mPendingLogEntries) {
						remoCon.addLogEntry(entry);
					}
					mPendingLogEntries.clear();
				}
			}
		}
	}

	static private final FallbackLog mFallbackLog = new FallbackLog();
	static IRcaContext mRcaContext = null;
	static IRcaApplication mRcaApp = null;
	static IRcaMutableSettings mMutableSettings = null;

	private class LocalBinder extends Binder implements IRcaService {
		@Override
		public IRcaContext getContext() {
			return RcaService.getContext();
		}
	}

	private final Binder mBinder = new LocalBinder();

	public static IRcaLog getLog() {
		final RemoCon remoCon = getRemoConInstance();
		if (remoCon != null) {
			mFallbackLog.handlePendingLogEntries();
			return remoCon.getLog();
		}
		return mFallbackLog;
	}

	static public IRcaContext getContext() {
		return mRcaContext;
	}

	static public IRcaApplication getApp() {
		return mRcaApp;
	}

	private static RemoCon getRemoConInstance() {
		if (mRcaContext != null) {
			// Note: Using method {@link IRcaContext#getRemoCon()} could lead to delays and deadlocks here! We need
			// direct access here.
			return ((RemoConMainActivity) mRcaContext).getRemoConInstance();
		}
		return null;
	}

	static public IRcaMutableSettings getMutableSettings() {
		return mMutableSettings;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
}
