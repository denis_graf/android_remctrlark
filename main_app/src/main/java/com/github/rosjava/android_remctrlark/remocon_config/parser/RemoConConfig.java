/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigGlobals;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigSettings;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConSpecialization;
import com.github.rosjava.android_remctrlark.remocon_config.ISerializableRemoConConfig;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

class RemoConConfig extends ARemoCoConfig implements ISerializableRemoConConfig {

	static final String XML_TAG = "remocon";
	static final String XML_ATTR_BASECONF = "baseconf";
	static final String XML_ATTR_BASECONFAPK = "baseconfapk";
	static final String XML_ATTR_LOGO = "logo";
	static final String XML_ATTR_MASTERURI = "masteruri";
	static final String XML_ATTR_NEWMASTER = "newmaster";
	static final String XML_ATTR_MUTABLEMASTER = "mutablemaster";

	final String mConfigFileFullDir;
	String mLogo;
	String mBaseConfApk;
	URI mMasterUri = null;
	String mNewMaster = null;
	boolean mMutableMaster = true;
	final RemoConConfigSettings mSettings = new RemoConConfigSettings();
	final RemoConConfigGlobals mGlobals = new RemoConConfigGlobals();
	final List<IRemoConSpecialization> mSpecializations = new ArrayList<IRemoConSpecialization>();

	RemoConConfig(String configFileFullPath) {
		mConfigFileFullDir = configFileFullPath;
	}

	@Override
	public String getConfigFileFullDir() {
		return mConfigFileFullDir;
	}

	@Override
	public String getBaseConfApk() {
		return mBaseConfApk;
	}

	@Override
	public String getLogo() {
		return mLogo;
	}

	@Override
	public URI getMasterUri() {
		return mMasterUri;
	}

	@Override
	public String getNewMaster() {
		return mNewMaster;
	}

	@Override
	public boolean isMasterMutable() {
		return mMutableMaster;
	}

	@Override
	public IRemoConConfigSettings getSettings() {
		return mSettings;
	}

	@Override
	public IRemoConConfigGlobals getGlobals() {
		return mGlobals;
	}

	@Override
	public List<IRemoConSpecialization> getSpecializations() {
		return mSpecializations;
	}

}
