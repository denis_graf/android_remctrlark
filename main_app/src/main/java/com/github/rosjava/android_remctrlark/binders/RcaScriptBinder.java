/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.binders;

import android.annotation.SuppressLint;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.ErrorInScriptException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventDispatcher;
import com.github.rosjava.android_remctrlark.event_handling.RcaEvents;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFieldsToJsonMappable;
import com.google.common.base.Preconditions;
import com.googlecode.android_scripting.jsonrpc.JsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;


public class RcaScriptBinder implements IRcaBinder, IRcaScriptBinder {

	protected static String INTERNAL_RPC_HANDLING_PREFIX = "rca.rpc.handle";

	class AsyncResult {

		final CountDownLatch latch = new CountDownLatch(1);
		Object result = null;
	}

	@SuppressLint("UseSparseArrays")
	private final HashMap<Integer, AsyncResult> mAsyncResultMap = new HashMap<Integer, AsyncResult>();
	private final RcaJavaLog mLog;
	private Integer mLastRpcId = 0;
	private List<IRcaBinderMethod> mMethodsInfo = null;
	private IRcaEventDispatcher mScriptClient = null;
	private String mScriptId = "<unknown>";
	private final Object mScriptClientLock = new Object();

	public RcaScriptBinder(String logTag) {
		mLog = new RcaJavaLog(logTag);
	}

	@Override
	public final void bind(String scriptId, IRcaEventDispatcher client, List<IRcaBinderMethod> methodsInfo) {
		String state = null;
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument(mScriptClient != client || client == null);
			if (client == null) {
				state = "unbinding";
			} else if (mScriptClient == null) {
				state = "binding";
			} else {
				state = "rebinding";
			}
			mLog.d(state + " script...");
		}
		final Collection<AsyncResult> unfinishedMethodInvocations;
		synchronized (mScriptClientLock) {
			if (mScriptClient != client) {
				synchronized (mAsyncResultMap) {
					unfinishedMethodInvocations = mAsyncResultMap.values();
				}
			} else {
				unfinishedMethodInvocations = null;
			}
			mScriptId = scriptId != null ? scriptId : "<unknown>";
			mScriptClient = client;
			mMethodsInfo = methodsInfo;
		}
		if (unfinishedMethodInvocations != null && !unfinishedMethodInvocations.isEmpty()) {
			mLog.w("closing " + Integer.toString(unfinishedMethodInvocations.size()) + " unfinished method invocation(s)...");
			for (AsyncResult asyncResult : unfinishedMethodInvocations) {
				asyncResult.result = new RcaException("Script unbound during method invocation.");
				asyncResult.latch.countDown();
			}
		}
		if (BuildConfig.DEBUG) {
			mLog.d(state + " script done");
		}
	}

	@Override
	public final void unbind() {
		bind(null, null, null);
	}

	@Override
	public Object invokeMethod(String name, Object[] args) throws RcaBinderInvocationException {
		JSONArray jsonArgs;
		try {
			jsonArgs = (JSONArray) JsonBuilder.build(args);
		} catch (Exception e) {
			throw new RcaBinderInvocationException(name, e);
		}
		return invokeMethod(name, jsonArgs);
	}

	@Override
	public Object invokeMethod(String name, JSONArray args) throws RcaBinderInvocationException {
		if (name.equals(INTERNAL_RPC_HANDLING_PREFIX + "Result")) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(args.length() == 2);
			}
			try {
				rpcHandleResult(args.getInt(0), args.get(1));
			} catch (Exception e) {
				throw new RcaBinderInvocationException(name, e);
			}
			return null;
		}

		if (name.equals(INTERNAL_RPC_HANDLING_PREFIX + "Error")) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(args.length() == 2);
			}
			try {
				rpcHandleError(args.getInt(0), args.getJSONObject(1));
			} catch (Exception e) {
				throw new RcaBinderInvocationException(name, e);
			}
			return null;
		}

		return rpcInvokeMethod(name, args);
	}

	@Override
	public final List<IRcaBinderMethod> getMethods() {
		List<IRcaBinderMethod> result;
		if (mMethodsInfo != null) {
			result = new ArrayList<IRcaBinderMethod>(mMethodsInfo.size());
			result.addAll(mMethodsInfo);
		} else {
			result = new ArrayList<IRcaBinderMethod>();
		}
		return result;
	}

	public final Object rpcInvokeMethod(String name, JSONArray args) throws RcaBinderInvocationException {
		// This is a request for a RPC.
		// 1. Send an event from client to server for a RPC, and wait for the result.
		// 2a. After successful execution of RPS, server calls 'rca.rpc.success' of this binder.
		// 2b. After failed execution of RPS, server calls 'rca.rpc.fail' of this binder.
		try {
			final Integer rpcId;
			AsyncResult asyncResult;
			String logCallToString = null;
			synchronized (mScriptClientLock) {
				if (mScriptClient == null) {
					throw new RcaException("Binder not bound to script.");
				}

				synchronized (mAsyncResultMap) {
					rpcId = ++mLastRpcId;
					asyncResult = new AsyncResult();
					mAsyncResultMap.put(rpcId, asyncResult);
				}

				final String fName = name;
				final JSONArray fArgs = args;
				//noinspection UnusedDeclaration
				IRcaFieldsToJsonMappable data = new IRcaFieldsToJsonMappable() {
					public int rpc_id = rpcId;
					public String name = fName;
					public JSONArray args = fArgs;
				};
				if (BuildConfig.DEBUG) {
					String argsToString = "...";
					try {
						argsToString = fArgs.toString();
					} catch (Throwable ignored) {
					}
					logCallToString = ": [" + rpcId + "]" + fName.substring(fName.lastIndexOf(".") + 1) + "(" + argsToString + ")";
					mLog.d("RPC enter " + logCallToString);
				}
				mScriptClient.dispatch(RcaEvents.CALL_METHOD, data);
			}

			asyncResult.latch.await();

			synchronized (mAsyncResultMap) {
				mAsyncResultMap.remove(rpcId);
			}

			Object result = asyncResult.result;
			if (BuildConfig.DEBUG) {
				String resultToString = "...";
				try {
					resultToString = result.toString();
				} catch (Throwable ignored) {
				}
				mLog.d("rpc returned " + logCallToString + " -> " + resultToString);
			}
			if (result instanceof Exception) {
				throw (Exception) result;
			}
			return result;
		} catch (Exception e) {
			final RcaBinderInvocationException rcaException = new RcaBinderInvocationException(name, e);
			final RcaException calleeException = new RcaException("Error on serving a binder call.", rcaException);
			final IRemoCo remoCo = RcaService.getContext().findRemoCo(mScriptId);
			if (remoCo != null) {
				remoCo.handleErrorException(calleeException);
			} else {
				RcaService.getLog().w(calleeException);
			}
			throw rcaException;
		}
	}

	public final void rpcHandleResult(int rpcId, Object result) throws JSONException {
		// This is a request for the result of the already done RPC.
		AsyncResult asyncResult;
		synchronized (mAsyncResultMap) {
			asyncResult = mAsyncResultMap.get(rpcId);
		}
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(asyncResult.result == null);
		}
		asyncResult.result = result;
		asyncResult.latch.countDown();
	}

	public final void rpcHandleError(int rpcId, JSONObject excInfo) throws JSONException {
		// This is a request for handling the occurred error during the RPC.
		AsyncResult asyncResult;
		synchronized (mAsyncResultMap) {
			asyncResult = mAsyncResultMap.get(rpcId);
		}
		asyncResult.result = new ErrorInScriptException(mScriptId, excInfo);
		asyncResult.latch.countDown();
	}

}
