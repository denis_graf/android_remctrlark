/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.binders.IRcaScriptBinder;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.sl4a.proxies.IScriptProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.ScriptBinderServerProxy;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

public abstract class ARcaScript implements IRcaScript {
	public static final String MSG_SCRIPT_EXIT_REACHED = "rca.script_exit_reached";

	private final Object mShutdownLock = new Object();
	private boolean mScriptExitReached = false;
	private boolean mScriptShutdown = false;
	private boolean mScriptShutdownComplete = false;

	private final String mId;
	protected IRcaFacade mRcaFacade;
	private IRcaScriptBinder mBinder;
	private IScriptProxy mScriptProxy = null;
	private ScriptBinderServerProxy mScriptBinderServerProxy = null;
	private String mHostname = null;
	private int mPid = -1;

	public ARcaScript(IRcaFacade rcaFacade, String id, IRcaScriptBinder binder) {
		mRcaFacade = rcaFacade;
		mId = id;
		mBinder = binder;
		mRcaFacade.putPyScript(this);
	}

	protected void setBinder(IRcaScriptBinder binder) {
		mBinder = binder;
	}

	protected abstract void onBound();

	protected abstract void onShutdown();

	protected abstract void onShutdownComplete();

	protected abstract void onScriptExitReached();

	protected abstract void onUnprocessedScriptProcessMessage(@NotNull String message);

	@Override
	public final void bindScriptProxy(IScriptProxy proxy) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mBinder != null);
			Preconditions.checkState(mScriptProxy == null);
		}
		mScriptProxy = proxy;
	}

	@Override
	public final void onShutdownScriptProxy() {
		shutdown();
		mScriptProxy = null;
		shutdownComplete();
	}

	@Override
	public final void bindScriptBinderServerProxy(ScriptBinderServerProxy proxy) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mBinder != null);
			Preconditions.checkState(mScriptProxy != null);
			Preconditions.checkState(mScriptBinderServerProxy == null);
		}
		mScriptBinderServerProxy = proxy;
		mHostname = mScriptBinderServerProxy.getHostname();
		mPid = mScriptBinderServerProxy.getPid();
		// Now hostname and pid are known and set so the script can be reput into RCA facade.
		mRcaFacade.putPyScript(this);
		onBound();
	}

	@Override
	public final void onShutdownScriptBinderServerProxy() {
		shutdown();
		mScriptBinderServerProxy = null;
		shutdownComplete();
	}

	@Override
	public final void onScriptProcessMessage(@NotNull String message) {
		if (message.equals(MSG_SCRIPT_EXIT_REACHED)) {
			scriptExitReached();
		} else {
			onUnprocessedScriptProcessMessage(message);
		}
	}

	@Override
	public final String getId() {
		return mId;
	}

	@Override
	public final String getHostname() {
		return mHostname;
	}

	@Override
	public final int getPid() {
		return mPid;
	}

	@Override
	public final IRcaScriptBinder getBinder() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mBinder != null);
		}
		return mBinder;
	}

	private void shutdown() {
		if (mScriptProxy != null && mScriptBinderServerProxy != null) {
			scriptShutdown();
			if (mRcaFacade != null) {
				mRcaFacade.removePyScript(this);
				mRcaFacade = null;
			}
			mBinder = null;
		}
	}

	private void shutdownComplete() {
		if (mScriptProxy == null && mScriptBinderServerProxy == null) {
			IRcaScript script = null;
			try {
				script = mRcaFacade.getScriptById(mId);
			} catch (Throwable ignored) {
			}
			if (script != null) {
				// Note: It's important that the script is unregistered, if we want to relaunch it. After an abnormal
				// shutdown the script can be still not unregistered.
				mRcaFacade.removePyScript(script);
			}
			scriptShutdownComplete();
		}
	}

	protected final IRcaContext getRcaContext() {
		return mRcaFacade.getRcaContext();
	}

	private void scriptShutdown() {
		synchronized (mShutdownLock) {
			if (!mScriptShutdown) {
				mScriptShutdown = true;
				onShutdown();
			}
		}
	}

	private void scriptExitReached() {
		synchronized (mShutdownLock) {
			if (!mScriptExitReached) {
				mScriptExitReached = true;
				onScriptExitReached();
			}
		}
	}

	private void scriptShutdownComplete() {
		synchronized (mShutdownLock) {
			if (!mScriptShutdownComplete) {
				// Note: Guarantee that all shutdown methods are called!
				scriptShutdown();
				scriptExitReached();
				onShutdownComplete();
				mScriptShutdownComplete = true;
			}
		}
	}
}
