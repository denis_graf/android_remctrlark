/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.googlecode.android_scripting.event.Event;
import com.googlecode.android_scripting.facade.EventFacade;

import org.jetbrains.annotations.NotNull;

public class ProxyProxy extends APyProxy implements IRcaPyProxy {

	private final IRcaPyProxy mProxy;

	private final EventFacade.EventObserver mEventObserver = new EventFacade.EventObserver() {
		@Override
		public void onEventReceived(Event event) {
			mRcaFacade.getEventDispatcher().dispatch(event.getName(), event.getData());
		}
	};

	public ProxyProxy(@NotNull IRcaFacade rcaFacade, @NotNull IRcaPyProxy proxy) {
		super(rcaFacade);
		mProxy = proxy;
		mProxy.addEventObserver(mEventObserver);
	}

	@Override
	public IRcaBinder getBinder() {
		return mProxy.getBinder();
	}

	@Override
	public void shutdown() {
		mProxy.removeEventObserver(mEventObserver);
	}

}
