/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.binders;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.google.common.base.Preconditions;

import org.json.JSONArray;

public class RcaScriptTwoWaysBinder extends RcaScriptBinder {
	static private final String INTERNAL_PYTHON_METHODS_PREFIX = "rca.script.python.";
	private static final String INTERNAL_JAVA_METHODS_PREFIX = "rca.script.java.";

	private final IRcaBinder mInternalBinder;

	public RcaScriptTwoWaysBinder(String logTag, IRcaBinder internalBinder) {
		super(logTag);
		mInternalBinder = internalBinder;
	}

	public final Object invokeInternalMethod(String name, Object[] args) throws RcaBinderInvocationException {
		return invokeMethod(INTERNAL_PYTHON_METHODS_PREFIX + name, args);
	}

	@Override
	public final Object invokeMethod(String name, Object[] args) throws RcaBinderInvocationException {
		return invokeMethod(name, args, null);
	}

	@Override
	public final Object invokeMethod(String name, JSONArray args) throws RcaBinderInvocationException {
		return invokeMethod(name, null, args);
	}

	private Object invokeMethod(final String name, final Object[] argsObj, final JSONArray argsJson) throws RcaBinderInvocationException {
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument((argsJson == null) != (argsObj == null));
		}

		if (name.startsWith(INTERNAL_JAVA_METHODS_PREFIX)) {
			return invokeInternalMethod(name, argsObj, argsJson);
		}

		if (name.startsWith(INTERNAL_RPC_HANDLING_PREFIX)) {
			return invokeRpcHandlingMethod(name, argsObj, argsJson);
		}

		return invokeScriptMethod(name, argsObj, argsJson);
	}

	protected Object invokeRpcHandlingMethod(String name, Object[] argsObj, JSONArray argsJson) throws RcaBinderInvocationException {
		return superInvokeMethod(name, argsObj, argsJson);
	}

	protected Object invokeScriptMethod(final String name, final Object[] argsObj, final JSONArray argsJson) throws RcaBinderInvocationException {
		return superInvokeMethod(name, argsObj, argsJson);
	}

	protected Object invokeInternalMethod(String name, Object[] argsObj, JSONArray argsJson) throws RcaBinderInvocationException {
		return argsObj != null ?
				mInternalBinder.invokeMethod(name.substring(INTERNAL_JAVA_METHODS_PREFIX.length()), argsObj) :
				mInternalBinder.invokeMethod(name.substring(INTERNAL_JAVA_METHODS_PREFIX.length()), argsJson);
	}

	protected final Object superInvokeMethod(String name, Object[] argsObj, JSONArray argsJson) throws RcaBinderInvocationException {
		return argsObj != null ?
				RcaScriptTwoWaysBinder.super.invokeMethod(name, argsObj) :
				RcaScriptTwoWaysBinder.super.invokeMethod(name, argsJson);
	}
}
