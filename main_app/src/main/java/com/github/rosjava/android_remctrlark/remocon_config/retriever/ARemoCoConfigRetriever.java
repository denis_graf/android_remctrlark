/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.retriever;

import android.util.Pair;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoNamespace;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoParam;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoRemapping;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoSpecCondition;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoSpecialization;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class ARemoCoConfigRetriever implements IRemoCoConfigRetriever {

	private static class MutableRemoCoNamespace implements IRemoCoNamespace {
		private final IRcaId mNsId;
		private final GraphName mNs;

		public MutableRemoCoNamespace(IRcaId nsId, GraphName ns) {
			mNsId = nsId;
			mNs = ns;
		}

		@Override
		public IRcaId getId() {
			return mNsId;
		}

		@Override
		public GraphName getNamespace() {
			return mNs;
		}

		@Override
		public final boolean isMutable() {
			return true;
		}
	}

	private static class MutableRemoCoType implements IRemoCoType {
		private final IRcaId mTId;
		private final IRcaId mType;
		private final String mApk;

		public MutableRemoCoType(IRcaId tId, IRcaId type, String apk) {
			mTId = tId;
			mType = type;
			mApk = apk;
		}

		@Override
		public IRcaId getId() {
			return mTId;
		}

		@Override
		public IRcaId getType() {
			return mType;
		}

		@Override
		public String getApk() {
			return mApk;
		}

		@Override
		public final boolean isMutable() {
			return true;
		}
	}

	private static class ResolvedRemoCoParam implements IRemoCoParam {
		private final GraphName mName;
		private final Object mValue;
		private final String mType;
		private final boolean mMutable;

		public ResolvedRemoCoParam(ARemoCoConfigRetriever retriever, GraphName name, Object value, boolean mutable) {
			mName = retriever.getNameResolver().resolve(name);
			mValue = value;
			if (mValue instanceof Double || mValue instanceof Float) {
				mType = "double";
			} else if (mValue instanceof Integer || mValue instanceof Short || mValue instanceof Character) {
				mType = "int";
			} else if (mValue instanceof Boolean) {
				mType = "bool";
			} else /*if (mValue instanceof String)*/ {
				mType = "str";
			}
			mMutable = mutable;
		}

		@Override
		public GraphName getName() {
			return mName;
		}

		@Override
		public Object getValue() {
			return mValue;
		}

		@Override
		public String getType() {
			return mType;
		}

		@Override
		public boolean isMutable() {
			return mMutable;
		}
	}

	protected static class ResolvedRemoCoRemapping implements IRemoCoRemapping {

		private GraphName mFrom;
		private GraphName mTo;
		private IRcaId mNsId;
		private boolean mMutable;

		public ResolvedRemoCoRemapping(ARemoCoConfigRetriever retriever, GraphName from, GraphName to, IRcaId nsId, boolean mutable) {
			final GraphName resolvedNamespace = retriever.retrieveResolvedNamespace(nsId);
			mFrom = retriever.getNameResolver().resolve(from);
			mTo = resolvedNamespace.join(to);
			mNsId = nsId != null ? nsId : retriever.getId();
			mMutable = mutable;
		}

		@Override
		public GraphName getFrom() {
			return mFrom;
		}

		@Override
		public GraphName getTo() {
			return mTo;
		}

		@Override
		public IRcaId getNsId() {
			return mNsId;
		}

		@Override
		public boolean isMutable() {
			return mMutable;
		}
	}

	private IRemoConConfig mRemoConConfig;
	private NameResolver mRootNameResolver;
	private NameResolver mNameResolver;

	/**
	 * Needed for serialization/deserialization.
	 */
	protected ARemoCoConfigRetriever() {
	}

	protected ARemoCoConfigRetriever(@NotNull IRemoConConfig config, @NotNull IRcaId id, NameResolver rootNameResolver) {
		mRemoConConfig = config;
		mRootNameResolver = rootNameResolver;
		mNameResolver = mRootNameResolver.newChild(id.toString());
	}

	@NotNull
	@Override
	public final URI retrieveMasterUri() {
		URI result = null;
		if (retrieveNewMaster() != RemoConConfigNewMaster.None) {
			// A new master has been specified which has a higher priority, hence we ignore the master URI.
			return RcaGlobalConstants.NULL_URI;
		}
		if (isMasterMutable()) {
			result = getMutablesConfig().getMasterUri();
		}
		if (result == null || result == RcaGlobalConstants.NULL_URI) {
			result = mRemoConConfig.getMasterUri();
		}
		return result != null && result != RcaGlobalConstants.NULL_URI ? result.normalize() : RcaGlobalConstants.NULL_URI;
	}

	@NotNull
	@Override
	public final RemoConConfigNewMaster retrieveNewMaster() {
		if (isMasterMutable()) {
			final RemoConConfigNewMaster newMaster = getMutablesConfig().getNewMaster();
			if (newMaster != null) {
				return newMaster;
			}
		}
		return convertNewMasterAttr(mRemoConConfig.getNewMaster());
	}

	@Override
	public final boolean isMasterMutable() {
		return mRemoConConfig.isMasterMutable();
	}

	@Override
	public boolean containsTypeDefinition() {
		return retrieveConfigType(retrieveTypeId()) != null;
	}

	@NotNull
	@Override
	public final IRcaId retrieveType() {
		return retrieveType(retrieveTypeId());
	}

	@NotNull
	@Override
	public final String retrieveApk() {
		return retrieveApk(retrieveTypeId());
	}

	@NotNull
	public final IRcaId retrieveType(IRcaId tId) {
		final IRemoCoType type = retrieveConfigType(tId);
		if (type != null) {
			return type.getType();
		}
		return tId;
	}

	@NotNull
	public String retrieveApk(IRcaId tId) {
		final IRemoCoType type = retrieveConfigType(tId);
		if (type != null && type.getApk() != null) {
			return type.getApk();
		} else {
			return getRemoConConfig().getBaseConfApk();
		}
	}

	@NotNull
	@Override
	public final Map<GraphName, Object> retrieveParams() {
		final Map<GraphName, Object> params = new HashMap<GraphName, Object>();
		for (IRemoCoParam param : retrieveResolvedConfigParams()) {
			params.put(param.getName(), param.getValue());
		}
		return params;
	}

	@NotNull
	@Override
	public final Collection<IRemoCoParam> retrieveResolvedConfigParams() {
		final Map<GraphName, IRemoCoParam> configParams = new HashMap<GraphName, IRemoCoParam>();
		if (getRemoConConfig().getSettings().isDebug()) {
			final ResolvedRemoCoParam debugParam = new ResolvedRemoCoParam(this, GraphName.of("DEBUG"), true, true);
			configParams.put(debugParam.getName(), debugParam);
		}

		final List<? extends IRemoCoSpecialization> specs = retrieveSpecializations();
		for (IRemoCoSpecialization spec : specs) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(conditionMatch(spec.getCondition()));
			}
			for (IRemoCoParam param : spec.getParams()) {
				final ResolvedRemoCoParam value = new ResolvedRemoCoParam(this, param.getName(), param.getValue(), param.isMutable());
				configParams.put(value.getName(), value);
			}
		}

		final Map<GraphName, Object> mutables = getMutablesConfig().getParams().get(getId());
		if (mutables != null) {
			for (Map.Entry<GraphName, Object> mutable : mutables.entrySet()) {
				final GraphName name = mutable.getKey();
				final IRemoCoParam param = configParams.get(name);
				if (param == null || param.isMutable()) {
					final ResolvedRemoCoParam value = new ResolvedRemoCoParam(this, name, mutable.getValue(), true);
					configParams.put(value.getName(), value);
				}
			}
		}

		return configParams.values();
	}

	@NotNull
	@Override
	public final Map<GraphName, GraphName> retrieveRemappings() {
		final Map<GraphName, GraphName> remappings = new HashMap<GraphName, GraphName>();
		for (IRemoCoRemapping remapping : retrieveResolvedConfigRemappings()) {
			remappings.put(remapping.getFrom(), remapping.getTo());
		}
		return remappings;
	}

	@NotNull
	@Override
	public final Collection<IRemoCoRemapping> retrieveResolvedConfigRemappings() {
		final Map<GraphName, IRemoCoRemapping> configRemppings = new HashMap<GraphName, IRemoCoRemapping>();
		final List<? extends IRemoCoSpecialization> specs = retrieveSpecializations();
		for (IRemoCoSpecialization spec : specs) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(conditionMatch(spec.getCondition()));
			}
			for (IRemoCoRemapping remapping : spec.getRemappings()) {
				ResolvedRemoCoRemapping value = new ResolvedRemoCoRemapping(this,
						remapping.getFrom(),
						remapping.getTo(),
						remapping.getNsId(),
						remapping.isMutable());
				configRemppings.put(value.getFrom(), value);
			}
		}

		final Map<GraphName, Pair<GraphName, IRcaId>> mutables = getMutablesConfig().getRemappings().get(getId());
		if (mutables != null) {
			for (Map.Entry<GraphName, Pair<GraphName, IRcaId>> mutable : mutables.entrySet()) {
				final GraphName from = mutable.getKey();
				final IRemoCoRemapping remapping = configRemppings.get(from);
				if (remapping == null || remapping.isMutable()) {
					final GraphName to = mutable.getValue().first;
					final IRcaId nsId = mutable.getValue().second;
					final ResolvedRemoCoRemapping value = new ResolvedRemoCoRemapping(this, from, to, nsId, true);
					configRemppings.put(value.getFrom(), value);
				}
			}
		}

		return configRemppings.values();
	}

	protected abstract List<? extends IRemoCoSpecialization> retrieveSpecializations();

	@NotNull
	protected abstract IRemoConMutablesConfig getMutablesConfig();

	@NotNull
	protected final IRemoConConfig getRemoConConfig() {
		return mRemoConConfig;
	}

	@NotNull
	public final GraphName retrieveNamespace(IRcaId nsId) {
		if (nsId == null) {
			nsId = getId();
		}
		final IRemoCoNamespace ns = retrieveConfigNamespace(nsId);
		if (ns != null) {
			return ns.getNamespace();
		} else {
			return GraphName.of(nsId.toString());
		}
	}

	protected final IRemoCoNamespace retrieveConfigNamespace(final IRcaId nsId) {
		final IRemoCoNamespace namespace = mRemoConConfig.getGlobals().getNamespaces().get(nsId);
		if (namespace == null || namespace.isMutable()) {
			final GraphName mutable = getMutablesConfig().getNamespaces().get(nsId);
			if (mutable != null) {
				return new MutableRemoCoNamespace(nsId, mutable);
			}
		}
		return namespace;
	}

	protected GraphName retrieveResolvedNamespace(IRcaId nsId) {
		return getRootNameResolver().resolve(retrieveNamespace(nsId));
	}

	protected final IRemoCoType retrieveConfigType(final IRcaId tId) {
		final IRemoCoType type = mRemoConConfig.getGlobals().getTypes().get(tId);
		if (type == null || type.isMutable()) {
			final Pair<IRcaId, String> mutable = getMutablesConfig().getTypes().get(tId);
			if (mutable != null) {
				return new MutableRemoCoType(tId, mutable.first, mutable.second);
			}
		}
		return type;
	}

	protected boolean conditionMatch(IRemoCoSpecCondition condition) {
		return (condition.getType() == null || condition.getType().equals(retrieveType())) &&
				(condition.getApk() == null || condition.getApk().equals(retrieveApk())) &&
				(condition.getMasterUri() == null || equalUris(condition.getMasterUri(), retrieveMasterUri())) &&
				(condition.getNewMaster() == null || convertNewMasterAttr(condition.getNewMaster()) == retrieveNewMaster());
	}

	private boolean equalUris(URI uri1, URI uri2) {
		return uri1.getScheme().equals(uri2.getScheme()) && uri1.getAuthority().equals(uri2.getAuthority());
	}

	@NotNull
	protected final NameResolver getNameResolver() {
		return mNameResolver;
	}

	@NotNull
	protected final NameResolver getRootNameResolver() {
		return mRootNameResolver;
	}

	@NotNull
	private RemoConConfigNewMaster convertNewMasterAttr(String newMaster) {
		if (newMaster != null && !newMaster.isEmpty()) {
			if (newMaster.equals("public")) {
				return RemoConConfigNewMaster.Public;
			} else if (newMaster.equals("private")) {
				return RemoConConfigNewMaster.Private;
			}
		}
		return RemoConConfigNewMaster.None;
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		// TODO: why do we need this, shouldn't it happen automatically?
		out.writeObject(mRemoConConfig);
		out.writeObject(mRootNameResolver);
		out.writeObject(mNameResolver);
	}

	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		// TODO: why do we need this, shouldn't it happen automatically?
		mRemoConConfig = (IRemoConConfig) in.readObject();
		mRootNameResolver = (NameResolver) in.readObject();
		mNameResolver = (NameResolver) in.readObject();
	}
}
