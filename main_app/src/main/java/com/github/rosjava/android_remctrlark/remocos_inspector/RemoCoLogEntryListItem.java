/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;


import android.view.View;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImplLogEntry;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoLogEntry;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class RemoCoLogEntryListItem {

	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");
	static final Map<View, RemoCoLogEntryListItem> mAssociatedViews = new HashMap<View, RemoCoLogEntryListItem>();

	public final IRemoCoLogEntry mLogEntry;
	public boolean mSelected = false;
	private View mAssociatedView = null;
	private String mTimeText = null;
	private String mShortText = null;
	private String mDetailsText = null;
	private String mTagText = null;
	private String mExceptionText = null;

	RemoCoLogEntryListItem(IRemoCoLogEntry logEntry) {
		mLogEntry = logEntry;
	}

	public IRemoCoLogEntry getLogEntry() {
		return mLogEntry;
	}

	public boolean isImplEntry() {
		return mLogEntry instanceof IRemoCoImplLogEntry;
	}

	public RcaLogLevel getLogLevel() {
		return mLogEntry.getLogLevel();
	}

	public boolean isSelected() {
		return mSelected;
	}

	public void setSelected(boolean selected) {
		mSelected = selected;
	}

	public View getAssociatedView() {
		return mAssociatedView;
	}

	public void setAssociatedView(View view) {
		// Attention: Views are recycled!
		if (view == null) {
			if (BuildConfig.DEBUG) {
				if (mAssociatedView != null) {
					final RemoCoLogEntryListItem oldItem = mAssociatedViews.get(mAssociatedView);
					Preconditions.checkState(oldItem == this);
				}
			}
			mAssociatedViews.remove(mAssociatedView);
			mAssociatedView = null;
			return;
		}
		final RemoCoLogEntryListItem oldItem = mAssociatedViews.get(view);
		if (oldItem != null) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(oldItem.mAssociatedView == view);
			}
			oldItem.setAssociatedView(null);
		}
		mAssociatedViews.put(view, this);
		mAssociatedView = view;
	}

	public String getRemoCoId() {
		return mLogEntry.getRemoCoId();
	}

	public long getTime() {
		return mLogEntry.getTime();
	}

	public boolean isExceptionCanceled() {
		final IRemoCo remoCo = RcaService.getContext().findRemoCo(mLogEntry.getRemoCoId(), true);
		return remoCo == null || !remoCo.containsException(getException());
	}

	public Throwable getException() {
		return mLogEntry.getException();
	}

	public String getTimeText() {
		if (mTimeText == null) {
			mTimeText = TIME_FORMAT.format(new Date(mLogEntry.getTime()));
		}
		return mTimeText;
	}

	public String getShortText() {
		if (mShortText == null) {
			//noinspection ThrowableResultOfMethodCallIgnored
			final Throwable exception = mLogEntry.getException();
			mShortText = joinStrings(mLogEntry.getMessage(), retrieveExceptionMessage(exception), " ");
		}
		return mShortText;
	}

	private String retrieveExceptionMessage(Throwable e) {
		if (e == null) {
			return null;
		}
		final String message = e.getMessage();
		return message == null || message.isEmpty() ? e.getClass().getSimpleName() : message;
	}

	public String getDetailsText() {
		if (mDetailsText == null) {
			mDetailsText = joinStrings(getTagText(), getExceptionText(), "\n\n");
		}
		return mDetailsText;
	}

	private String getTagText() {
		if (mTagText == null) {
			mTagText = "[" + joinStrings(mLogEntry.getTag(), mLogEntry.getSubTag(), ":") + "]";
		}
		return mTagText;
	}

	private String getExceptionText() {
		if (mExceptionText == null) {
			//noinspection ThrowableResultOfMethodCallIgnored
			final Throwable e = mLogEntry.getException();
			if (e == null) {
				return "";
			}
			Writer writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter(writer);
			e.printStackTrace(printWriter);
			mExceptionText = writer.toString();
		}
		return mExceptionText;
	}

	private String joinStrings(String first, String second, @NotNull String join) {
		String result = "";
		final boolean firstNotEmpty = isNotEmpty(first);
		final boolean secondNotEmpty = isNotEmpty(second);
		if (firstNotEmpty) {
			result += first;
			if (secondNotEmpty) {
				result += join;
			}
		}
		if (secondNotEmpty) {
			result += second;
		}
		return result;
	}

	private boolean isNotEmpty(String str) {
		return str != null && !str.isEmpty();
	}
}
