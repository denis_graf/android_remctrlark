/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.ARcaPluginsLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.IRcaLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.application.IRcaLoadingTask;
import com.github.rosjava.android_remctrlark.application.IRcaPlugin;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.error_handling.RcaTimeoutException;
import com.github.rosjava.android_remctrlark.misc.IdGenerator;
import com.github.rosjava.android_remctrlark.rca_logging.IRcaLog;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLoadingTask;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.IllegalOperationOnUiThreadException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImplLogEntry;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.ARemoCo;
import com.github.rosjava.android_remctrlark.ros_messages.RcaMessageFactory;
import com.github.rosjava.android_remctrlark.ros_messages.RcaMessageInterfaceClassProvider;
import com.github.rosjava.android_remctrlark.ros_messages.RcaMessageSerializationFactory;
import com.github.rosjava.android_remctrlark.ros_messages.RcaServiceRequestMessageFactory;
import com.github.rosjava.android_remctrlark.ros_messages.RcaServiceResponseMessageFactory;
import com.github.rosjava.android_remctrlark.ros_messages.RemoCoMessageDefinitionJarsProvider;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import org.apache.commons.io.filefilter.IOFileFilter;
import org.jetbrains.annotations.NotNull;
import org.ros.internal.message.StringFileProvider;
import org.ros.internal.message.definition.MessageDefinitionFileProvider;
import org.ros.internal.message.definition.MessageDefinitionProviderChain;
import org.ros.internal.message.service.ServiceDescriptionFactory;
import org.ros.internal.message.service.ServiceRequestMessageFactory;
import org.ros.internal.message.service.ServiceResponseMessageFactory;
import org.ros.internal.message.topic.TopicDescriptionFactory;
import org.ros.message.MessageDefinitionProvider;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;
import org.ros.node.NodeConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

public abstract class ARemoCoImpl implements IRemoCoImpl {

	@Override
	protected void finalize() throws Throwable {
		if (BuildConfig.DEBUG) {
			final String msg = "remoco impl instance finalized";
			boolean logged = false;
			try {
				getLog().d(msg);
				logged = true;
			} catch (Throwable ignored) {
			}
			if (!logged) {
				try {
					RcaService.getLog().d(msg + ", id='" + getIdName() + "'");
				} catch (Throwable ignored) {
				}
			}
		}
		super.finalize();
	}

	public static class LogEntry extends ARemoCo.LogEntry implements IRemoCoImplLogEntry {
		public LogEntry(String remoCoId, RcaLogLevel logLevel, long time, String tag, String subTag, String message, Throwable exception) {
			super(remoCoId, logLevel, time, tag, subTag, message, exception);
		}
	}

	private final IdGenerator mLoadingTaskIdGenerator = new IdGenerator();
	private MessageDefinitionProvider mMessageDefinitionProvider = null;
	private NodeConfiguration mDefaultNodeConfiguration = null;

	@NotNull
	protected static IRcaContext getRcaContext() {
		IRcaContext rcaContext = RcaService.getContext();
		assert rcaContext != null;
		return rcaContext;
	}

	private interface ISupervisedTask {
		public void done();
	}

	private class SupervisedTask implements ISupervisedTask {
		private RcaTimeoutException mWarning;
		private final TimerTask mTimerTask = new TimerTask() {
			@Override
			public void run() {
				onTimeout();
			}
		};

		private SupervisedTask(RcaTimeoutException timeoutWarning) {
			// Note: we propagate the exception for having the stack trace of where the task was started.
			mWarning = timeoutWarning;
			synchronized (mTasks) {
				mTasks.add(this);
			}
		}

		public void startSupervision() {
			getRcaContext().scheduleTask(mTimerTask, getRcaContext().getSettings().getRemoConBlockNotRespondingRemoCosTimeout());
		}

		@Override
		public void done() {
			synchronized (mTasks) {
				if (mTasks.remove(this) && !mTimerTask.cancel()) {
					mRemoCo.cancelException(mWarning);
				}
			}
		}

		private void onTimeout() {
			synchronized (mTasks) {
				if (mTasks.contains(this)) {
					mRemoCo.handleWarningException(mWarning);
				}
			}
		}
	}

	protected class RemoCoLoadingTask implements IRemoCoLoadingTask {
		private final String mMessage;
		private final int mId;
		private IRcaLoadingTask mTask = null;

		private RemoCoLoadingTask(String msg) {
			mMessage = msg;
			mId = mLoadingTaskIdGenerator.generate();
		}

		@Override
		public void done() {
			synchronized (mLoadingTasks) {
				if (mTask != null && mLoadingTasks.remove(getId()) != null) {
					mTask.done();
				}
			}
		}

		public int getId() {
			return mId;
		}

		private void start() {
			synchronized (mLoadingTasks) {
				if (mTask == null) {
					mTask = getRcaContext().startLoadingTask(mMessage);
					mLoadingTasks.put(getId(), this);
				}
			}
		}

		private void cancel() {
			synchronized (mLoadingTasks) {
				if (mTask != null && mLoadingTasks.remove(getId()) != null) {
					getLog().w("RemoCo task '" + mMessage + "' canceled.");
					mTask.done();
				}
			}
		}
	}

	private static final class LayoutInflaterFactory extends ARcaPluginsLayoutInflaterFactory {
		private final WeakReference<ARemoCoImpl> mParent;

		public LayoutInflaterFactory(ARemoCoImpl parent, ClassLoader classLoader) {
			super(parent.mApkContext, classLoader);
			mParent = new WeakReference<ARemoCoImpl>(parent);
		}

		@Override
		public View onErrorCreatingCustomViewClass(String name, Throwable e) {
			if (mParent.get() != null) {
				mParent.get().getRemoCo().handleErrorException(new RcaException("error on inflating custom view class '" + name +
						"' from package '" + mParent.get().mApk +
						"' with context '" + getRcaContext().getActivityContext().getPackageName() + "'", e));
			}
			return null;
		}
	}

	private boolean mShutdownImplThread = false;
	private final HandlerThread mImplThread;
	private final Handler mImplHandler;
	private final List<ISupervisedTask> mTasks = new LinkedList<ISupervisedTask>();
	@SuppressLint("UseSparseArrays")
	private final HashMap<Integer, RemoCoLoadingTask> mLoadingTasks = new HashMap<Integer, RemoCoLoadingTask>();
	private IRemoCo mRemoCo = null;

	private final IRcaApplication mApp;
	private final String mIdName;
	private final String mType;
	private final String mApk;
	private Context mApkContext = null;
	private IRcaLayoutInflaterFactory mApkViewFactory = null;
	private IRcaPlugin mPlugin = null;

	protected boolean mRebootRemoConOnRelaunch = false;

	public ARemoCoImpl(IRcaApplication app, String idName, String type, String apk) {
		assert app != null;
		assert idName != null;
		assert type != null;
		assert apk != null;
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument(!type.isEmpty());
			Preconditions.checkArgument(!apk.isEmpty());
		}

		mImplThread = new HandlerThread(idName + ".ImplThread");
		mImplThread.start();
		assert mImplThread.getLooper() != null;
		mImplHandler = new Handler(mImplThread.getLooper());

		mApp = app;
		mIdName = idName;
		mType = type;
		mApk = apk;

		try {
			mPlugin = getApp().getMainRemoCoImplsFactory().getPlugin(mApk);
			mApkContext = mPlugin.createContext();
			mApkViewFactory = new LayoutInflaterFactory(this, mPlugin.getClassLoader());
		} catch (RcaPluginCouldNotBeLoadedException e1) {
			getLog().e("plugin could not be loaded for remoco '" + idName + "', creating a fallback plugin", e1);
			mPlugin = getApp().getMainRemoCoImplsFactory().createFallbackPlugin(mApk);
			mApkContext = mPlugin.createContext();
			try {
				mApkViewFactory = new LayoutInflaterFactory(this, mPlugin.getClassLoader());
			} catch (RcaPluginCouldNotBeLoadedException e2) {
				throw new RcaRuntimeError("Plugin could not be loaded and no fallback could be created.'", e2);
			}
		}

		LayoutInflater.from(mApkContext).setFactory(mApkViewFactory);
	}

	@NotNull
	protected final IRemoCo getRemoCo() {
		assert mRemoCo != null;
		return mRemoCo;
	}

	@NotNull
	protected final IRcaApplication getApp() {
		return mApp;
	}

	protected final String getIdName() {
		return mIdName;
	}

	protected final String getType() {
		return mType;
	}

	protected final String getApk() {
		return mApk;
	}

/*
	@NotNull
	protected Context getPlugin() {
		return mPlugin;
	}
*/

	@NotNull
	protected Context getApkContext() {
		return mApkContext;
	}

	@NotNull
	@Override
	public IRcaLayoutInflaterFactory getApkViewFactory() {
		return mApkViewFactory;
	}

	protected void create(@NotNull IRemoCo remoCo) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mRemoCo == null);
		}
		mRemoCo = remoCo;
	}

	@Override
	public void start(final NodeConfiguration nodeConfiguration) {
		mDefaultNodeConfiguration = NodeConfiguration.copyOf(nodeConfiguration);
		final RcaMessageInterfaceClassProvider messageInterfaceClassProvider;
		try {
			messageInterfaceClassProvider = new RcaMessageInterfaceClassProvider(mPlugin.getClassLoader());
		} catch (RcaPluginCouldNotBeLoadedException e) {
			throw new RcaRuntimeError("Plugin class loader could not be loaded.", e);
		}

		// Note: We need to set out own messages/services factories here so it works properly with plugins which are
		// shipped with their own messages/services definition.

		final MessageDefinitionProvider messageDefinitionProvider = getMessageDefinitionProvider();
		final ServiceRequestMessageFactory serviceRequestMessageFactory =
				new RcaServiceRequestMessageFactory(messageDefinitionProvider, messageInterfaceClassProvider);
		final ServiceResponseMessageFactory serviceResponseMessageFactory =
				new RcaServiceResponseMessageFactory(messageDefinitionProvider, messageInterfaceClassProvider);

		mDefaultNodeConfiguration.setTopicDescriptionFactory(new TopicDescriptionFactory(messageDefinitionProvider));
		mDefaultNodeConfiguration.setTopicMessageFactory(new RcaMessageFactory(messageDefinitionProvider, messageInterfaceClassProvider));
		mDefaultNodeConfiguration.setServiceDescriptionFactory(new ServiceDescriptionFactory(messageDefinitionProvider));
		mDefaultNodeConfiguration.setServiceRequestMessageFactory(serviceRequestMessageFactory);
		mDefaultNodeConfiguration.setServiceResponseMessageFactory(serviceResponseMessageFactory);
		mDefaultNodeConfiguration.setMessageSerializationFactory(new RcaMessageSerializationFactory(messageDefinitionProvider,
				serviceRequestMessageFactory, serviceResponseMessageFactory));
	}

	@Override
	public boolean isRebootRemoConOnRelaunchEnabled() {
		return mRebootRemoConOnRelaunch;
	}

	protected NodeConfiguration createDefaultNodeConfiguration(String nodeName) {
		final String fNodeName = nodeName != null ? nodeName : "node";
		final NameResolver parentResolver = mDefaultNodeConfiguration.getParentResolver();
		final String nodeNamespace = parentResolver.getNamespace().toString();

		final Map<GraphName, GraphName> remappings = parentResolver.getRemappings();
		final Map<GraphName, GraphName> nodeRemappings = new HashMap<GraphName, GraphName>(remappings);
		for (Map.Entry<GraphName, GraphName> remapping : remappings.entrySet()) {
			final String from = remapping.getKey().toString();
			if (from.startsWith(nodeNamespace)) {
				final String relativeFrom = from.substring(nodeNamespace.length() + 1);
				nodeRemappings.put(GraphName.of(relativeFrom), remapping.getValue());
				if (relativeFrom.startsWith(fNodeName)) {
					if (relativeFrom.length() == fNodeName.length()) {
						if (BuildConfig.DEBUG) {
							Preconditions.checkState(relativeFrom.equals(fNodeName));
						}
						nodeRemappings.put(GraphName.of(fNodeName), remapping.getValue());
					} else {
						nodeRemappings.put(GraphName.of("~" + relativeFrom.substring(fNodeName.length() + 1)), remapping.getValue());
					}
				}
			}
		}

		final NodeConfiguration configuration = NodeConfiguration.copyOf(mDefaultNodeConfiguration);
		configuration.setNodeName(fNodeName);
		configuration.setParentResolver(NameResolver.newFromNamespaceAndRemappings(nodeNamespace, nodeRemappings));

		return configuration;
	}

	protected final boolean syncRunOnImplThread(@NotNull final ICheckedRunnable runnable) {
		if (getRcaContext().isOnUiThread()) {
			throw new IllegalOperationOnUiThreadException(getIdName());
		}
		return syncRunOnImplThread(runnable, createTimeoutWarning(), false);
	}

	protected final boolean runOnImplThread(@NotNull final ICheckedRunnable runnable) {
		if (isOnImplThread()) {
			return checkedForUnexpectedErrorsOnImplThread(runnable, createTimeoutWarning());
		} else {
			return postToImplThread(runnable);
		}
	}

	protected final boolean postToImplThread(@NotNull final ICheckedRunnable runnable) {
		return postToImplThread(runnable, createTimeoutWarning(), false);
	}

	protected final synchronized void shutdownImplThread() {
		if (!mShutdownImplThread) {
			mShutdownImplThread = true;
			// Note: quitSafely() requires API level 18.
			// Here we use our own implementation of safely quitting the thread.
			syncRunOnImplThread(new ICheckedRunnable() {
				@Override
				public void run() {
					cancelUndoneLoadingTasks();
					mImplThread.quit();
				}
			}, createTimeoutWarning(), true);
		}
	}

	protected RemoCoLoadingTask startRemoCoLoadingTask(String msg) {
		if (getRemoCo().isShutdown()) {
			getLog().w("Starting RemoCo task '" + msg + "' denied during shutdown.");
			return null;
		}
		RemoCoLoadingTask task = new RemoCoLoadingTask(msg);
		task.start();
		return task;
	}

	protected RemoCoLoadingTask getRemoCoLoadingTaskById(int taskId) {
		return mLoadingTasks.get(taskId);
	}

	private MessageDefinitionProvider getMessageDefinitionProvider() {
		if (mMessageDefinitionProvider == null) {
			final MessageDefinitionProviderChain messageDefinitionProvider = new MessageDefinitionProviderChain();

			final StringFileProvider messageDefinitionStringFileProvider = createMessageDefinitionStringFileProvider(getRemoCo());
			if (messageDefinitionStringFileProvider != null) {
				final MessageDefinitionFileProvider messageDefinitionFileProvider = new MessageDefinitionFileProvider(messageDefinitionStringFileProvider);
				messageDefinitionFileProvider.update();
				// Note: This adding order is important.
				messageDefinitionProvider.addMessageDefinitionProvider(messageDefinitionFileProvider);
			}

			// Note: We always need the {@link RcaMessageDefinitionJarsProvider} for common messages (like ROS logging).
			messageDefinitionProvider.addMessageDefinitionProvider(new RemoCoMessageDefinitionJarsProvider(getRemoCo()));
			mMessageDefinitionProvider = messageDefinitionProvider;
		}
		return mMessageDefinitionProvider;
	}

	private static StringFileProvider createMessageDefinitionStringFileProvider(@NotNull IRemoCo remoCo) {
		final String apk = remoCo.getApk();
		final String remoCoId = remoCo.getIdName();

		final String javaMsgsFullDir = RcaService.getApp().getFileNamesManager().getJavaMsgsFullDir(apk);
		final File file = new File(javaMsgsFullDir);
		if (!file.isDirectory()) {
			if (BuildConfig.DEBUG) {
				remoCo.getLog().d("no message definition files defined for remoco '" + remoCoId + "'");
			}
			return null;
		}
		final String logPrefix = "searching for message definition files for remoco '" + remoCoId + "'";

		final WeakReference<IRemoCo> remoCoRef = new WeakReference<IRemoCo>(remoCo);
		final StringFileProvider provider = new StringFileProvider(new IOFileFilter() {
			@Override
			public boolean accept(File file) {
				final String name = file.getName();
				final String type = file.getParentFile().getName();
				return acceptFile(name, type, file.getParent(), null);
			}

			@Override
			public boolean accept(File dir, String name) {
				String type = null;
				Throwable error = null;
				try {
					final String dirName = dir.getName();
					if (dir.getCanonicalFile().getParent().equals(javaMsgsFullDir)) {
						new RcaId(dirName);  // Check for validity of the package name. An unchecked exception is thrown on failure.
					} else {
						type = dir.getName();
						if (!type.equals("msg") && !type.equals("srv")) {
							throw new RcaException("Message definition folder of a package should be 'msg' or 'srv'.");
						}
						new RcaId(dir.getParentFile().getName());  // Check for validity of the package name. An unchecked exception is thrown on failure.
						new RcaId(Files.getNameWithoutExtension(name));  // Check for validity of the name. An unchecked exception is thrown on failure.
					}
				} catch (Throwable e) {
					error = e;
				}
				return acceptFile(name, type, dir.toString(), error);
			}

			private boolean acceptFile(String name, String type, String dir, Throwable error) {
				boolean success = error == null;
				if (success) {
					final String extension = Files.getFileExtension(name);
					success = type.equals(extension) && (type.equals("msg") || type.equals("srv"));
				}
				final IRemoCo remoCo = remoCoRef.get();
				final IRcaLog log = remoCo != null ? remoCo.getLog() : RcaService.getLog();
				if (success) {
					if (BuildConfig.DEBUG) {
						log.d(logPrefix + ": accepted file '" + name + "' from directory '" + dir + "'");
					}
				} else {
					boolean ignoring = true;
					if (BuildConfig.DEBUG && Files.getFileExtension(name).equals("jar")) {
						try {
							if (new File(dir).getCanonicalPath().equals(javaMsgsFullDir)) {
								ignoring = false;
							}
						} catch (IOException ignored) {
						}
					}
					if (ignoring) {
						log.w(logPrefix + ": ignoring file '" + name + "' in directory '" + dir + "'", error);
					}
				}
				return success;
			}
		});

		provider.addDirectory(file);
		return provider;
	}

	private void cancelUndoneLoadingTasks() {
		try {
			synchronized (mLoadingTasks) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(getRemoCo().isShutdown());
				}
				List<RemoCoLoadingTask> loadingTasksCopy = new ArrayList<RemoCoLoadingTask>(mLoadingTasks.values());
				for (RemoCoLoadingTask task : loadingTasksCopy) {
					task.cancel();
				}
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(mLoadingTasks.isEmpty());
				}
			}
		} catch (Throwable throwable) {
			getRemoCo().handleErrorException(throwable);
		}
	}

	private boolean syncRunOnImplThread(@NotNull final ICheckedRunnable runnable, RcaTimeoutException timeoutWarning, boolean ignoreShutdownImplThreadFlag) {
		if (isOnImplThread()) {
			return checkedForUnexpectedErrorsOnImplThread(runnable, timeoutWarning);
		} else {
			final CountDownLatch latch = new CountDownLatch(1);
			if (postToImplThread(new ICheckedRunnable() {
				@Override
				public void run() throws Throwable {
					try {
						runnable.run();
					} finally {
						latch.countDown();
					}
				}
			}, timeoutWarning, ignoreShutdownImplThreadFlag)) {
				try {
					latch.await();
					return true;
				} catch (InterruptedException e) {
					mImplThread.interrupt();
					// Restore the interrupted status.
					Thread.currentThread().interrupt();
				}
			} else {
				mRemoCo.handleErrorException(new RcaRuntimeError("Requested action could not be posted to RemoCo implementation's thread."));
			}
		}
		return false;
	}

	private boolean postToImplThread(@NotNull final ICheckedRunnable runnable, final RcaTimeoutException timeoutWarning, boolean ignoreShutdownImplThreadFlag) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mShutdownImplThread || mImplThread.isAlive());
		}
		assert mRemoCo != null;
		return (ignoreShutdownImplThreadFlag || !mShutdownImplThread) && mImplHandler.post(new Runnable() {
			@Override
			public void run() {
				checkedForUnexpectedErrorsOnImplThread(runnable, timeoutWarning);
			}
		});
	}

	private boolean checkedForUnexpectedErrorsOnImplThread(@NotNull ICheckedRunnable runnable, RcaTimeoutException timeoutWarning) {
		try {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(isOnImplThread(), "Not on RemoCo implementation's thread.");
			}

			if (timeoutWarning != null) {
				ISupervisedTask task = startTask(timeoutWarning);
				try {
					runnable.run();
				} finally {
					task.done();
				}
			} else {
				runnable.run();
			}
		} catch (Throwable e) {
			mRemoCo.handleErrorException(new RcaRuntimeError(e));
			return false;
		}
		return true;
	}

	@Override
	public boolean isOnImplThread() {
		return mImplThread == Thread.currentThread();
	}

	@NotNull
	private ISupervisedTask startTask(RcaTimeoutException timeoutWarning) {
		SupervisedTask task = new SupervisedTask(timeoutWarning);
		task.startSupervision();
		return task;
	}

	private RcaTimeoutException createTimeoutWarning() {
		final int timeoutSeconds = getRcaContext().getSettings().getRemoConBlockNotRespondingRemoCosTimeout() / 1000;
		if (timeoutSeconds > 0) {
			return new RcaTimeoutException("RemoCo '" + getIdName() + "' is being not responsive for " + timeoutSeconds + "s.");
		}
		return null;
	}

	protected final IRcaLog getLog() {
		return mRemoCo != null ? mRemoCo.getLog() : RcaService.getLog();
	}
}
