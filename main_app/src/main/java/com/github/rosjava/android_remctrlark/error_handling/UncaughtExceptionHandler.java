/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.error_handling;

import android.content.Context;
import android.content.Intent;

public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

	private final Context mContext;
	private final Thread.UncaughtExceptionHandler mDefaultHandler;

	public UncaughtExceptionHandler(Context context, Thread.UncaughtExceptionHandler defaultHandler) {
		mContext = context;
		mDefaultHandler = defaultHandler;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {

		System.err.append("RemCtrlArk: Handling uncaught java exception...");
		ex.printStackTrace();

/*
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));

		String nl = "\n";
		String report = "************ CAUSE OF ERROR ************\n\n" +
				stackTrace.toString() +
				"\n************ DEVICE INFORMATION ***********\n" +
				"Brand: " + Build.BRAND + nl +
				"Device: " + Build.DEVICE + nl +
				"Model: " + Build.MODEL + nl +
				"Id: " + Build.ID + nl +
				"Product: " + Build.PRODUCT + nl +
				"\n************ FIRMWARE ************\n" +
				"SDK: " + Build.VERSION.SDK_INT + nl +
				"Release: " + Build.VERSION.RELEASE + nl +
				"Incremental: " + Build.VERSION.INCREMENTAL + nl;
*/

		Intent intent = new Intent(mContext, UncaughtExceptionDialogActivity.class);
//		intent.putExtra("error", report);
		mContext.startActivity(intent);

/*
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(10);
*/

		if (mDefaultHandler != null && thread.getName().equals("main")) {
			mDefaultHandler.uncaughtException(thread, ex);
		}
	}
}
