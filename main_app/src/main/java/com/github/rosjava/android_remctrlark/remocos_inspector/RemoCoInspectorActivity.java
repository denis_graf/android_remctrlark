/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.os.Bundle;

import com.github.rosjava.android_remctrlark.R;

public class RemoCoInspectorActivity extends ARemoCoInspectorActivity implements RemoCoInspectorFragment.Callbacks {
	public static final String EXTRA_REMOCO_ID = RemoCoInspectorFragment.ARG_REMOCO_ID;
	public static final String EXTRA_SHOW_TAB = RemoCoInspectorFragment.ARG_SHOW_TAB;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remoco_inspector_activity);

		if (savedInstanceState == null) {
			startDetailFragment();
		}
	}

	@Override
	public void selectRemoCo(String remoCoId) {
		mRemoCoId = remoCoId;
		startDetailFragment();
	}

	private void startDetailFragment() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Bundle arguments = new Bundle();
				arguments.putString(RemoCoInspectorFragment.ARG_REMOCO_ID, mRemoCoId);
				arguments.putString(RemoCoInspectorFragment.ARG_SHOW_TAB, mShowTab);
				final RemoCoInspectorFragment fragment = new RemoCoInspectorFragment();
				fragment.setArguments(arguments);
				getFragmentManager().beginTransaction()
						.replace(R.id.remocos_inspector_details_container, fragment)
						.commit();
			}
		});
	}
}
