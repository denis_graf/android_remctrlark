/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.remocos.IOnRemoCoStateChangedListener;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.RemoCoStateChange;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

abstract class ARemoCoInspectorDetailFragment extends Fragment {
	public static final String ARG_REMOCO_ID = "remoco_id";

	private IRemoCo mRemoCo = null;
	private boolean mActiveOnStartup = false;
	private boolean mActive = false;
	private boolean mDeactivated = false;

	@NotNull
	protected final IRemoCo getRemoCo() {
		assert mRemoCo != null;
		return mRemoCo;
	}

	public void setDetailFragmentStateOnStartup(boolean active) {
		mActiveOnStartup = active;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		assert getArguments() != null;
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getArguments().containsKey(ARG_REMOCO_ID));
		}
		String remoCoId = getArguments().getString(ARG_REMOCO_ID);
		if (savedInstanceState != null) {
			remoCoId = savedInstanceState.getString(ARG_REMOCO_ID, remoCoId);
		}
		mRemoCo = RcaService.getContext().findRemoCo(remoCoId, true);
		if (mRemoCo == null) {
			mRemoCo = RcaService.getContext().getRemoCon();
			RcaService.getLog().e("remoco '" + remoCoId + "' not found for showing log, fallback to remocon");
		}

		final View result = onRemoInspectorCreateView(inflater, container, savedInstanceState);
		if (mActiveOnStartup) {
			changeDetailFragmentState(true);
		}
		return result;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (mActive) {
			changeDetailFragmentState(false);
		}
	}

	protected abstract View onRemoInspectorCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

	public final void changeDetailFragmentState(boolean active) {
		// Guarantee calling the callbacks only once for activating and deactivating, and calling deactivating only
		// after activating was called.
		if (mDeactivated) {
			// Already deactivated.
			return;
		}
		if (active) {
			if (mActive) {
				// Already activated.
				return;
			}
			// Activate.
			mActive = true;
		} else {
			mDeactivated = true;
			if (!mActive) {
				// Already deactivated.
				return;
			}
			// Deactivate.
			mActive = false;
		}
		if (RcaService.getContext() == null) {
			return;
		}
		onDetailFragmentStateChanged(active);
	}

	public void onDetailFragmentStateChanged(boolean active) {
		final IRemoCon remoCon = RcaService.getContext().getRemoCon();
		if (active) {
			remoCon.setOnStateChangedListener(new IOnRemoCoStateChangedListener() {
				@Override
				public void onStateChanged(IRemoCo remoCo, RemoCoStateChange stateChange) {
					if (stateChange == RemoCoStateChange.RELAUNCHED) {
						// This will reload the fragment and choose the right remoco.
						getRemoCoInspectorActivity().selectRemoCo(getRemoCo().getIdName());
					}
				}
			});
		} else {
			remoCon.setOnStateChangedListener(null);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(ARG_REMOCO_ID, mRemoCo.getIdName());
	}

	protected final ARemoCoInspectorActivity getRemoCoInspectorActivity() {
		return (ARemoCoInspectorActivity) getActivity();
	}
}
