/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.rca_logging.RcaPythonLog;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoLogEntry;
import com.github.rosjava.android_remctrlark.remocos.impls.ARemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.ARemoCo;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.github.rosjava.android_remctrlark.sl4a.IRcaScript;
import com.github.rosjava.android_remctrlark.sl4a.RemoCoScript;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

public class LoggerProxy extends APyProxy implements IRcaPyProxy {

	private class PythonLog extends RcaPythonLog {
		private final List<IRemoCoLogEntry> mPendingLogEntries = new LinkedList<IRemoCoLogEntry>();
		private final String mSubTag;
		private final boolean mIsRemoCoImplLog;
		private long mCurrentFromPythonLogTime = 0;
		private IRemoCo mRemoCo = null;

		private PythonLog(boolean isRemoCoImplLog, @NotNull String subTag) {
			mSubTag = subTag;
			mIsRemoCoImplLog = isRemoCoImplLog;
		}

		@NotNull
		@Override
		protected String getSubTag() {
			return mSubTag;
		}

		public synchronized void fromPython(long time, @NotNull String logLevel, @NotNull String msg) {
			mCurrentFromPythonLogTime = time;
			super.fromPython(logLevel, msg);
			mCurrentFromPythonLogTime = 0;
		}

		@Override
		protected void onLog(RcaLogLevel logLevel, String tag, String subTag, String msg, Throwable exception) {
			final long currentTime;
			if (BuildConfig.DEBUG) {
				currentTime = mCurrentFromPythonLogTime;
				Preconditions.checkState(currentTime != 0);
			} else {
				currentTime = mCurrentFromPythonLogTime != 0 ? mCurrentFromPythonLogTime : System.currentTimeMillis();
			}
			final IRemoCoLogEntry logEntry;
			if (mIsRemoCoImplLog) {
				logEntry = new ARemoCoImpl.LogEntry(null, logLevel, currentTime, tag, subTag, msg, exception);
			} else {
				logEntry = new ARemoCo.LogEntry(null, logLevel, currentTime, tag, subTag, msg, exception);
			}

			if (getRemoCo() != null) {
				getRemoCo().addLogEntry(logEntry);
			} else {
				synchronized (mPendingLogEntries) {
					mPendingLogEntries.add(logEntry);
				}
			}
		}

		private IRemoCo getRemoCo() {
			if (mRemoCo == null) {
				// Note: The script can be still not bound when the logger is created.
				final IRcaScript script = mRcaFacade.getScriptByHostPid(mRcaFacade.getPythonHostname(), mRcaFacade.getPythonPid());
				final RemoCoScript remoCoScript = script instanceof RemoCoScript ? (RemoCoScript) script : null;
				mRemoCo = remoCoScript != null ? remoCoScript.getRemoCo() : null;
				if (mRemoCo != null) {
					synchronized (mPendingLogEntries) {
						if (!mPendingLogEntries.isEmpty()) {
							for (IRemoCoLogEntry entry : mPendingLogEntries) {
								mRemoCo.addLogEntry(entry);
							}
							mPendingLogEntries.clear();
						}
					}
				}
			}
			return mRemoCo;
		}
	}

	private final PythonLog mLog;

	@SuppressWarnings("UnusedDeclaration")
	private final IRcaBinder mBinder = new ReflectingBinder(new IReflectingBinderImpl() {
		public void log(Long time, String logLevel, String message) {
			mLog.fromPython(time, logLevel, message);
		}
	});

	public LoggerProxy(IRcaFacade rcaFacade, final boolean isRemoCoImplLog, String tag) {
		super(rcaFacade);
		tag = tag.trim();
		if (tag.startsWith("[") && tag.endsWith("]")) {
			tag = tag.substring(1, tag.length() - 1);
		}
		mLog = new PythonLog(isRemoCoImplLog, tag);
	}

	@Override
	public IRcaBinder getBinder() {
		return mBinder;
	}

	@Override
	public void shutdown() {
	}
}
