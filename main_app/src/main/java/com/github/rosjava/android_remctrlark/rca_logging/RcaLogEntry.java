/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rca_logging;

public class RcaLogEntry implements IRcaLogEntry {
	private final RcaLogLevel mLogLevel;
	private final long mTime;
	private final String mTag;
	private final String mSubTag;
	private final String mMessage;
	private final Throwable mException;

	public RcaLogEntry(RcaLogLevel logLevel, long time, String tag, String subTag, String message, Throwable exception) {
		mLogLevel = logLevel;
		mTime = time;
		mTag = tag;
		mSubTag = subTag;
		mMessage = message;
		mException = exception;
	}

	@Override
	public RcaLogLevel getLogLevel() {
		return mLogLevel;
	}

	@Override
	public long getTime() {
		return mTime;
	}

	@Override
	public String getTag() {
		return mTag;
	}

	@Override
	public String getSubTag() {
		return mSubTag;
	}

	@Override
	public String getMessage() {
		return mMessage;
	}

	@Override
	public Throwable getException() {
		return mException;
	}
}
