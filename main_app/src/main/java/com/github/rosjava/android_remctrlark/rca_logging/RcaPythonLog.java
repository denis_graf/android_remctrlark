/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rca_logging;

import org.jetbrains.annotations.NotNull;

public class RcaPythonLog extends ARcaLog {

	public static final RcaPythonLog LOG = new RcaPythonLog();

	private static final String TAG_PYTHON = "RCA_PYTHON";

	public synchronized void fromPython(@NotNull String logLevel, @NotNull String msg) {
		if (logLevel.startsWith(LEVEL_DEBUG)) {
			d(msg);
		} else if (logLevel.startsWith(LEVEL_INFO)) {
			i(msg);
		} else if (logLevel.startsWith(LEVEL_ERROR)) {
			e(msg);
		} else if (logLevel.startsWith(LEVEL_WARN)) {
			w(msg);
		} else {
			// This should actually not happen...
			d(msg);
		}
	}

	@NotNull
	@Override
	protected String getTag() {
		return TAG_PYTHON;
	}

	@NotNull
	@Override
	protected String composeMessage(@NotNull String level, @NotNull String subTag, @NotNull String msg) {
		return msg.startsWith(level) ? msg : level + " " + msg;
	}
}
