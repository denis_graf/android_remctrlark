/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoNamespace;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigGlobals;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RemoConConfigGlobals implements IRemoConConfigGlobals, Serializable {

	static final String XML_TAG = "globals";

	final Map<IRcaId, IRemoCoNamespace> mNamespaces = new HashMap<IRcaId, IRemoCoNamespace>();
	final Map<IRcaId, IRemoCoType> mTypes = new HashMap<IRcaId, IRemoCoType>();

	@Override
	public Map<IRcaId, IRemoCoNamespace> getNamespaces() {
		return mNamespaces;
	}

	@Override
	public Map<IRcaId, IRemoCoType> getTypes() {
		return mTypes;
	}
}
