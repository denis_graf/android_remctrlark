/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.binders.IRcaBinderMethod;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoNamespace;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoParam;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoRemapping;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocos.impls.python.PythonRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.python.PythonRemoConImpl;

import org.ros.namespace.GraphName;

import java.io.StringWriter;
import java.net.URI;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class RemoCoInfoRetriever {
	public static enum ImplLanguage {
		JAVA,
		PYTHON;

		@Override
		public String toString() {
			switch (this) {
				case JAVA:
					return "Java";
				case PYTHON:
					return "Python";
				default:
					return super.toString();
			}
		}
	}

	private RemoCoInfoRetriever() {
	}

	public static String retrieveFormattedInfo(String remoCoId) {
		return retrieveFormattedInfo(RcaService.getContext().findRemoCo(remoCoId, true));
	}

	public static String retrieveFormattedInfo(IRemoCo remoCo) {
		final IRcaContext rcaContext = RcaService.getContext();
		final IRemoConConfigRetriever remoConConfig = rcaContext.getRemoConConfig();
		final boolean isRemoCon = retrieveIsRemoCon(remoCo);
		final IRemoCoConfigRetriever remoCoConfig = isRemoCon ? remoConConfig : remoConConfig.retrieveRemoCompConfig(new RcaId(remoCo.getIdName()));

		final String status;
		if (remoCo.isShutdown()) {
			status = "shutdown";
		} else if (remoCo.isStarted()) {
			status = "started";
		} else if (remoCo.isCreated()) {
			status = "created";
		} else {
			status = "instantiated";
		}

		final String blockState;
		if (remoCo.isInErrorState()) {
			blockState = "blocked by error";
		} else if (remoCo.isInWarningState()) {
			blockState = "blocked by warning";
		} else {
			blockState = "not blocked";
		}

		final StringWriter info = new StringWriter();
		final String apk = remoCoConfig.retrieveApk();
		final String type = remoCoConfig.retrieveType().toString();
		final ImplLanguage language = retrieveImplLanguage(remoCo);
		info.append(isRemoCon ? "RemoCon: " : "RemoComp: ").append(remoCo.getIdName()).append("\n")
				.append("\n")
				.append("Type: (tid=").append(remoCoConfig.retrieveTypeId().toString()).append(")\n\t")
				.append(type).append("\n")
				.append("Package:\n\t")
				.append(apk).append("\n")
				.append("Implementation:\n\t")
				.append(language != null ? language.toString() : "<error>").append("\n")
				.append("\n");

/*
		if (language.equals("Python")) {
			info.append("\tLocation: ").append(app.getFileNamesManager().getPyRemoCoFileNamesManager(type, apk).getMainFullDir()).append("\n");
		}
		info.append("\n");
*/

		info.append("Status:\n\t").append(remoCo.isRelaunching() ? "relaunching" + ", " : "").append(status).append(", ").append(blockState).append("\n")
				.append("\n");

		info.append("Parameters:\n");
		final List<IRemoCoParam> params = new LinkedList<IRemoCoParam>();
		params.addAll(remoCoConfig.retrieveResolvedConfigParams());
		Collections.sort(params, new Comparator<IRemoCoParam>() {
			@Override
			public int compare(IRemoCoParam lhs, IRemoCoParam rhs) {
				return lhs.getName().toString().compareTo(rhs.getName().toString());
			}
		});
		for (IRemoCoParam param : params) {
			info.append("\t").append(param.getName().toString()).append(" = ").append(param.getValue().toString())
					.append(" (").append(param.getType()).append(")");
			if (!param.isMutable()) {
				info.append(" [not mutable]");
			}
			info.append("\n");
		}
		info.append("\n");

		info.append("Remappings:\n");
		final List<IRemoCoRemapping> remappings = new LinkedList<IRemoCoRemapping>();
		remappings.addAll(remoCoConfig.retrieveResolvedConfigRemappings());
		Collections.sort(remappings, new Comparator<IRemoCoRemapping>() {
			@Override
			public int compare(IRemoCoRemapping lhs, IRemoCoRemapping rhs) {
				return lhs.getFrom().toString().compareTo(rhs.getFrom().toString());
			}
		});
		for (IRemoCoRemapping remap : remappings) {
			info.append("\t").append(remap.getFrom().toString()).append(" -> ").append(remap.getTo().toString())
					.append(" (nsid=").append(remap.getNsId().toString()).append(")\n");
		}
		info.append("\n");

		if (isRemoCon) {
			final RemoConConfigNewMaster newMaster = remoConConfig.retrieveNewMaster();
			info.append("\nROS master: ").append(newMaster == RemoConConfigNewMaster.Public ? " new public" :
					(newMaster == RemoConConfigNewMaster.Private ? "new private" : "external")).append("\n");

			// Current URI is also set, if a new master was created.
			info.append("\tcurrent URI: ");
			final URI currentUri = rcaContext.getCurrentMasterUri();
			info.append(currentUri != null && currentUri != RcaGlobalConstants.NULL_URI ? currentUri.toString() : "<not set>").append("\n");

			info.append("\tconfig URI: ");
			final URI configUri = remoConConfig.retrieveMasterUri();
			info.append(configUri != RcaGlobalConstants.NULL_URI ? configUri.toString() : "<not set>").append("\n");
			info.append("\n");

			info.append("Namespaces:\n");
			final List<IRemoCoNamespace> namespaces = new LinkedList<IRemoCoNamespace>();
			namespaces.addAll(remoConConfig.retrieveResolvedConfigNamespaces());
			Collections.sort(namespaces, new Comparator<IRemoCoNamespace>() {
				@Override
				public int compare(IRemoCoNamespace lhs, IRemoCoNamespace rhs) {
					return lhs.getId().compareTo(rhs.getId());
				}
			});
			for (IRemoCoNamespace configNamespace : namespaces) {
				final IRcaId nsId = configNamespace.getId();
				final GraphName ns = configNamespace.getNamespace();
				info.append("\t").append(nsId.toString()).append(" := ").append(ns.toString());
				if (!configNamespace.isMutable()) {
					info.append(" [not mutable]");
				}
				info.append("\n");
			}
			info.append("\n");

			final String defaultApk = remoConConfig.retrieveApk(null);
			info.append("Types:").append(" (default apk: ").append(defaultApk).append(")\n");
			final List<IRemoCoType> types = new LinkedList<IRemoCoType>();
			types.addAll(remoConConfig.retrieveResolvedConfigTypes());
			Collections.sort(types, new Comparator<IRemoCoType>() {
				@Override
				public int compare(IRemoCoType lhs, IRemoCoType rhs) {
					return lhs.getId().compareTo(rhs.getId());
				}
			});
			for (IRemoCoType configType : types) {
				final IRcaId tId = configType.getId();
				final IRcaId retrType = configType.getType();
				final String retrApk = configType.getApk();  // Note: retrieve the apk, to avoid it being null.
				info.append("\t").append(tId.toString()).append(" := ").append(retrType.toString());
				if (!retrApk.equals(defaultApk)) {
					info.append(" (").append(retrApk).append(")");
				}
				if (!configType.isMutable()) {
					info.append(" [not mutable]");
				}
				info.append("\n");
			}
			info.append("\n");

			info.append("Config:\n\t")
					.append(remoConConfig.getConfigFileFullDir()).append("\n");
			info.append("\n");
			info.append("\n");
		} else {
			try {
				info.append("\n---- Binder interface documentation ----\n\n");
				if (remoCo.isShutdown()) {
					info.append("\t<RemoCo is shutdown, no interface available>\n\n");
				} else {
					final List<IRcaBinderMethod> methods = remoCo.getBinder().getMethods();
					Collections.sort(methods, new Comparator<IRcaBinderMethod>() {
						@Override
						public int compare(IRcaBinderMethod lhs, IRcaBinderMethod rhs) {
							return lhs.getName().compareTo(rhs.getName());
						}
					});
					for (IRcaBinderMethod method : methods) {
						String doc = method.getDoc();
						info.append(doc).append("\n");
					}
				}
				info.append("\n");
			} catch (Throwable e) {
				RcaService.getLog().e("could not retrieve binder interface documentation for remocomp '" + remoCo.getIdName() + "'", e);
			}
		}

		info.append("---- RemoCo documentation ----\n\n")
				.append(remoCo.getImplDoc()).append("\n");

		return info.toString();
	}

	public static ImplLanguage retrieveImplLanguage(IRemoCo remoCo) {
		final boolean isRemoCon = retrieveIsRemoCon(remoCo);
		final IRcaApplication app = RcaService.getApp();
		if (isRemoCon) {
			final IRemoConImpl remoConImpl = app.getMainRemoCoImplsFactory().createRemoConImpl(remoCo.getIdName(), remoCo.getType(), remoCo.getApk());
			return remoConImpl != null ? (remoConImpl instanceof PythonRemoConImpl ? ImplLanguage.PYTHON : ImplLanguage.JAVA) : null;
		} else {
			final IRemoCompImpl remoCompImpl = app.getMainRemoCoImplsFactory().createRemoCompImpl(remoCo.getIdName(), remoCo.getType(), remoCo.getApk());
			return remoCompImpl != null ? (remoCompImpl instanceof PythonRemoCompImpl ? ImplLanguage.PYTHON : ImplLanguage.JAVA) : null;
		}
	}

	public static boolean retrieveIsRemoCon(IRemoCo remoCo) {
		return remoCo instanceof IRemoCon;
	}
}
