/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.retriever;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompSpecialization;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConSpecialization;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

class RemoCompConfigRetriever extends ARemoCoConfigRetriever implements IRemoCompConfigRetriever {
	private final IRcaId mId;
	private final RemoConConfigRetriever mRemoConConfigRetriever;

	RemoCompConfigRetriever(RemoConConfigRetriever remoConConfigRetriever, @NotNull IRcaId id) {
		super(remoConConfigRetriever.getRemoConConfig(), id, remoConConfigRetriever.getRootNameResolver());
		mRemoConConfigRetriever = remoConConfigRetriever;
		mId = id;
	}

	@NotNull
	@Override
	public final IRcaId getId() {
		return mId;
	}

	@NotNull
	@Override
	public final IRcaId retrieveTypeId() {
		// Get the "last" not-null definition of type id.
		IRcaId typeId = null;
		for (IRemoConSpecialization remoConSpec : mRemoConConfigRetriever.retrieveSpecializations()) {
			final IRemoCompConfig remoCompConfig = remoConSpec.getRemoCompConfigs().get(getId());
			if (remoCompConfig != null && remoCompConfig.getTypeId() != null) {
				typeId = remoCompConfig.getTypeId();
			}
		}
		return typeId != null ? typeId : getId();
	}

	@Override
	protected final List<IRemoCompSpecialization> retrieveSpecializations() {
		final List<IRemoCompSpecialization> specs = new LinkedList<IRemoCompSpecialization>();
		for (IRemoConSpecialization remoConSpec : mRemoConConfigRetriever.retrieveSpecializations()) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(mRemoConConfigRetriever.conditionMatch(remoConSpec.getCondition()));
			}
			final IRemoCompConfig remoCompConfig = remoConSpec.getRemoCompConfigs().get(getId());
			if (remoCompConfig != null) {
				for (IRemoCompSpecialization remoCompSpec : remoCompConfig.getSpecializations()) {
					if (conditionMatch(remoCompSpec.getCondition())) {
						specs.add(remoCompSpec);
					}
				}
			}
		}
		return specs;
	}

	@NotNull
	@Override
	protected IRemoConMutablesConfig getMutablesConfig() {
		return mRemoConConfigRetriever.getMutablesConfig();
	}
}
