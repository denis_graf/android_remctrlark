/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.java;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.error_handling.LayoutException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.IJavaRemoCompBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCoConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCoConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCoLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaXmlStringLayoutInflater;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLoadingTask;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLog;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.LayoutInflatorCouldNotBeCreatedException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCoNotStartedException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCoParamClassCastException;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCoParamNotFoundException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.ARemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.Layout;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.ros.namespace.GraphName;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.util.Map;

abstract class AJavaRemoCoImpl extends ARemoCoImpl implements IRemoCoImpl {

	protected abstract class AJavaRemoCoConfiguration implements IJavaRemoCoConfiguration {
		@Override
		public NodeConfiguration createNodeConfiguration(String nodeName) {
			return AJavaRemoCoImpl.this.createDefaultNodeConfiguration(nodeName);
		}

		@Override
		public NodeConfiguration createNodeConfiguration(GraphName nodeName) {
			return createNodeConfiguration(nodeName.toString());
		}
	}

	protected class JavaRemoCoImplBinder extends ReflectingBinder {
		public JavaRemoCoImplBinder(IJavaRemoCompBinderImpl impl) {
			super(impl);
		}

		@Override
		public final Object invokeMethod(final String name, final Object[] args) throws RcaBinderInvocationException {
			return invokeMethod(name, args, null);
		}

		@Override
		public final Object invokeMethod(final String name, final JSONArray args) throws RcaBinderInvocationException {
			return invokeMethod(name, null, args);
		}

		private Object invokeMethod(final String name, final Object[] argsObj, final JSONArray argsJson) throws RcaBinderInvocationException {
			if (BuildConfig.DEBUG) {
				Preconditions.checkArgument((argsJson == null) != (argsObj == null));
			}
			final Object vResult[] = new Object[1];
			final RcaBinderInvocationException vException[] = new RcaBinderInvocationException[1];
			syncRunOnImplThread(new ICheckedRunnable() {
				@Override
				public void run() {
					try {
						vResult[0] = argsObj != null ?
								JavaRemoCoImplBinder.super.invokeMethod(name, argsObj) :
								JavaRemoCoImplBinder.super.invokeMethod(name, argsJson);
					} catch (RcaBinderInvocationException e) {
						vException[0] = e;
						getLog().w(e);
					}
				}
			});
			if (vException[0] != null) {
				throw vException[0];
			}
			return vResult[0];
		}
	}

	protected abstract class AJavaRemoCoContext implements IJavaRemoCoContext {
		private final class RemoCoLog extends RcaJavaLog implements IRemoCoLog {
			@Override
			protected void onLog(RcaLogLevel logLevel, String tag, String subTag, String msg, Throwable exception) {
				getRemoCo().addLogEntry(new LogEntry(getIdName(), logLevel, System.currentTimeMillis(), tag, subTag, msg, exception));
			}

			@NotNull
			@Override
			protected String getSubTag() {
				return getIdName();
			}
		}

		private final RemoCoLog mLog = new RemoCoLog();

		@Override
		public String getId() {
			return AJavaRemoCoImpl.this.getIdName();
		}

		@Override
		public String getType() {
			return AJavaRemoCoImpl.this.getType();
		}

		@Override
		public String getApk() {
			return AJavaRemoCoImpl.this.getApk();
		}

		@Override
		public GraphName getNamespace() {
			return getRemoCo().getNameResolver().getNamespace();
		}

		@Override
		public IRemoCoLog getLog() {
			return mLog;
		}

		@Override
		public boolean postToImplThread(ICheckedRunnable runnable) {
			return AJavaRemoCoImpl.this.postToImplThread(runnable);
		}

		@Override
		public boolean runOnImplThread(ICheckedRunnable runnable) {
			return AJavaRemoCoImpl.this.runOnImplThread(runnable);
		}

		@Override
		public boolean syncRunOnImplThread(ICheckedRunnable runnable) {
			return AJavaRemoCoImpl.this.syncRunOnImplThread(runnable);
		}

		@Override
		public boolean postToUiThread(final ICheckedRunnable runnable) {
			return getRcaContext().postToUiThread(new ICheckedRunnable() {
				@Override
				public void run() {
					checkedOnUiThread(runnable);
				}
			});
		}

		@Override
		public boolean runOnUiThread(ICheckedRunnable runnable) {
			if (getRcaContext().isOnUiThread()) {
				return checkedOnUiThread(runnable);
			} else {
				return postToUiThread(runnable);
			}
		}

		@Override
		public boolean syncRunOnUiThread(final ICheckedRunnable runnable) {
			final Boolean vResult[] = new Boolean[1];
			return getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
				@Override
				public void run() {
					vResult[0] = checkedOnUiThread(runnable);
				}
			}) || vResult[0];
		}

		@Override
		public IRemoCoLoadingTask startLoadingTask(String msg) {
			return startRemoCoLoadingTask(msg);
		}

		@Override
		public void handleException(Throwable e) {
			getRemoCo().handleErrorException(e);
		}

		@Override
		public NodeMainExecutor getNodeExecutor() {
			if (!getRemoCo().isCreated()) {
				throw new RemoCoNotStartedException(getIdName());
			}
			return getRcaContext().getNodeExecutor();
		}

		@Override
		public Context getApkContext() {
			return AJavaRemoCoImpl.this.getApkContext();
		}

		@Override
		public void requestRemoConRebootOnRelaunch(boolean enable) {
			mRebootRemoConOnRelaunch = enable;
		}

		@Override
		public boolean isOnShutdown() {
			return getRemoCo().isShutdown();
		}

		@Override
		public boolean isOnRelaunch() {
			return getRemoCo().isRelaunching();
		}

		@Override
		public Object getRemoConSessionState() {
			return getRemoCo().getSessionState();
		}

		@Override
		public void setRemoConSessionState(Object state) {
			getRemoCo().setSessionState(state);
		}

		private boolean checkedOnUiThread(ICheckedRunnable runnable) {
			try {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(RcaService.getContext() == null || getRcaContext().isOnUiThread(), "Not on UI thread.");
				}
				runnable.run();
				return true;
			} catch (Throwable e) {
				handleException(e);
				return false;
			}
		}
	}

	abstract class AJavaRemoCoConfig implements IJavaRemoCoConfig {
		@Override
		public Map<GraphName, Object> getParams() {
			return getRemoCo().getParams();
		}

		@Override
		public Map<GraphName, Object> getParams(String namespace) {
			return getRemoCo().getParams(namespace);
		}

		@Override
		public Map<GraphName, Object> getParams(GraphName namespace) {
			return getRemoCo().getParams(namespace);
		}


		@Override
		public Object getParam(GraphName name) {
			return getParam(name, Object.class);
		}

		@Override
		public Object getParam(String name) {
			return getParam(GraphName.of(name));
		}

		@Override
		public Object getParam(GraphName name, Object defaultValue) {
			return getParamInternal(name, Object.class, defaultValue);
		}

		@Override
		public Object getParam(String name, Object defaultValue) {
			return getParam(GraphName.of(name), defaultValue);
		}


		@Override
		public double getParamDouble(GraphName name) {
			return getParam(name, Double.class);
		}

		@Override
		public double getParamDouble(String name) {
			return getParamDouble(GraphName.of(name));
		}

		@Override
		public double getParamDouble(GraphName name, double defaultValue) {
			return getParamInternal(name, Double.class, defaultValue);
		}

		@Override
		public double getParamDouble(String name, double defaultValue) {
			return getParamDouble(GraphName.of(name), defaultValue);
		}


		@Override
		public int getParamInteger(GraphName name) {
			return getParam(name, Integer.class);
		}

		@Override
		public int getParamInteger(String name) {
			return getParamInteger(GraphName.of(name));
		}

		@Override
		public int getParamInteger(GraphName name, int defaultValue) {
			return getParamInternal(name, Integer.class, defaultValue);
		}

		@Override
		public int getParamInteger(String name, int defaultValue) {
			return getParamInteger(GraphName.of(name), defaultValue);
		}


		@Override
		public boolean getParamBoolean(GraphName name) {
			return getParam(name, Boolean.class);
		}

		@Override
		public boolean getParamBoolean(String name) {
			return getParamBoolean(GraphName.of(name));
		}

		@Override
		public boolean getParamBoolean(GraphName name, boolean defaultValue) {
			return getParamInternal(name, Boolean.class, defaultValue);
		}

		@Override
		public boolean getParamBoolean(String name, boolean defaultValue) {
			return getParamBoolean(GraphName.of(name), defaultValue);
		}


		@Override
		public String getParamString(GraphName name) {
			return getParam(name, String.class);
		}

		@Override
		public String getParamString(String name) {
			return getParamString(GraphName.of(name));
		}

		@Override
		public String getParamString(GraphName name, String defaultValue) {
			return getParamInternal(name, String.class, defaultValue);
		}

		@Override
		public String getParamString(String name, String defaultValue) {
			return getParamString(GraphName.of(name), defaultValue);
		}


		private <T> T getParam(GraphName name, Class<T> type) {
			final Object value = getRemoCo().getParam(name);
			try {
				if (value != null) {
					return type.cast(value);
				}
			} catch (ClassCastException e) {
				throw new RemoCoParamClassCastException(getIdName(), name, type, e);
			}
			throw new RemoCoParamNotFoundException(getIdName(), name);
		}

		private <T> T getParamInternal(GraphName name, Class<T> type, T defaultValue) {
			final Object value = getRemoCo().getParam(name);
			if (value != null) {
				try {
					//noinspection unchecked
					return type.cast(value);
				} catch (ClassCastException e) {
					throw new RemoCoParamClassCastException(getIdName(), name, defaultValue.getClass(), e);
				}
			} else {
				return defaultValue;
			}
		}
	}

	abstract class AJavaRemoCoLayout implements IJavaRemoCoLayout {
		@Override
		public IJavaXmlStringLayoutInflater createFromXmlString(String layout) throws RcaLayoutException {
			ViewGroup remoCoRootView = getRemoCo().getImplUiLayer();
			assert remoCoRootView.getContext() != null;
			try {
				final Context context = getApkContext();
				// TODO: here it would be useful to be able choosing the right context... Here some examples: (see also ViewInflater#getRcaPluginClassLoader())
//				context = RcaService.getApp().getMainRemoCoImplsFactory().getPlugin(RcaService.getContext().getPluginApk()).createContext();
//				context = remoCoRootView.getContext();
				return Layout.createLayoutFromXmlString(getApkViewFactory(), context, remoCoRootView, layout);
			} catch (LayoutException e) {
				throw new RcaLayoutException(getIdName(), e);
			}
		}

		@Override
		public ViewGroup getParentView() {
			return getRemoCo().getImplUiLayer();
		}

		@Override
		public LayoutInflater getInflater() {
			try {
				return getInflaterFrom(getApkContext());
			} catch (LayoutInflatorCouldNotBeCreatedException e) {
				throw new RcaRuntimeError(e);
			}
		}

		@Override
		public LayoutInflater.Factory getViewFactory() {
			return getApkViewFactory();
		}

		private LayoutInflater getInflaterFrom(Context context) throws LayoutInflatorCouldNotBeCreatedException {
			final LayoutInflater layoutInflater = LayoutInflater.from(context);
			final LayoutInflater.Factory setFactory = layoutInflater.getFactory();
			if (setFactory != getApkViewFactory()) {
				if (setFactory == null) {
					layoutInflater.setFactory(getApkViewFactory());
				} else {
					throw new LayoutInflatorCouldNotBeCreatedException(getIdName(),
							new RcaException("RCA's plugin view factory could not be set on this LayoutInflater."));
				}
			}
			return layoutInflater;
		}
	}

	private final IJavaRemoCoImpl mImpl;
	private final String mDoc;

	public AJavaRemoCoImpl(IRcaApplication app, String idName, String type, String apk, IJavaRemoCoImpl impl) {
		super(app, idName, type, apk);
		assert impl != null;
		mImpl = impl;
		mDoc = createDoc();
	}

	protected IJavaRemoCoImpl getImpl() {
		return mImpl;
	}

	@NotNull
	@Override
	public String getDoc() {
		return mDoc;
	}

	@Override
	public void shutdown() {
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().shutdown();
			}
		});

		shutdownImplThread();
	}

	@Override
	public void createUI() {
		syncRunOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				getImpl().createUI();
			}
		});
	}

	private String createDoc() {
		final RcaType doc = getImpl().getClass().getAnnotation(RcaType.class);
		return doc != null ? doc.value() + "\n" : "";
	}
}
