/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.SharedPreferences;

import com.github.rosjava.android_remctrlark.rca_logging.ARcaLog;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;

class RcaMutableSettings implements IRcaMutableSettings {

	static final String PREF_KEY_REMOCO_INSPCETOR_FILTER_LOG_LEVEL = "pref_key_remoco_inspector_filter_log_level";
	static final String PREF_KEY_REMOCO_INSPCETOR_FILTER_INTERNAL_LOG_LEVEL = "pref_key_remoco_inspector_filter_internal_log_level";

	private final SharedPreferences mSharedPreferences;

	RcaMutableSettings(SharedPreferences sharedPreferences) {
		mSharedPreferences = sharedPreferences;
	}

	@Override
	public synchronized RcaLogLevel getRemoCoInspectorFilterLogLevel() {
		String pref = PREF_KEY_REMOCO_INSPCETOR_FILTER_LOG_LEVEL;
		return geLogLevel(pref, ARcaLog.LEVEL_INFO);
	}

	@Override
	public synchronized void setRemoCoInspectorFilterLogLevel(RcaLogLevel logLevel) {
		setLogLevel(PREF_KEY_REMOCO_INSPCETOR_FILTER_LOG_LEVEL, logLevel);
	}

	@Override
	public synchronized RcaLogLevel getRemoCoInspectorFilterInternalLogLevel() {
		String pref = PREF_KEY_REMOCO_INSPCETOR_FILTER_INTERNAL_LOG_LEVEL;
		return geLogLevel(pref, ARcaLog.LEVEL_WARN);
	}

	@Override
	public synchronized void setRemoCoInspectorFilterInternalLogLevel(RcaLogLevel logLevel) {
		setLogLevel(PREF_KEY_REMOCO_INSPCETOR_FILTER_INTERNAL_LOG_LEVEL, logLevel);
	}

	private RcaLogLevel geLogLevel(String pref, String defaultValue) {
		final String logLevel = mSharedPreferences.getString(pref, defaultValue);
		if (logLevel.endsWith(ARcaLog.LEVEL_DEBUG)) {
			return RcaLogLevel.DEBUG;
		}
		if (logLevel.endsWith(ARcaLog.LEVEL_INFO)) {
			return RcaLogLevel.INFO;
		}
		if (logLevel.endsWith(ARcaLog.LEVEL_WARN)) {
			return RcaLogLevel.WARN;
		}
		if (logLevel.endsWith(ARcaLog.LEVEL_ERROR)) {
			return RcaLogLevel.ERROR;
		}
		return null;
	}

	private void setLogLevel(String pref, RcaLogLevel logLevel) {
		final SharedPreferences.Editor editor = mSharedPreferences.edit();
		final String string;
		switch (logLevel) {
			case INFO:
			default:
				string = ARcaLog.LEVEL_INFO;
				break;
			case DEBUG:
				string = ARcaLog.LEVEL_DEBUG;
				break;
			case WARN:
				string = ARcaLog.LEVEL_WARN;
				break;
			case ERROR:
				string = ARcaLog.LEVEL_ERROR;
				break;
		}
		editor.putString(pref, string).commit();
	}
}
