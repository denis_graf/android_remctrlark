/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls;

import android.util.Pair;

import com.github.rosjava.android_remctrlark.application.RcaGlobalConstants;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import org.ros.namespace.GraphName;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class MutablesConfig extends RemoConMutablesConfig implements IRemoConMutablesConfigCommit {
	private boolean mSetAllToInitialState = false;

	public void setMasterUri(URI uri) {
		mMasterUri = uri != null ? uri : RcaGlobalConstants.NULL_URI;
	}

	public void setNewMaster(RemoConConfigNewMaster newMaster) {
		mNewMaster = newMaster != null ? newMaster : RemoConConfigNewMaster.None;
	}

	public void setType(IRcaId tId, IRcaId type, String apk) {
		if (tId != null) {
			mTypes.put(tId, new Pair<IRcaId, String>(type, apk));
		}
	}

	public void setNamespace(IRcaId nsId, GraphName namespace) {
		if (nsId != null) {
			mNamespaces.put(nsId, namespace);
		}
	}

	public void setParam(IRcaId id, GraphName name, Object value) {
		if (id == null || name == null) {
			return;
		}
		Map<GraphName, Object> params = mParams.get(id);
		if (params == null) {
			params = new HashMap<GraphName, Object>();
			mParams.put(id, params);
		}
		params.put(name, value);
	}

	public void setRemapping(IRcaId id, GraphName from, GraphName to, IRcaId nsid) {
		if (id == null || from == null || to == null) {
			return;
		}
		Map<GraphName, Pair<GraphName, IRcaId>> remappings = mRemappings.get(id);
		if (remappings == null) {
			remappings = new HashMap<GraphName, Pair<GraphName, IRcaId>>();
			mRemappings.put(id, remappings);
		}
		remappings.put(from, new Pair<GraphName, IRcaId>(to, nsid));
	}

	@Override
	public boolean isSetAllToInitialState() {
		return mSetAllToInitialState;
	}

	@Override
	public void setAllToInitialState() {
		super.setAllToInitialState();
		mSetAllToInitialState = true;
	}
}
