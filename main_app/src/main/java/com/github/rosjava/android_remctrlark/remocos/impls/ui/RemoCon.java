/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RemoConTypeNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.RemoCompLaunchedEvent;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoLogEntry;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.ros.node.NodeConfiguration;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@SuppressLint("ViewConstructor")
public final class RemoCon extends ARemoCo implements IRemoCon {

	private interface RemoCompRunnable {
		void run(RemoComp remoComp);
	}

	private final String mIdName;
	private final Map<String, IRemoComp> mShutdownRemoCompsBackup =
			getRcaContext().getSettings().getDeveloperModeEnabled() ? new HashMap<String, IRemoComp>() : null;

	public RemoCon(Context context, String idName, String type, String apk) {
		super(context, type, apk);
		mIdName = idName;
	}

	String getIdName(int id) {
		return getImpl().getIdName(id);
	}

	@Override
	public String getIdName() {
		return mIdName;
	}

	@Override
	public List<IRemoComp> findAllRemoComps() {
		return findAllRemoComps(false);
	}

	@Override
	public List<IRemoComp> findAllRemoComps(boolean includeBackup) {
		return findAllRemoComps(IRemoComp.class, includeBackup);
	}

	@Override
	public IRemoComp findRemoCompByIdName(String idName) {
		return findRemoCompByIdName(idName, false);
	}

	@Override
	public IRemoComp findRemoCompByIdName(String idName, boolean includeBackup) {
		final IRemoComp remoComp = findRemoCompByIdName(IRemoComp.class, idName);

		if (includeBackup && remoComp == null && getRcaContext().getSettings().getDeveloperModeEnabled()) {
			return mShutdownRemoCompsBackup.get(idName);
		}

		return remoComp;
	}

	@Override
	public IRemoComp findRemoCompAtPosition(int x, int y) {
		IRemoComp result = null;
		List<RemoComp> remoComps = findAllRemoComps(RemoComp.class);
		for (RemoComp remoComp : remoComps) {
			// Note: remoComp.getImplUiLayer() can be null, if e.g. remoComp is currently relaunching!
			Rect rect = new Rect();
			remoComp.getGlobalVisibleRect(rect);
			if (rect.contains(x, y)) {
				result = remoComp;
				break;
			}
		}
		return result;
	}

	@Override
	public void create() {
		// Note: When being in developer mode, we would like to see the view while it is created.
		final boolean attachUiLayerImmediately = getRcaContext().getSettings().getDeveloperModeEnabled();
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			updateMetaUiLayer();

			super.create();
			if (attachUiLayerImmediately) {
				attachImplUiLayer();
			}
			attachRemoComps();

			runAllRemoCompsInParallel(new RemoCompRunnable() {
				@Override
				public void run(RemoComp remoComp) {
					if (getRcaContext().getSettings().getDeveloperModeEnabled()) {
						assert mShutdownRemoCompsBackup != null;
						final IRemoComp backupRemoComp = mShutdownRemoCompsBackup.get(remoComp.getIdName());
						if (backupRemoComp != null) {
							// The remocon is relaunching its remocomps. Restore remocomp's log.
							final List<IRemoCoLogEntry> logEntries = backupRemoComp.retrieveInternalLogEntries();
							logEntries.addAll(backupRemoComp.retrieveImplLogEntries());
							for (IRemoCoLogEntry logEntry : logEntries) {
								remoComp.addLogEntry(logEntry);
							}
						}
					}
					remoComp.create();
				}
			});
		} catch (InterruptedException e) {
			handleErrorException(e);
			Thread.currentThread().interrupt();
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			if (!attachUiLayerImmediately) {
				attachImplUiLayer();
			}
			setCreated();
			updateMetaUiLayer();
		}
	}

	@Override
	public void start(@NotNull final NodeConfiguration defaultNodeConfiguration) {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
				Preconditions.checkState(isCreated());
				Preconditions.checkState(!isStarted());
				Preconditions.checkState(!isShutdown());
			}
			updateMetaUiLayer();

			runAllRemoCompsInParallel(new RemoCompRunnable() {
				@Override
				public void run(RemoComp remoComp) {
					remoComp.start(defaultNodeConfiguration);
				}
			});

			try {
				getImpl().start(createNewNodeConfiguration(defaultNodeConfiguration));
			} catch (Throwable e) {
				handleErrorException(e);
			}

			runAllRemoCompsInParallel(new RemoCompRunnable() {
				@Override
				public void run(RemoComp remoComp) {
					if (remoComp.isStarted()) {
						dispatchRemoCompLaunchedEvent(remoComp);
					}
				}
			});
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			setStarted();
			updateMetaUiLayer();
		}
	}

	@Override
	public void shutdown() {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			cancelAllExceptions();
			if (isShutdown()) {
				return;
			}
			setShutdown();
			if (!isCreated()) {
				return;
			}
			updateMetaUiLayer();

			try {
				getImpl().shutdown();
			} catch (Throwable e) {
				handleErrorException(e);
			}

			runAllRemoCompsInParallel(new RemoCompRunnable() {
				@Override
				public void run(RemoComp remoComp) {
					remoComp.shutdown();
				}
			});

			if (getRcaContext().getSettings().getDeveloperModeEnabled()) {
				assert mShutdownRemoCompsBackup != null;
				for (IRemoComp remoComp : findAllRemoComps()) {
					mShutdownRemoCompsBackup.put(remoComp.getIdName(), remoComp);
				}
			}

		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			updateMetaUiLayer();
		}
		super.shutdown();

		if (!isRelaunching()) {
			synchronized (mSessionState) {
				mSessionState.clear();
			}
		}
	}

	public void relaunchRemoCos(@NotNull final List<IRcaId> remoCos, final NodeConfiguration defaultNodeConfiguration) {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			if (remoCos.contains(new RcaId(getIdName()))) {
				// Relaunching the remocon means relaunching all.
				getLog().i("relaunching remocon...");

				final List<RemoCompAnchor> remoCompAnchors = findAllRemoCompAnchors();

				relaunch(defaultNodeConfiguration);

				for (RemoCompAnchor anchor : remoCompAnchors) {
					anchor.removeAllViews();
					ViewParent parent = anchor.getParent();
					if (parent != null) {
						((ViewGroup) parent).removeView(anchor);
					}
				}

			} else {
				getLog().i("relaunching remocomps " + remoCos + "...");
				runAllRemoCompsInParallel(new RemoCompRunnable() {
					@Override
					public void run(RemoComp remoComp) {
						if (remoCos.contains(new RcaId(remoComp.getIdName()))) {
							remoComp.relaunch(defaultNodeConfiguration);
							if (remoComp.isStarted()) {
								dispatchRemoCompLaunchedEvent(remoComp);
							}
						}
					}
				});
			}
		} catch (Throwable e) {
			handleErrorException(e);
		}
	}

	@Override
	public void updateMetaUiLayer() {
		super.updateMetaUiLayer();
		List<IRemoComp> remoComps = findAllRemoComps();
		for (IRemoComp remoComp : remoComps) {
			remoComp.updateMetaUiLayer();
		}
	}

	@NotNull
	@Override
	public final IRemoConConfigRetriever retrieveRemoCoConfig() {
		return (IRemoConConfigRetriever) super.retrieveRemoCoConfig();
	}

	@Override
	protected final IRemoConImpl getImpl() {
		return (IRemoConImpl) super.getImpl();
	}

	@NotNull
	@Override
	protected final Pair<IRemoCoImpl, IRcaBinder> createNewImpl() throws RemoConTypeNotFoundException, RcaPluginCouldNotBeLoadedException {
		IRemoConImpl impl = getRcaContext().getApp().getMainRemoCoImplsFactory().createRemoConImpl(getIdName(), getType(), getApk());
		if (impl == null) {
			RcaException cause = null;
			if (!getRcaContext().getSettings().getPythonEnabled()) {
				final boolean pythonRemoConExists =
						getRcaContext().getApp().getFileNamesManager().getPyRemoConFileNamesManager(getType(), getApk()) != null;
				if (pythonRemoConExists) {
					cause = new RcaException("Running python code is forbidden by RCA. In order to load this RemoCon, please enable python in RCA settings.");
				}
			}
			throw new RemoConTypeNotFoundException(getType(), getApk(), cause);
		}
		// Note: For constructing Python Android proxies we need the view factory for while creation.
		setViewFactory(impl.getApkViewFactory());
		IRcaBinder binder = impl.create(this);
		return new Pair<IRemoCoImpl, IRcaBinder>(impl, binder);
	}

	@NotNull
	@Override
	protected final Pair<IRemoCoImpl, IRcaBinder> createNewDummyImpl() throws RcaPluginCouldNotBeLoadedException {
		IRemoConImpl impl = getRcaContext().getApp().getDummyRemoCoImplsFactory().createRemoConImpl(getIdName(), getType(), getApk());
		// Note: For constructing Python Android proxies we need the view factory for while creation.
		setViewFactory(impl.getApkViewFactory());
		IRcaBinder binder = impl.create(this);
		return new Pair<IRemoCoImpl, IRcaBinder>(impl, binder);
	}

	private final Map<String, Object> mSessionState = new HashMap<String, Object>();

	protected final Object getSessionState(ARemoCo remoCo) {
		synchronized (mSessionState) {
			return mSessionState.get(createSessionStateKey(remoCo));
		}
	}

	protected final void setSessionState(ARemoCo remoCo, Object state) {
		synchronized (mSessionState) {
			mSessionState.put(createSessionStateKey(remoCo), state);
		}
	}

	private String createSessionStateKey(ARemoCo remoCo) {
		return remoCo.getIdName() + ":" + remoCo.getApk() + ":" + remoCo.getType();
	}

	private void dispatchRemoCompLaunchedEvent(RemoComp remoComp) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(remoComp.isStarted());
		}
		remoComp.dispatchEvent(RemoCompLaunchedEvent.EVENT_NAME, new RemoCompLaunchedEvent(remoComp.getType(), remoComp.getApk()));
	}

	private void attachRemoComps() {
		getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				final Map<IRcaId, Pair<IRcaId, String>> remoCompAnchorTypes = new HashMap<IRcaId, Pair<IRcaId, String>>();
				final List<RemoCompAnchor> remoCompAnchors = findAllRemoCompAnchors();
				for (RemoCompAnchor anchor : remoCompAnchors) {

					String type = null;
					String apk = null;
					try {
						type = anchor.getType();
						apk = anchor.getApk();
						if (anchor.getChildCount() > 0) {
							throw new RcaException("RemoComp anchors are not allowed to have children views.");
						}
					} catch (Throwable e) {
						handleErrorException(e);
					}

					if (apk == null || apk.isEmpty()) {
						apk = anchor.getClass().getPackage().getName();
					}

					final RemoComp remoComp = new RemoComp(getContext(), getIdName(anchor.getId()), type, apk);
					final IRemoCoConfigRetriever remoCompConfig = remoComp.retrieveRemoCoConfig();
					final IRcaId typeId = remoCompConfig.retrieveTypeId();
					if (remoComp.getIdName().equals(typeId.toString()) && !remoCompConfig.containsTypeDefinition()) {
						// Apply Remocomp types from remocomp anchors only when id=tid and the tid's type is not defined
						// in config's globals.
						remoCompAnchorTypes.put(typeId, new Pair<IRcaId, String>(new RcaId(remoComp.getType()), remoComp.getApk()));
					}
					anchor.addView(remoComp);
				}

				if (!remoCompAnchorTypes.isEmpty()) {
					try {
						// Initialize the current config with remocomp types read from the remocomp anchors.
						getRcaContext().getRemoConConfig().applyMutablesConfigCommit(new RemoConMutablesConfigCommit(new RemoConMutablesConfig() {
							@Override
							protected void onConstruct() {
								mTypes.putAll(remoCompAnchorTypes);
							}
						}, false));
					} catch (Throwable e) {
						handleErrorException(e);
					}
				}
			}
		});
	}

	private List<RemoCompAnchor> findAllRemoCompAnchors() {
		List<RemoCompAnchor> remoComps = new LinkedList<RemoCompAnchor>();
		findAllRemoCompAnchors(getImplUiLayer(), remoComps);
		return remoComps;
	}

	private void findAllRemoCompAnchors(ViewGroup parent, List<RemoCompAnchor> remoComps) {
		if (parent == null) {
			return;
		}
		for (int i = 0; i < parent.getChildCount(); ++i) {
			final View child = parent.getChildAt(i);
			assert child != null;
			if (child instanceof RemoCompAnchor) {
/*
				if (((RemoCompAnchor) child).getChildCount() > 0) {
					handleErrorException(new RcaRuntimeError("RemoComp anchors are not allowed to have view children."));
					((RemoCompAnchor) child).removeAllViews();
				}
*/
				remoComps.add((RemoCompAnchor) child);
			} else if (child instanceof ViewGroup) {
				findAllRemoCompAnchors((ViewGroup) child, remoComps);
			}
		}
	}

	private NodeConfiguration createNewNodeConfiguration(NodeConfiguration defaultNodeConfiguration) {
		final NodeConfiguration newNodeConfiguration = NodeConfiguration.copyOf(defaultNodeConfiguration);
		setupRemoCoNodeConfiguration(newNodeConfiguration);
		return newNodeConfiguration;
	}

	private <T extends IRemoComp> List<T> findAllRemoComps(Class<T> type) {
		return findAllRemoComps(type, false);
	}

	private <T extends IRemoComp> List<T> findAllRemoComps(Class<T> type, boolean includeBackup) {
		final List<T> remoComps = new LinkedList<T>();
		findAllRemoComps(type, getImplUiLayer(), remoComps);

		if (includeBackup && getRcaContext().getSettings().getDeveloperModeEnabled()) {
			for (IRemoComp remoCompBackup : mShutdownRemoCompsBackup.values()) {
				boolean addRemoCompBackup = true;
				for (T remoComp : remoComps) {
					if (remoComp.getIdName().equals(remoCompBackup.getIdName())) {
						addRemoCompBackup = false;
						break;
					}
				}
				if (addRemoCompBackup) {
					//noinspection unchecked
					remoComps.add((T) remoCompBackup);
				}
			}

			Collections.sort(remoComps, new Comparator<T>() {
				@Override
				public int compare(T lhs, T rhs) {
					return lhs.getIdName().compareTo(rhs.getIdName());
				}
			});
		}

		return remoComps;
	}

	private <T extends IRemoComp> T findRemoCompByIdName(Class<T> type, String idName) {
		if (idName == null) {
			return null;
		}
		T result = null;
		List<T> remoComps = findAllRemoComps(type);
		for (T remoComp : remoComps) {
			if (remoComp.getIdName().equals(idName)) {
				result = remoComp;
				break;
			}
		}
		return result;
	}

	private static <T extends IRemoComp> void findAllRemoComps(Class<T> type, ViewGroup parent, List<T> remoComps) {
		if (parent == null) {
			return;
		}
		for (int i = 0; i < parent.getChildCount(); ++i) {
			View child = parent.getChildAt(i);
			// TODO: #12: Forbid Nested RemoComps -> dont't search in children of RemoComps
			if (child instanceof ViewGroup) {
				findAllRemoComps(type, (ViewGroup) child, remoComps);
			}
			if (child instanceof IRemoComp) {
				remoComps.add(type.cast(child));
			}
		}
	}

	private void runAllRemoCompsInParallel(final RemoCompRunnable runnable) throws InterruptedException {
		List<RemoComp> remoComps = findAllRemoComps(RemoComp.class);
		final CountDownLatch latch = new CountDownLatch(remoComps.size());
		for (final RemoComp remoComp : remoComps) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						runnable.run(remoComp);
					} finally {
						latch.countDown();
					}
				}
			}).start();
		}
		latch.await();
	}
}
