/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original ServiceResponseMessageFactory.java written by Damon Kohler (damonkohler@google.com) modified by Denis Graf.
 */
package com.github.rosjava.android_remctrlark.ros_messages;

import org.ros.internal.message.MessageInterfaceClassProvider;
import org.ros.internal.message.MessageProxyFactory;
import org.ros.internal.message.service.ServiceDescription;
import org.ros.internal.message.service.ServiceDescriptionFactory;
import org.ros.internal.message.service.ServiceResponseMessageFactory;
import org.ros.message.MessageDeclaration;
import org.ros.message.MessageDefinitionProvider;

public class RcaServiceResponseMessageFactory extends ServiceResponseMessageFactory {

	private final ServiceDescriptionFactory mServiceDescriptionFactory;
	private final MessageProxyFactory mMessageProxyFactory;

	public RcaServiceResponseMessageFactory(MessageDefinitionProvider messageDefinitionProvider, MessageInterfaceClassProvider messageInterfaceClassProvider) {
		super(null);
		mServiceDescriptionFactory = new ServiceDescriptionFactory(messageDefinitionProvider);
		mMessageProxyFactory = new MessageProxyFactory(messageInterfaceClassProvider, new RcaMessageFactory(messageDefinitionProvider, messageInterfaceClassProvider));
	}

	@Override
	public <T> T newFromType(String serviceType) {
		ServiceDescription serviceDescription = mServiceDescriptionFactory.newFromType(serviceType);
		MessageDeclaration messageDeclaration =
				MessageDeclaration.of(serviceDescription.getResponseType(),
						serviceDescription.getResponseDefinition());
		return mMessageProxyFactory.newMessageProxy(messageDeclaration);
	}
}
