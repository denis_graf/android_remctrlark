/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;

public class AndroidProxy extends APyProxy implements IRcaPyProxy {

	private final IRcaBinder mBinder = new ReflectingBinder(new IReflectingBinderImpl() {
	});

	public AndroidProxy(IRcaFacade rcaFacade) {
		super(rcaFacade);
	}

	@Override
	public IRcaBinder getBinder() {
		return mBinder;
	}

	@Override
	public void shutdown() {
	}
}
