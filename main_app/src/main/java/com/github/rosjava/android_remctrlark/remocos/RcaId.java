/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos;

import com.github.rosjava.android_remctrlark.error_handling.InvalidRcaIdException;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

public final class RcaId implements IRcaId {
	private static final Pattern VALID_ID = Pattern.compile("^[_(0-9)(a-z)(A-Z)]+$");

	private String mId;

	public RcaId(@NotNull String id) {
		//noinspection ConstantConditions
		if (id == null) {
			throw new InvalidRcaIdException("Id not defined.");
		}
		if (!VALID_ID.matcher(id).matches()) {
			throw new InvalidRcaIdException("Invalid id name: '" + id + "'. Ids must match the pattern '" + VALID_ID + "'.");
		}
		mId = id;
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o) || (o instanceof RcaId && mId.equals(((RcaId) o).mId));
	}

	@Override
	public int hashCode() {
		return mId.hashCode();
	}

	@Override
	public int compareTo(IRcaId another) {
		return mId.compareTo(((RcaId) another).mId);
	}

	@Override
	public String toString() {
		return mId;
	}
}
