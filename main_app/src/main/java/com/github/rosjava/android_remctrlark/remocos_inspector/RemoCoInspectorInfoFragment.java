/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.IOnMutablesConfigChangedListener;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocos.IOnRemoCoStateChangedListener;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.RemoCoInfoRetriever;
import com.github.rosjava.android_remctrlark.remocos.RemoCoStateChange;

public class RemoCoInspectorInfoFragment extends ARemoCoInspectorDetailFragment {
	public static final String TAB = "Info";
	private TextView mInfoTextView;

	public RemoCoInspectorInfoFragment() {
	}

	@Override
	public View onRemoInspectorCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.remoco_inspector_info_fragment, container, false);
		assert rootView != null;

		mInfoTextView = ((TextView) rootView.findViewById(R.id.remoco_info));
		updateInfo();

		return rootView;
	}

	@Override
	public void onDetailFragmentStateChanged(boolean active) {
		final IRcaContext rcaContext = RcaService.getContext();
		final IRemoCon remoCon = rcaContext.getRemoCon();
		if (active) {
			final IOnRemoCoStateChangedListener onStateChangedListener = new IOnRemoCoStateChangedListener() {
				@Override
				public void onStateChanged(IRemoCo remoCo, RemoCoStateChange stateChange) {
					if (stateChange == RemoCoStateChange.RELAUNCHED && remoCo instanceof IRemoCon) {
						// This will reload the fragment and choose the right remoco.
						getRemoCoInspectorActivity().selectRemoCo(getRemoCo().getIdName());
					} else {
						updateInfo();
					}
				}
			};
			remoCon.setOnStateChangedListener(onStateChangedListener);
			getRemoCo().setOnStateChangedListener(onStateChangedListener);
			rcaContext.setOnMutablesConfigChangedListener(new IOnMutablesConfigChangedListener() {
				@Override
				public void onConfigChanged(IRemoConMutablesConfigCommit commit) {
					updateInfo();
				}
			});
			updateInfo();
		} else {
			remoCon.setOnStateChangedListener(null);
			getRemoCo().setOnStateChangedListener(null);
			rcaContext.setOnMutablesConfigChangedListener(null);
		}
	}

	private void updateInfo() {
		final String infoString = RemoCoInfoRetriever.retrieveFormattedInfo(getRemoCo());
		assert getActivity() != null;
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				assert mInfoTextView != null;
				mInfoTextView.setText(infoString);
			}
		});
	}
}
