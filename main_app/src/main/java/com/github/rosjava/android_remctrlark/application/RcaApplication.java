/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.content.Context;
import android.preference.PreferenceManager;

import com.android.python27.ScriptApplication;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.remocos.IMainRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.remocos.impls.DummyMainRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.remocos.impls.MainAppRemoCoImplsFactory;
import com.googlecode.android_scripting.RcaFacadeConfiguration;
import com.googlecode.android_scripting.jsonrpc.RcaJsonBuilder;

public class RcaApplication extends ScriptApplication implements IRcaApplication {

	private IMainAppRemoCoImplsFactory mRemoCoImplsFactory;
	private IMainRemoCoImplsFactory mDummyRemoCoImplsFactory;
	private IMainFileNamesManager mMainFileNamesManager;

	@Override
	public void onCreate() {
		super.onCreate();

		final Thread uiThread = Thread.currentThread();
		final Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				try {
					final String message = "Unhandled exception in a thread:\n"
							+ "  Thread id: " + thread.getId() + "\n"
							+ "  Thread name: " + thread.getName();

					// Note: RcaService.getContext() can be null.
					RcaService.getContext().handleError(new RcaRuntimeError(message, ex));
				} catch (Throwable ignored) {
				}

				if (mDefaultUncaughtExceptionHandler != null && Thread.currentThread() == uiThread) {
					mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
				}
			}
		});

		RcaFacadeConfiguration.initRca();
		RcaJsonBuilder.initRca();

		mMainFileNamesManager = new MainFileNamesManager(this);
		mRemoCoImplsFactory = new MainAppRemoCoImplsFactory(this);
		mDummyRemoCoImplsFactory = new DummyMainRemoCoImplsFactory(this);

		RcaService.mRcaApp = this;
		RcaService.mMutableSettings = new RcaMutableSettings(PreferenceManager.getDefaultSharedPreferences(this));
	}

	@Override
	public Context getApplicationContext() {
		return this;
	}

	@Override
	public IMainAppRemoCoImplsFactory getMainRemoCoImplsFactory() {
		return mRemoCoImplsFactory;
	}

	@Override
	public IMainRemoCoImplsFactory getDummyRemoCoImplsFactory() {
		return mDummyRemoCoImplsFactory;
	}

	@Override
	public IMainFileNamesManager getFileNamesManager() {
		return mMainFileNamesManager;
	}
}
