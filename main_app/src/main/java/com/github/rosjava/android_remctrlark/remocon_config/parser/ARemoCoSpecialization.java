/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoParam;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoRemapping;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoSpecCondition;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoSpecialization;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

abstract class ARemoCoSpecialization implements IRemoCoSpecialization, Serializable {

	static final String XML_ATTR_TYPE = "type";
	static final String XML_ATTR_APK = "apk";
	static final String XML_ATTR_MASTERURI = "masteruri";
	static final String XML_ATTR_NEWMASTER = "newmaster";

	final RemoCoSpecCondition mCondition = new RemoCoSpecCondition();
	final List<IRemoCoRemapping> mRemappings = new LinkedList<IRemoCoRemapping>();
	final List<IRemoCoParam> mParams = new LinkedList<IRemoCoParam>();

	@Override
	public IRemoCoSpecCondition getCondition() {
		return mCondition;
	}

	@Override
	public List<IRemoCoRemapping> getRemappings() {
		return mRemappings;
	}

	@Override
	public List<IRemoCoParam> getParams() {
		return mParams;
	}

}
