/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls;

import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.dummy.DummyRemoComp;
import com.github.rosjava.android_remctrlark.remocos.impls.dummy.DummyRemoCon;
import com.github.rosjava.android_remctrlark.remocos.impls.java.JavaRemoCompImpl;
import com.github.rosjava.android_remctrlark.remocos.impls.java.JavaRemoConImpl;

import java.util.Set;
import java.util.TreeSet;

public class DummyMainRemoCoImplsFactory extends AMainRemoCoImplsFactory {

	public DummyMainRemoCoImplsFactory(IRcaApplication app) {
		super(app);
	}

	@Override
	public void reload() {
	}

	@Override
	public Set<String> getAllRemoConTypes(String apk) {
		return new TreeSet<String>();
	}

	@Override
	public Set<String> getAllRemoCompTypes(String apk) {
		return new TreeSet<String>();
	}

	@Override
	public IRemoConImpl createRemoConImpl(String idName, String type, String apk) {
		return new JavaRemoConImpl(getApp(), idName, type, apk, new DummyRemoCon());
	}

	@Override
	public IRemoCompImpl createRemoCompImpl(String idName, String type, String apk) {
		return new JavaRemoCompImpl(getApp(), idName, type, apk, new DummyRemoComp());
	}
}
