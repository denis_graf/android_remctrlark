/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.application.IMainFileNamesManager;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.googlecode.android_scripting.facade.EventFacade;

import org.jetbrains.annotations.NotNull;

abstract class APyProxy implements IRcaPyProxy {
	protected final IRcaFacade mRcaFacade;

	public APyProxy(@NotNull IRcaFacade rcaFacade) {
		mRcaFacade = rcaFacade;
	}

	@Override
	public final String getType() {
		return getClass().getSimpleName();
	}

	@Override
	public final String getHostname() {
		return mRcaFacade.getPythonHostname();
	}

	@Override
	public final int getPid() {
		return mRcaFacade.getPythonPid();
	}

	@Override
	public void onInterruptEventRequest() {
		mRcaFacade.sendInterruptEvent();
	}

	@Override
	public void addEventObserver(EventFacade.EventObserver observer) {
		mRcaFacade.addEventObserver(observer);
	}

	@Override
	public void removeEventObserver(EventFacade.EventObserver observer) {
		mRcaFacade.removeEventObserver(observer);
	}

	protected final IRcaContext getRcaContext() {
		return mRcaFacade.getRcaContext();
	}

	protected final IMainFileNamesManager getMainFileNamesManager() {
		return mRcaFacade.getRcaContext().getApp().getFileNamesManager();
	}
}
