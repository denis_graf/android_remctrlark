/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original DefaultMessageSerializationFactory.java written by Damon Kohler (damonkohler@google.com) modified by Denis Graf.
 */
package com.github.rosjava.android_remctrlark.ros_messages;

import org.ros.internal.message.DefaultMessageDeserializer;
import org.ros.internal.message.DefaultMessageFactory;
import org.ros.internal.message.DefaultMessageSerializer;
import org.ros.internal.message.service.ServiceRequestMessageFactory;
import org.ros.internal.message.service.ServiceResponseMessageFactory;
import org.ros.message.MessageDefinitionProvider;
import org.ros.message.MessageDeserializer;
import org.ros.message.MessageFactory;
import org.ros.message.MessageIdentifier;
import org.ros.message.MessageSerializationFactory;
import org.ros.message.MessageSerializer;

public class RcaMessageSerializationFactory implements MessageSerializationFactory {

	private final MessageFactory mTopicMessageFactory;
	private final ServiceRequestMessageFactory mServiceRequestMessageFactory;
	private final ServiceResponseMessageFactory mServiceResponseMessageFactory;

	public RcaMessageSerializationFactory(MessageDefinitionProvider messageDefinitionProvider,
	                                      ServiceRequestMessageFactory serviceRequestMessageFactory,
	                                      ServiceResponseMessageFactory serviceResponseMessageFactory) {
		mTopicMessageFactory = new DefaultMessageFactory(messageDefinitionProvider);
		mServiceRequestMessageFactory = serviceRequestMessageFactory;
		mServiceResponseMessageFactory = serviceResponseMessageFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> MessageSerializer<T> newMessageSerializer(String messageType) {
		return (MessageSerializer<T>) new DefaultMessageSerializer();
	}

	@Override
	public <T> MessageDeserializer<T> newMessageDeserializer(String messageType) {
		return new DefaultMessageDeserializer<T>(MessageIdentifier.of(messageType), mTopicMessageFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> MessageSerializer<T> newServiceRequestSerializer(String serviceType) {
		return (MessageSerializer<T>) new DefaultMessageSerializer();
	}

	@Override
	public <T> org.ros.message.MessageDeserializer<T>
	newServiceRequestDeserializer(String serviceType) {
		return new DefaultMessageDeserializer<T>(MessageIdentifier.of(serviceType), mServiceRequestMessageFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> org.ros.message.MessageSerializer<T> newServiceResponseSerializer(String serviceType) {
		return (MessageSerializer<T>) new DefaultMessageSerializer();
	}

	@Override
	public <T> org.ros.message.MessageDeserializer<T> newServiceResponseDeserializer(String serviceType) {
		return new DefaultMessageDeserializer<T>(MessageIdentifier.of(serviceType), mServiceResponseMessageFactory);
	}
}
