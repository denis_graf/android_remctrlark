/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.binders.RcaScriptTwoWaysBinder;
import com.github.rosjava.android_remctrlark.binders.ReflectingBinder;
import com.github.rosjava.android_remctrlark.error_handling.ErrorInScriptException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.error_handling.ScriptCouldNotStartedException;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

public class MainScript extends ARcaScript implements IMainScript, IRcaScript {
	static final String SCRIPT_ID = "rca.mainscript";

	private class InternalScriptBinderImpl implements IReflectingBinderImpl {
		@SuppressWarnings("UnusedDeclaration")
		public void onScriptInitialized() {
			getRcaContext().setPyMainScript(MainScript.this);
		}
	}

	private CountDownLatch mShutdownMainScriptLatch = null;
	private final HashMap<String, Runnable> mStartRemoCoScriptReplacements = new HashMap<String, Runnable>();

	public MainScript(IRcaFacade rcaFacade, String id) {
		super(rcaFacade, id, new RcaScriptTwoWaysBinder(id, null));
		setBinder(new RcaScriptTwoWaysBinder(id, new ReflectingBinder(new InternalScriptBinderImpl())));
	}

	@Override
	public void startRemoCoScript(@NotNull IScriptRemoCo scriptRemoCo) {
		String id = "<unknown>";
		try {
			id = scriptRemoCo.getRemoCo().getIdName();
			final Runnable onStartRemoCoScriptRequst = removeNextStartRemoCoScriptRequest(id);
			// Create a proxy which registers itself globally.
			new RemoCoScript(mRcaFacade, scriptRemoCo);
			if (onStartRemoCoScriptRequst != null) {
				// Replace starting the python script by calling the replacement hook.
				onStartRemoCoScriptRequst.run();
			} else {
				// Start the python script which will connect to the created proxy.
				rpcRemoCoScriptStart(id);
			}
		} catch (RcaException e) {
			scriptRemoCo.handleScriptError(new ScriptCouldNotStartedException(id, e));
		}
	}

	@Override
	public void replaceNextStartRemoCoScriptRequest(@NotNull String id, @NotNull Runnable onStartRemoCoScriptRequst) {
		synchronized (mStartRemoCoScriptReplacements) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(!mStartRemoCoScriptReplacements.containsKey(id));
			}
			mStartRemoCoScriptReplacements.put(id, onStartRemoCoScriptRequst);
		}
	}

	@Override
	public Runnable removeNextStartRemoCoScriptRequest(@NotNull String id) {
		Runnable onStartRemoCoScriptRequst;
		synchronized (mStartRemoCoScriptReplacements) {
			onStartRemoCoScriptRequst = mStartRemoCoScriptReplacements.remove(id);
		}
		return onStartRemoCoScriptRequst;
	}

	@Override
	public synchronized void shutdownMainScript() {
		if (mShutdownMainScriptLatch != null) {
			// Shutdown already requested.
			return;
		}

		mShutdownMainScriptLatch = new CountDownLatch(1);
		try {
			rpcMainScriptShutdown();
		} catch (RcaException e) {
			getRcaContext().handleError(new RcaRuntimeError("Error on shutting down main script.", e));
		}

		try {
			mShutdownMainScriptLatch.await();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public void shutdownAllMainScripts() {
		for (IMainScript script = mRcaFacade.getNewestMainScript(getId());
		     script != null;
		     script = mRcaFacade.getNewestMainScript(getId())) {
			script.shutdownMainScript();
		}

		// Note: Shutdown itself as the last one. It causes that {@link #mRcaFacade} becomes {@code null}.
		shutdownMainScript();
	}

	@Override
	protected void onBound() {
	}

	@Override
	protected void onShutdown() {
		releaseShutdownLatch();
	}

	@Override
	protected void onShutdownComplete() {
		releaseShutdownLatch();
	}

	@Override
	protected void onScriptExitReached() {
		releaseShutdownLatch();
	}

	@Override
	public void handleError(ErrorInScriptException e) {
		try {
			getRcaContext().handleError(e);
		} catch (Throwable ignored) {
			RcaService.getLog().e("Error on main script.", e);
		}
		if (!e.getAsPyException().getPyClassName().equals("RemoCoScriptCouldNotBeStartedException")) {
			releaseShutdownLatch();
		}
	}

	@Override
	protected void onUnprocessedScriptProcessMessage(@NotNull String message) {
		RcaService.getLog().w("ignoring unknown script process message: '" + message + "'");
	}

	private void rpcRemoCoScriptStart(String scriptId) throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remoco_script_start", scriptId);
	}

	private void rpcMainScriptShutdown() throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("main_script_shutdown");
	}

	private Object rpcInvokeInternalMethod(String name, Object... args) throws RcaBinderInvocationException {
		return getBinder().invokeMethod(name, args);
	}

	private void releaseShutdownLatch() {
		try {
			getRcaContext().setPyMainScript(mRcaFacade.getNewestMainScript(getId()));
		} catch (Throwable ignored) {
		} finally {
			if (mShutdownMainScriptLatch != null) {
				mShutdownMainScriptLatch.countDown();
			}
		}
	}
}
