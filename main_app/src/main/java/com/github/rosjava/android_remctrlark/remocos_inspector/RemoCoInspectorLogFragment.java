/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogEntriesComparator;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;
import com.github.rosjava.android_remctrlark.remocos.IOnNewRemoCoLogEntryListener;
import com.github.rosjava.android_remctrlark.remocos.IOnRemoCoStateChangedListener;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImplLogEntry;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoLogEntry;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.RemoCoStateChange;
import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class RemoCoInspectorLogFragment extends ARemoCoInspectorDetailFragment {
	static class State {
		final List<String> mFilterRemoCos = new LinkedList<String>();
		long mSelectedLogTime = Long.MAX_VALUE;
		long mLastFirstVisibleLogTime = Long.MAX_VALUE;
	}

	public static final String TAB = "Log";

	private static final Map<String, Map<String, State>> mSessionState = new HashMap<String, Map<String, State>>();
	private static final Map<RcaLogLevel, Integer> mMenuLogLevelMap = new HashMap<RcaLogLevel, Integer>();
	private static final Map<RcaLogLevel, Integer> mMenuInternalLogLevelMap = new HashMap<RcaLogLevel, Integer>();

	static {
		mMenuLogLevelMap.put(RcaLogLevel.DEBUG, R.id.log_filter_debug);
		mMenuLogLevelMap.put(RcaLogLevel.INFO, R.id.log_filter_info);
		mMenuLogLevelMap.put(RcaLogLevel.WARN, R.id.log_filter_warn);
		mMenuLogLevelMap.put(RcaLogLevel.ERROR, R.id.log_filter_error);

		mMenuInternalLogLevelMap.put(RcaLogLevel.DEBUG, R.id.log_filter_internal_debug);
		mMenuInternalLogLevelMap.put(RcaLogLevel.INFO, R.id.log_filter_internal_info);
		mMenuInternalLogLevelMap.put(RcaLogLevel.WARN, R.id.log_filter_internal_warn);
		mMenuInternalLogLevelMap.put(RcaLogLevel.ERROR, R.id.log_filter_internal_error);
	}

	private static class RemoCoLogEntryListItemsComparator implements Comparator<RemoCoLogEntryListItem> {
		private RcaLogEntriesComparator mComparator = new RcaLogEntriesComparator();

		@Override
		public int compare(RemoCoLogEntryListItem lhs, RemoCoLogEntryListItem rhs) {
			return mComparator.compare(lhs.getLogEntry(), rhs.getLogEntry());
		}
	}

	final Comparator<RemoCoLogEntryListItem> mListEntriesComparator = new RemoCoLogEntryListItemsComparator();

	private State mState = null;
	private final List<RcaLogLevel> mFilterLogLevel = new LinkedList<RcaLogLevel>();
	private final List<RcaLogLevel> mFilterInternalLogLevel = new LinkedList<RcaLogLevel>();
	private final List<String> mAllRemoCoIds = retrieveAllRemoCoIds();
	private Menu mMenu = null;
	private RemoCoEntriesListItemAdapter mAdapter = null;
	private ListView mListView = null;

	public RemoCoInspectorLogFragment() {
	}

	private void loadState() {
		final String remoConId = RcaService.getContext().getRemoCon().getIdName();

		Map<String, State> remoConState = mSessionState.get(remoConId);
		if (remoConState == null) {
			remoConState = new HashMap<String, State>();
			mSessionState.put(remoConId, remoConState);
		}

		State remoCoState = remoConState.get(getRemoCo().getIdName());
		if (remoCoState == null) {
			remoCoState = new State();
			remoConState.put(getRemoCo().getIdName(), remoCoState);
		}

		mState = remoCoState;

		setFilterLogLevel(RcaService.getMutableSettings().getRemoCoInspectorFilterLogLevel());
		setFilterInternalLogLevel(RcaService.getMutableSettings().getRemoCoInspectorFilterInternalLogLevel());

		if (!mState.mFilterRemoCos.contains(getRemoCo().getIdName())) {
			mState.mFilterRemoCos.add(getRemoCo().getIdName());
		}
	}

	@Override
	public View onRemoInspectorCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		loadState();
		updateMenu();

		final View rootView = inflater.inflate(R.layout.remoco_inspector_log_fragment, container, false);
		assert rootView != null;
		assert getActivity() != null;

		mAdapter = new RemoCoEntriesListItemAdapter(getActivity(), new LinkedList<RemoCoLogEntryListItem>(), getRemoCo().getIdName());

		mListView = (ListView) rootView.findViewById(R.id.remoco_log_entries_list);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			private RemoCoLogEntryListItem mOldSelctedItem = null;

			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
				final RemoCoLogEntryListItem item = (RemoCoLogEntryListItem) parent.getItemAtPosition(position);
				assert item != null;
				if (mOldSelctedItem != null) {
					if (mOldSelctedItem == item) {
						mOldSelctedItem = null;
					} else {
						final View oldSelectedView = mOldSelctedItem.getAssociatedView();
						mAdapter.update(oldSelectedView, mOldSelctedItem, false);
						mOldSelctedItem = item;
					}
				} else {
					mOldSelctedItem = item;
				}

				mAdapter.update(view, item, mOldSelctedItem == item);
				mState.mSelectedLogTime = item.getTime();
			}
		});

		asyncUpdateLog();

		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mAdapter.cleanUp();
		RemoCoLogEntryListItem.mAssociatedViews.clear();
	}

	@Override
	public void onDetailFragmentStateChanged(boolean active) {
		final IRemoCon remoCon = RcaService.getContext().getRemoCon();
		if (active) {
			final IOnRemoCoStateChangedListener onStateChangedListener = new IOnRemoCoStateChangedListener() {
				@Override
				public void onStateChanged(IRemoCo remoCo, RemoCoStateChange stateChange) {
					if (stateChange == RemoCoStateChange.RELAUNCHED) {
						if (remoCo instanceof IRemoCon) {
							// This will reload the fragment and choose the right remoco.
							getRemoCoInspectorActivity().selectRemoCo(getRemoCo().getIdName());
						} else {
							asyncUpdateLog();
						}
					} else if (stateChange == RemoCoStateChange.EXCEPTIONS_STATE_CHANGED) {
						asyncUpdateLog();
					}
				}
			};
			remoCon.setOnStateChangedListener(onStateChangedListener);
			getRemoCo().setOnStateChangedListener(onStateChangedListener);
			asyncUpdateLog();
		} else {
			try {
				saveFirstVisibleLogTime();
			} catch (Throwable ignored) {
				mState.mLastFirstVisibleLogTime = Long.MAX_VALUE;
			}

			remoCon.setOnStateChangedListener(null);
			if (mState.mFilterRemoCos.contains(remoCon.getIdName())) {
				remoCon.setOnNewLogEntryListener(null);
			}
			for (IRemoComp remoComp : remoCon.findAllRemoComps(true)) {
				if (mState.mFilterRemoCos.contains(remoComp.getIdName())) {
					remoComp.setOnNewLogEntryListener(null);
					remoComp.setOnStateChangedListener(null);
				}
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		mMenu = menu;
		updateMenu();
	}

	private void updateMenu() {
		if (mMenu == null) {
			return;
		}

		mMenu.clear();

		assert getActivity() != null;
		getActivity().getMenuInflater().inflate(R.menu.remoco_inspector_log_actionbar, mMenu);

		final MenuItem filterLevelItem =
				mMenu.findItem(mMenuLogLevelMap.get(RcaService.getMutableSettings().getRemoCoInspectorFilterLogLevel()));
		assert filterLevelItem != null;
		filterLevelItem.setChecked(true);

		final MenuItem filterInternalLevelItem =
				mMenu.findItem(mMenuInternalLogLevelMap.get(RcaService.getMutableSettings().getRemoCoInspectorFilterInternalLogLevel()));
		assert filterInternalLevelItem != null;
		filterInternalLevelItem.setChecked(true);

		final MenuItem filterRemoCosItem = mMenu.findItem(R.id.log_filter_remocos);
		assert filterRemoCosItem != null;
		final SubMenu filterRemoCosMenu = filterRemoCosItem.getSubMenu();
		assert filterRemoCosMenu != null;

		boolean allSelected = true;
		boolean noneSelected = true;
		for (String remoCoId : mAllRemoCoIds) {
			final MenuItem item = filterRemoCosMenu.add(remoCoId);
			item.setCheckable(true);
			boolean checked = mState.mFilterRemoCos.contains(remoCoId);
			item.setChecked(checked);
			allSelected &= checked;
			if (remoCoId.equals(getRemoCo().getIdName())) {
				item.setEnabled(false);
			} else {
				noneSelected &= !checked;
			}
		}

		final MenuItem filterRemoCosAllItem = mMenu.findItem(R.id.log_filter_remocos_all);
		final MenuItem filterRemoCosSingleItem = mMenu.findItem(R.id.log_filter_remocos_single);
		assert filterRemoCosAllItem != null;
		assert filterRemoCosSingleItem != null;
		filterRemoCosAllItem.setEnabled(!allSelected);
		filterRemoCosSingleItem.setEnabled(!noneSelected);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean update = false;

		RcaLogLevel newLogLevel = null;
		RcaLogLevel newInternalLogLevel = null;

		switch (item.getItemId()) {
			case R.id.log_navi_select:
				asyncUpdateLog(mState.mSelectedLogTime);
				break;
			case R.id.log_navi_top:
				mState.mSelectedLogTime = getLogTimeFromItemsPosition(0, Long.MIN_VALUE);
				asyncUpdateLog(mState.mSelectedLogTime);
				break;
			case R.id.log_navi_bottom:
				mState.mSelectedLogTime = getLogTimeFromItemsPosition(mListView.getCount() - 1, Long.MAX_VALUE);
				asyncUpdateLog(mState.mSelectedLogTime);
				break;
			case R.id.log_filter_remocos_all:
				mState.mFilterRemoCos.clear();
				mState.mFilterRemoCos.addAll(mAllRemoCoIds);
				update = true;
				break;
			case R.id.log_filter_remocos_single:
				mState.mFilterRemoCos.clear();
				mState.mFilterRemoCos.add(getRemoCo().getIdName());
				update = true;
				break;
			case R.id.log_filter_debug:
				newLogLevel = RcaLogLevel.DEBUG;
				break;
			case R.id.log_filter_info:
				newLogLevel = RcaLogLevel.INFO;
				break;
			case R.id.log_filter_warn:
				newLogLevel = RcaLogLevel.WARN;
				break;
			case R.id.log_filter_error:
				newLogLevel = RcaLogLevel.ERROR;
				break;
			case R.id.log_filter_internal_debug:
				newInternalLogLevel = RcaLogLevel.DEBUG;
				break;
			case R.id.log_filter_internal_info:
				newInternalLogLevel = RcaLogLevel.INFO;
				break;
			case R.id.log_filter_internal_warn:
				newInternalLogLevel = RcaLogLevel.WARN;
				break;
			case R.id.log_filter_internal_error:
				newInternalLogLevel = RcaLogLevel.ERROR;
				break;
			case R.id.log_clear:
				RcaService.getContext().findRemoCo(getRemoCo().getIdName(), true).clearLog();
				update = true;
				break;
			case R.id.log_clear_all:
				final IRemoCon remoCon = RcaService.getContext().getRemoCon();
				remoCon.clearLog();
				for (IRemoComp remoComp : remoCon.findAllRemoComps(true)) {
					remoComp.clearLog();
				}
				update = true;
				break;
		}

		if (!update && newLogLevel != null) {
			RcaService.getMutableSettings().setRemoCoInspectorFilterLogLevel(newLogLevel);
			setFilterLogLevel(newLogLevel);
			item.setChecked(!item.isChecked());
			update = true;
		}

		if (!update && newInternalLogLevel != null) {
			RcaService.getMutableSettings().setRemoCoInspectorFilterInternalLogLevel(newInternalLogLevel);
			setFilterInternalLogLevel(newInternalLogLevel);
			item.setChecked(!item.isChecked());
			update = true;
		}

		if (!update) {
			final String remoCoId = item.getTitle().toString();
			final boolean newFilterRemoCos = mAllRemoCoIds.contains(remoCoId);
			if (newFilterRemoCos) {
				if (item.isChecked()) {
					mState.mFilterRemoCos.remove(remoCoId);
				} else {
					if (!mState.mFilterRemoCos.contains(remoCoId)) {
						mState.mFilterRemoCos.add(remoCoId);
					}
				}
				update = true;
			}
		}

		if (update) {
			saveFirstVisibleLogTime();
			asyncUpdateLog(mState.mLastFirstVisibleLogTime);
			updateMenu();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void saveFirstVisibleLogTime() {
		if (mListView.getLastVisiblePosition() == mAdapter.getCount() - 1) {
			// We wanna stay at the very last position even if the log grows.
			mState.mLastFirstVisibleLogTime = Long.MAX_VALUE;
		} else {
			// We wanna stay at the currently visible position.
			mState.mLastFirstVisibleLogTime = mAdapter.getItem(mListView.getFirstVisiblePosition() + 1).getTime();
		}
	}

	private static List<String> retrieveAllRemoCoIds() {
		final IRemoCon remoCon = RcaService.getContext().getRemoCon();
		final List<IRemoComp> remoComps = remoCon.findAllRemoComps(true);
		final List<String> ids = new ArrayList<String>(remoComps.size() + 1);
		ids.add(remoCon.getIdName());
		for (IRemoComp remoComp : remoComps) {
			ids.add(remoComp.getIdName());
		}
		return ids;
	}

	private void asyncUpdateLog() {
		asyncUpdateLog(-1);
	}

	private void asyncUpdateLog(final long scrollToLogTime) {
		new Thread() {
			@Override
			public void run() {
				final CountDownLatch logActivatedLatch = new CountDownLatch(1);
				try {
					final List<RemoCoLogEntryListItem> listItems = new LinkedList<RemoCoLogEntryListItem>();
					final IRemoCon remoCon = RcaService.getContext().getRemoCon();
					if (mState.mFilterRemoCos.contains(remoCon.getIdName())) {
						addRemoCoLogEntryListItems(listItems, remoCon, logActivatedLatch);
					}
					for (IRemoComp remoComp : remoCon.findAllRemoComps(true)) {
						if (mState.mFilterRemoCos.contains(remoComp.getIdName())) {
							addRemoCoLogEntryListItems(listItems, remoComp, logActivatedLatch);
						}
					}

					Collections.sort(listItems, mListEntriesComparator);

					final long usedScrollToLogTime;
					if (scrollToLogTime == -1 && mAdapter.getCount() == 0) {
						usedScrollToLogTime = mState.mLastFirstVisibleLogTime;
					} else {
						usedScrollToLogTime = scrollToLogTime;
					}

					assert getActivity() != null;
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								mAdapter.clear();
								mAdapter.addAll(listItems);

								final int selectedPosition = determineItemPositionFromLogTime(mState.mSelectedLogTime);
								mListView.setItemChecked(selectedPosition, true);
								mState.mSelectedLogTime = getLogTimeFromItemsPosition(selectedPosition, mState.mSelectedLogTime);

								if (usedScrollToLogTime != -1) {
									final int scrollToPostion = usedScrollToLogTime == mState.mSelectedLogTime ?
											selectedPosition : determineItemPositionFromLogTime(usedScrollToLogTime);
									final Runnable selectItem = new Runnable() {
										@Override
										public void run() {
											mListView.setSelection(scrollToPostion);
										}
									};
									if (mListView.getLastVisiblePosition() == mAdapter.getCount() - 1) {
										// We need to post it, otherwise if we are at the very last position, we will stay there.
										mListView.post(selectItem);
									} else {
										selectItem.run();
									}
								}

							} catch (Throwable e) {
								RcaService.getLog().e("failed to update log view for remocos: " + mState.mFilterRemoCos, e);
							} finally {
								logActivatedLatch.countDown();
							}
						}

						private int determineItemPositionFromLogTime(long logTime) {
							int position = Collections.binarySearch(listItems, createTimestampDummyEntry(logTime), mListEntriesComparator);
							if (position < 0) {
								// There is no exact match. Take the next entry following in time.
								position = -position - 1;
								if (position >= listItems.size()) {
									position = listItems.size() - 1;
								}
							}
							return position;
						}
					});
				} catch (Throwable e) {
					logActivatedLatch.countDown();
					RcaService.getLog().e("failed to update log entries for remocos: " + mState.mFilterRemoCos, e);
				}
			}
		}.start();
	}

	private long getLogTimeFromItemsPosition(int position, long ifItemNotFoundTime) {
		RemoCoLogEntryListItem item = null;
		try {
			item = mAdapter.getItem(position);
		} catch (Throwable ignored) {
		}
		return item != null ? item.getTime() : ifItemNotFoundTime;
	}

	private RemoCoLogEntryListItem createTimestampDummyEntry(final long timestamp) {
		return new RemoCoLogEntryListItem(new IRemoCoLogEntry() {
			@Override
			public String getRemoCoId() {
				return null;
			}

			@Override
			public RcaLogLevel getLogLevel() {
				return null;
			}

			@Override
			public long getTime() {
				return timestamp;
			}

			@Override
			public String getTag() {
				return null;
			}

			@Override
			public String getSubTag() {
				return null;
			}

			@Override
			public String getMessage() {
				return null;
			}

			@Override
			public Throwable getException() {
				return null;
			}
		});
	}

	private void addRemoCoLogEntryListItems(List<RemoCoLogEntryListItem> listItems, IRemoCo remoCo, final CountDownLatch logActivatedLatch) {
		remoCo.setOnNewLogEntryListener(new IOnNewRemoCoLogEntryListener() {
			@Override
			public void onNewLogEntry(IRemoCo remoCo, final IRemoCoLogEntry entry) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(!RcaService.getContext().isOnUiThread(), "Not allowed to run on UI thread!");
				}
				try {
					logActivatedLatch.await();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					return;
				}
				if (getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (checkFilter(entry)) {
								mAdapter.add(new RemoCoLogEntryListItem(entry));
								mAdapter.sort(mListEntriesComparator);
							}
						}
					});
				}
			}
		});

		final List<IRemoCoLogEntry> logEntries = remoCo.retrieveInternalLogEntries();
		logEntries.addAll(remoCo.retrieveImplLogEntries());
		for (IRemoCoLogEntry entry : logEntries) {
			if (checkFilter(entry)) {
				listItems.add(new RemoCoLogEntryListItem(entry));
			}
		}
	}

	private boolean checkFilter(IRemoCoLogEntry entry) {
		final boolean internal = !(entry instanceof IRemoCoImplLogEntry);
		return ((!internal && mFilterLogLevel.contains(entry.getLogLevel())) ||
				(internal && mFilterInternalLogLevel.contains(entry.getLogLevel()))) &&
				mState.mFilterRemoCos.contains(entry.getRemoCoId());
	}

	private void setFilterLogLevel(RcaLogLevel logLevel) {
		setLogLevel(mFilterLogLevel, logLevel);
	}

	private void setFilterInternalLogLevel(RcaLogLevel logLevel) {
		setLogLevel(mFilterInternalLogLevel, logLevel);
	}

	private void setLogLevel(List<RcaLogLevel> filterLogLevel, RcaLogLevel logLevel) {
		filterLogLevel.clear();
		switch (logLevel) {
			case DEBUG:
				filterLogLevel.add(RcaLogLevel.DEBUG);
			case INFO:
				filterLogLevel.add(RcaLogLevel.INFO);
			case WARN:
				filterLogLevel.add(RcaLogLevel.WARN);
			case ERROR:
				filterLogLevel.add(RcaLogLevel.ERROR);
		}
	}
}
