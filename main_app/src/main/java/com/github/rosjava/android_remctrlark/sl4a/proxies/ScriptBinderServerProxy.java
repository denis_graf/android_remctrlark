/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a.proxies;

import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.binders.IRcaBinderMethod;
import com.github.rosjava.android_remctrlark.binders.IRcaScriptBinder;
import com.github.rosjava.android_remctrlark.sl4a.IRcaFacade;
import com.github.rosjava.android_remctrlark.sl4a.IRcaScript;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ScriptBinderServerProxy extends APyProxy implements IRcaPyProxy {

	private IRcaScript mScript;
	private IRcaScriptBinder mBinder;

	public ScriptBinderServerProxy(IRcaFacade rcaFacade, IRcaScript script, JSONArray methodsInfo) throws JSONException {
		super(rcaFacade);

		// TODO: Issue #24: Implement Life-Circle of Python-Components -> implement properly
		mScript = script;
		mBinder = mScript.getBinder();

		List<IRcaBinderMethod> castedMethodsInfo = new ArrayList<IRcaBinderMethod>(methodsInfo.length());
		for (int i = 0; i < methodsInfo.length(); ++i) {
			JSONObject methodInfo = (JSONObject) methodsInfo.get(i);
			final String getName = methodInfo.getString("name");
			final String getDoc = methodInfo.getString("doc");
			castedMethodsInfo.add(new IRcaBinderMethod() {
				@Override
				public String getName() {
					return getName;
				}

				@Override
				public String getDoc() {
					return getDoc;
				}
			});
		}

		// TODO: Issue #24: Implement Life-Circle of Python-Components -> binding server to script is allowed only once
		mBinder.bind(mScript.getId(), mRcaFacade.getEventDispatcher(), castedMethodsInfo);

		script.bindScriptBinderServerProxy(this);
	}

	@Override
	public IRcaBinder getBinder() {
		return mBinder;
	}

	@Override
	public void shutdown() {
		if (mBinder != null) {
			mBinder.unbind();
			mBinder = null;
		}
		if (mScript != null) {
			mScript.onShutdownScriptBinderServerProxy();
			mScript = null;
		}
	}
}
