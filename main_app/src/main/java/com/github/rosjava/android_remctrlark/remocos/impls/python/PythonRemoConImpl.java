/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.python;

import android.util.SparseArray;
import android.view.KeyEvent;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IRcaApplication;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.error_handling.LayoutException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoTypeNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaOnKeyListener;
import com.github.rosjava.android_remctrlark.rcajava.binders.IReflectingBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCompNotFoundException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.IRemoConImpl;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ros.node.NodeConfiguration;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class PythonRemoConImpl extends APythonRemoCoImpl implements IRemoConImpl {

	private class KeyListener implements IRcaOnKeyListener {
		private final SparseArray<List<Integer>> mEnabledKeysActions = new SparseArray<List<Integer>>();

		public boolean isActionSupported(int action) {
			return action == KeyEvent.ACTION_DOWN || action == KeyEvent.ACTION_UP;
		}

		public void setKeyActionsEnabled(int keyCode, List<Integer> castedActions, Boolean enable) {
			if (enable) {
				mEnabledKeysActions.put(keyCode, castedActions);
			} else {
				mEnabledKeysActions.delete(keyCode);
			}
		}

		@Override
		public boolean onKey(int keyCode, @NotNull KeyEvent event) {
			if (!getRcaContext().isRemoConActive()) {
				return false;
			}

			final int action = event.getAction();

			if (isKeyActionEnabled(keyCode, action)) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(isActionSupported(action));
				}

				final Map<String, Integer> data = new HashMap<String, Integer>();
				data.put("key", keyCode);
				data.put("action", action);

				// Note: The main thread must always be free when calling Python methods, to avoid deadlocks caused by
				// some from Python called Java routines, which require being executed on the main thread.
				postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() throws RcaBinderInvocationException {
						rpcRemoConHandleKeyEvent("key", data);
					}
				});

				return true;
			}

			return false;
		}

		private boolean isKeyActionEnabled(int keyCode, int action) {
			List<Integer> actions = mEnabledKeysActions.get(keyCode);
			return actions != null && actions.contains(action);
		}
	}

	private KeyListener mKeyListener = new KeyListener();

	public PythonRemoConImpl(IRcaApplication app, String idName, String type, String apk) throws RcaPluginCouldNotBeLoadedException, RemoCoTypeNotFoundException {
		super(app, idName, type, apk);
	}

	public String getIdName(int id) {
		final String pyIdName = getLayout().getInflater().getIdName(id);
		// Note: When using custom view classes with RemoComps inside, the ids are located in APK's resources.
		return pyIdName != null ? pyIdName : getApkContext().getResources().getResourceEntryName(id);
	}

	@Override
	public void start(NodeConfiguration nodeConfiguration) {
		super.start(nodeConfiguration);
		getRcaContext().setKeyListener(mKeyListener);
	}

	@Override
	public void shutdown() {
		getRcaContext().setKeyListener(null);
		super.shutdown();
	}

	@Override
	protected IReflectingBinderImpl createInternalScriptBinderImpl() {
		return new InternalScriptBinder() {
			@SuppressWarnings("UnusedDeclaration")
			public void layoutSetOnKeyListener(JSONArray actions, Integer keyCode, Boolean enable) throws LayoutException, JSONException {
				List<Integer> castedActions = new ArrayList<Integer>(actions.length());
				for (int i = 0; i < actions.length(); i++) {
					int action = (int) actions.getLong(i);
					if (!mKeyListener.isActionSupported(action)) {
						throw new LayoutException(new RcaException("Unsupported key action: " + Integer.toString(action) + "."));
					}
					castedActions.add(action);
				}
				mKeyListener.setKeyActionsEnabled(keyCode, castedActions, enable);
			}

			@SuppressWarnings("UnusedDeclaration")
			public void RemoConRelauncher_commit(JSONObject commit) throws RcaException, JSONException, URISyntaxException {
				RemoConPyRelauncher.commit(commit);
			}

			@SuppressWarnings("UnusedDeclaration")
			public String RemoCompProxy_getType(String id) {
				final IRemoComp remoComp = getRcaContext().getRemoCon().findRemoCompByIdName(id);
				if (remoComp == null) {
					throw new RemoCompNotFoundException(getIdName(), id);
				}
				return remoComp.getType();
			}

			@SuppressWarnings("UnusedDeclaration")
			public String RemoCompProxy_getApk(String id) {
				final IRemoComp remoComp = getRcaContext().getRemoCon().findRemoCompByIdName(id);
				if (remoComp == null) {
					throw new RemoCompNotFoundException(getIdName(), id);
				}
				return remoComp.getApk();
			}
		};
	}

	@Override
	public IRcaBinder create(IRemoCon remoCon) {
		super.create(remoCon);
		return getBinder();
	}

	private void rpcRemoConHandleKeyEvent(String name, Object data) throws RcaBinderInvocationException {
		rpcInvokeInternalMethod("remocon_handle_key_event", name, data);
	}
}
