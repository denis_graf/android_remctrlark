/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.binders.IRcaBinder;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaPluginCouldNotBeLoadedException;
import com.github.rosjava.android_remctrlark.error_handling.RemoCompTypeNotFoundException;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompConfigRetriever;
import com.github.rosjava.android_remctrlark.remocos.IRemoCoImpl;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCompImpl;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.ros.node.NodeConfiguration;

public class RemoComp extends ARemoCo implements IRemoComp {

	private String mIdName = null;

	@SuppressWarnings("UnusedDeclaration")
	public RemoComp(Context context, String idName, String type, String apk) {
		super(context, type, apk);
		mIdName = idName;
	}

	@SuppressWarnings("UnusedDeclaration")
	public RemoComp(Context context) {
		super(context);
	}

	@SuppressWarnings("UnusedDeclaration")
	public RemoComp(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// Take into account only {@code mImplUiLayer} and apply its measured size to {@code mMetaUiLayer} and to this
		// container. Otherwise it can happen that the size of {@code mMetaUiLayer} becomes different and influences the
		// measuring of {@code mImplUiLayer}.
		// Note: Use references here, because they can be changed from other threads.
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final ViewGroup implUiLayer = getImplUiLayer();
		if (implUiLayer != null) {
			implUiLayer.measure(widthMeasureSpec, heightMeasureSpec);
			setMeasuredDimension(implUiLayer.getMeasuredWidth(), implUiLayer.getMeasuredHeight());
			final RemoCoMetaUiLayer metaUiLayer = getMetaUiLayer();
			if (metaUiLayer != null) {
				metaUiLayer.setMeasuredDimensionInternal(implUiLayer.getMeasuredWidth(), implUiLayer.getMeasuredHeight());
			}
		}
	}

	@Override
	public final String getIdName() {
		if (mIdName == null) {
			mIdName = getRemoCon().getIdName(getId());
			assert mIdName != null : "No id specified for a remo-comp!";
		}
		return mIdName;
	}

	@Override
	public final void create() {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			super.create();
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			attachImplUiLayer();
			setCreated();
		}
	}

	@Override
	public final void start(@NotNull NodeConfiguration defaultNodeConfiguration) {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
				Preconditions.checkState(isCreated());
				Preconditions.checkState(!isStarted());
				Preconditions.checkState(!isShutdown());
			}

			try {
				getImpl().start(createNewNodeConfiguration(defaultNodeConfiguration));
			} catch (Throwable e) {
				handleErrorException(e);
			}
		} catch (Throwable e) {
			handleErrorException(e);
		} finally {
			setStarted();
		}
	}

	@Override
	public final void shutdown() {
		try {
			if (BuildConfig.DEBUG) {
				checkNotOnUiThread();
			}
			cancelAllExceptions();
			if (isShutdown()) {
				return;
			}
			setShutdown();
			if (!isCreated()) {
				return;
			}
			try {
				getImpl().shutdown();
			} catch (Throwable e) {
				handleErrorException(e);
			}
		} catch (Throwable e) {
			handleErrorException(e);
		}
		super.shutdown();
	}

	@NotNull
	@Override
	public final IRemoCompConfigRetriever retrieveRemoCoConfig() {
		return (IRemoCompConfigRetriever) super.retrieveRemoCoConfig();
	}

	@Override
	protected final IRemoCompImpl getImpl() {
		return (IRemoCompImpl) super.getImpl();
	}

	@NotNull
	@Override
	protected final Pair<IRemoCoImpl, IRcaBinder> createNewImpl() throws RemoCompTypeNotFoundException, RcaPluginCouldNotBeLoadedException {
		IRemoCompImpl impl = getRcaContext().getApp().getMainRemoCoImplsFactory().createRemoCompImpl(getIdName(), getType(), getApk());
		if (impl == null) {
			RcaException cause = null;
			if (!getRcaContext().getSettings().getPythonEnabled()) {
				final boolean pythonRemoCompExists =
						getRcaContext().getApp().getFileNamesManager().getPyRemoCompFileNamesManager(getType(), getApk()) != null;
				if (pythonRemoCompExists) {
					cause = new RcaException("Running python code is forbidden by RCA. In order to load this RemoComp, please enable python in RCA settings.");
				}
			}
			throw new RemoCompTypeNotFoundException(getType(), getApk(), cause);
		}
		// Note: For constructing Python Android proxies we need the view factory for while creation.
		setViewFactory(impl.getApkViewFactory());
		IRcaBinder binder = impl.create(this);
		return new Pair<IRemoCoImpl, IRcaBinder>(impl, binder);
	}

	@NotNull
	@Override
	protected final Pair<IRemoCoImpl, IRcaBinder> createNewDummyImpl() throws RcaPluginCouldNotBeLoadedException {
		IRemoCompImpl impl = getRcaContext().getApp().getDummyRemoCoImplsFactory().createRemoCompImpl(getIdName(), getType(), getApk());
		// Note: For constructing Python Android proxies we need the view factory for while creation.
		setViewFactory(impl.getApkViewFactory());
		IRcaBinder binder = impl.create(this);
		return new Pair<IRemoCoImpl, IRcaBinder>(impl, binder);
	}

	private NodeConfiguration createNewNodeConfiguration(@NotNull NodeConfiguration defaultNodeConfiguration) {
		final NodeConfiguration newNodeConfiguration = NodeConfiguration.copyOf(defaultNodeConfiguration);
		setupRemoCoNodeConfiguration(newNodeConfiguration);
		return newNodeConfiguration;
	}

}
