/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.event_handling.IRcaOnKeyListener;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigGlobals;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfigSettings;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConSpecialization;
import com.github.rosjava.android_remctrlark.remocon_config.ISerializableRemoConConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConConfigNewMaster;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocon_config.parser.RemoConConfigGlobals;
import com.github.rosjava.android_remctrlark.remocon_config.parser.RemoConConfigParser;
import com.github.rosjava.android_remctrlark.remocon_config.parser.RemoConConfigSettings;
import com.github.rosjava.android_remctrlark.remocon_config.retriever.ISerializeableRemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocon_config.retriever.RemoConConfigRetriever;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoComp;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.github.rosjava.android_remctrlark.remocos.impls.ui.RemoCon;
import com.github.rosjava.android_remctrlark.sl4a.IMainScript;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import org.jetbrains.annotations.NotNull;
import org.ros.address.InetAddressFactory;
import org.ros.android.RcaRosActivity;
import org.ros.exception.RosRuntimeException;
import org.ros.namespace.NameResolver;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

public class RemoConMainActivity extends RcaRosActivity implements IRcaContext {

	public static final String EXTRA_REMOCON_TITLE = "title";
	public static final String EXTRA_REMOCON_ACTIONBAR_TITLE = "actionbar_title";
	public static final String EXTRA_REMOCON_ACTIONBAR_SUBTITLE = "actionbar_subtitle";
	public static final String EXTRA_REMOCON_APK = "RemoConApk";
	public static final String EXTRA_REMOCON_CONFIG_PATH = "RemoConConfigPath";
	public static final String EXTRA_REMOCON_MUTABLES_CONFIG = "MutablesConfig";

	private static final int REQUEST_CODE_FILE_CHOOSER = 3;
	private static final String STATE_REMOCON_CONFIG = "StateRemoConConfig";
	private static final String LOADING_STATUS_SHUTDOWN = "Shutting down...";

	private final ServiceConnection mPythonServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			RcaService.getLog().i("python service connected");
			try {
				mPythonService = ((PythonService.LocalBinder) binder).getService();
				mPythonService.setOnLaunchScriptFinishedHook(new PythonService.IOnLaunchScriptFinished() {
					@Override
					public void onLaunchScriptFinished(Throwable e) {
						if (e != null) {
							if (mMainScriptStartFirstTimeTask != null) {
								mMainScriptStartFirstTimeTask.done();
							}
							if (setErrorState()) {
								showErrorDialog("Python service could not be started.", true, e);
							}
						}
					}
				});
			} catch (Throwable e) {
				showErrorDialog("Error on starting python service. Please try to restart RCA.", true, e);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			RcaService.getLog().i("python service disconnected");
			mPythonService = null;
		}
	};

	private boolean mShutdownRequested = false;
	private final HandlerThread mHandlerThread;
	private final Handler mHandler;
	private final Handler mUiHandler;
	private final Timer mScheduler;
	private PythonService mPythonService = null;
	private RemoCon mRemoCon = null;
	private final CountDownLatch mRemoConSetLatch = new CountDownLatch(1);
	private String mActionbarTitle = null;
	private String mActionbarSubtitle = null;
	private String mPluginApk = null;
	private String mRemoConConfigPath = null;
	private IRemoConMutablesConfig mInitialMutablesConfig = null;
	private ISerializeableRemoConConfigRetriever mRemoConConfig = null;
	private Menu mMenu = null;
	private IRcaOnKeyListener mKeyListener = null;
	private final List<IRcaLoadingTask> mLoadingTasks = new CopyOnWriteArrayList<IRcaLoadingTask>();
	private ProgressDialog mProgressDialog = null;
	private boolean mIsInspectMode = false;
	private ActionMode mInspectActionMode = null;
	private NodeConfiguration mDefaultNodeConfiguration = null;
	private IRcaSettings mSettings;
	private boolean mIsInErrorState = false;
	private final Object mInErrorStateLock = new Object();
	private IMainScript mMainScript = null;
	private IRcaLoadingTask mMainScriptStartFirstTimeTask = null;
	private final Object mMainScriptStartLock = new Object();
	private boolean mMainScriptWasStartedFirstTime = false;
	private NameResolver mRootNameResolver = null;
	private String mMenuLoadingStatus = null;
	private boolean mRemoConConfigInitialized = false;
	private final List<Throwable> mNotByRemoConHandledErrors = new LinkedList<Throwable>();
	private URI mChoosenMasterUri = null;
	private RemoConConfigNewMaster mCreatedNewMaster = null;
	private IOnMutablesConfigChangedListener mOnMutablesConfigChangedListener = null;

	public RemoConMainActivity() {
		mHandlerThread = new HandlerThread("RemoConMainActivity.HandlerThread");
		mHandlerThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				askForAbnormalShutdown(new RcaRuntimeError(ex));
				handleError(ex);
			}
		});
		mHandlerThread.start();
		assert mHandlerThread.getLooper() != null;
		mHandler = new Handler(mHandlerThread.getLooper());
		mUiHandler = new Handler();
		mScheduler = new Timer();
	}

	RemoCon getRemoConInstance() {
		return mRemoCon;
	}

	/*------------------------------------------------------------------------------------------------------------------
	 * Implement IRcaContext
	 */

	@NotNull
	@Override
	public IRcaApplication getApp() {
		return (IRcaApplication) RemoConMainActivity.this.getApplication();
	}

	@Override
	public Context getActivityContext() {
		return RemoConMainActivity.this;
	}

	@NotNull
	@Override
	public NameResolver getRootNameResolver() {
		if (mRootNameResolver == null) {
			mRootNameResolver = NameResolver.newFromNamespace(getSettings().getDmRosNamespace());
		}
		return mRootNameResolver;
	}

	@Override
	public void scheduleTask(TimerTask task, long delay) {
		mScheduler.schedule(task, delay);
	}

	@NotNull
	@Override
	public IRcaSettings getSettings() {
		if (mSettings == null) {
			// Note: Loading settings only once per RemoCon lifetime!
			mSettings = new RcaSettings(PreferenceManager.getDefaultSharedPreferences(this));
		}
		return mSettings;
	}

	@Override
	@NotNull
	public IRemoCon getRemoCon() {
		try {
			mRemoConSetLatch.await();
		} catch (InterruptedException e) {
			throw new RcaRuntimeError(e);
		}
		return mRemoCon;
	}

	@Override
	public boolean isRemoConCreated() {
		return mRemoCon != null && mRemoCon.isCreated();
	}

	@Override
	public boolean isRemoConStarted() {
		return mRemoCon != null && mRemoCon.isStarted();
	}

	@Override
	public boolean isRemoConActive() {
		return mRemoCon != null && mRemoCon.isStarted() && !mRemoCon.isInErrorState() && !mRemoCon.isInWarningState() &&
				!mRemoCon.isShutdown() && !isLoadingMode() && !isInspectMode();
	}

	@Override
	public boolean isRemoConShutdown() {
		return mRemoCon != null && mRemoCon.isShutdown();
	}

	@Override
	public boolean isRemoConRelaunching() {
		return mRemoCon != null && mRemoCon.isRelaunching();
	}

	@NotNull
	@Override
	public String getPluginApk() {
		return mPluginApk;
	}

	@Override
	public IRemoCo findRemoCo(String id) {
		return findRemoCo(id, false);
	}

	@Override
	public IRemoCo findRemoCo(String id, boolean includeBackup) {
		final IRemoCon remoCon = getRemoCon();
		final IRemoCo remoCo;
		if (remoCon.getIdName().equals(id)) {
			remoCo = remoCon;
		} else {
			remoCo = remoCon.findRemoCompByIdName(id, includeBackup);
		}
		return remoCo;
	}

	@NotNull
	@Override
	public NodeMainExecutor getNodeExecutor() {
		assert mNodeMainExecutorService != null;
		return mNodeMainExecutorService;
	}

	@Override
	public void setPyMainScript(IMainScript mainScript) {
		mMainScript = mainScript;
		if (setMainScriptStartingFirstTimeDone()) {
			onMainScriptStartedFirstTime();
		}
	}

	private boolean setMainScriptStartingFirstTimeDone() {
		synchronized (mMainScriptStartLock) {
			if (!mMainScriptWasStartedFirstTime) {
				mMainScriptWasStartedFirstTime = true;
				if (mMainScriptStartFirstTimeTask != null) {
					// Note: {@code mMainScriptStartFirstTimeTask} is always {@code null}, if python is deactivated.
					mMainScriptStartFirstTimeTask.done();
					mMainScriptStartFirstTimeTask = null;
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public IMainScript getPyMainScript() {
		return mMainScript;
	}

	@Override
	public void alertDialog(@NotNull String msg, @NotNull final String title) {
		RemoConMainActivity.this.showAlertDialog(msg, title);
	}

	@Override
	public void setKeyListener(IRcaOnKeyListener listener) {
		mKeyListener = listener;
	}

	@Override
	public ActionMode startInspectMode(@NotNull final ActionMode.Callback callback) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getSettings().getDeveloperModeEnabled());
		}
		mInspectActionMode = super.startActionMode(new ActionMode.Callback() {
			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				if (callback.onCreateActionMode(mode, menu)) {
					mIsInspectMode = true;
					mRemoCon.updateMetaUiLayer();
					return true;
				}
				return false;
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return callback.onPrepareActionMode(mode, menu);
			}

			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				return callback.onActionItemClicked(mode, item);
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
				mIsInspectMode = false;
				mInspectActionMode = null;
				mRemoCon.updateMetaUiLayer();
				callback.onDestroyActionMode(mode);
			}
		});
		return mInspectActionMode;
	}

	@Override
	public void startRemoConInspectMode() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getSettings().getDeveloperModeEnabled());
		}
		assert mRemoCon != null;
		mRemoCon.startInspectMode();
	}

	@Override
	public boolean isInspectMode() {
		return mIsInspectMode;
	}

	@NotNull
	@Override
	public IRcaLoadingTask startLoadingTask(@NotNull final String msg) {
		synchronized (mLoadingTasks) {
			final IRcaLoadingTask task = new IRcaLoadingTask() {
				@Override
				public String getMessage() {
					return msg;
				}

				@Override
				public void done() {
					synchronized (mLoadingTasks) {
						if (mLoadingTasks.remove(this)) {
							if (mLoadingTasks.isEmpty()) {
								mUiHandler.post(new Runnable() {
									@Override
									public void run() {
										onLoadingFinished();
									}
								});
							} else {
								final String newMsg = mLoadingTasks.get(mLoadingTasks.size() - 1).getMessage();
								mUiHandler.post(new Runnable() {
									@Override
									public void run() {
										onLoadingUpdate(newMsg);
									}
								});
							}
						}
					}
				}
			};
			mLoadingTasks.add(task);
			if (mLoadingTasks.size() == 1) {
				mUiHandler.post(new Runnable() {
					@Override
					public void run() {
						onLoadingStarted(msg);
					}
				});
			}
			return task;
		}
	}

	@Override
	public boolean isLoadingMode() {
		return !mLoadingTasks.isEmpty();
	}

	@Override
	public void handleError(final Throwable e) {
		if (mRemoCon != null) {
			// This will call {@link #onHandleRemoCoErrorException()}.
			mRemoCon.handleErrorException(e);
		} else {
			mNotByRemoConHandledErrors.add(e);
			// Show the first error, and if not in developer mode, then shutdown.
			final String excType = e instanceof Error || e instanceof RuntimeException ? "unchecked" : "checked";
			RcaService.getLog().e("Handling unexpected " + excType + " exception.", e);
			final boolean developerModeEnabled = getSettings().getDeveloperModeEnabled();
			if (setErrorState() || developerModeEnabled) {
				showErrorDialog("Unexpected Error", !developerModeEnabled, e);
			}
		}
	}

	@Override
	public void onHandleRemoCoErrorException(final IRemoCo remoCo, final Throwable e) {
		// If not in developer mode, then show the first error and shutdown. Otherwise only enter into error state.
		if (setErrorState() && !getSettings().getDeveloperModeEnabled()) {
			final boolean isRemoCon = remoCo instanceof IRemoCon;
			String type = isRemoCon ? "RemoCon" : "RemoComp";
			final String id = remoCo.getIdName();
			if (id != null) {
				type += " '" + id + "'";
			} else {
				type = (isRemoCon ? "the " : "a ") + type;
			}
			showErrorDialog("Error on " + type + ".", true, e);
		}
	}

	private boolean runOnHandlerThread(@NotNull Runnable runnable) {
		if (isOnRcaHandlerThread()) {
			return checkedForUnexpectedErrorsOnHandlerThread(runnable);
		}
		return postToHandlerThread(runnable);
	}

	@Override
	public boolean postToUiThread(final ICheckedRunnable runnable) {
		return mUiHandler.post(new Runnable() {
			@Override
			public void run() {
				checkedForUnexpectedErrorsOnUiThread(runnable);
			}
		});
	}

	@Override
	public boolean runOnUiThread(@NotNull ICheckedRunnable runnable) {
		if (isOnUiThread()) {
			return checkedForUnexpectedErrorsOnUiThread(runnable);
		} else {
			return postToUiThread(runnable);
		}
	}

	@Override
	public boolean syncRunOnUiThread(@NotNull final ICheckedRunnable runnable) {
		if (isOnUiThread()) {
			return checkedForUnexpectedErrorsOnUiThread(runnable);
		} else {
			final CountDownLatch latch = new CountDownLatch(1);
			mUiHandler.post(new Runnable() {
				@Override
				public void run() {
					checkedForUnexpectedErrorsOnUiThread(runnable);
					latch.countDown();
				}
			});
			try {
				latch.await();
				return true;
			} catch (InterruptedException e) {
				askForAbnormalShutdown(new RcaRuntimeError(e));
			}
		}
		return false;
	}

	public boolean isOnRcaHandlerThread() {
		return Thread.currentThread() == mHandler.getLooper().getThread();
	}

	@Override
	public boolean isOnUiThread() {
		return Thread.currentThread() == mUiHandler.getLooper().getThread();
	}

	@Override
	public void postRelaunchRemoCo(@NotNull String idName) {
		postRelaunchRemoCo(idName, null);
	}

	@Override
	public void postRelaunchRemoCo(@NotNull String idName, Runnable onFinishedHook) {
		handlerRelaunchRemoCo(new RcaId(idName), onFinishedHook);
	}

	@Override
	public void postRelaunchRemoCos(@NotNull final IRemoConMutablesConfigCommit config) {
		handlerRelaunchRemoCos(config);
	}

	@Override
	public void openFileInExplorer(@NotNull String fileName) {
		try {
			RcaActivities.openFileInExplorer(this, fileName, REQUEST_CODE_FILE_CHOOSER);
		} catch (Throwable e) {
			showErrorDialog("Could not open file '" + fileName + "'.", false, e);
		}
	}

	@Override
	public void openFileInTextEditor(@NotNull String fileName) {
		try {
			RcaActivities.openFileInTextEditor(this, fileName);
		} catch (Throwable e) {
			showErrorDialog("Could not open file '" + fileName + "'.", false, e);
		}
	}

	@NotNull
	@Override
	public IRemoConConfigRetriever getRemoConConfig() {
		return mRemoConConfig;
	}

	@Override
	public URI getCurrentMasterUri() {
		return mDefaultNodeConfiguration != null ? mDefaultNodeConfiguration.getMasterUri() : null;
	}

	@Override
	public void setOnMutablesConfigChangedListener(IOnMutablesConfigChangedListener listener) {
		mOnMutablesConfigChangedListener = listener;
	}

	private boolean checkedForUnexpectedErrorsOnHandlerThread(Runnable runnable) {
		try {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(isOnRcaHandlerThread(), "Not on RCA handler thread.");
			}
			runnable.run();
			return true;
		} catch (Throwable e) {
			handleError(new RcaRuntimeError(e));
			return false;
		}
	}

	private boolean checkedForUnexpectedErrorsOnUiThread(@NotNull ICheckedRunnable runnable) {
		try {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(isOnUiThread(), "Not on UI thread.");
			}
			runnable.run();
			return true;
		} catch (Throwable e) {
			handleError(new RcaRuntimeError(e));
			return false;
		}
	}

	/*
	 * Implement IRcaContext
	 *----------------------------------------------------------------------------------------------------------------*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			if (RcaService.getContext() != null) {
				throw new RcaException("Another application runs a RemoCon instance already.");
			}

			RcaService.mRcaContext = this;

			assert getIntent().getExtras() != null;
			final String title = getIntent().getExtras().getString(EXTRA_REMOCON_TITLE);
			if (title != null) {
				setTitle(title);
			}
			mActionbarTitle = getIntent().getExtras().getString(EXTRA_REMOCON_ACTIONBAR_TITLE);
			mActionbarSubtitle = getIntent().getExtras().getString(EXTRA_REMOCON_ACTIONBAR_SUBTITLE);
			mPluginApk = getIntent().getExtras().getString(EXTRA_REMOCON_APK);
			mRemoConConfigPath = getIntent().getExtras().getString(EXTRA_REMOCON_CONFIG_PATH);
			mInitialMutablesConfig = (IRemoConMutablesConfig) getIntent().getExtras().getSerializable(EXTRA_REMOCON_MUTABLES_CONFIG);
		} catch (Throwable e) {
			if (setErrorState()) {
				showErrorDialog("Could not start RemoCon activity.", true, e);
			}
			return;
		}

		if (savedInstanceState != null) {
			mRemoConConfig = (ISerializeableRemoConConfigRetriever) savedInstanceState.getSerializable(STATE_REMOCON_CONFIG);
			if (mRemoConConfig != null) {
				mRemoConConfigInitialized = true;
				RcaService.getLog().i("remocon state reloaded");
			}
		}

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.remocon_main_activity);

		if (getSettings().getPythonEnabled()) {
			startPythonService();
		} else {
			RcaService.getLog().i("python service is disabled");
			createRemoConFragment();
		}
	}

	@Override
	protected void onSaveInstanceState(@NotNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(STATE_REMOCON_CONFIG, mRemoConConfig);
		RcaService.getLog().i("remocon state saved");
	}

	private void onMainScriptStartedFirstTime() {
		RcaService.getLog().i("python core started");
		createRemoConFragment();
	}

	@NotNull
	private static ISerializableRemoConConfig loadRemoConConfig(@NotNull String path, @NotNull String apk) throws RcaException {
		try {
			ISerializableRemoConConfig config = RemoConConfigParser.parse(path, apk);
			assert config != null;
			return config;
		} catch (Throwable e) {
			throw new RcaException("Could not load RemoCon configuration file '" + path + "'.", e);
		}
	}

	private void createRemoConFragment() {
		// Now the RemoCon can be created.
		getFragmentManager().beginTransaction().replace(R.id.container, new RemoConFragment()).commit();
	}

	public static class RemoConFragment extends Fragment {
		public RemoConFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			final RemoConMainActivity activity = (RemoConMainActivity) getActivity();
			assert activity != null;

			View rootView = inflater.inflate(R.layout.remocon_fragment, container, false);
			assert rootView != null;
			ViewGroup remoConRootView = (ViewGroup) rootView.findViewById(R.id.remocon_root);
			activity.createRemoCon(remoConRootView);
			if (!activity.getSettings().getDmRemoConInspectAfterLoaded()) {
				activity.startRemoCon();
			}
			return rootView;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.remocon_actionbar, menu);

		mMenu = menu;
		updateUi();

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.remocon_action_close:
			case android.R.id.home:
				shutdown();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickActionLoadingStatus(MenuItem item) {
		if (mProgressDialog == null) {
			showProgressDialog(mMenuLoadingStatus);
		}
	}

	public void onClickActionInspect(MenuItem item) {
		startRemoConInspectMode();
	}

	public void onClickActionStart(MenuItem item) {
		unsetErrorStateInDeveloperMode();
		startRemoCon();
	}

	public void onClickActionShutdown(MenuItem item) {
		shutdownRemoCon();
	}

	private void updateUi() {
		syncRunOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				if (mMenu == null) {
					return;
				}

				if (getSettings().getDeveloperModeEnabled()) {
					MenuItem itemInspect = mMenu.findItem(R.id.remocon_action_inspect);
					MenuItem itemStart = mMenu.findItem(R.id.remocon_action_start);
					MenuItem itemShutdown = mMenu.findItem(R.id.remocon_action_shutdown);

					assert itemInspect != null;
					assert itemStart != null;
					assert itemShutdown != null;

					if (isLoadingMode() || mRemoCon == null || !mRemoCon.isCreated()) {
						itemInspect.setVisible(false);
						itemStart.setVisible(false);
						itemShutdown.setVisible(false);
					} else if (mRemoCon.isShutdown()) {
						itemInspect.setVisible(false);
						itemStart.setVisible(false);
						itemShutdown.setVisible(false);
					} else if (mRemoCon.isStarted()) {
						itemInspect.setVisible(true);
						itemStart.setVisible(false);
						itemShutdown.setVisible(true);
					} else /*if (mRemoCon.isCreated())*/ {
						itemInspect.setVisible(false);
						itemStart.setVisible(true);
						itemShutdown.setVisible(false);
					}
				}

				MenuItem itemLoadingStatus = mMenu.findItem(R.id.remocon_action_loading_status);
				assert itemLoadingStatus != null;
				if (mMenuLoadingStatus != null) {
					itemLoadingStatus.setTitle(mMenuLoadingStatus);
					itemLoadingStatus.setVisible(true);
				} else {
					itemLoadingStatus.setVisible(false);
				}
			}
		});
		if (mRemoCon != null) {
			mRemoCon.updateMetaUiLayer();
		}
	}

	private boolean chooseMasterAndInitDefaultNodeConfiguration(boolean openMasterChooser, Runnable retry) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnRcaHandlerThread());
		}

		mDefaultNodeConfiguration = null;

		final RemoConConfigNewMaster newMaster = mRemoConConfig.retrieveNewMaster();
		final URI masterUri = mRemoConConfig.retrieveMasterUri();
		if (openMasterChooser || (masterUri == RcaGlobalConstants.NULL_URI && newMaster == RemoConConfigNewMaster.None)) {
			try {
				if (!chooseRosMaster()) {
					showErrorDialog("Could not reach ROS master.", retry);
					return false;
				}
			} catch (MasterChooserCanceledException e) {
				if (!getSettings().getDeveloperModeEnabled()) {
					postShutdown();
				}
				return false;
			} catch (Throwable e) {
				showErrorDialog("Could not reach ROS master.", retry, e);
				return false;
			}
		} else if (newMaster == RemoConConfigNewMaster.None) {
			if (!chooseRosMaster(masterUri.toString())) {
				showErrorDialog("Could not reach ROS master on predefined URL '" + masterUri + "'.", retry);
				return false;
			}
		} else {
			final boolean isPrivate = newMaster == RemoConConfigNewMaster.Private;
			if (!createRosMaster(isPrivate)) {
				showErrorDialog("Could not create new " + (isPrivate ? "private" : "public") + " ROS master.", retry);
				return false;
			}
		}

		if (isRosMasterPrivate()) {
			mDefaultNodeConfiguration = NodeConfiguration.newPrivate();
		} else {
			final String hostAddress;
			try {
				hostAddress = InetAddressFactory.newNonLoopback().getHostAddress();
			} catch (RosRuntimeException e) {
				showErrorDialog("Could not reach ROS master. Please check your network settings.", retry, e);
				return false;
			}
			mDefaultNodeConfiguration = NodeConfiguration.newPublic(hostAddress);
		}

		mDefaultNodeConfiguration.setMasterUri(getRosMasterUri());

		final String uriSpec = mDefaultNodeConfiguration.getMasterUri().toString();
		try {
			URL url = new URL(uriSpec);
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(RcaGlobalConstants.CHECK_ROS_MASTER_CONNECTION_TIMEOUT);
			connection.connect();
		} catch (Throwable e) {
			showErrorDialog("Could not reach ROS master on URL '" + uriSpec + "'.", retry);
			return false;
		}

		return true;
	}

	@Override
	protected boolean createRosMaster(boolean isPrivate) {
		if (super.createRosMaster(isPrivate)) {
			mCreatedNewMaster = isPrivate ? RemoConConfigNewMaster.Private : RemoConConfigNewMaster.Public;
			return true;
		}
		return false;
	}

	@Override
	protected boolean chooseRosMaster(@NotNull String uri) {
		if (super.chooseRosMaster(uri)) {
			mChoosenMasterUri = URI.create(uri);
			mCreatedNewMaster = RemoConConfigNewMaster.None;
			return true;
		}
		return false;
	}

	private void createRemoCon(ViewGroup remoConRootView) {
		String id;
		String type;
		String apk;
		if (mRemoConConfig == null) {
			try {
				final IRemoConConfig config = loadRemoConConfig(mRemoConConfigPath, mPluginApk);
				mRemoConConfig = new RemoConConfigRetriever(config, getRootNameResolver());
			} catch (Throwable e) {
				handleError(e);
			}
		}
		try {
			if (mRemoConConfig == null) {
				// Create a dummy config.
				mRemoConConfig = new RemoConConfigRetriever(new IRemoConConfig() {
					private final RcaId mId = createId();
					private final IRemoConConfigSettings mSettings = new RemoConConfigSettings();
					private final IRemoConConfigGlobals mGlobals = new RemoConConfigGlobals();
					private final List<IRemoConSpecialization> mSpecializations = new LinkedList<IRemoConSpecialization>();

					@Override
					public String getConfigFileFullDir() {
						return mRemoConConfigPath;
					}

					@Override
					public String getBaseConfApk() {
						return mPluginApk;
					}

					@Override
					public String getLogo() {
						return null;
					}

					@Override
					public URI getMasterUri() {
						return null;
					}

					@Override
					public String getNewMaster() {
						return null;
					}

					@Override
					public boolean isMasterMutable() {
						return true;
					}

					@Override
					public IRemoConConfigSettings getSettings() {
						return mSettings;
					}

					@Override
					public IRemoConConfigGlobals getGlobals() {
						return mGlobals;
					}

					@Override
					public List<IRemoConSpecialization> getSpecializations() {
						return mSpecializations;
					}

					@Override
					public IRcaId getId() {
						return mId;
					}

					@Override
					public IRcaId getTypeId() {
						return null;
					}

					private RcaId createId() {
						try {
							return new RcaId(Files.getNameWithoutExtension(mRemoConConfigPath));
						} catch (Throwable ignored) {
							return new RcaId("remocon");
						}
					}
				}, getRootNameResolver());
			}
			id = mRemoConConfig.getId().toString();
			type = mRemoConConfig.retrieveType().toString();
			apk = mRemoConConfig.retrieveApk();
		} catch (Throwable e) {
			final String dummy = "<error>";
			id = dummy;
			type = dummy;
			apk = dummy;
			handleError(e);
		}
		updateTitleBar();
		createRemoCon(remoConRootView, id, type, apk);
	}

	private void updateTitleBar() {
		final ActionBar actionBar = getActionBar();
		assert actionBar != null;

		// Note (Denis Graf): On my device (Lenovo Yoga Tablet 10) the icon is not visible for an unknown reason to me.
		// So I use the custom view as a workaround here, and disable the standard icon by activating the logo which is
		// not set.
		actionBar.setDisplayOptions(ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
//		actionBar.setIcon(android.R.color.transparent);  // requires API level 14
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(R.layout.remocon_main_activity_tilte);

		final String title;
		if (mActionbarTitle != null) {
			title = mActionbarTitle;
		} else {
			title = mRemoConConfig != null ?
					RcaActivities.retrieveRemoConTitle(mRemoConConfig.getConfig()) :
					getString(R.string.title_remocon_main_activity);
		}
		final String newTitle = RcaActivities.updateTitle(getSettings(), title);
		final String usedTitle = newTitle != null ? newTitle : title;
		((TextView) actionBar.getCustomView().findViewById(R.id.title)).setText(usedTitle);

		if (mActionbarSubtitle != null) {
			TextView subtitleView = (TextView) actionBar.getCustomView().findViewById(R.id.subtitle);
			subtitleView.setText(mActionbarSubtitle);
			subtitleView.setTypeface(null, Typeface.ITALIC);
			subtitleView.setVisibility(View.VISIBLE);
		}

		final Drawable logo = RcaActivities.retrieveRemoConLogo(
				getResources(), getApp().getFileNamesManager(), mRemoConConfigPath, mPluginApk, mRemoConConfig.getConfig());
		((ImageView) actionBar.getCustomView().findViewById(R.id.logo)).setImageDrawable(logo);
	}

	private void createRemoCon(final ViewGroup rootView, String idName, String type, String apk) {
		RcaService.getLog().i("creating RemoCon...");

		final IRcaLoadingTask task = startLoadingTask("Loading...");

		// Note: When being in developer mode, we would like to see the view while it is created.
		final boolean attachUiLayerImmediately = getSettings().getDeveloperModeEnabled();
		mRemoCon = new RemoCon(this, idName, type, apk);
		mRemoCon.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		if (attachUiLayerImmediately) {
			attachRemoCon(rootView);
		}
		if (getSettings().getDeveloperModeEnabled()) {
			for (Throwable error : mNotByRemoConHandledErrors) {
				handleError(error);
			}
			mNotByRemoConHandledErrors.clear();
		}
		mRemoConSetLatch.countDown();

		// Note: The ui thread must always be free when calling Python methods, to avoid deadlocks caused by
		// some from Python called Java routines, which require being executed on the main thread.
		boolean posted = postToHandlerThread(new Runnable() {
			@Override
			public void run() {
				try {
					unsetErrorStateInDeveloperMode();
					if (mShutdownRequested || mIsInErrorState) {
						return;
					}

					getApp().getMainRemoCoImplsFactory().applySettings(getSettings());
					if (mShutdownRequested || mIsInErrorState) {
						return;
					}

					if (!mRemoConConfigInitialized && mInitialMutablesConfig != null) {
						applyMutablesConfigCommit(new RemoConMutablesConfigCommit(mInitialMutablesConfig, false));
					}
					mRemoCon.create();
				} finally {
					if (!mRemoConConfigInitialized) {
						// During first creation some initial types (read from layout) could be written into the config.
						// They are part of the initial state.
						mRemoConConfig.setCurrentStateAsInitial();
						mRemoConConfigInitialized = true;
					}
					if (!attachUiLayerImmediately) {
						attachRemoCon(rootView);
					}
					task.done();
				}
			}
		});
		if (!posted) {
			task.done();
		}
	}

	private void attachRemoCon(final ViewGroup rootView) {
		syncRunOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				rootView.addView(mRemoCon);
			}
		});
	}

	private void startRemoCon() {
		startRemoCon(false);
	}

	private void startRemoCon(final boolean openMasterChooser) {
		RcaService.getLog().i("starting RemoCon...");

		final IRcaLoadingTask task = startLoadingTask("Starting...");
		boolean posted = runOnHandlerThread(new Runnable() {
			@Override
			public void run() {
				try {
					if (mShutdownRequested || mIsInErrorState) {
						return;
					}
					if (mRemoCon != null && mRemoCon.isCreated()) {
						try {
							startRos(getString(R.string.app_name), getString(R.string.app_name));
							if (mShutdownRequested || mIsInErrorState) {
								return;
							}
							final Runnable retry = new Runnable() {
								@Override
								public void run() {
									if (mRemoConConfig.isMasterMutable()) {
										startRemoCon(true);
									} else if (!getSettings().getDeveloperModeEnabled()) {
										postShutdown();
									}
								}
							};
							if (chooseMasterAndInitDefaultNodeConfiguration(openMasterChooser, retry)) {
								if (mShutdownRequested || mIsInErrorState) {
									return;
								}
								assert mNodeMainExecutorService != null;
								assert mDefaultNodeConfiguration != null;

								// Relaunch remocon, if the chosen master configuration requires it.
								handlerRelaunchRemoCos(new RemoConMutablesConfigCommit(new RemoConMutablesConfig() {
									@Override
									protected void onConstruct() {
										mMasterUri = RemoConMainActivity.this.mChoosenMasterUri;
										mNewMaster = RemoConMainActivity.this.mCreatedNewMaster;
									}
								}, false));

								mRemoCon.start(mDefaultNodeConfiguration);
							}
						} finally {
							if (!mRemoCon.isStarted()) {
								shutdownRos();
							}
						}
					}
				} finally {
					task.done();
				}
			}
		});
		if (!posted) {
			task.done();
		}
	}

	private void shutdownRemoCon() {
		RcaService.getLog().i("shutting down RemoCon...");

		final IRcaLoadingTask task = startLoadingTask(LOADING_STATUS_SHUTDOWN);
		boolean posted = postToHandlerThread(new Runnable() {
			@Override
			public void run() {
				try {
					if (mRemoCon != null && !mRemoCon.isShutdown()) {
						mRemoCon.shutdown();
						shutdownRos();
					}
				} finally {
					task.done();
				}
			}
		});
		if (!posted) {
			task.done();
		}
	}

	private void handlerRelaunchRemoCos(final IRemoConMutablesConfigCommit config) {
		runOnHandlerThread(new Runnable() {
			@Override
			public void run() {
				final List<IRcaId> allRemoCos = new LinkedList<IRcaId>();
				allRemoCos.add(new RcaId(mRemoCon.getIdName()));
				for (IRemoComp remoComp : mRemoCon.findAllRemoComps()) {
					allRemoCos.add(new RcaId(remoComp.getIdName()));
				}

				final List<IRcaId> remoCosToRelaunch = new LinkedList<IRcaId>();
				final boolean relaunchMaster = mRemoConConfig.retrieveRemoCosToRelaunch(allRemoCos, config, remoCosToRelaunch);
				final Runnable onBeginHook = new Runnable() {
					@Override
					public void run() {
						try {
							applyMutablesConfigCommit(config);
						} catch (Throwable e) {
							handleError(new RcaException("Error on applying mutables config.", e));
						}
					}
				};
				if (remoCosToRelaunch.isEmpty()) {
					onBeginHook.run();
				} else {
					handlerRelaunchRemoCos(remoCosToRelaunch, relaunchMaster, onBeginHook, null);
				}
			}
		});
	}

	private void handlerRelaunchRemoCo(@NotNull IRcaId id, final Runnable onFinishedHook) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(getSettings().getDeveloperModeEnabled(), "This feature is currently available only in developer mode.");
			// TODO: When this feature should become available from the remocos implemetations, then it must not reload
			// the plugins which is only used for developer mode!
		}
		// Some plugins may have changed during development, reload them all.
		getApp().getMainRemoCoImplsFactory().reload();
		final List<IRcaId> list = new ArrayList<IRcaId>(1);
		list.add(id);
		handlerRelaunchRemoCos(list, false, null, onFinishedHook);
	}

	private void handlerRelaunchRemoCos(@NotNull final List<IRcaId> remoCos, final boolean relaunchMaster, final Runnable onBeginHook, final Runnable onFinishedHook) {
		RcaService.getLog().i("relaunching remocon...");

		final IRcaLoadingTask task = startLoadingTask("Relaunching...");
		boolean posted = runOnHandlerThread(new Runnable() {
			@Override
			public void run() {
				try {
					if (onBeginHook != null) {
						onBeginHook.run();
					}
					if (remoCos.isEmpty()) {
						RcaService.getLog().i("relaunch request: no remocos to relaunch");
						return;
					}
					if (mRemoCon != null && mRemoCon.isCreated()) {
						final IRemoCon remoCon = getRemoCon();
						final boolean relaunchRemoCon = remoCos.contains(new RcaId(remoCon.getIdName()));
						boolean rebootRemoConRequested = relaunchRemoCon && getRemoConConfig().retrieveRemoConRebootOnRelaunchEnabled(remoCon);
						if (!rebootRemoConRequested) {
							for (IRemoComp remoComp : remoCon.findAllRemoComps()) {
								if ((relaunchRemoCon || remoCos.contains(new RcaId(remoComp.getIdName()))) &&
										getRemoConConfig().retrieveRemoConRebootOnRelaunchEnabled(remoComp)) {
									rebootRemoConRequested = true;
									break;
								}
							}
						}

						final List<IRcaId> finalRemoCos;
						final boolean finalRelaunchMaster;
						if (rebootRemoConRequested) {
							finalRelaunchMaster = true;
							finalRemoCos = new ArrayList<IRcaId>(1);
							finalRemoCos.add(new RcaId(remoCon.getIdName()));
						} else {
							finalRelaunchMaster = relaunchMaster;
							finalRemoCos = remoCos;
						}

						final boolean wasStarted = mRemoCon.isStarted();
						mRemoCon.relaunchRemoCos(finalRemoCos, finalRelaunchMaster ? null : mDefaultNodeConfiguration);

						if (wasStarted && finalRelaunchMaster) {
							shutdownRos();
							mDefaultNodeConfiguration = null;
							unsetErrorStateInDeveloperMode();
							startRemoCon();
						}
					}
				} finally {
					task.done();
					if (onFinishedHook != null) {
						onFinishedHook.run();
					}
				}
			}
		});
		if (!posted) {
			task.done();
			if (onBeginHook != null) {
				onBeginHook.run();
			}
			if (onFinishedHook != null) {
				onFinishedHook.run();
			}
		}
	}

	@Override
	public void onBackPressed() {
		shutdown();
	}

	@Override
	protected void onDestroy() {
		RcaService.getLog().i("destroying remocon main activity...");
		if (!mShutdownRequested) {
			abnormalShutdown();
		}
		RcaService.mRcaContext = null;
		super.onDestroy();
	}

	@Override
	protected void onNodeMainExecutorServiceUnexpectedShutdown() {
		if (!mShutdownRequested) {
			abnormalShutdown();
		}
	}

	private void postShutdown() {
		mUiHandler.post(new Runnable() {
			@Override
			public void run() {
				if (!mShutdownRequested) {
					shutdown();
				}
			}
		});
	}

	private void shutdown() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
		}

		if (mShutdownRequested) {
			if (mLoadingTasks.isEmpty()) {
				finish();
			} else {
				askForAbnormalShutdown("There are tasks running in background.");
			}
			return;
		}

		unsetErrorStateInDeveloperMode();

		final IRcaLoadingTask task = startLoadingTask(LOADING_STATUS_SHUTDOWN);
		setMainScriptStartingFirstTimeDone();

		shutdownRemoCon();

		postToHandlerThread(new Runnable() {
			@Override
			public void run() {
				if (mMainScript != null) {
					mMainScript.shutdownAllMainScripts();
				}
				if (mMainScript != null) {
					// Fixme: This should actually not happen. But as long as it is only for using in developer mode,
					// and in usual case only one main script is running, we shouldn't get in trouble...
					mMainScript = null;
				}
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(mMainScript == null);
				}
				shutdownPythonService();
			}
		});

		if (!mShutdownRequested) {
			// Note: On first shutdown request {@link #mShutdownRequested} must be false until here, otherwise actions
			// would not be able to be posted (see {@link #postToHandlerThread()}).
			mShutdownRequested = true;
			// Note: quitSafely() requires API level 18.
			// Here we use our own implementation of safely quitting the thread.
			final boolean posted = postToHandlerThread(new Runnable() {
				@Override
				public void run() {
					mHandlerThread.quit();
					mScheduler.cancel();
					// Note: task.done() can change the value of mProgressDialog on ui thread.
					mUiHandler.post(new Runnable() {
						@Override
						public void run() {
							if (mProgressDialog != null && mProgressDialog.isShowing()) {
								task.done();
								if (mIsInErrorState && getSettings().getDmRemoConInspectShutdownErrors()) {
									Toast.makeText(RemoConMainActivity.this, "Errors occurred while shutdown. Stopped for inspection.", Toast.LENGTH_SHORT).show();
								} else {
									finish();
								}
							} else {
								task.done();
							}
						}
					});
				}
			}, true);
			if (!posted) {
				task.done();
			}
		} else {
			task.done();
		}
	}

	private void shutdownPythonService() {
		if (mPythonService != null) {
			RcaService.getLog().i("shutting down python service...");
			PythonService pythonService = mPythonService;
			mPythonService = null;
			pythonService.shutdown();
			unbindService(mPythonServiceConnection);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, @NotNull KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return super.onKeyDown(keyCode, event);
		}
		return (mKeyListener != null && mKeyListener.onKey(keyCode, event)) || super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, @NotNull KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return super.onKeyUp(keyCode, event);
		}
		return (mKeyListener != null && mKeyListener.onKey(keyCode, event)) || super.onKeyUp(keyCode, event);
	}

	private void startPythonService() {
		RcaService.getLog().i("starting python service...");
		String msg = "Starting RCA-Python-Core...";
		if (getSettings().getDmPythonExternalRcaPyCore()) {
			msg += "\nYour current settings require you to start it externally now.";
		}
		mMainScriptStartFirstTimeTask = startLoadingTask(msg);
		try {
			Intent intent = new Intent(RemoConMainActivity.this, PythonService.class);
			// TODO: *DEG* ACTION_START
//			intent.setAction(PythonService.ACTION_START);
//			intent.putExtra(PythonService.EXTRA_NOTIFICATION_TICKER, notificationTicker);
//			intent.putExtra(PythonService.EXTRA_NOTIFICATION_TITLE, notificationTitle);
			startService(intent);
			if (!bindService(intent, mPythonServiceConnection, BIND_AUTO_CREATE)) {
				RcaService.getLog().e("failed to start python service");
				abnormalShutdown();
			}
		} catch (Throwable e) {
			mMainScriptStartFirstTimeTask.done();
			if (setErrorState()) {
				showErrorDialog("Python service could not be started.", true, e);
			}
		}
	}

	private void onLoadingStarted(String msg) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
		}

		showMenuLoadingStatus(msg);
		showProgressDialog(msg);
		setProgressBarIndeterminateVisibility(true);
	}

	private void onLoadingUpdate(String msg) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
		}
		showMenuLoadingStatus(msg);
		setProgressDialogMessage(msg);
	}

	private void onLoadingFinished() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
		}
		updateUi();

		dismissMenuLoadingStatus();
		setProgressBarIndeterminateVisibility(false);

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		} else {
			if (getSettings().getDeveloperModeEnabled() && mShutdownRequested && mLoadingTasks.isEmpty()) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(RemoConMainActivity.this);
				alertDialog.setTitle("Shutdown");
				alertDialog.setMessage("The RemoCon has properly shutdown. Do you wish to exit now?");
				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						shutdown();
					}
				});
				alertDialog.setNegativeButton("No", null);
				alertDialog.show();
			}
		}
		mProgressDialog = null;
	}

	private void showMenuLoadingStatus() {
		showMenuLoadingStatus(mMenuLoadingStatus);
	}

	private void showMenuLoadingStatus(String msg) {
		if (mShutdownRequested) {
			mMenuLoadingStatus = LOADING_STATUS_SHUTDOWN;
		} else {
			final int end = msg.indexOf("\n");
			mMenuLoadingStatus = end != -1 ? msg.substring(0, end) : msg.substring(0);
		}
		updateUi();
	}

	private void dismissMenuLoadingStatus() {
		mMenuLoadingStatus = null;
		updateUi();
	}

	private void showProgressDialog() {
		showProgressDialog(mMenuLoadingStatus);
	}

	private void showProgressDialog(String msg) {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
			Preconditions.checkState(mProgressDialog == null);
		}
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setTitle("Please wait...");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(getSettings().getDeveloperModeEnabled());
		mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				cancelProgressDialog();
			}
		});
		mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
					final boolean shutdownWasNotRequested = !mShutdownRequested;
					if (shutdownWasNotRequested) {
						// Hide the progress dialog.
						cancelProgressDialog();
					}
					mUiHandler.post(new Runnable() {
						@Override
						public void run() {
							RemoConMainActivity.this.onBackPressed();
							mUiHandler.post(new Runnable() {
								@Override
								public void run() {
									if (shutdownWasNotRequested && mProgressDialog == null && mMenuLoadingStatus != null) {
										showMenuLoadingStatus();
										showProgressDialog();
									}
								}
							});
						}
					});
					return true;
				}
				return false;
			}
		});
		setProgressDialogMessage(msg);
		mProgressDialog.show();
	}

	private void setProgressDialogMessage(String msg) {
		if (mProgressDialog != null) {
			mProgressDialog.setMessage(mShutdownRequested ? LOADING_STATUS_SHUTDOWN : msg);
		}
	}

	private void cancelProgressDialog() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(isOnUiThread());
		}
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		mProgressDialog = null;
		if (mInspectActionMode != null) {
			mInspectActionMode.finish();
		}
	}

	private void showErrorDialog(String message, Runnable onClose) {
		showAlertDialog(message, "Error", onClose);
	}

	private void showErrorDialog(String message, Runnable onClose, Throwable e) {
		showAlertDialog(createErrorMessage(message, e), "Error", onClose);
	}

	private void showErrorDialog(String message, boolean shutdown, Throwable e) {
		showAlertDialog(createErrorMessage(message, e), "Error", shutdown ? new Runnable() {
			@Override
			public void run() {
				shutdown();
			}
		} : null);
	}

	private void showAlertDialog(@NotNull final String msg, @NotNull final String title) {
		showAlertDialog(msg, title, null);
	}

	private void showAlertDialog(@NotNull final String msg, @NotNull final String title, final Runnable onClose) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (!mShutdownRequested || getSettings().getDeveloperModeEnabled()) {
					new AlertDialog.Builder(RemoConMainActivity.this)
							.setTitle(title)
							.setMessage(msg)
							.setCancelable(getSettings().getDeveloperModeEnabled())
							.setOnCancelListener(onClose != null ? new DialogInterface.OnCancelListener() {
								@Override
								public void onCancel(DialogInterface dialog) {
									onClose.run();
								}
							} : null)
							.setNeutralButton("OK", onClose != null ? new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									onClose.run();
								}
							} : null)
							.show();
				}
			}
		});
	}

	private boolean postToHandlerThread(@NotNull final Runnable runnable) {
		return postToHandlerThread(runnable, false);
	}

	private boolean postToHandlerThread(@NotNull final Runnable runnable, boolean ignoreShutdownRequestedFlag) {
		boolean posted = (ignoreShutdownRequestedFlag || !mShutdownRequested) && mHandlerThread.isAlive() && mHandler.post(new Runnable() {
			@Override
			public void run() {
				checkedForUnexpectedErrorsOnHandlerThread(runnable);
			}
		});
		if (!posted) {
			RcaService.getLog().w("warning", new RcaRuntimeError("Action could not be posted."));
		}
		return posted;
	}

	private void askForAbnormalShutdown(RcaRuntimeError e) {
		askForAbnormalShutdown("An unexpected error occurred.", e);
	}

	private void askForAbnormalShutdown(String message) {
		askForAbnormalShutdown(message, null);
	}

	private void askForAbnormalShutdown(String message, RcaRuntimeError e) {
		final String finalMessage = createErrorMessage(message + "\nDo you want to enforce an abnormal shutdown?", e);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(RemoConMainActivity.this);
				alertDialog.setTitle("Abnormal Shutdown");
				alertDialog.setMessage(finalMessage);
				alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						abnormalShutdown();
					}
				});
				alertDialog.setNegativeButton("No", null);
				alertDialog.show();
			}
		});
	}

	private void abnormalShutdown() {
		RcaService.getLog().w("abnormal shutdown");
		try {
			mHandlerThread.quit();
			shutdownRos(false);
			shutdownPythonService();
		} catch (Throwable ignored) {
		}
		finish();
	}

	private String createErrorMessage(String message, Throwable e) {
		if (e != null) {
			final String error = exceptionToString(e);
			if (error != null && !error.isEmpty()) {
				message += (getSettings().getDeveloperModeEnabled() ? "\n\n" : "\n") + "Reason: " + error;
			}
		}
		return message;
	}

	private String exceptionToString(@NotNull Throwable e) {
		if (getSettings().getDeveloperModeEnabled()) {
			Writer writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter(writer);
			e.printStackTrace(printWriter);
			return writer.toString();
		} else {
			final String message = e.getMessage();
			return message == null || message.isEmpty() ? e.getClass().getSimpleName() : message;
		}
	}

	/**
	 * Returns true, if error state is entered.
	 */
	private boolean setErrorState() {
		synchronized (mInErrorStateLock) {
			if (mIsInErrorState) {
				return false;
			}
			mIsInErrorState = true;
			return true;
		}
	}

	public void unsetErrorStateInDeveloperMode() {
		synchronized (mInErrorStateLock) {
			if (mIsInErrorState && getSettings().getDeveloperModeEnabled()) {
				mIsInErrorState = false;
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_FILE_CHOOSER) {
			if (resultCode == RESULT_OK) {
				String fileName = null;
				try {
					final Uri uri = data.getData();
					assert uri != null;
					fileName = uri.getPath();
					RcaActivities.openFileInTextEditor(this, fileName);
				} catch (Throwable e) {
					showErrorDialog("Could not open file '" + fileName + "'.", false, e);
				}
			}
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void applyMutablesConfigCommit(IRemoConMutablesConfigCommit commit) {
		mRemoConConfig.applyMutablesConfigCommit(commit);
		callOnMutablesConfigChanged(commit);
	}

	private void callOnMutablesConfigChanged(final IRemoConMutablesConfigCommit config) {
		// Note: To avoid deadlocks, we call the listener (if defined) on a new thread.
		if (mOnMutablesConfigChangedListener != null) {
			new Thread() {
				@Override
				public void run() {
					try {
						// Note: {@code mOnMutablesConfigChangedListener} may change from other threads.
						final IOnMutablesConfigChangedListener listener = mOnMutablesConfigChangedListener;
						if (listener != null) {
							listener.onConfigChanged(config);
						}
					} catch (Throwable e) {
						RcaService.getLog().e("error on calling mutables config listener", e);
					}
				}
			}.start();
		}
	}
}
