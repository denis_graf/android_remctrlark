/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos_inspector;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.rca_logging.RcaLogLevel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RemoCoEntriesListItemAdapter extends ArrayAdapter<RemoCoLogEntryListItem> {

	private static class AnimatedObjectsGroup implements IAnimatedObject {
		public final List<IAnimatedObject> mObjects = new LinkedList<IAnimatedObject>();
		public final ObjectAnimator mAnimator;

		private int mValue;

		private AnimatedObjectsGroup() {
			mAnimator = ObjectAnimator.ofInt(this, "value", 0x00, 0xff);
		}

		@Override
		public int getValue() {
			return mValue;
		}

		@Override
		public void setValue(int value) {
			mValue = value;
			for (IAnimatedObject object : mObjects) {
				object.setValue(value);
			}
		}
	}

	private static class AnimatedTextView implements IAnimatedObject {
		public final TextView mObject;

		private final boolean mAnimateBackround;
		private final int mARGB;
		private final int mRGB;

		private AnimatedTextView(TextView textView, boolean animateBackround) {
			mObject = textView;
			mAnimateBackround = animateBackround;
			mARGB = mObject.getCurrentTextColor();
			mRGB = 0xffffff & mARGB;
		}

		@Override
		public int getValue() {
			// Note: This method is never called.
			return 0;
		}

		@Override
		public void setValue(int value) {
			final float alpha = value / 255f;
			mObject.setTextColor(multColor(mARGB, alpha));
			if (mAnimateBackround) {
				mObject.setBackgroundColor((0xff - value) << 24 | mRGB);
			}
		}

		@Override
		public boolean equals(Object o) {
			return super.equals(o) || (o instanceof AnimatedTextView && mObject.equals(((AnimatedTextView) o).mObject));
		}

		@Override
		public int hashCode() {
			return mObject.hashCode();
		}
	}

	private static final Map<RcaLogLevel, Integer> mLogLevelColors = new HashMap<RcaLogLevel, Integer>();
	private static final Map<String, Integer> mRemoCoColorsMap = new HashMap<String, Integer>();

	static {
		mLogLevelColors.put(RcaLogLevel.DEBUG, Color.LTGRAY);
		mLogLevelColors.put(RcaLogLevel.INFO, Color.GREEN);
		mLogLevelColors.put(RcaLogLevel.WARN, Color.YELLOW);
		mLogLevelColors.put(RcaLogLevel.ERROR, Color.RED);
	}

	private final Context mContext;
	private final List<RemoCoLogEntryListItem> mListItems;
	private final String mRemoCoId;
	private final AnimatedObjectsGroup mAnimatedTextViews = new AnimatedObjectsGroup();

	public RemoCoEntriesListItemAdapter(Context context, List<RemoCoLogEntryListItem> values, String remoCoId) {
		super(context, R.layout.remocon_configs_list_item, values);

		mContext = context;
		mListItems = values;
		mRemoCoId = remoCoId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.remoco_log_entry_list_item, parent, false);
		}
		assert convertView != null;

		final RemoCoLogEntryListItem item;
		item = mListItems.get(position);
		update(convertView, item, item.isSelected());

		return convertView;
	}

	private interface IAnimatedObject {
		int getValue();

		void setValue(int value);
	}

	public void update(View view, RemoCoLogEntryListItem item, boolean selected) {
		item.setSelected(selected);
		item.setAssociatedView(view);

		if (view == null) {
			return;
		}

		final RcaLogLevel logLevel = item.getLogLevel();

		final TextView timeTextView = (TextView) view.findViewById(R.id.time);
		timeTextView.setText(item.getTimeText());
		timeTextView.setTextColor(getRemoCoColor(item.getRemoCoId()));
		timeTextView.setTypeface(null, Typeface.NORMAL);

		final TextView shortMessageTextView = (TextView) view.findViewById(R.id.short_message);
		shortMessageTextView.setText(item.getShortText());
		shortMessageTextView.setTextColor(mLogLevelColors.get(logLevel));
		shortMessageTextView.setTypeface(null, Typeface.NORMAL);
		if (selected) {
			shortMessageTextView.setMaxLines(-1);
			shortMessageTextView.setSingleLine(false);
		} else {
			shortMessageTextView.setMaxLines(1);
			shortMessageTextView.setSingleLine();
		}

		final TextView detailsTextView = (TextView) view.findViewById(R.id.details);
		detailsTextView.setText(selected ? item.getDetailsText() : "");
		detailsTextView.setTextColor(Color.GRAY);
		detailsTextView.setTypeface(null, Typeface.NORMAL);
		detailsTextView.setVisibility(selected ? View.VISIBLE : View.GONE);

		final boolean ownLog = item.getRemoCoId().equals(mRemoCoId);
		if (ownLog) {
			timeTextView.setTypeface(null, getTextViewStyle(timeTextView) | Typeface.BOLD);
		}

		final boolean alert = logLevel == RcaLogLevel.ERROR || logLevel == RcaLogLevel.WARN;
		if (alert) {
			shortMessageTextView.setTypeface(null, getTextViewStyle(shortMessageTextView) | Typeface.BOLD);
		}

		int darker = 0;

		if (!item.isImplEntry()) {
			darker += 1;
		}

		if (!ownLog) {
			darker += 1;

			timeTextView.setTypeface(null, getTextViewStyle(timeTextView) | Typeface.ITALIC);
			shortMessageTextView.setTypeface(null, getTextViewStyle(shortMessageTextView) | Typeface.ITALIC);
			detailsTextView.setTypeface(null, getTextViewStyle(detailsTextView) | Typeface.ITALIC);
		}

		if (darker > 0 && !selected) {
			float c = 0.7f;
			float colorFactor = (float) Math.pow(c, darker);
			if (ownLog) {
				timeTextView.setTextColor(multColor(timeTextView.getCurrentTextColor(), colorFactor));
			}
			shortMessageTextView.setTextColor(multColor(shortMessageTextView.getCurrentTextColor(), alert ? colorFactor / c : colorFactor));
		}

		final List<IAnimatedObject> animatedObjects = mAnimatedTextViews.mObjects;
		final ObjectAnimator animator = mAnimatedTextViews.mAnimator;
		final AnimatedTextView animated = new AnimatedTextView(shortMessageTextView, true);
		animatedObjects.remove(animated);
		//noinspection ThrowableResultOfMethodCallIgnored
		if (item.getException() != null && !selected && !item.isExceptionCanceled()) {
			animatedObjects.add(animated);
			if (!animator.isRunning()) {
				animator.setIntValues(0x00, 0xff);
				animator.setDuration(500);
				animator.setRepeatMode(ValueAnimator.REVERSE);
				animator.setRepeatCount(ValueAnimator.INFINITE);
				animator.start();
			}
		} else {
			shortMessageTextView.setBackgroundColor(Color.TRANSPARENT);
			if (animatedObjects.isEmpty() && animator.isRunning()) {
				animator.end();
			}
		}
	}

	public void cleanUp() {
		mAnimatedTextViews.mAnimator.cancel();
	}

	private int getRemoCoColor(String remoCoId) {
		if (remoCoId.equals(mRemoCoId)) {
			return Color.WHITE;
		}

		final Integer color = mRemoCoColorsMap.get(remoCoId);
		if (color != null) {
			return color;
		}

		final Random rnd = new Random();

		int th = 0xa;
		int red, green, blue;
		do {
			red = rnd.nextInt(0x100);
			green = rnd.nextInt(0x100);
			blue = rnd.nextInt(0x100);
		} while (red < th && green < th && blue < th);

		int newColor = Color.argb(0xff, red, green, blue);

		mRemoCoColorsMap.put(remoCoId, newColor);
		return newColor;
	}

	private int getTextViewStyle(TextView view) {
		final Typeface typeface = view.getTypeface();
		return typeface != null ? typeface.getStyle() : 0;
	}

	private static int multColor(int color, float mult) {
		return filterColor(color, mult, 0f, 0x00FFFFFF);
	}

	private static int filterColor(int color, float mult, float add, int mask) {
		final float red = Math.min((Color.red(color) / 255f) * mult + add, 1f);
		final float green = Math.min((Color.green(color) / 255f) * mult + add, 1f);
		final float blue = Math.min((Color.blue(color) / 255f) * mult + add, 1f);
		final int newColor = Color.argb(Color.alpha(color), (int) (255f * red), (int) (255f * green), (int) (255f * blue));
		return (color & ~mask) | (newColor & mask);
	}
}
