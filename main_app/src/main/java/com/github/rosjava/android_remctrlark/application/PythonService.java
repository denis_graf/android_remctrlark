/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2012, Anthony Prieur & Daniel Oppenheim. All rights reserved.
 *
 * Original from SL4A modified to allow to embed Interpreter and scripts into an APK
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original from SL4A modified to allow to embed Interpreter and scripts into an APK
 */

package com.github.rosjava.android_remctrlark.application;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.android.python27.process.MyScriptProcess;
import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.python_service.R;
import com.google.common.base.Preconditions;
import com.googlecode.android_scripting.AndroidProxy;
import com.googlecode.android_scripting.ForegroundService;
import com.googlecode.android_scripting.NotificationIdFactory;
import com.googlecode.android_scripting.interpreter.InterpreterConfiguration;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PythonService extends ForegroundService {

	private final static int NOTIFICATION_ID = NotificationIdFactory.create();

	public interface IOnLaunchScriptFinished {
		void onLaunchScriptFinished(Throwable e);
	}

	private final IBinder mBinder;
	private final Object mOnLaunchScriptFinishedHandleLock = new Object();
	private boolean mLaunchScriptFinished = false;
	private Throwable mScriptLauchedException = null;
	private IOnLaunchScriptFinished mOnLaunchScriptFinishedHook = null;
	private MyScriptProcess myScriptProcess = null;
	private AndroidProxy mProxy = null;
	private InterpreterConfiguration mInterpreterConfiguration = null;

	public class LocalBinder extends Binder {
		public PythonService getService() {
			return PythonService.this;
		}
	}

	public PythonService() {
		super(NOTIFICATION_ID);
		mBinder = new LocalBinder();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	private void killProcess() {
		if (myScriptProcess != null) {
			myScriptProcess.kill();  // Note: It shuts down the proxy as well.
			myScriptProcess = null;
			mProxy = null;
		}
		if (mProxy != null) {
			// Note: The proxy can be running without any script, e.g. when starting the script externally.
			mProxy.shutdown();
			mProxy = null;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, final int startId) {
		new Thread() {
			@Override
			public void run() {
				try {
					try {
						killProcess();
					} catch (Throwable ignored) {
					}
					launchScript(startId);
				} catch (Throwable e) {
					mScriptLauchedException = e;
				} finally {
					if (mScriptLauchedException != null) {
						RcaService.getLog().e("script process [" + Integer.toString(startId) + "] could not be launched", mScriptLauchedException);
					} else {
						RcaService.getLog().i("script process [" + Integer.toString(startId) + "] launched");
					}
					mLaunchScriptFinished = true;
					handleOnLaunchScriptFinished();
				}
			}
		}.start();

		return START_NOT_STICKY;
	}

	public void shutdown() {
		try {
			killProcess();
		} catch (Throwable ignored) {
		}
		stopForeground(true);
		stopSelf();
	}

	private void launchScript(final int startId) {
		try {
			killProcess();
		} catch (Throwable ignored) {
		}

		assert RcaService.getApp() != null;
		assert RcaService.getContext() != null;

		final IRcaSettings rcaSettings = RcaService.getContext().getSettings();
		final IMainFileNamesManager fileNamesManager = RcaService.getApp().getFileNamesManager();

		final String pythonDir = fileNamesManager.getPythonFullDir();
		final String pythonLibDir = fileNamesManager.getPythonLibFullDir();
		final String mainScriptName = fileNamesManager.getMainScriptFullDir();
		final String pluginApkDir = fileNamesManager.getPluginApkFullDir(RcaService.getContext().getPluginApk());
		final String pythonLibDynLoadFullDir = fileNamesManager.getPythonLibDynLoadFullDir();
		final String pythonLibPythonDir = fileNamesManager.getPythonLibPythonDir();
		final String pythonExtrasFullDir = fileNamesManager.getPythonExtrasPythonFullDir();
		final String pythonTempFullDir = fileNamesManager.getPythonExtrasTempFullDir();
		final String rcaPythonCoreFullDir = fileNamesManager.getRcaPythonCoreFullDir();
		final String pythonBinFullFileName = fileNamesManager.getPythonBinFullFileName();
		final File mainScriptFile = new File(mainScriptName);

		// arguments
		final ArrayList<String> args = new ArrayList<String>();
		switch (rcaSettings.getDmPythonOptimization()) {
			case 0:
				RcaService.getLog().i("no python optimizations set");
				break;
			case 1:
				args.add("-O");
				RcaService.getLog().i("python optimizations -O set");
				break;
			case 2:
				args.add("-OO");
				RcaService.getLog().i("python optimizations -OO set");
				break;
			default:
				RcaService.getLog().w("ignoring unknown python optimization setting");
				break;
		}

		// Note: The file argument has to be the last one, because it terminates the option list!
		args.add(mainScriptName);
		args.add("--foreground");

		// env var
		final Map<String, String> environmentVariables = new HashMap<String, String>();
		if (!rcaSettings.getDmPythonLogcatLogging()) {
			environmentVariables.put("RCA_LOGCAT_DISABLED", "x");
		}
		environmentVariables.put("PYTHONPATH",
				pythonExtrasFullDir + ":" +
						rcaPythonCoreFullDir + ":" +
						pythonLibDynLoadFullDir + ":" +
						pythonLibPythonDir
		);
		environmentVariables.put("TEMP", pythonTempFullDir);
		environmentVariables.put("PYTHONHOME", pythonDir);
		environmentVariables.put("LD_LIBRARY_PATH", pythonLibDir + ":" + pythonLibDynLoadFullDir);

		// Launch script.
		final boolean handshake = !rcaSettings.getDmPythonNoHandshake();
		final int port = rcaSettings.getPythonPort();
		RcaService.getLog().i("android proxy settings:" +
				"\n  handshake: " + (handshake ? "enabled" : "disabled") +
				"\n  port: " + Integer.toString(port));

		boolean proxyStarted = false;
		try {
			mProxy = new AndroidProxy(this, null, handshake);
			mProxy.startLocal(port);
			proxyStarted = true;
		} finally {
			if (proxyStarted) {
				RcaService.getLog().i("android proxy started");
			} else {
				RcaService.getLog().e("android proxy could not be started");
			}
		}

		String loggedArguments = "";
		for (String arg : args) {
			loggedArguments += arg + " ";
		}

		String loggedEnvVars = "";
		for (Map.Entry<String, String> entry : environmentVariables.entrySet()) {
			loggedEnvVars += "\n    " + entry.getKey() + "=" + entry.getValue();
		}

		RcaService.getLog().i("python settings:" +
						"\n  binary: " + pythonBinFullFileName +
						"\n  working dir: " + pluginApkDir +
						"\n  sdcard package dir: " + pluginApkDir +
						"\n  arguments: " + loggedArguments +
						"\n  environment variables:\n" + loggedEnvVars
		);

		final File pythonBinary = new File(pythonBinFullFileName);

		if (!RcaService.getContext().getSettings().getDmPythonExternalRcaPyCore()) {
			myScriptProcess = MyScriptProcess.launchScript(mainScriptFile, mInterpreterConfiguration, mProxy, new Runnable() {
				@Override
				public void run() {
					RcaService.getLog().i("script process [" + Integer.toString(startId) + "] shut down");
				}
			}, pluginApkDir, pluginApkDir, args, environmentVariables, pythonBinary);
		}
	}

	public void setOnLaunchScriptFinishedHook(@NotNull IOnLaunchScriptFinished hook) {
		synchronized (mOnLaunchScriptFinishedHandleLock) {
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(mOnLaunchScriptFinishedHook == null);
			}
			mOnLaunchScriptFinishedHook = hook;
			// Call the hook immediately, if the script is already launched.
			handleOnLaunchScriptFinished();
		}
	}

	private void handleOnLaunchScriptFinished() {
		synchronized (mOnLaunchScriptFinishedHandleLock) {
			if (mLaunchScriptFinished && mOnLaunchScriptFinishedHook != null) {
				mOnLaunchScriptFinishedHook.onLaunchScriptFinished(mScriptLauchedException);
				mOnLaunchScriptFinishedHook = null;
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		handleOnLaunchScriptFinished();
	}

	@Override
	protected Notification createNotification() {

		// This contentIntent is a noop.
		PendingIntent contentIntent = PendingIntent.getService(this, 0, new Intent(), 0);
		return new NotificationCompat.Builder(this)
				.setContentTitle(this.getString(R.string.app_name))
				.setContentText(this.getString(R.string.stat_python_service_running))
				.setSmallIcon(R.drawable.ic_stat_python_logo_glassy)
				.setContentIntent(contentIntent)
				.setAutoCancel(true)
				.build();
	}
}
