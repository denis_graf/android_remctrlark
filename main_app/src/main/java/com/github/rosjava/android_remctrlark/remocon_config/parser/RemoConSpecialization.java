/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import com.github.rosjava.android_remctrlark.remocon_config.IRemoCompConfig;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConSpecialization;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

class RemoConSpecialization extends ARemoCoSpecialization implements IRemoConSpecialization, Serializable {

	static final String XML_TAG = "spec";

	final Map<IRcaId, IRemoCompConfig> mRemoCompConfigs = new HashMap<IRcaId, IRemoCompConfig>();

	@Override
	public Map<IRcaId, IRemoCompConfig> getRemoCompConfigs() {
		return mRemoCompConfigs;
	}

}
