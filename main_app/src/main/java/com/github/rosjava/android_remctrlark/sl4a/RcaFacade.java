/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import android.annotation.SuppressLint;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.IRcaContext;
import com.github.rosjava.android_remctrlark.application.IRcaLayoutInflaterFactory;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.ProxyNotFoundException;
import com.github.rosjava.android_remctrlark.error_handling.RcaBinderInvocationException;
import com.github.rosjava.android_remctrlark.error_handling.RcaException;
import com.github.rosjava.android_remctrlark.error_handling.RemoCoNotFoundException;
import com.github.rosjava.android_remctrlark.error_handling.ScriptNotFoundException;
import com.github.rosjava.android_remctrlark.event_handling.IRcaEventDispatcher;
import com.github.rosjava.android_remctrlark.event_handling.RcaEvents;
import com.github.rosjava.android_remctrlark.misc.IdGenerator;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.IRemoCon;
import com.github.rosjava.android_remctrlark.sl4a.proxies.AndroidProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.ErrorProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.IRcaPyProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.InterrupterProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.IpyQueryProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.LoggerProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.MainScriptProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.ProxyProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.RemoCoProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.RemoCoScriptProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.ScriptBinderServerProxy;
import com.github.rosjava.android_remctrlark.sl4a.proxies.ScriptProcessMessengerProxy;
import com.google.common.base.Preconditions;
import com.googlecode.android_scripting.facade.EventFacade;
import com.googlecode.android_scripting.facade.FacadeManager;
import com.googlecode.android_scripting.facade.ui.UiFacade;
import com.googlecode.android_scripting.jsonrpc.RpcReceiver;
import com.googlecode.android_scripting.rpc.Rpc;
import com.googlecode.android_scripting.rpc.RpcMinSdk;
import com.googlecode.android_scripting.rpc.RpcParameter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

@RpcMinSdk(13)
public class RcaFacade extends RpcReceiver implements IRcaFacade {

	@SuppressLint("UseSparseArrays")
	static private final Map<Integer, IRcaPyProxy> mProxies = new HashMap<Integer, IRcaPyProxy>();
	static private final IdGenerator mPythonRidGenerator = new IdGenerator();
	static private final IdGenerator mMainScriptIdGenerator = new IdGenerator();
	static private final Map<String, IRcaScript> mScriptsByIds = new ConcurrentHashMap<String, IRcaScript>();
	static private final Map<String, IRcaScript> mScriptsByHostPids = new ConcurrentHashMap<String, IRcaScript>();

	private enum State {
		WAIT_FOR_INITIALIZATION,
		WAIT_FOR_BINDING,
		BOUND,
		SHUTDOWN
	}

	private final EventFacade mEventFacade;
	private final IRcaEventDispatcher mEventDispatcher = new IRcaEventDispatcher() {
		@Override
		public void dispatch(String name, Object data) {
			mEventFacade.postEvent(name, data);
		}
	};

	private State mState = State.WAIT_FOR_INITIALIZATION;

	private String mPythonTypeName;
	private String mPythonHostname = null;
	private int mPythonPid = -1;
	private int mPythonTid = -1;
	private int mPythonRid = -1;

	private IRcaPyProxy mProxy = null;

	public RcaFacade(FacadeManager manager) {
		super(manager);
		mEventFacade = manager.getReceiver(EventFacade.class);
	}

	@Override
	public IRcaContext getRcaContext() {
		return RcaService.getContext();
	}

	@Override
	public String getPythonTypeName() {
		return mPythonTypeName;
	}

	@Override
	public String getPythonHostname() {
		return mPythonHostname;
	}

	@Override
	public int getPythonPid() {
		return mPythonPid;
	}

	@Override
	public int getPythonTid() {
		return mPythonTid;
	}

	@Override
	public String getPyScriptHostPid(String hostname, int pid) {
		return hostname != null && pid > 0 ? Integer.toString(pid) + "@" + hostname : null;
	}

	@Override
	public String getPyScriptHostPid(IRcaScript script) {
		return getPyScriptHostPid(script.getHostname(), script.getPid());
	}

	/**
	 * Fixme: This method is not thread synchronized! But as long as it is only for using in developer mode, and in
	 * usual case only one main script is running, we shouldn't get in trouble...
	 */
	@Override
	public IMainScript getNewestMainScript(String ignoredId) {
		final Set<String> scriptIds = mScriptsByIds.keySet();
		final SortedSet<String> mainScriptIds = new TreeSet<String>();
		for (String id : scriptIds) {
			if (id.startsWith(MainScript.SCRIPT_ID) && (ignoredId == null || !id.equals(ignoredId))) {
				mainScriptIds.add(id);
			}
		}
		try {
			return (IMainScript) getScriptById(mainScriptIds.last());
		} catch (Throwable ignored) {
			return null;
		}
	}

	@Override
	@NotNull
	public IRcaPyProxy getProxyByRid(int rid) throws ProxyNotFoundException {
		IRcaPyProxy proxy;
		synchronized (mProxies) {
			proxy = mProxies.get(rid);
		}
		if (proxy == null) {
			throw new ProxyNotFoundException(rid);
		}
		return proxy;
	}

	@Override
	public List<IRcaPyProxy> getAllProxies() {
		synchronized (mProxies) {
			// Avoid ConcurrentModificationException by making a copy of proxies list.
			return new ArrayList<IRcaPyProxy>(mProxies.values());
		}
	}

	@Override
	public void putPyScript(IRcaScript script) {
		// TODO: #24: Implement Life-Circle of Python-Components: shutdown previous script with same id
		mScriptsByIds.put(script.getId(), script);
		String hostPidKey = getPyScriptHostPid(script);
		if (hostPidKey != null) {
			mScriptsByHostPids.put(hostPidKey, script);
		}
	}

	@Override
	public void removePyScript(IRcaScript script) {
		String hostPidKey = getPyScriptHostPid(script);
		if (hostPidKey != null) {
			mScriptsByHostPids.remove(hostPidKey);
		}
		mScriptsByIds.remove(script.getId());
	}

	@Override
	@NotNull
	public IRcaScript getScriptById(String id) throws ScriptNotFoundException {
		IRcaScript script = mScriptsByIds.get(id);
		if (script == null) {
			throw new ScriptNotFoundException(id);
		}
		return script;
	}

	@Override
	public IRcaScript getScriptByHostPid(String hostname, int pid) {
		String hostPidKey = getPyScriptHostPid(hostname, pid);
		return hostPidKey != null ? mScriptsByHostPids.get(hostPidKey) : null;
	}

	@Override
	public IRcaEventDispatcher getEventDispatcher() {
		return mEventDispatcher;
	}

	@Override
	public void addEventObserver(EventFacade.EventObserver observer) {
		mEventFacade.addGlobalEventObserver(observer);
	}

	@Override
	public void removeEventObserver(EventFacade.EventObserver observer) {
		mEventFacade.removeEventObserver(observer);
	}

	@Override
	public void sendInterruptEvent() {
		mEventFacade.postEvent(RcaEvents.INTERRUPT, null, true);
	}

	@Override
	public void shutdown() {
		shutdownProxy();
	}

	@Rpc(description = "TODO.")
	public int _rcaInit(
			@RpcParameter(name = "typeName", description = "TODO") String typeName,
			@RpcParameter(name = "hostname", description = "TODO") String hostname,
			@RpcParameter(name = "pid", description = "TODO") Integer pid,
			@RpcParameter(name = "tid", description = "TODO") Integer tid) {
		Preconditions.checkState(mState == State.WAIT_FOR_INITIALIZATION);
		mPythonTypeName = typeName;
		mPythonHostname = hostname;
		mPythonPid = pid;
		mPythonTid = tid;
		mPythonRid = mPythonRidGenerator.generate();
		mState = State.WAIT_FOR_BINDING;
		return mPythonRid;
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsProxy(@RpcParameter(name = "proxyRid", description = "TODO") Integer proxyRid) throws ProxyNotFoundException {
		bindProxy(new ProxyProxy(this, getProxyByRid(proxyRid)));
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsAndroid(
			@RpcParameter(name = "id", description = "TODO") final String id
	) throws RcaException {
		bindProxy(new AndroidProxy(this));

		final IRemoCo remoCo = getRcaContext().findRemoCo(id);
		if (remoCo == null) {
			throw new RemoCoNotFoundException(id);
		}

		final IRcaLayoutInflaterFactory viewFactory = remoCo.getApkViewFactory();
		assert viewFactory != null;

		final UiFacade uiFacade = mManager.getReceiver(UiFacade.class);
		assert uiFacade != null;
		uiFacade.setViewFactory(viewFactory);
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsIPyQuery() {
		bindProxy(new IpyQueryProxy(this));
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsError() {
		bindProxy(new ErrorProxy(this));
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsInterrupter() {
		bindProxy(new InterrupterProxy(this));
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsLogger(Boolean isRemoCoImplLog, String tag) {
		bindProxy(new LoggerProxy(this, isRemoCoImplLog, tag));
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsScriptProcessMessenger() {
		bindProxy(new ScriptProcessMessengerProxy(this));
	}

	@Rpc(description = "TODO.")
	public String _rcaBindAsMainScript() throws RcaException {
		final String id = MainScript.SCRIPT_ID + "." + mMainScriptIdGenerator.generate();
		bindProxy(new MainScriptProxy(this, new MainScript(this, id)));
		return id;
	}

	@Rpc(description = "TODO.")
	public RemoCoInfo _rcaBindAsRemoCoScript(
			@RpcParameter(name = "id", description = "TODO") final String id) throws InterruptedException, RcaException {
		RemoCoScript script = null;
		try {
			script = (RemoCoScript) getScriptById(id);
		} catch (ScriptNotFoundException scriptNotFoundException) {
			final IRemoCon remoCon = RcaService.getContext().getRemoCon();
			if (!remoCon.getIdName().equals(id) && remoCon.findRemoCompByIdName(id) == null) {
				throw new RemoCoNotFoundException(id);
			}
		}

		if (script == null || script.getRemoCo().isCreated()) {
			// A script was started from outside, e.g. for debugging.
			final CountDownLatch latch = new CountDownLatch(1);
			final IMainScript mainScript = getRcaContext().getPyMainScript();
			mainScript.replaceNextStartRemoCoScriptRequest(id, new Runnable() {
				@Override
				public void run() {
					latch.countDown();
				}
			});
			getRcaContext().postRelaunchRemoCo(id, new Runnable() {
				@Override
				public void run() {
					latch.countDown();
					// Note: If there was an error and the script was not started, we have to remove it here.
					mainScript.removeNextStartRemoCoScriptRequest(id);
				}
			});
			latch.await();

			final RemoCoScript newScript = (RemoCoScript) getScriptById(id);
			if (script != null && script == newScript) {
				throw new RcaException("RemoCo script '" + id + "' could not be relaunched for binding script proxy.");
			}
			script = newScript;
		}

		RemoCoScriptProxy proxy = new RemoCoScriptProxy(this, script);
		bindProxy(proxy);
		return new RemoCoInfo(getRcaContext().getApp().getFileNamesManager(), script.getRemoCo());
	}

	@Rpc(description = "TODO.")
	public void _rcaBindAsScriptBinderServer(
			@RpcParameter(name = "name", description = "TODO") String id,
			@RpcParameter(name = "methodsInfo", description = "TODO") JSONArray methodsInfo) throws JSONException, ScriptNotFoundException {
		IRcaScript script = getScriptById(id);
		ScriptBinderServerProxy proxy = new ScriptBinderServerProxy(this, script, methodsInfo);
		bindProxy(proxy);
	}

	@Rpc(description = "TODO.")
	public RemoCoInfo _rcaBindAsRemoCo(
			@RpcParameter(name = "name", description = "TODO") String name) throws RcaException {
		RemoCoProxy remoCoProxy = new RemoCoProxy(this, name);
		bindProxy(remoCoProxy);
		return remoCoProxy.getInfo();
	}

	@Rpc(description = "TODO.")
	public Object _rcaCallMethod(
			@RpcParameter(name = "name", description = "TODO") String name,
			@RpcParameter(name = "args", description = "TODO") JSONArray args) throws RcaBinderInvocationException {
		Preconditions.checkState(mState == State.BOUND);
		assert mProxy != null;
		return mProxy.getBinder().invokeMethod(name, args);
	}

	private void bindProxy(@NotNull IRcaPyProxy proxy) {
		Preconditions.checkState(mState == State.WAIT_FOR_BINDING);
		if (BuildConfig.DEBUG) {
			Preconditions.checkState(mProxy == null);
		}
		synchronized (mProxies) {
			mProxy = proxy;
			if (BuildConfig.DEBUG) {
				Preconditions.checkState(!mProxies.values().contains(mProxy));
				Preconditions.checkState(!mProxies.keySet().contains(mPythonRid));
			}
			mProxies.put(mPythonRid, mProxy);
		}
		mState = State.BOUND;
	}

	private synchronized void shutdownProxy() {
		if (mState == State.BOUND) {
			assert mProxy != null;
			mProxy.shutdown();
			synchronized (mProxies) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkState(mProxies.values().contains(mProxy));
					Preconditions.checkState(mProxies.keySet().contains(mPythonRid));
					Preconditions.checkState(mProxies.get(mPythonRid) == mProxy);
				}
				mProxies.remove(mPythonRid);
				mProxy = null;
			}
		} else if (mProxy != null) {
			synchronized (mProxies) {
				if (mProxies.get(mPythonRid) == mProxy) {
					mProxies.remove(mPythonRid);
				}
				mProxy = null;
			}
		} else {
			mProxy = null;
		}
		mState = State.SHUTDOWN;
	}
}
