/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.application;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.error_handling.RemoConConfigBaseCycleException;
import com.github.rosjava.android_remctrlark.error_handling.RemoConConfigParserException;
import com.github.rosjava.android_remctrlark.rca_logging.RcaJavaLog;
import com.github.rosjava.android_remctrlark.rca_plugin_main_app.application.ARcaActivity;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoConConfig;
import com.github.rosjava.android_remctrlark.remocon_config.ISerializableRemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfig;
import com.github.rosjava.android_remctrlark.remocon_config.RemoConMutablesConfigCommit;
import com.github.rosjava.android_remctrlark.remocon_config.parser.RemoConConfigParser;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.RcaId;
import com.google.common.base.Preconditions;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class InitialChooserActivity extends InstallRcaActivity {

	public static final String EXTRA_RCA_APK = ARcaActivity.RCA_ACTIVITY_EXTRA_RCA_APK;
	public static final String EXTRA_TITLE = ARcaActivity.RCA_ACTIVITY_EXTRA_TITLE;
	public static final String EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL = ARcaActivity.RCA_ACTIVITY_EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL;
	public static final String EXTRA_MODE = "mode";

	public static final String MODE_DEFAULT = "default";
	public static final String MODE_REMOCONS_VIEWER = "remocons_viewer";
	public static final String MODE_REMOCOMPS_VIEWER = "remocomps_viewer";

	private static final int REQUEST_CODE_REMOCON_MAIN_ACTIVITY = 2;
	private static final int REQUEST_CODE_FILE_CHOOSER = 3;
	private static final int REQUEST_CODE_SETTINGS_ACTIVITY = 4;
	private static final int REQUEST_CODE_PLUGIN_MAIN_ACTIVITY = 5;

	private String mApk = null;
	private boolean mOnlyForRcaPluginInstall = false;
	private String mMode = MODE_DEFAULT;
	private Menu mMenu = null;
	private RcaSettings mSettings = null;
	private boolean mFinishAfterRemoConShutdown = false;
	final Stack<String> mSubfoldersStack = new Stack<String>();

	private IRcaSettings getSettings() {
		if (mSettings == null) {
			mSettings = new RcaSettings(PreferenceManager.getDefaultSharedPreferences(this));
		}
		return mSettings;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chooser_actionbar, menu);
		mMenu = menu;
		applySettings();
		return true;
	}

	public void onClickActionSettings(MenuItem item) {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivityForResult(intent, REQUEST_CODE_SETTINGS_ACTIVITY);
	}

	public void onClickActionReload(MenuItem item) {
		loadList();
	}

	public void onClickActionClose(MenuItem item) {
		finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// The settings might have been changed, reload them.
		mSettings = null;
		applySettings();
	}

	@Override
	protected void onInstallRcaFinished(boolean installStatus) {
		if (!installStatus) {
			showErrorDialog("RCA application could not be properly installed.");
		}

		setContentView(R.layout.initital_chooser_activity);

		final String title;
		if (getIntent().getExtras() != null) {
			title = getIntent().getExtras().getString(EXTRA_TITLE, null);
			mApk = getIntent().getExtras().getString(EXTRA_RCA_APK, getPackageName());
			mOnlyForRcaPluginInstall = getIntent().getExtras().getBoolean(EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL, mOnlyForRcaPluginInstall);
			mMode = getIntent().getExtras().getString(EXTRA_MODE, mMode);
		} else {
			mApk = getPackageName();
			title = null;
		}
		if (BuildConfig.DEBUG) {
			Preconditions.checkArgument(mApk != null && !mApk.isEmpty());
		}
		assert mApk != null;

		if (title != null) {
			setTitle(title);
		}

		if (mApk.equals(getPackageName())) {
			loadList();
		} else {
			installPlugin(mApk, new IOnInstallPluginFinished() {
				@Override
				public void onInstallPluginFinished(boolean installStatus) {
					if (mOnlyForRcaPluginInstall) {
						finish();
						return;
					}
					if (!installStatus) {
						// Just show the error message, but don't do anything about it...
						AlertDialog.Builder alertDialog = new AlertDialog.Builder(InitialChooserActivity.this);
						alertDialog.setTitle("Error");
						alertDialog.setMessage("Rem-Ctrl-Ark plugin could not be properly installed.");
/*
						alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								finish();
							}
						});
						alertDialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								finish();
							}
						});
*/
						alertDialog.show();
//						return;
					}
					loadList();
				}
			});
		}
	}

	private void applySettings() {
		final String newTitle = RcaActivities.updateTitle(getSettings(), this.getTitle().toString());
		if (newTitle != null) {
			setTitle(newTitle);
		}

		if (mMenu == null) {
			return;
		}

		MenuItem itemSettings = mMenu.findItem(R.id.chooser_action_settings);
		MenuItem itemReload = mMenu.findItem(R.id.chooser_action_reload);
		assert itemSettings != null;
		assert itemReload != null;

		itemSettings.setVisible(isMainApk());
		// Note: The reload button is also useful in not developer mode when installing new plugins.
		itemReload.setVisible(true);

		getApp().getMainRemoCoImplsFactory().applySettings(getSettings());
	}

	private void loadList() {
		new AInstallAsyncTask() {
			private final List<IInitialChooserListItem> mListItems = new LinkedList<IInitialChooserListItem>();

			@Override
			protected boolean install() {
				setMessageProgressDialog("Loading...");
				try {
					getApp().getMainRemoCoImplsFactory().reload();
				} catch (Throwable e) {
					RcaJavaLog.LOG.e("error on reloading remoco factories", e);
				}

				final IMainFileNamesManager fileNamesManager = getApp().getFileNamesManager();

				if (isInRootFolder()) {
					if (isMainApk()) {
						try {
							mListItems.addAll(loadPluginsListItems(fileNamesManager));
						} catch (Throwable e) {
							RcaJavaLog.LOG.e("error on loading plugins", e);
						}
					} else if (getSettings().getDeveloperModeEnabled()) {
						if (mMode.equals(MODE_DEFAULT)) {
							try {
								mListItems.addAll(loadRemoCoViewerLaunchersListItems(mApk));
							} catch (Throwable e) {
								RcaJavaLog.LOG.e("error on loading remocos for viewing", e);
							}
						} else {
							try {
								if (mMode.equals(MODE_REMOCONS_VIEWER)) {
									mListItems.addAll(loadRemoConViewersListItems(fileNamesManager, mApk));
								} else if (mMode.equals(MODE_REMOCOMPS_VIEWER)) {
									mListItems.addAll(loadRemoCompViewersListItems(fileNamesManager, mApk));
								}
							} catch (Throwable e) {
								RcaJavaLog.LOG.e("error on loading remocos for viewing", e);
							}
							return true;
						}
					}
				}

				try {
					mListItems.addAll(loadCategories(fileNamesManager, getCurrentDir(fileNamesManager), mApk));
				} catch (Throwable e) {
					RcaJavaLog.LOG.e("error on loading remocon configs subfolders", e);
				}

				try {
					mListItems.addAll(loadRemoConConfigsListItems(fileNamesManager, getCurrentDir(fileNamesManager), mApk));
				} catch (Throwable e) {
					RcaJavaLog.LOG.e("error on loading remocon configs", e);
				}

				return true;
			}

			@Override
			protected void onFinished(boolean installStatus) {
				if (BuildConfig.DEBUG) {
					Preconditions.checkArgument(installStatus);
				}

				if (!isMainApk() && mListItems.size() == 1 &&
						!getSettings().getDeveloperModeEnabled() &&
						mListItems.get(0) instanceof IInitialChooserRemoConConfigListItem) {
					final IInitialChooserRemoConConfigListItem item = (IInitialChooserRemoConConfigListItem) mListItems.get(0);
					assert item != null;
					mFinishAfterRemoConShutdown = true;
					startRemoConMainActivity(item.getApk(), item.getFullFileName());
					return;
				}

				final ListAdapter adapter = new InitialChooserListItemAdapter(InitialChooserActivity.this, mListItems);
				final ListView listView = (ListView) findViewById(R.id.remocon_configs_list);
				listView.setAdapter(adapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
						final IInitialChooserListItem item = (IInitialChooserListItem) parent.getItemAtPosition(position);
						assert item != null;
						item.onClick();
					}
				});
				listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
						if (!getSettings().getDeveloperModeEnabled()) {
							return false;
						}
						final IInitialChooserListItem item = (IInitialChooserListItem) parent.getItemAtPosition(position);
						assert item != null;
						return item.onDmLongClick();
					}
				});
			}
		}.execute();
	}

	private boolean isMainApk() {
		return mApk == null || mApk.equals(RcaGlobalConstants.MAIN_APK);
	}

	private List<IInitialChooserRemoCoViewersLauncherListItem> loadRemoCoViewerLaunchersListItems(final String apk) {
		final List<IInitialChooserRemoCoViewersLauncherListItem> items = new ArrayList<IInitialChooserRemoCoViewersLauncherListItem>(2);

		final Drawable logo = RcaActivities.retrieveRemoCoViewerLogo(getResources());

		items.add(new IInitialChooserRemoCoViewersLauncherListItem() {
			@NotNull
			@Override
			public String getTitle() {
				return "All RemoCon Types";
			}

			@Override
			public String getSubtitle() {
				return "RemoCon Viewer";
			}

			@NotNull
			@Override
			public Drawable getLogo() {
				return logo;
			}

			@Override
			public void onClick() {
				if (!getSettings().getDeveloperModeEnabled()) {
					loadList();
				} else {
					startRemoConViewersActivity(apk);
				}
			}

			@Override
			public boolean onDmLongClick() {
				return false;
			}
		});

		items.add(new IInitialChooserRemoCoViewersLauncherListItem() {
			@NotNull
			@Override
			public String getTitle() {
				return "All RemoComp Types";
			}

			@Override
			public String getSubtitle() {
				return "RemoComp Viewer";
			}

			@NotNull
			@Override
			public Drawable getLogo() {
				return logo;
			}

			@Override
			public void onClick() {
				if (!getSettings().getDeveloperModeEnabled()) {
					loadList();
				} else {
					startRemoCompViewersActivity(apk);
				}
			}

			@Override
			public boolean onDmLongClick() {
				return false;
			}
		});

		return items;
	}

	private List<IInitialChooserPluginListItem> loadPluginsListItems(IMainFileNamesManager fileNamesManager) {
		final List<IInitialChooserPluginListItem> items = new LinkedList<IInitialChooserPluginListItem>();

		for (final String pluginApk : getApp().getMainRemoCoImplsFactory().retrievePluginApks()) {
			if (pluginApk.equals(RcaGlobalConstants.MAIN_APK)) {
				continue;
			}

			final String label;
			try {
				assert getApplicationContext() != null;
				final PackageManager packageManager = getApplicationContext().getPackageManager();
				assert packageManager != null;
				final ApplicationInfo appInfo = getApp().getMainRemoCoImplsFactory().getPlugin(pluginApk).getApplicationInfo();
				label = packageManager.getApplicationLabel(appInfo).toString();
			} catch (Throwable e) {
				RcaJavaLog.LOG.e("rca plugin '" + pluginApk + "' could not be loaded for preview", e);
				continue;
			}
			final String title = label != null && !label.isEmpty() ? label : pluginApk.substring(pluginApk.lastIndexOf(".") + 1);
			final Drawable logo = RcaActivities.retrievePluginLogo(getResources(), fileNamesManager, pluginApk);

			items.add(new IInitialChooserPluginListItem() {
				@NotNull
				@Override
				public String getTitle() {
					return title;
				}

				@Override
				public String getSubtitle() {
					return getApk();
				}

				@NotNull
				@Override
				public Drawable getLogo() {
					return logo;
				}

				@NotNull
				@Override
				public String getApk() {
					return pluginApk;
				}

				@Override
				public void onClick() {
					startPluginActivity(getApk());
				}

				@Override
				public boolean onDmLongClick() {
					final String fileName = getApp().getFileNamesManager().getPluginApkFullDir(getApk());
					try {
						RcaActivities.openFileInExplorer(InitialChooserActivity.this, fileName, REQUEST_CODE_FILE_CHOOSER);
					} catch (Throwable e) {
						showErrorDialog("Could not open file '" + fileName + "'.");
					}
					return true;
				}
			});
		}

		sortRemoConListItems(items);
		return items;
	}

	private List<IInitialChooserRemoCoViewerListItem> loadRemoConViewersListItems(IMainFileNamesManager fileNamesManager, String apk) throws RemoConConfigBaseCycleException, IOException, RemoConConfigParserException {
		return loadRemoCoViewersListItems(fileNamesManager,
				getApp().getMainRemoCoImplsFactory().getAllRemoConTypes(apk),
				apk,
				fileNamesManager.getRemoConViewerFullFileName(),
				"RemoCon: ");
	}

	private List<IInitialChooserRemoCoViewerListItem> loadRemoCompViewersListItems(IMainFileNamesManager fileNamesManager, String apk) throws RemoConConfigBaseCycleException, IOException, RemoConConfigParserException {
		return loadRemoCoViewersListItems(
				fileNamesManager,
				getApp().getMainRemoCoImplsFactory().getAllRemoCompTypes(apk),
				apk,
				fileNamesManager.getRemoCompViewerFullFileName(),
				"RemoComp: ");
	}

	private List<IInitialChooserRemoCoViewerListItem> loadRemoCoViewersListItems(IMainFileNamesManager fileNamesManager,
	                                                                             Set<String> types, final String apk,
	                                                                             final String fullFileName,
	                                                                             final String titlePrefix) throws IOException, RemoConConfigBaseCycleException, RemoConConfigParserException {
		final List<IInitialChooserRemoCoViewerListItem> items = new LinkedList<IInitialChooserRemoCoViewerListItem>();
		for (final String type : types) {

			final IRemoConConfig remoConConfigPreview = RemoConConfigParser.parsePreview(fullFileName, RcaGlobalConstants.MAIN_APK);
			final Drawable logo = RcaActivities.retrieveRemoConLogo(getResources(), fileNamesManager, fullFileName, RcaGlobalConstants.MAIN_APK, remoConConfigPreview);

			items.add(new IInitialChooserRemoCoViewerListItem() {
				@NotNull
				@Override
				public String getTitle() {
					return type;
				}

				@Override
				public String getSubtitle() {
					return null;
				}

				@NotNull
				@Override
				public Drawable getLogo() {
					return logo;
				}

				@Override
				public void onClick() {
					if (!getSettings().getDeveloperModeEnabled()) {
						finish();
						return;
					}
					final RemoConMutablesConfig config = new RemoConMutablesConfig();
					config.applyCommit(new RemoConMutablesConfigCommit(new RemoConMutablesConfig() {
						@Override
						protected void onConstruct() {
							mTypes.put(new RcaId("type"), new Pair<IRcaId, String>(new RcaId(type), apk));
						}
					}, false));
					startRemoConMainActivity(apk, fullFileName, config, titlePrefix + type, apk);
				}

				@Override
				public boolean onDmLongClick() {
					return false;
				}
			});
		}
		sortRemoConListItems(items);
		return items;
	}

	private List<IInitialChooserSubfolderListItem> loadCategories(IMainFileNamesManager fileNamesManager, String folder, final String apk) {
		final List<IInitialChooserSubfolderListItem> items = new LinkedList<IInitialChooserSubfolderListItem>();

		final File dir = new File(folder);
		final File[] files = dir.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY);
		if (files != null) {
			for (File file : files) {
				if (!getSettings().getDeveloperModeEnabled() && file.getName().startsWith(".")) {
					continue;
				}

				final String fullFileName = file.getAbsolutePath();

				final String title = file.getName();
				final Drawable logo = RcaActivities.retrieveRemoConsCategoryLogo(getResources(), fileNamesManager, fullFileName, apk);
				assert logo != null;

				items.add(new IInitialChooserSubfolderListItem() {
					@NotNull
					@Override
					public String getTitle() {
						return title;
					}

					@Override
					public String getSubtitle() {
						return "Category";
					}

					@NotNull
					@Override
					public Drawable getLogo() {
						return logo;
					}

					@Override
					public void onClick() {
						enterSubfolder(fullFileName);
						loadList();
					}

					@Override
					public boolean onDmLongClick() {
						try {
							RcaActivities.openFileInExplorer(InitialChooserActivity.this, fullFileName, REQUEST_CODE_FILE_CHOOSER);
						} catch (Throwable e) {
							showErrorDialog("Could not open file '" + fullFileName + "'.");
						}
						return true;
					}
				});

			}
		}

		sortRemoConListItems(items);
		return items;
	}

	@Override
	public void onBackPressed() {
		if (leaveSubfolder()) {
			loadList();
		} else {
			super.onBackPressed();
		}
	}

	private List<IInitialChooserRemoConConfigListItem> loadRemoConConfigsListItems(IMainFileNamesManager fileNamesManager, String folder, final String apk) {
		final List<IInitialChooserRemoConConfigListItem> items = new LinkedList<IInitialChooserRemoConConfigListItem>();

		final File dir = new File(folder);
		final FileFilter fileFilter = new SuffixFileFilter(fileNamesManager.getRemoConConfigSuffix());
		final File[] files = dir.listFiles(fileFilter);

		if (files != null) {
			for (File file : files) {
				if (!getSettings().getDeveloperModeEnabled() && file.getName().startsWith(".")) {
					continue;
				}

				final String fullFileName = file.getAbsolutePath();
				final IRemoConConfig remoConConfigPreview;
				try {
					remoConConfigPreview = RemoConConfigParser.parsePreview(fullFileName, apk);
				} catch (Throwable e) {
					RcaJavaLog.LOG.e("could not read preview of remocon file '" + fullFileName + "'");
					continue;
				}

				final String title = RcaActivities.retrieveRemoConTitle(remoConConfigPreview);
				final Drawable logo = RcaActivities.retrieveRemoConLogo(getResources(), fileNamesManager, fullFileName, apk, remoConConfigPreview);

				items.add(new IInitialChooserRemoConConfigListItem() {
					@NotNull
					@Override
					public String getTitle() {
						return title;
					}

					@Override
					public String getSubtitle() {
						return null;
					}

					@NotNull
					@Override
					public Drawable getLogo() {
						return logo;
					}

					@NotNull
					@Override
					public String getApk() {
						return apk;
					}

					@NotNull
					@Override
					public String getFullFileName() {
						return fullFileName;
					}

					@Override
					public void onClick() {
						startRemoConMainActivity(getApk(), getFullFileName());
					}

					@Override
					public boolean onDmLongClick() {
						try {
							RcaActivities.openFileInTextEditor(InitialChooserActivity.this, getFullFileName());
						} catch (Throwable e) {
							showErrorDialog("Could not open file '" + getFullFileName() + "'.");
						}
						return true;
					}
				});
			}
		}

		sortRemoConListItems(items);
		return items;
	}

	private boolean isInRootFolder() {
		return mSubfoldersStack.isEmpty();
	}

	private void enterSubfolder(String folder) {
		mSubfoldersStack.add(folder);
	}

	private boolean leaveSubfolder() {
		if (mSubfoldersStack.isEmpty()) {
			return false;
		}
		mSubfoldersStack.pop();
		return true;
	}

	private String getCurrentDir(IMainFileNamesManager fileNamesManager) {
		if (mSubfoldersStack.isEmpty()) {
			return fileNamesManager.getRemoConConfigsFullDir(mApk);
		}
		return mSubfoldersStack.peek();
	}

	private void sortRemoConListItems(List<? extends IInitialChooserListItem> items) {
		Collections.sort(items, new Comparator<IInitialChooserListItem>() {
			@Override
			public int compare(IInitialChooserListItem lhs, IInitialChooserListItem rhs) {
				return lhs.getTitle().compareTo(rhs.getTitle());
			}
		});
	}

	private void startRemoConViewersActivity(String apk) {
		Intent intent = new Intent(this, InitialChooserActivity.class);
		intent.putExtra(InitialChooserActivity.EXTRA_RCA_APK, apk);
		intent.putExtra(InitialChooserActivity.EXTRA_TITLE, getTitle());
		intent.putExtra(InitialChooserActivity.EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL, false);
		intent.putExtra(InitialChooserActivity.EXTRA_MODE, MODE_REMOCONS_VIEWER);
		startActivity(intent);
	}

	private void startRemoCompViewersActivity(String apk) {
		Intent intent = new Intent(this, InitialChooserActivity.class);
		intent.putExtra(InitialChooserActivity.EXTRA_RCA_APK, apk);
		intent.putExtra(InitialChooserActivity.EXTRA_TITLE, getTitle());
		intent.putExtra(InitialChooserActivity.EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL, false);
		intent.putExtra(InitialChooserActivity.EXTRA_MODE, MODE_REMOCOMPS_VIEWER);
		startActivity(intent);
	}

	private void startPluginActivity(String apk) {
		Intent intent = new Intent();
		intent.setClassName(apk, apk + ".application.MainActivity");
		startActivityForResult(intent, REQUEST_CODE_PLUGIN_MAIN_ACTIVITY);
	}

	private void startRemoConMainActivity(String apk, String configPath) {
		startRemoConMainActivity(apk, configPath, null, null, null);
	}

	private void startRemoConMainActivity(String apk, String configPath, ISerializableRemoConMutablesConfig config, String title, String subtitle) {
		if (!checkNoRemoConIsRunning()) {
			return;
		}

		Intent intent = new Intent(this, RemoConMainActivity.class);
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_TITLE, getTitle());
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_ACTIONBAR_TITLE, title);
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_ACTIONBAR_SUBTITLE, subtitle);
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_APK, apk);
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_CONFIG_PATH, configPath);
		intent.putExtra(RemoConMainActivity.EXTRA_REMOCON_MUTABLES_CONFIG, config);

		startActivityForResult(intent, REQUEST_CODE_REMOCON_MAIN_ACTIVITY);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_REMOCON_MAIN_ACTIVITY) {
			// Note: Setting {@link RcaService#mRcaContext} to {@code null} only in
			// {@link RemoConMainActivity#onDestroy()} doesn't always work...
			RcaService.mRcaContext = null;
			if (mFinishAfterRemoConShutdown) {
				finish();
			}
		} else if (requestCode == REQUEST_CODE_FILE_CHOOSER && resultCode == RESULT_OK) {
			String fileName = null;
			try {
				final Uri uri = data.getData();
				assert uri != null;
				fileName = uri.getPath();
				RcaActivities.openFileInTextEditor(InitialChooserActivity.this, fileName);
			} catch (Throwable e) {
				showErrorDialog("Could not open file '" + fileName + "'.");
			}
		} else if (requestCode == REQUEST_CODE_SETTINGS_ACTIVITY) {
			loadList();
		} else if (requestCode == REQUEST_CODE_PLUGIN_MAIN_ACTIVITY) {
			loadList();
		}
	}

	private boolean checkNoRemoConIsRunning() {
		if (RcaService.getContext() != null) {
			showErrorDialog("Another application runs a RemoCon instance already.\nPlease first shut it down.");
			return false;
		}
		return true;
	}

	private void showErrorDialog(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(InitialChooserActivity.this);
				alertDialog.setTitle("Error");
				alertDialog.setMessage(message);
				alertDialog.setNeutralButton("Ok", null);
				alertDialog.show();
			}
		});
	}

	private IRcaApplication getApp() {
		return RcaService.getApp();
	}
}
