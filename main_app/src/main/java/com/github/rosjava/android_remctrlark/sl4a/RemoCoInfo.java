/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.sl4a;

import com.github.rosjava.android_remctrlark.application.IMainFileNamesManager;
import com.github.rosjava.android_remctrlark.application.IPyRemoCoFileNamesManager;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.github.rosjava.android_remctrlark.remocos.RemoCoInfoRetriever;

public class RemoCoInfo implements IRcaFieldsToJsonMappable {
	public String id;
	public String type;
	public String apk;
	public String script;
	public String super_type;

	public RemoCoInfo(IMainFileNamesManager fileNamesManager, IRemoCo remoCo) {
		assert remoCo != null;
		id = remoCo.getIdName();
		type = remoCo.getType();
		apk = remoCo.getApk();
		IPyRemoCoFileNamesManager pyRemoCoFileNamesManager = fileNamesManager.getPyRemoCoFileNamesManager(type, apk);
		if (pyRemoCoFileNamesManager != null && RemoCoInfoRetriever.retrieveImplLanguage(remoCo) == RemoCoInfoRetriever.ImplLanguage.PYTHON) {
			script = pyRemoCoFileNamesManager.getMainScriptRelName();
		} else {
			// This is not a python RemoCo, there is no script.
			script = "<Non-Python-RemoCo>";
		}
		super_type = RemoCoInfoRetriever.retrieveIsRemoCon(remoCo) ? "remocon" : "remocomp";
	}

}
