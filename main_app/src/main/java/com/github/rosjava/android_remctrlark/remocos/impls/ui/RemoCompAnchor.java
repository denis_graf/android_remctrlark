/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocos.impls.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.github.rosjava.android_remctrlark.R;
import com.github.rosjava.android_remctrlark.application.RcaService;

import java.lang.reflect.Field;

public class RemoCompAnchor extends FrameLayout {
	public static final String TAG = "rca.RemoComp";
	private static final String TAG_RESOURCE_CLASS_NAME = TAG.replaceAll("\\.", "_");

	private String mType = null;
	private String mApk = null;

	public RemoCompAnchor(Context pluginContext) {
		super(pluginContext);
	}

	public RemoCompAnchor(Context pluginContext, AttributeSet attrs) {
		this(pluginContext, attrs, 0);
	}

	public RemoCompAnchor(Context pluginContext, AttributeSet attrs, int defStyle) {
		super(pluginContext, attrs, defStyle);
		assert pluginContext.getTheme() != null;
		int attrArray[] = getResourceDeclareStyleableIntArray(pluginContext);
		TypedArray a = pluginContext.obtainStyledAttributes(attrs, attrArray);
		assert a != null;

		setType(a.getString(R.styleable.rca_RemoComp_type));
		setApk(a.getString(R.styleable.rca_RemoComp_apk));
		a.recycle();
	}

	// Note: Setter are needed for inflating from string XMLs.

	public final String getType() {
		return mType;
	}

	public final void setType(String type) {
		mType = type;
	}

	public final String getApk() {
		return mApk;
	}

	public final void setApk(String apk) {
		mApk = apk;
	}

	private static int[] getResourceDeclareStyleableIntArray(Context pluginContext) {
		try {
			final String apk = pluginContext.getPackageName();
			final ClassLoader classLoader = RcaService.getApp().getMainRemoCoImplsFactory().getPlugin(apk).getClassLoader();
			final String className = apk + ".R$styleable";

			for (Field field : classLoader.loadClass(className).getFields()) {
				if (field.getName().equals(TAG_RESOURCE_CLASS_NAME)) {
					return (int[]) field.get(null);
				}
			}
		} catch (Throwable ignored) {
		}
		return null;
	}
}
