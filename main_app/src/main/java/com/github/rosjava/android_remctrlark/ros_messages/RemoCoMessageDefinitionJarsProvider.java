/*
 * Copyright (C) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original MessageDefinitionReflectionProvider.java written by Damon Kohler (damonkohler@google.com) modified by Denis Graf.
 */
package com.github.rosjava.android_remctrlark.ros_messages;

import android.content.Context;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.RcaRuntimeError;
import com.github.rosjava.android_remctrlark.remocos.IRemoCo;
import com.google.common.collect.Maps;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.jetbrains.annotations.NotNull;
import org.ros.message.MessageDefinitionProvider;
import org.ros.message.MessageIdentifier;

import java.io.File;
import java.io.FileFilter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;

import dalvik.system.DexClassLoader;

public class RemoCoMessageDefinitionJarsProvider implements MessageDefinitionProvider {

	private final Map<String, String> mCache;
	private final ClassLoader mClassLoader;

	public RemoCoMessageDefinitionJarsProvider(@NotNull IRemoCo remoCo) {
		this(remoCo.getApk(), remoCo);
	}

	protected RemoCoMessageDefinitionJarsProvider(@NotNull String apk, IRemoCo remoCo) {
		try {
			final File tmpDir = RcaService.getApp().getApplicationContext().getDir("dex_" + "apk", Context.MODE_PRIVATE);
			mClassLoader = new DexClassLoader(loadDexPath(apk, remoCo), tmpDir.getAbsolutePath(), null, this.getClass().getClassLoader());
		} catch (Throwable e) {
			throw new RcaRuntimeError("Class loader for message definitions could not be created for plugin '" + apk + "'.", e);
		}
		mCache = Maps.newConcurrentMap();
	}

	@Override
	public String get(String messageType) {
		String messageDefinition = mCache.get(messageType);
		if (messageDefinition == null) {
			final String className = messageType.replace("/", ".");
			try {
				final Class<?> loadedClass = mClassLoader.loadClass(className);
				messageDefinition = (String) loadedClass.getDeclaredField("_DEFINITION").get(null);
				mCache.put(messageType, messageDefinition);
			} catch (Throwable e) {
				throw new RcaRuntimeError("Message definition for '" + messageType + "' could not be loaded", e);
			}
		}
		return messageDefinition;
	}

	@Override
	public boolean has(String messageType) {
		try {
			get(messageType);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Collection<String> getPackages() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<MessageIdentifier> getMessageIdentifiersByPackage(String pkg) {
		throw new UnsupportedOperationException();
	}

	private static String loadDexPath(@NotNull String apk, IRemoCo remoCo) {
		final StringWriter dexPath = new StringWriter();

		final File dir = new File(RcaService.getApp().getFileNamesManager().getJavaMsgsFullDir(apk));
		final FileFilter fileFilter = new SuffixFileFilter("jar");
		final File[] files = dir.listFiles(fileFilter);

		if (files != null) {
			boolean notFirst = true;
			for (File file : files) {
				if (notFirst) {
					dexPath.append(":");
					notFirst = false;
				}
				dexPath.append(file.getAbsolutePath());
				if (BuildConfig.DEBUG) {
					if (remoCo != null) {
						final String logPrefix = "searching for message definition files for remoco '" + remoCo.getIdName() + "'";
						remoCo.getLog().d(logPrefix + ": accepted jar file '" + file.getName() + "'");
					}
				}
			}
		}

		return dexPath.toString();
	}
}
