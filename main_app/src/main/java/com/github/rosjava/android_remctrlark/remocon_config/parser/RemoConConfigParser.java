/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.remocon_config.parser;

import android.util.Pair;
import android.util.Xml;

import com.github.rosjava.android_remctrlark.application.RcaService;
import com.github.rosjava.android_remctrlark.error_handling.InvalidValueTypeException;
import com.github.rosjava.android_remctrlark.error_handling.PrivateGraphNameException;
import com.github.rosjava.android_remctrlark.error_handling.RemoConConfigBaseCycleException;
import com.github.rosjava.android_remctrlark.error_handling.RemoConConfigParserException;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoNamespace;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoParam;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoRemapping;
import com.github.rosjava.android_remctrlark.remocon_config.IRemoCoType;
import com.github.rosjava.android_remctrlark.remocon_config.ISerializableRemoConConfig;
import com.github.rosjava.android_remctrlark.remocos.IRcaId;
import com.github.rosjava.android_remctrlark.remocos.RcaId;

import org.jetbrains.annotations.NotNull;
import org.ros.namespace.GraphName;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RemoConConfigParser {

	private static class Context {
		final String mDefaultApk;
		final List<String> mBaseConfsHistory;
		final boolean mPreview;

		public Context(@NotNull String defaultApk, @NotNull List<String> baseConfsHistory) {
			this(defaultApk, baseConfsHistory, false);
		}

		public Context(@NotNull String defaultApk, @NotNull List<String> baseConfsHistory, boolean preview) {
			mDefaultApk = defaultApk;
			mBaseConfsHistory = baseConfsHistory;
			mPreview = preview;
		}

		public void addBaseConf(@NotNull File file) throws IOException, RemoConConfigBaseCycleException {
			final String baseConfCanonicalPath = file.getCanonicalPath();
			if (mBaseConfsHistory.contains(baseConfCanonicalPath)) {
				throw new RemoConConfigBaseCycleException(baseConfCanonicalPath);
			}
			mBaseConfsHistory.add(baseConfCanonicalPath);
		}
	}

	private static final String ELEM_NS = "http://android_remctrlark.rosjava.github.com/schemas/reco";
	private static final String ATTR_NS = null;

	public static RemoConConfig parsePreview(String path, String pluginApk) throws IOException, RemoConConfigBaseCycleException, RemoConConfigParserException {
		final File file = new File(path);
		final RemoConConfig remoConConfig = new RemoConConfig(file.getCanonicalPath());
		remoConConfig.mBaseConfApk = pluginApk;
		final Context context = new Context(remoConConfig.mBaseConfApk, new LinkedList<String>(), true);
		context.addBaseConf(file);
		parse(file, context, remoConConfig);
		return remoConConfig;
	}

	public static ISerializableRemoConConfig parse(String path, String pluginApk) throws RemoConConfigParserException, IOException, RemoConConfigBaseCycleException {
		final File file = new File(path);
		final RemoConConfig remoConConfig = new RemoConConfig(file.getCanonicalPath());
		remoConConfig.mBaseConfApk = pluginApk;
		final Context context = new Context(remoConConfig.mBaseConfApk, new LinkedList<String>());
		context.addBaseConf(file);
		parse(file, context, remoConConfig);
		if (remoConConfig.getId() == null) {
			throw new RemoConConfigParserException("No RemoCon id defined.");
		}
		return remoConConfig;
	}

	private static void parse(File file, Context context, RemoConConfig remoConConfig) throws RemoConConfigParserException {
		FileInputStream in = null;
		XmlPullParser parser = null;
		try {
			in = new FileInputStream(file);

			parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
			parser.setInput(in, null);
			parser.nextTag();

			parseRemoConConfig(parser, context, remoConConfig);
			in.close();
		} catch (Throwable e) {
			try {
				if (parser != null) {
					throw new RemoConConfigParserException("RemoCon config line #" + parser.getLineNumber() + ": Error parsing tag <" + parser.getName() + ">.", e);
				} else {
					throw new RemoConConfigParserException("RemoCon config: Error reading file.", e);
				}
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException ignored) {
					}
				}
			}
		}
		try {
			in.close();
		} catch (IOException e) {
			throw new RemoConConfigParserException("RemoCon config: Error closing file.", e);
		}
	}

	private static void parseRemoConConfig(XmlPullParser parser, Context context, RemoConConfig remoConConfig) throws IOException, XmlPullParserException, URISyntaxException, InvalidValueTypeException, PrivateGraphNameException, RemoConConfigBaseCycleException, RemoConConfigParserException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoConConfig.XML_TAG);

		remoConConfig.mBaseConfApk = parseApk(parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_BASECONFAPK), remoConConfig.mBaseConfApk);
		final String baseConf = parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_BASECONF);
		if (baseConf != null) {
			final File baseConfFile = new File(RcaService.getApp().getFileNamesManager().getRemoConConfigsFullDir(remoConConfig.mBaseConfApk), baseConf);
			context.addBaseConf(baseConfFile);
			parse(baseConfFile, context, remoConConfig);
		}

		parseARemoCoConfig(parser, context, remoConConfig);
		remoConConfig.mLogo = parseString(parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_LOGO), remoConConfig.mLogo);
		remoConConfig.mMasterUri = parseUri(parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_MASTERURI), remoConConfig.mMasterUri);
		remoConConfig.mNewMaster = parseString(parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_NEWMASTER), remoConConfig.mNewMaster);
		remoConConfig.mMutableMaster = parseBoolean(parser.getAttributeValue(ATTR_NS, RemoConConfig.XML_ATTR_MUTABLEMASTER), remoConConfig.mMutableMaster);

		if (context.mPreview) {
			return;
		}

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals(RemoConConfigSettings.XML_TAG)) {
				parseRemoConConfigSettings(parser, context, remoConConfig.mSettings);
			} else if (name.equals(RemoConConfigGlobals.XML_TAG)) {
				parseRemoConConfigGlobals(parser, context, remoConConfig.mGlobals);
			} else if (name.equals(RemoConSpecialization.XML_TAG)) {
				final RemoConSpecialization spec = new RemoConSpecialization();
				parseRemoConSpecialization(parser, context, spec);
				remoConConfig.mSpecializations.add(spec);
			} else {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoConConfig.XML_TAG);
	}

	private static void parseRemoConConfigSettings(XmlPullParser parser, Context context, RemoConConfigSettings settings) throws IOException, XmlPullParserException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoConConfigSettings.XML_TAG);

		settings.mDebug = parseBoolean(parser.getAttributeValue(ATTR_NS, RemoConConfigSettings.XML_ATTR_DEBUG), settings.mDebug);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals(RebootOnType.XML_TAG)) {
				parseRebootOnType(parser, context, settings.mRebootOnTypeMap);
			} else {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoConConfigSettings.XML_TAG);
	}

	private static void parseRemoConConfigGlobals(XmlPullParser parser, Context context, RemoConConfigGlobals remoConConfigGlobals) throws IOException, XmlPullParserException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoConConfigGlobals.XML_TAG);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals(RemoCoNamespace.XML_TAG)) {
				parseRemoCoNamespace(parser, context, remoConConfigGlobals.mNamespaces);
			} else if (name.equals(RemoCoType.XML_TAG)) {
				parseRemoCoType(parser, context, remoConConfigGlobals.mTypes);
			} else {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoConConfigGlobals.XML_TAG);
	}

	private static void parseRemoConSpecialization(XmlPullParser parser, Context context, RemoConSpecialization spec) throws IOException, XmlPullParserException, URISyntaxException, InvalidValueTypeException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoConSpecialization.XML_TAG);

		parseRemoCoSpecCondition(parser, context, spec.mCondition);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String name = parser.getName();
			// Starts by looking for the entry tag
			if (parseTagARemoCoSpecialization(parser, context, name, spec)) {
				continue;
			}
			if (name.equals(RemoCompConfig.XML_TAG)) {
				final RemoCompConfig remoCompConfig = new RemoCompConfig();
				parseRemoCompConfig(parser, context, remoCompConfig);
				final RemoCompConfig oldRemoCompConfig = (RemoCompConfig) spec.mRemoCompConfigs.get(remoCompConfig.getId());
				if (oldRemoCompConfig != null) {
					if (remoCompConfig.mTypeId == null) {
						remoCompConfig.mTypeId = oldRemoCompConfig.mTypeId;
					}
					remoCompConfig.mSpecializations.addAll(0, oldRemoCompConfig.mSpecializations);
				}
				spec.mRemoCompConfigs.put(remoCompConfig.getId(), remoCompConfig);
			} else {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoConSpecialization.XML_TAG);
	}

	private static boolean parseTagARemoCoSpecialization(XmlPullParser parser, Context context, String tag, ARemoCoSpecialization spec) throws IOException, XmlPullParserException, InvalidValueTypeException, PrivateGraphNameException {
		if (tag.equals(RemoCoParam.XML_TAG)) {
			parseRemoCoParam(parser, context, spec.mParams);
		} else if (tag.equals(RemoCoRemapping.XML_TAG)) {
			parseRemoCoRemappings(parser, context, spec.mRemappings);
		} else {
			return false;
		}
		return true;
	}

	private static void parseRemoCoSpecCondition(XmlPullParser parser, @SuppressWarnings("UnusedParameters") Context context, RemoCoSpecCondition condition) throws URISyntaxException {
		condition.mType = parseRcaId(parser.getAttributeValue(ATTR_NS, ARemoCoSpecialization.XML_ATTR_TYPE), condition.mType);
		condition.mApk = parseApk(parser.getAttributeValue(ATTR_NS, ARemoCoSpecialization.XML_ATTR_APK), condition.mApk);
		condition.mMasterUri = parseUri(parser.getAttributeValue(ATTR_NS, ARemoCoSpecialization.XML_ATTR_MASTERURI), condition.mMasterUri);
		condition.mNewMaster = parseString(parser.getAttributeValue(ATTR_NS, ARemoCoSpecialization.XML_ATTR_NEWMASTER), condition.mNewMaster);
	}

	private static void parseRebootOnType(XmlPullParser parser, Context context, Map<Pair<IRcaId, String>, Boolean> rebootOnType) throws IOException, XmlPullParserException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RebootOnType.XML_TAG);

		final IRcaId type = new RcaId(parser.getAttributeValue(ATTR_NS, RebootOnType.XML_ATTR_TYPE));
		final String apk = parseApk(parser.getAttributeValue(ATTR_NS, RebootOnType.XML_ATTR_APK), context.mDefaultApk);
		final boolean enabled = parseBoolean(parser.getAttributeValue(ATTR_NS, RebootOnType.XML_ATTR_ENABLED), true);

		rebootOnType.put(new Pair<IRcaId, String>(type, apk), enabled);

		skipTags(parser);
		parser.require(XmlPullParser.END_TAG, ELEM_NS, RebootOnType.XML_TAG);
	}

	private static void parseRemoCoNamespace(XmlPullParser parser, Context context, Map<IRcaId, IRemoCoNamespace> namespaces) throws IOException, XmlPullParserException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCoNamespace.XML_TAG);

		final RemoCoNamespace namespace = new RemoCoNamespace();
		parseARemoCoMutable(parser, context, namespace);
		namespace.mId = new RcaId(parser.getAttributeValue(ATTR_NS, RemoCoNamespace.XML_ATTR_ID));
		namespace.mNamespace = creteNonPrivateGraphName(parser.getAttributeValue(ATTR_NS, RemoCoNamespace.XML_ATTR_NS));

		namespaces.put(namespace.getId(), namespace);

		skipTags(parser);
		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCoNamespace.XML_TAG);
	}

	private static void parseRemoCoType(XmlPullParser parser, Context context, Map<IRcaId, IRemoCoType> types) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCoType.XML_TAG);

		final RemoCoType type = new RemoCoType();
		parseARemoCoMutable(parser, context, type);
		type.mId = new RcaId(parser.getAttributeValue(ATTR_NS, RemoCoType.XML_ATTR_ID));
		type.mType = new RcaId(parser.getAttributeValue(ATTR_NS, RemoCoType.XML_ATTR_TYPE));
		type.mApk = parseApk(parser.getAttributeValue(ATTR_NS, RemoCoType.XML_ATTR_APK), type.mApk);

		types.put(type.getId(), type);

		skipTags(parser);
		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCoType.XML_TAG);
	}

	private static void parseRemoCompConfig(XmlPullParser parser, Context context, RemoCompConfig remoCompConfig) throws IOException, XmlPullParserException, URISyntaxException, InvalidValueTypeException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCompConfig.XML_TAG);

		parseARemoCoConfig(parser, context, remoCompConfig);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals(RemoCompSpecialization.XML_TAG)) {
				final RemoCompSpecialization spec = new RemoCompSpecialization();
				parseRemoCompSpecialization(parser, context, spec);
				remoCompConfig.mSpecializations.add(spec);
			} else {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCompConfig.XML_TAG);
	}

	private static void parseRemoCompSpecialization(XmlPullParser parser, Context context, RemoCompSpecialization spec) throws IOException, XmlPullParserException, URISyntaxException, InvalidValueTypeException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCompSpecialization.XML_TAG);

		parseRemoCoSpecCondition(parser, context, spec.mCondition);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String name = parser.getName();
			// Starts by looking for the entry tag
			if (!parseTagARemoCoSpecialization(parser, context, name, spec)) {
				skip(parser);
			}
		}

		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCompSpecialization.XML_TAG);
	}

	private static void parseRemoCoParam(XmlPullParser parser, Context context, List<IRemoCoParam> params) throws XmlPullParserException, IOException, InvalidValueTypeException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCoParam.XML_TAG);

		final RemoCoParam param = new RemoCoParam();
		parseARemoCoMutable(parser, context, param);
		param.mName = creteNonPrivateGraphName(parser.getAttributeValue(ATTR_NS, RemoCoParam.XML_ATTR_NAME));
		param.mType = parser.getAttributeValue(ATTR_NS, RemoCoParam.XML_ATTR_TYPE);
		param.mValue = createValue(parser.getAttributeValue(ATTR_NS, RemoCoParam.XML_ATTR_VALUE), param.mType);

		params.add(param);

		skipTags(parser);
		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCoParam.XML_TAG);
	}

	private static void parseRemoCoRemappings(XmlPullParser parser, @SuppressWarnings("UnusedParameters") Context context, List<IRemoCoRemapping> remappings) throws XmlPullParserException, IOException, PrivateGraphNameException {
		parser.require(XmlPullParser.START_TAG, ELEM_NS, RemoCoRemapping.XML_TAG);

		final RemoCoRemapping remap = new RemoCoRemapping();
		parseARemoCoMutable(parser, context, remap);
		remap.mFrom = creteNonPrivateGraphName(parser.getAttributeValue(ATTR_NS, RemoCoRemapping.XML_ATTR_FROM));
		remap.mTo = creteNonPrivateGraphName(parser.getAttributeValue(ATTR_NS, RemoCoRemapping.XML_ATTR_TO));
		remap.mNsId = parseRcaId(parser.getAttributeValue(ATTR_NS, RemoCoRemapping.XML_ATTR_NSID), remap.mNsId);

		remappings.add(remap);

		skipTags(parser);
		parser.require(XmlPullParser.END_TAG, ELEM_NS, RemoCoRemapping.XML_TAG);
	}

	private static void parseARemoCoConfig(XmlPullParser parser, @SuppressWarnings("UnusedParameters") Context context, ARemoCoConfig config) {
		config.mId = config.mId != null ?
				parseRcaId(parser.getAttributeValue(ATTR_NS, ARemoCoConfig.XML_ATTR_ID), config.mId) :
				new RcaId(parser.getAttributeValue(ATTR_NS, ARemoCoConfig.XML_ATTR_ID));
		config.mTypeId = parseRcaId((parser.getAttributeValue(ATTR_NS, ARemoCoConfig.XML_ATTR_TID)), config.mTypeId);
	}

	private static void parseARemoCoMutable(XmlPullParser parser, @SuppressWarnings("UnusedParameters") Context context, ARemoCoMutable mutable) {
		mutable.mMutable = parseBoolean(parser.getAttributeValue(ATTR_NS, ARemoCoMutable.XML_ATTR_MUTABLE), mutable.mMutable);
	}

	private static RcaId parseRcaId(String str, RcaId defaultValue) {
		return str != null ? new RcaId(str) : defaultValue;
	}

	private static URI parseUri(String str, URI defaultValue) throws URISyntaxException {
		return str != null ? new URI(str) : defaultValue;
	}

	private static String parseApk(String apk, String defaultValue) {
		return parseString(apk, defaultValue);
	}

	private static String parseString(String str, String defaultValue) {
		return str != null ? str : defaultValue;
	}

	private static boolean parseBoolean(String str, Boolean defaultValue) {
		return str != null ? Boolean.parseBoolean(str) : defaultValue;
	}

	private static GraphName creteNonPrivateGraphName(String str) throws PrivateGraphNameException {
		final GraphName graphName = GraphName.of(str);
		if (graphName.isPrivate()) {
			throw new PrivateGraphNameException(graphName);
		}
		return graphName;
	}

	public static Object createValue(@NotNull String value, String type) throws InvalidValueTypeException {
		if (type != null) {
			final String canonicalType = type.toLowerCase();
			if (canonicalType.equals("str")) {
				return value;
			}
			if (canonicalType.equals("int")) {
				return Integer.parseInt(value);
			}
			if (canonicalType.equals("double")) {
				return Double.parseDouble(value);
			}
			if (canonicalType.equals("bool")) {
				return Boolean.parseBoolean(value);
			}
			throw new InvalidValueTypeException(type);
		} else {
			if (value.contains(".")) {
				try {
					return Double.parseDouble(value);
				} catch (NumberFormatException ignored) {
				}
			} else {
				try {
					return Integer.parseInt(value);
				} catch (NumberFormatException ignored) {
				}
			}
			final String valueLowerCase = value.toLowerCase();
			if (valueLowerCase.equals("true")) {
				return true;
			}
			if (valueLowerCase.equals("false")) {
				return false;
			}
			return value;
		}
	}

	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
				case XmlPullParser.END_TAG:
					depth--;
					break;
				case XmlPullParser.START_TAG:
					depth++;
					break;
			}
		}
	}

	private static void skipTags(XmlPullParser parser) throws XmlPullParserException, IOException {
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			skip(parser);
		}
	}
}
