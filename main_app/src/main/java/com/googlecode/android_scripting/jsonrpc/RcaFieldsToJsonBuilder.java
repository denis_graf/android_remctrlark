/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.googlecode.android_scripting.jsonrpc;

import com.github.rosjava.android_remctrlark.sl4a.IRcaFieldsToJsonMappable;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

class RcaFieldsToJsonBuilder implements ICustomJsonBuilder {

	@Override
	public boolean checkType(Object data) {
		return data instanceof IRcaFieldsToJsonMappable;
	}

	@Override
	public Object build(Object data) throws JSONException {
		JSONObject result = new JSONObject();
		for (Field field : data.getClass().getFields()) {
			int modifiers = field.getModifiers();
			if (!java.lang.reflect.Modifier.isStatic(modifiers)) {
				try {
					result.put(field.getName(), JsonBuilder.build(field.get(data)));
				} catch (IllegalAccessException ignored) {
					// This field is not public, so it has to be ignored.
				}
			}
		}
		return result;
	}

}
