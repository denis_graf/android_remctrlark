/*
 * Copyright (C) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Originally written {@link org.ros.android.RosActivity} by Damon Kohler modified by Denis Graf.
 */

package org.ros.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;

import com.github.rosjava.android_remctrlark.BuildConfig;
import com.github.rosjava.android_remctrlark.application.RcaService;
import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;
import org.ros.exception.RosRuntimeException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class RcaRosActivity extends Activity {

	private static final int MASTER_CHOOSER_REQUEST_CODE = 0;

	protected interface IOnRosStartingFinished {
		void onFinished(Bundle args);
	}

	protected class MasterChooserCanceledException extends Exception {
	}

	private final ServiceConnection mNodeMainExecutorServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			try {
				mNodeMainExecutorService = ((NodeMainExecutorService.LocalBinder) binder).getService();
				mNodeMainExecutorService.addListener(new NodeMainExecutorServiceListener() {
					@Override
					public void onShutdown(NodeMainExecutorService nodeMainExecutorService) {
						if (!mShutdownRequested) {
							onNodeMainExecutorServiceUnexpectedShutdown();
						}
					}
				});
			} catch (Throwable e) {
				mNodeMainExecutorService = null;
				RcaService.getLog().e("could not start service", e);
			}
			releaseStartedLatch();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			releaseStartedLatch();
		}
	};

	private final Object mMasterChooserLock = new Object();
	private IOnRosStartingFinished mOnRosStartingFinished = null;
	protected NodeMainExecutorService mNodeMainExecutorService = null;
	private CountDownLatch mNodeMainExecutorServiceSetLatch = null;
	private boolean mIsPrivateMaster = false;
	private boolean mShutdownRequested = false;

	protected abstract void onNodeMainExecutorServiceUnexpectedShutdown();

	public boolean isRosMasterPrivate() {
		return mIsPrivateMaster;
	}

	/**
	 * Starts ROS. This method returns immediately, without waiting for ROS to have been started.
	 */
	protected void startRos(@NotNull String notificationTicker, @NotNull String notificationTitle) {
		mShutdownRequested = false;
		releaseStartedLatch();
		mNodeMainExecutorServiceSetLatch = new CountDownLatch(1);
		boolean startRequested = false;
		try {
			startNodeMainExecutorService(notificationTicker, notificationTitle);
			startRequested = true;
		} finally {
			if (!startRequested) {
				releaseStartedLatch();
			}
		}
	}

	private void startNodeMainExecutorService(@NotNull String notificationTicker, @NotNull String notificationTitle) {
		Intent intent = new Intent(this, NodeMainExecutorService.class);
		intent.setAction(NodeMainExecutorService.ACTION_START);
		intent.putExtra(NodeMainExecutorService.EXTRA_NOTIFICATION_TICKER, notificationTicker);
		intent.putExtra(NodeMainExecutorService.EXTRA_NOTIFICATION_TITLE, notificationTitle);
		startService(intent);
		Preconditions.checkState(
				bindService(intent, mNodeMainExecutorServiceConnection, BIND_AUTO_CREATE),
				"Failed to bind NodeMainExecutorService.");
	}

	protected void shutdownRos() {
		shutdownRos(true);
	}

	protected void shutdownRos(boolean awaitTermination) {
		if (mNodeMainExecutorService != null) {
			mShutdownRequested = true;

			final ScheduledExecutorService scheduledExecutorService = mNodeMainExecutorService.getScheduledExecutorService();
			if (awaitTermination) {
				try {
					if (BuildConfig.DEBUG) {
						Preconditions.checkState(!RcaService.getContext().isOnUiThread());
					}
					scheduledExecutorService.shutdownNow();
					if (scheduledExecutorService.awaitTermination(10, TimeUnit.SECONDS)) {
						RcaService.getLog().i("ros terminated");
					} else {
						RcaService.getLog().w("timeout on ros termination");
					}
				} catch (InterruptedException e) {
					RcaService.getLog().w("interruption on ros termination");
					Thread.currentThread().interrupt();
				} catch (Throwable e) {
					RcaService.getLog().e("error on ros termination", e);
				}
			}

			mNodeMainExecutorService.shutdown();
			unbindService(mNodeMainExecutorServiceConnection);
			mNodeMainExecutorService = null;
			releaseStartedLatch();
		}
	}

	/**
	 * Chooses a ROS master. This method waits for ROS to have been started.
	 */
	protected boolean chooseRosMaster() throws MasterChooserCanceledException {
		synchronized (mMasterChooserLock) {
			if (!awaitRosStarted()) {
				return false;
			}


			if (BuildConfig.DEBUG) {
				Preconditions.checkState(mOnRosStartingFinished == null);
			}

			final Bundle[] vArgs = new Bundle[1];
			final CountDownLatch latch = new CountDownLatch(1);
			mOnRosStartingFinished = new IOnRosStartingFinished() {
				@Override
				public void onFinished(Bundle args) {
					vArgs[0] = args;
					latch.countDown();
				}
			};

			// Call this method on super to avoid triggering our precondition in the overridden
			// {@link #startActivityForResult()}.
			RcaRosActivity.super.startActivityForResult(
					new Intent(RcaRosActivity.this, MasterChooser.class), MASTER_CHOOSER_REQUEST_CODE);

			try {
				latch.await();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return false;
			}

			final Bundle args = vArgs[0];
			if (args == null) {
				throw new MasterChooserCanceledException();
			}
			if (args.getBoolean("NEW_MASTER", false)) {
				return createRosMaster(args.getBoolean("ROS_MASTER_PRIVATE", true));
			} else {
				final String uri = args.getString("ROS_MASTER_URI");
				assert uri != null;
				return chooseRosMaster(uri);
			}
		}
	}

	public URI getRosMasterUri() {
		Preconditions.checkNotNull(mNodeMainExecutorService);
		return mNodeMainExecutorService.getMasterUri();
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		Preconditions.checkArgument(requestCode != MASTER_CHOOSER_REQUEST_CODE);
		super.startActivityForResult(intent, requestCode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == MASTER_CHOOSER_REQUEST_CODE) {
			assert mOnRosStartingFinished != null;
			IOnRosStartingFinished onStartingFinished = mOnRosStartingFinished;
			mOnRosStartingFinished = null;
			onStartingFinished.onFinished(resultCode == RESULT_OK ? data.getExtras() : null);
		}
	}

	/**
	 * Chooses a ROS master. This method waits for ROS to have been started.
	 */
	protected boolean createRosMaster(final boolean isPrivate) {
		if (!awaitRosStarted()) {
			return false;
		}

		final RosRuntimeException[] vException = new RosRuntimeException[1];
		AsyncTask<Void, Void, URI> task = new AsyncTask<Void, Void, URI>() {
			@Override
			protected URI doInBackground(Void... params) {
				try {
					RcaRosActivity.this.mNodeMainExecutorService.startMaster(isPrivate);
					return RcaRosActivity.this.mNodeMainExecutorService.getMasterUri();
				} catch (RosRuntimeException e) {
					vException[0] = e;
					return null;
				}
			}
		};
		task.execute();
		try {
			task.get();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			return false;
		} catch (ExecutionException e) {
			return false;
		}

		if (vException[0] != null) {
			throw vException[0];
		}

		mIsPrivateMaster = isPrivate;

		return true;
	}

	/**
	 * Chooses a ROS master. This method waits for ROS to have been started.
	 */
	protected boolean chooseRosMaster(@NotNull String uri) {
		if (!awaitRosStarted()) {
			return false;
		}

		try {
			mNodeMainExecutorService.setMasterUri(new URI(uri));
		} catch (URISyntaxException e) {
			return false;
		}

		mIsPrivateMaster = false;

		return true;
	}

	private boolean awaitRosStarted() {
		if (BuildConfig.DEBUG) {
			Preconditions.checkNotNull(mNodeMainExecutorServiceSetLatch);
		}
		try {
			mNodeMainExecutorServiceSetLatch.await();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			return false;
		}
		return true;
	}

	private void releaseStartedLatch() {
		if (mNodeMainExecutorServiceSetLatch != null) {
			mNodeMainExecutorServiceSetLatch.countDown();
		}
	}
}
