#!/usr/bin/python2.7

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import getopt
from os import walk
import os
import re
import shutil
import sys
import traceback


PROJECT_NAME = "android_remctrlark"
VALID_PROJECT_NAME = '^[_(0-9)(a-z)(A-Z)]+$'
REPLACE_EXTS = ['.bat', '.gitignore', '.gradle', '.iml', '.java', '.md', '.name', '.properties', '.py', '.reco', '.sh',
                '.txt', '.xml']
IGNORE_DIRS = [".git", ".gradle", ".svn"]
ARG_PROJECT_NAME = ["n", "project-name"]

SCRIPT_ABSDIR = os.path.abspath(os.path.split(sys.argv[0])[0])

_log_file = None
_project_name_new = ""
_project_dir_new = ""
_ignored_exts = set()


def help_doc():
    print "usage: [-%s <PROJECT-NAME>]" % ("|--".join(ARG_PROJECT_NAME),)


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "h%s:" % (ARG_PROJECT_NAME[0],), [ARG_PROJECT_NAME[1] + "="])
    except getopt.GetoptError:
        help_doc()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            help_doc()
            sys.exit()
        elif opt in all_opts(ARG_PROJECT_NAME):
            global _project_name_new
            _project_name_new = arg
            if re.match(VALID_PROJECT_NAME, _project_name_new) is None:
                print "Error: Invalid project name. It must be alphanumeric with optional understrikes."
                sys.exit(1)

    if _project_name_new == "":
        help_doc()
        sys.exit(2)

    global _project_dir_new
    project_contating_dir = os.path.normpath(os.path.join(SCRIPT_ABSDIR, ".."))
    project_dir = os.path.join(project_contating_dir, PROJECT_NAME)
    _project_dir_new = os.path.join(project_contating_dir, _project_name_new)

    success = False
    log_filename = os.path.basename(sys.argv[0] + ".log")
    # noinspection PyBroadException
    try:
        log_counter = 1
        while os.path.exists(log_filename):
            log_counter += 1
            log_filename = os.path.splitext(log_filename)[0] + ".log" + str(log_counter)

        global _log_file
        _log_file = open(log_filename, 'w')
        log("Renaming project from '%s' to '%s':" % (PROJECT_NAME, _project_name_new))
        log("  Project dir: %s" % project_dir)
        log("  Renamed dir: %s" % _project_dir_new)

        def ingnore(src, names):
            if os.path.basename(src) in IGNORE_DIRS:
                return names
            return []

        shutil.copytree(project_dir, _project_dir_new, True, ingnore)

        process_dir(_project_dir_new)
        log("Renaming project succeeded.")

        if len(_ignored_exts) > 0:
            log()
            log("Following file extensions were ignored:")
            log("  " + str(sorted_nicely(_ignored_exts)))

        success = True
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        extracted_list = traceback.format_exception(exc_type, exc_value, exc_traceback)
        error_log = "".join(extracted_list)
        log()
        log("Error:")
        log(error_log)
        log("Renaming project failed.")

    if _log_file is not None:
        # noinspection PyBroadException
        try:
            _log_file.close()
            if success:
                log_filename_new = os.path.join(_project_dir_new, log_filename)
                os.remove(log_filename_new)
                os.rename(log_filename, log_filename_new)
            else:
                log_filename_new = os.path.join(project_dir, log_filename)
            print
            print "Log saved in '%s'." % log_filename_new
        except:
            pass


def process_dir(path):
    """
    @type path: str
    @rtype: None
    """
    if os.path.islink(path):
        return
    if os.path.basename(path) in IGNORE_DIRS:
        os.removedirs(path)
        return

    dirpath = ""
    dirnames = []
    filenames = []

    for (dirpath, dirnames, filenames) in walk(path):
        break

    for filename_old in filenames:
        process_file(dirpath, filename_old)

    for dirname_old in dirnames:
        file_new = process_file(dirpath, dirname_old)
        process_dir(file_new)


def process_file(dirpath, filename):
    """
    @type dirpath: str
    @type filename: str
    @rtype: str
    """
    renamed = False
    if check_string(filename):
        filename_new = process_string(filename)
        file_old = os.path.join(dirpath, filename)
        file_new = os.path.join(dirpath, filename_new)
        os.rename(file_old, file_new)
        renamed = True
    else:
        filename_new = filename
        file_old = os.path.join(dirpath, filename)
        file_new = os.path.join(dirpath, filename_new)

    replacements = []
    if os.path.isfile(file_new):
        replacements = process_file_content(file_new)

    if renamed or len(replacements) > 0:
        start = len(_project_dir_new)
        log()
        log("  Changes on file '%s':" % ("." + file_new[start:]))

        if renamed:
            log("    renamed from: '%s'" % ("." + file_old[start:]))

        def end(line):
            """
            @type line: str
            """
            if line[-1] == '\n':
                return ''
            else:
                return '\n'

        for replacement in replacements:
            log("    line #%d:" % replacement[0])
            log("      original: %s" % replacement[1], end(replacement[1]))
            log("      replaced: %s" % replacement[2], end(replacement[2]))

    return file_new


def process_file_content(filename):
    """
    @type filename: str
    @rtype: list[list[object]]
    """
    replacements = []

    if os.path.islink(filename):
        return replacements

    root, ext = os.path.splitext(filename)
    if ext == '':
        basename = os.path.basename(root)
        """@type: str"""
        if basename.startswith('.'):
            ext = basename

    if not ext in REPLACE_EXTS:
        _ignored_exts.add(ext)
        return replacements

    new_filename = filename
    while os.path.exists(new_filename):
        new_filename += '.tmp'

    with open(filename, 'r') as input_file:
        with open(new_filename, 'w') as output_file:
            linenumber = 0
            for line in input_file:
                linenumber += 1
                if check_string(line):
                    replaced_line = process_string(line)
                    output_file.write(replaced_line)
                    replacements.append([linenumber, line, replaced_line])
                else:
                    output_file.write(line)

    if len(replacements) > 0:
        os.remove(filename)
        os.rename(new_filename, filename)
    else:
        os.remove(new_filename)

    return replacements


def check_string(string):
    """
    @type string: str
    @rtype: bool
    """
    return string.find(PROJECT_NAME) != -1


def process_string(string):
    """
    @type string: str
    @rtype: str
    """
    return string.replace(PROJECT_NAME, _project_name_new)


def sorted_nicely(collection):
    """
    Sort the given iterable in the way that humans expect.
    Source: http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
    @type collection: set
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(collection, key=alphanum_key)


def all_opts(arg_names):
    """
    @type arg_names: list[str]
    @rtype: str
    """
    return "-" + arg_names[0], "--" + arg_names[1]


def log(msg="", end='\n'):
    """
    @type msg: str
    @type end: str
    @rtype: None
    """
    line = msg + end
    print line,
    if _log_file is not None:
        _log_file.write(line)


if __name__ == "__main__":
    os.chdir(SCRIPT_ABSDIR)
    main(sys.argv[1:])
