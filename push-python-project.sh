#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

SCRIPT_DIR=$(dirname $0)
. ${SCRIPT_DIR}/scripts-config.sh

if [ -e $PYTHON_CORE_DIR ]; then
  $PYTHON_CORE_DIR/push-project.sh
else
  echo "Error: No RCA-Python-Core found. Please copy sources or make a symbolic link into directory '$PYTHON_CORE_DIR'."
fi
