#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

SCRIPT_DIR=$(dirname $0)
. ${SCRIPT_DIR}/scripts-config.sh

REMOCON_CONFIGS_DIR_NAME=$(basename $REMOCON_CONFIGS_DIR)

echo "Pushing remocon configs to device..."
$RUN_ADB push ${REMOCON_CONFIGS_DIR} $PACKAGE_ON_DEVICE_PATH$REMOCON_CONFIGS_DIR_NAME

echo
echo "Pushing python project to device..."
$PUSH_PYTHON_PROJECT

echo
echo "Pushing python core project to device..."
$PUSH_PYTHON_CORE_PROJECT
